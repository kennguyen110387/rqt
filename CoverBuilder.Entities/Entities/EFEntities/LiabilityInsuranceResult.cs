//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoverBuilder.Entities.Entities
{
    using System;
    using System.Collections.Generic;

    public partial class LiabilityInsuranceResult
    {
        public LiabilityInsuranceResult()
        {
            IsActive = true;
        }

        public int ResultId { get; set; }
        public System.Guid UserInfoId { get; set; }
        public int QuestionId { get; set; }
        public string Response { get; set; }
        public Nullable<int> GroupId { get; set; }
        public Nullable<bool> IsTriggered { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ActivityGroup { get; set; }

        public virtual LiabilityQuotationHeader LiabilityQuotationHeader { get; set; }
        public virtual Questionaire Questionaire { get; set; }
    }
}
