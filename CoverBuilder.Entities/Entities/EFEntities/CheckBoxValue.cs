//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoverBuilder.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class CheckBoxValue
    {
        public int ID { get; set; }
        public Nullable<int> InsuranceID { get; set; }
        public Nullable<int> QuestionID { get; set; }
        public string Item { get; set; }
        public string Value { get; set; }
        public Nullable<int> OrderID { get; set; }
        public Nullable<int> GroupID { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
