﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoverBuilder.Entities.Entities
{
    using System;
    using System.Collections.Generic;

    public partial class Lookup
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public Nullable<int> Sequence { get; set; }
        public Nullable<int> Group { get; set; }
    }
}
