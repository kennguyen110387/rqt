//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CoverBuilder.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Questionaire
    {
        public Questionaire()
        {
            this.LiabilityInsuranceResults = new HashSet<LiabilityInsuranceResult>();
        }

        public int QuestionId { get; set; }
        public int InsuranceId { get; set; }
        public string Question { get; set; }
        public string TriggerDesc { get; set; }
        public Nullable<int> ParentId { get; set; }
        public bool IsActive { get; set; }
        public string LeadFieldName { get; set; }
        public Nullable<int> OrderId { get; set; }
        public bool IsCurrencyField { get; set; }

        public virtual LiabilityInsuranceType LiabilityInsuranceType { get; set; }
        public virtual ICollection<LiabilityInsuranceResult> LiabilityInsuranceResults { get; set; }


    }
}
