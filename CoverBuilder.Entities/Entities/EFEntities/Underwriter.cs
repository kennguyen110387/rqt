﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities.Entities
{
    public partial class Underwriter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string AuthKey { get; set; }
        public string ExecutiveCode { get; set; }
        public string Role { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string UserId { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
