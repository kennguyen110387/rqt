﻿using System;
namespace CoverBuilder.Entities.Entities
{
    public class UserDetails
    {
        public Guid UserInfoId { get; set; }
        public int InsuranceId { get; set; }
        public string LiabilityInsurance { get; set; }
        public string BrokerageName { get; set; }
        public string FullName { get; set; }
        public string InsuredFullName { get; set; }
        public string SituationStreetNo { get; set; }
        public string SituationStreetName { get; set; }
        public string SituationSuburb { get; set; }
        public string SituationState { get; set; }
        public string SituationPostCode { get; set; }
        public System.DateTime InsurancePeriodFrom { get; set; }
        public System.DateTime InsurancePeriodTo { get; set; }
        public System.DateTime DateCreated { get; set; }
    }
}
