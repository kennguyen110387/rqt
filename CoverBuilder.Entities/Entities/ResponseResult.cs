﻿using System;

namespace CoverBuilder.Entities.Entities
{
    public class ResponseResult
    {

        public int? Code { get; set; }
        public string ZohoId { get; set; }
        public string CreatedTime { get; set; }
        public string ModifiedTime { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Message { get; set; }
    }
}
