﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities.Entities
{
    public class JsonResponseResult
    {
        public JsonResponseResult()
        {
            DateExecuted = DateTime.Now.ToString("yyyy-MM-dd");
        }
        public string Status { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
        public string DateExecuted { get; set; } 
    }
}
