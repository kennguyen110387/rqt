﻿namespace CoverBuilder.Entities.Entities
{
    public class LiabilityQuestionnaireResult
    {
        public string Question { get; set; }

        public string Response { get; set; }

        public string LeadFieldName { get; set; }
    }
}
