﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities.Entities
{
    public class PremiumStatusInfo
    {
        public string LeadId { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string InsuranceDateFrom { get; set; }
        public string InsuranceDateTo { get; set; }
        public string QuoteLostDate { get; set; }
    }
}
