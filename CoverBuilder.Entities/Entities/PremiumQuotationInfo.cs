﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities.Entities
{
    public class PremiumQuotationInfo
    {
       
        public string LeadId { get; set; }
        
        public string BrokerComm { get; set; }
        
        public string BrokerCommGst { get; set; }
       
        public string StampTax { get; set; }
    
        public string FinalPremium { get; set; }

        public string UnderwriterFeeGst { get; set; }

        public string UnderwriterFee { get; set; }

        public string PremiumSubTotal { get; set; }

        public string PremiumBase { get; set; }

        public string Gst { get; set; }

        public string StdExcess { get; set; }

        public string BusinessDesc { get; set; }

        public string Endorsements { get; set; }

    }
}
