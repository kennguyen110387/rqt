﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities.Entities
{
    public class PremiumFinancialInfo
    {
       
        public string LeadId { get; set; }
        
        public decimal BrokerComm { get; set; }

        public decimal BrokerCommGST { get; set; }

        public decimal StampTax { get; set; }

        public decimal FinalPremium { get; set; }

        public decimal UnderwriterFeeGST { get; set; }

        public decimal UnderwriterFee { get; set; }

        public decimal PremiumSubTotal { get; set; }

        public decimal PremiumBase { get; set; }

        public decimal GST { get; set; }

    }
}
