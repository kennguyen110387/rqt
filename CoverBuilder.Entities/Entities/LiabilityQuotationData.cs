﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities.Entities
{
    public class LiabilityQuotationData
    {
        public LiabilityQuotationHeader LiabilityQuotationHeader { get; set; }
        public List<LiabilityInsuranceResult> LiabilityResultList { get; set; }
        public List<LiabilityInsuranceResult> DeserialisedLiabilityResultList { get; set; }
    }
}
