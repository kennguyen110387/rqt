﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoverBuilder.Entities.Entities
{

    [MetadataType(typeof(LiabiiltyQuotationHeader_Metadata))]
    public partial class LiabilityQuotationHeader
    {
       
    }

    public class LiabiiltyQuotationHeader_Metadata
    {

        [Required(ErrorMessage = "*This is a required field")]
        public string BrokerageName { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string FullName { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        //[DataType(DataType.EmailAddress)]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Please enter a valid email address.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string PhoneNo { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string InsuredFullName { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string SitutationStreetName { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string SituationStreetNo { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string SituationSuburb { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string SituationState { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string SituationPostCode { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public Nullable<System.DateTime> InsurancePeriodFrom { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public Nullable<System.DateTime> InsurancePeriodTo { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public Nullable<double> LiabilityLimit { get; set; }
        public Nullable<double> GeneralLiability { get; set; }
        public Nullable<double> ProductLiability { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public Nullable<bool> DiscInfoClaim { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string DiscInfoClaimDetail { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public Nullable<bool> DiscInfoDecline { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string DiscInfoDeclineDetail { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public Nullable<bool> DiscInfoRenewal { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string DiscInfoRenewalDetail { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public Nullable<bool> DiscInfoExcess { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string DiscInfoExcessDetail { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public Nullable<bool> DiscInfoClaimRejected { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string DiscInfoClaimRejectedDetail { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public Nullable<bool> DiscInfoBankrupt { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string DiscInfoBankruptDetail { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public Nullable<bool> DiscInfoConvicted { get; set; }
        [Required(ErrorMessage = "*This is a required field")]
        public string DiscInfoConvictedDetail { get; set; }
        

    }

}
