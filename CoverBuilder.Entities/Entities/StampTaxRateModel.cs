﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities.Entities
{
    public class StampTaxRateModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
