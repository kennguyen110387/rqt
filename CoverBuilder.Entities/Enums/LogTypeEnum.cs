﻿namespace CoverBuilder.Entities.Enums
{
    using System;

    public enum LogTypeEnum
    {
        Info,
        Warning,
        Error,
        Fatal
    }
}
