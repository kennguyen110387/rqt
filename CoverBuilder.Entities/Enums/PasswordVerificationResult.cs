﻿namespace CoverBuilder.Entities.Enums
{
    public enum PasswordVerificationResult
    {
        Failed,
        Success,
        SuccessNeedsRehash
    }
    public enum UserVerificationResult
    {
        Failed,
        Success
    }
}
