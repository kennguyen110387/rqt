﻿namespace CoverBuilder.Entities.Enums
{
    using System;

    public enum UserResponseEnum
    {
        New,
        Accept,
        Deny,
        Requote
    }

    public enum ZohoApiCommand
    {
        insertRecords,
        updateRecords,
        searchRecords,
        deleteRecords,
    }
}
