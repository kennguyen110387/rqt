﻿
namespace CoverBuilder.Entities.Enums
{
    public enum TransportAndLogisticsQ49
    {
        Livestock = 0,
        Hazardous_or_Dangerous_Goods = 1,
        None_of_the_above = 2
    }

    public enum ScaffoldersQ82
    {
        Underground_Mines =0,
        Drag_Line_Excavators = 1,
        Refinery_or_Gas_producing_or_bulk_fuel_storage = 2,
        High_Voltage_power_supple = 3,
        Airports = 4,
        Railways = 5,
        Temporary_Seating = 6,
        Stages = 7,
        Lighting_Towers = 8,
        Camera_Towers = 9,
        Concerts = 10,
        Sporting_Events = 11,
        Work_outside_of_Australia=12,
        NONE_OF_THE_ABOVE = 13
    }

    public enum WeldersAndBoilermakersQ132
    { 
        Domestic_OR_Residential = 0,
        Work__Fence_OR_Gate_Manufacturing = 1,
        Non_Structural_Components = 2,
        Structural_Components = 3,
        Staircases_OR_Handrails_OR_Ballustrades_Manufacturing = 4,
        Earthmoving_Machinery_OPEN_excluding_underground_mining_equipment_CLOSE = 5,
        Pressure_Vessels = 6,
        Gas_Pipelines = 7,
        Plastics_Weldings = 8,
        Aboveground_Mining_Equipment_AND_Plant = 9,
        Other = 10
    }


    public enum CleanersActivitiesIncluded
    {
        Cleaning_of_Schools_During_School_Hours = 1,
        Cleaning_of_Medical_Facilities_AND_Hospitals = 2,
        Cleaning_of_Shopping_CentresORSupermarketsORRetail_Premises_during_Business_Hours = 3,
        Pest_Control = 4,
        Structural_Construction = 5,
        Asbestos_Removal = 6,
        NONE_OF_THE_ABOVE = 99,
    }
    public enum EventOrganiserSelectQ170
    {
        Pyrotechnic_OR_Fire_Works_Display = 1,
        Amusement_Rides_OR_Inflatables = 2,
        Sale_of_Alcohol = 3
    }
    public enum MarketOrganiserSelectQ170
    {
        Pyrotechnic_OR_Fire_Works_Display = 1,
        Amusement_Rides_OR_Inflatables = 2,
        Sale_of_Alcohol = 3
    }

    public enum CleanersAllActivitiesPerformedGroup1
    {
        Bathroom_Cleaning_Including_Tile_AND_Grout_Cleaning_AND_Mould_Removal = 1,
        Curtain_AND_Blinds_Cleaning = 2,
        Domestic_Bond_OR_End_of_Lease_Cleaning_OR_Move_Out_Cleaning_OR_Spring_Cleaning_OR_Renovation_Cleans = 3,
        Domestic_Washing_AND_Ironing_Services = 4,
        DomesticORResidential_Cleaning = 5,
        Office_Cleaning_DASH_Outside_Business_Hours_including_Move_Out_Cleans_OR_Spring_Cleans = 6,
        Retail_Shop_DASH_Outside_Business_Hours = 7

    }

    public enum CleanersAllActivitiesPerformedGroup2
    {
        Body_Corporate_Cleaning = 8,
        Builders_Clean_OPENInternalCLOSE = 9,
        Car_Washing_AND_Detailing = 10,
        Carpet_OR_Upholstery_Protection_DASH_Domestic_AND_Commerical = 11,
        Domestic_Swimming_Pool = 12,
        Factories_OPENOffice_and_toilet_areas_onlyCLOSE_ = 13,
        Gutter_AND_Roof_Cleaning_DASH_If_High_Pressure_Rate_in_Group_3 = 14,
        Handyman_Services_DASH_PaintingCOMMA_fencesCOMMA_gatesORdoorsCOMMA_inlcuding_minor_welding_etc_DASH_Excluding_Structural_Construction = 15,
        Landscaping_Services_DASH_Including_Minor_Tree_Lopping_OPENTree_Lopping_Endorsement_to_ApplyCLOSE = 16,
        Lawn_Mowing_AND_Garden_Maintenance = 17,
        Rubbish_Removal_DASH_Excluding_Asbestos = 18,
        Steam_Cleaning = 19,
        Window_cleaning_DASH_Ground_Level_only = 20

    }

    public enum CleanersAllActivitiesPerformedGroup3
    {
        Builders_Clean_OPENExternalCLOSE = 21,
        Cinema_Cleaning_DASH_Outside_Business_Hours = 22,
        Factory_Cleaning = 23,
        High_Pressure_Washing_DASH_External_WallsCOMMA_House_WashingCOMMA_RoofCOMMA_Driveways_AND_PathwaysCOMMA_PatiosCOMMA_Shade_SailsCOMMA_FencesCOMMA_Mould_Removal = 24,
        Sports_Leisure_facilitiesORGymnasiums = 25,
        Toilet_Cleaning_DASH_Outside_Business_Hours = 26,
        Window_Cleaning_DASH_Above_Ground_Level_up_to_10_metres_ = 27


    }

    public enum CleanersAllActivitiesPerformedGroup4
    {
        Chimney_Shafts_OPENDomesticCOMMA_but_not_IndustrialCLOSE_ = 28,
        Floor_Polishing = 29,
        Floor_Stripping_AND_Resealing = 30,
        Public_AreasCOLON_Common_Mall_OR_Arcade_OR_Shopping_Centre_cleaning_after_hours_ = 31,
        Schools_OPENOutside_Business_Hours_OnlyCLOSE = 32,
        Street_Cleaning_ = 33

    }

}
