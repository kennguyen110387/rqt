﻿
namespace CoverBuilder.Entities.Enums
{
    public enum GenericInfoLiabilityLimit
    {
        _5COMMA000COMMA000 = 5000000,
        _10COMMA000COMMA000 = 10000000,
        _20COMMA000COMMA000 = 20000000
    }

    public enum EventsAndMarketsSelect
    {
        Event_Organiser = 1,
        Market_Organiser = 2
    }

    public enum CleanersLossKeys
    {
        NslashA  = 0,
        _10_COMMA_000 = 10000,
        _25_COMMA_000 = 25000,
        _50_COMMA_000 = 50000
    }

    public enum EventOrganiserSelectQ165
    {
        Up_to_1_COMMA_000 = 1,
        __1_COMMA_001_to_2_COMMA_500 = 2,
        __2_COMMA_501_to_5_COMMA_000 = 3
    }
    public enum EventOrganiserSelectQ169
    {
        N_OR_A = 1,
        _DOLLAR_5_COMMA_000 = 2,
        _DOLLAR_10_COMMA_000 = 3,
        _DOLLAR_15_COMMA_000 = 4
    }
    
    public enum MarketOrganiserSelectQ165
    {
        Up_to_1_COMMA_000 = 1,
        __1_COMMA_001_to_2_COMMA_500 = 2,
        __2_COMMA_501_to_5_COMMA_000 = 3
    }
    public enum MarketOrganiserSelectQ169
    {
        N_OR_A = 1,
        _DOLLAR_5_COMMA_000 = 2,
        _DOLLAR_10_COMMA_000 = 3,
        _DOLLAR_15_COMMA_000 = 4
    }
    public enum StampDutyRates
    {
        Queensland = 9,
        New_South_Wales = 9,
        Victoria = 10,
        Western_Australia = 10,
        South_Australia = 11,
        Tasmania = 10,
        Australian_Capital_Territory = 0,
        Northern_Territory = 10

    }
}
