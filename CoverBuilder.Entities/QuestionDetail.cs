﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities
{
    public class QuestionReferenceDetail
    {
        public string[] DivergedResponse { get; set; }

        public List<string> LeadFieldName { get; set; }
        public void Split(string textStr)
        {
            DivergedResponse = textStr.Split('|').ToArray();
        }
    }
}
