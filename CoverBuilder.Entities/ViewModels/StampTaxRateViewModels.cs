﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoverBuilder.Entities.Entities;

namespace CoverBuilder.Entities.ViewModels
{
    public class StampTaxRateViewModels
    {
        public List<StampTaxRateModel> StampTaxRateList { get; set; }
        public StampTaxRateViewModels()
        {
            List<StampTaxRateModel> model = new List<StampTaxRateModel>();
            StampTaxRateModel tr1 = new StampTaxRateModel()
            {
                Text = "Queensland",
                Value = ((float)0.09).ToString()
            };
            StampTaxRateModel tr2 = new StampTaxRateModel()
            {
                Text = "New South Wales",
                Value = ((float)0.09).ToString()
            };
            StampTaxRateModel tr3 = new StampTaxRateModel()
            {
                Text = "Victoria",
                Value = ((float)0.10).ToString()
            };
            StampTaxRateModel tr4 = new StampTaxRateModel()
            {
                Text = "Western Australia",
                Value = ((float)0.10).ToString()
            };
            StampTaxRateModel tr5 = new StampTaxRateModel()
            {
                Text = "South Australia",
                Value = ((float)0.11).ToString()
            };
            StampTaxRateModel tr6 = new StampTaxRateModel()
            {
                Text = "Tasmania",
                Value = ((float)0.10).ToString()
            };
            StampTaxRateModel tr7 = new StampTaxRateModel()
            {
                Text = "Australian Capital Territory",
                Value = ((float)0.00).ToString()
            };
            StampTaxRateModel tr8 = new StampTaxRateModel()
            {
                Text = "Northern Territory",
                Value = ((float)0.10).ToString()
            };
            model.Add(tr1);
            model.Add(tr2);
            model.Add(tr3);
            model.Add(tr4);
            model.Add(tr5);
            model.Add(tr6);
            model.Add(tr7);
            model.Add(tr8);
            StampTaxRateList = model;
        }
    }
}
