﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities.ViewModels
{
    public class VacantLandSituationViewModel
    {
        public string SituationNumber { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string LandSize { get; set; }
        public string IsHobbyFarm { get; set; }
        public string HobbyFarmDetail { get; set; }
        public string IsForAgistment { get; set; }
        public string AgistmentDetail { get; set; }
        public string IsForMarketOrParking { get; set; }
        public string MarketOrParkingDetail { get; set; }
        public string HasStructuresOrBuildings { get; set; }
        public string StructureOrBuildingDetail { get; set; }
        public string BuildOrSellLand { get; set; }
    }
}
