﻿using System.Collections.Generic;
using System.Web.Mvc;
using CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.Enums;

namespace CoverBuilder.Entities.ViewModels
{
    public class GenericInfoViewModels
    {
        public LiabilityQuotationHeader GenericInfo { get; set; }
        public List<SelectListItem> States { get; set; }
        public List<SelectListItem> InsuranceTypes { get; set; }
        public List<SelectListItem> LiabilityLimit { get; set; }
        public string InsuranceType { get; set; }
        public bool NeedsUnderwritingReferral { get; set; }
    }
}