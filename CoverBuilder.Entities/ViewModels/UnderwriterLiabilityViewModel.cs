﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CoverBuilder.Entities.Entities;

namespace CoverBuilder.Entities.ViewModels
{
    public class UnderwriterLiabilityViewModel
    {
        public List<LiabilityInsuranceType> LiabilityInsuranceList { get; set; }
        public List<SelectListItem> UnderwriterList { get; set; }
        public LiabilityInsuranceType LiabilityInsuranceType { get; set; }
    }
}
