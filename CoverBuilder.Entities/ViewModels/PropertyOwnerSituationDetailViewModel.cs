﻿namespace CoverBuilder.Entities.ViewModels
{
    using CoverBuilder.Entities.Entities;
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class PropertyOwnerSituationDetailViewModel
    {
        public string SituationNumber { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public List<SelectListItemCheckboxes> Activities { get; set; }
        public string BuildingValue { get; set; }
        public string TenantOccupation { get; set; }
        public string RentalIncome { get; set; }
        public string Unoccupied { get; set; }
    }
}
