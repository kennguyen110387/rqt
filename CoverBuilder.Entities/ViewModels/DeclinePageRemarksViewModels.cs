﻿namespace CoverBuilder.Entities.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System;
    using System.Web.Mvc;
    using CoverBuilder.Entities.Entities;
    using CoverBuilder.Entities.Enums;

    public class DeclinePageRemarksViewModels
    {
        public List<SelectListItem> CheckboxReferenceLookup { get; set; }

    }
}