﻿namespace CoverBuilder.Entities.ViewModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System;
    using System.Web.Mvc;
    using CoverBuilder.Entities.Entities;
    using CoverBuilder.Entities.Enums;

    public class LiabilityInsuranceResultViewModels
    {
        public List<LiabilityInsuranceResult> LiabilityInsuranceListResult { get; set; }

        public LiabilityInsuranceResult liabilityInsuranceResult { get; set; }

        public List<SelectListItem> CheckboxReferenceLookup { get; set; }

        public List<SelectListItem> DropdownReferenceLookup { get; set; }

        public bool NeedsUnderwritingReferral { get; set; }

        public List<int> Trigger { get; set; }

        public int SituationNumber { get; set; }

        public int Status { get; set; }

        //Events and Markets
        public List<SelectListItem> EventType { get; set; }

        public string SelectedEventType { get; set; }

        //Accommodation
        public List<SelectListItem> AccommodationCoverRequired { get; set; }

        public List<SelectListItem> AccommodationIndoorOutdoor { get; set; }

        //Budget Accommodation
        public List<SelectListItem> BudgetModelCheckboxAccommodationUsed { get; set; }

        //Motels and B&B's
        public List<SelectListItem> MotelModelCheckboxAccommodationFall { get; set; }

        //Cleaners
        public List<SelectListItem> LossKeys { get; set; }

        public List<SelectListItemCheckboxes> CleanersModelCheckBoxGroup { get; set; }

        public List<SelectListItem> CleanersModelCheckBoxIncludedActivities { get; set; }

        //Scaffolders
        public List<SelectListItem> ScaffoldersOtherActivities { get; set; }

        //Transport and Logistics
        public List<SelectListItem> TransportAndLogisticsCheckBox { get; set; }

        public List<PropertyOwnerSituationDetailViewModel> PropertyOwnerSituation { get; set; }

        public List<SelectListItem> StateList { get; set; }

        public string UserQuotationStatus { get; set; }

        //Property Owners
        public List<List<System.Web.Mvc.SelectListItem>> PropertyOwnersCheckBox { get; set; }

        public List<VacantLandSituationViewModel> VacantLandSituation{ get; set; }
        public List<VacantLandPropertySituationModel> VacantLandPropertySituation { get; set; }

        public List<SelectListItem> HLScope { get; set; }

        public List<SelectListItem> HLDanceFrequency { get; set; }


        //Vacant Land Properties
        public List<SelectListItem> VLPUsePremises{ get; set; }
        public List<SelectListItem> VLPIsPremiseConnected { get; set; }
        public List<SelectListItem> VLPWhatIsLocalFireBrigade { get; set; }
        public List<SelectListItem> VLPGlassSumInsured { get; set; }

    }
}