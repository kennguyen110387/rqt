﻿using System.Collections.Generic;
using System.Web.Mvc;
using CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.Enums;

namespace CoverBuilder.Entities.ViewModels
{
    public class EmailTransactionViewModel
    {
        public List<EmailTransaction> EmailTransactionInfo { get; set; }

    }
}
