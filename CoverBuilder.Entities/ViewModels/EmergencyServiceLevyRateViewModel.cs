﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoverBuilder.Entities.Entities;

namespace CoverBuilder.Entities.ViewModels
{
    public class EmergencyServiceLevyRateViewModel
    {
        public List<EmergencyServiceLevyRateModel> ESLRateList { get; set; }
        public EmergencyServiceLevyRateViewModel()
        {
            List<EmergencyServiceLevyRateModel> model = new List<EmergencyServiceLevyRateModel>();
            EmergencyServiceLevyRateModel tr1 = new EmergencyServiceLevyRateModel()
            {
                Text = "Queensland",
                Value = ((float)0.00).ToString()
            };
            EmergencyServiceLevyRateModel tr2 = new EmergencyServiceLevyRateModel()
            {
                Text = "New South Wales",
                Value = ((float)0.30).ToString()
            };
            EmergencyServiceLevyRateModel tr3 = new EmergencyServiceLevyRateModel()
            {
                Text = "Victoria",
                Value = ((float)0.00).ToString()
            };
            EmergencyServiceLevyRateModel tr4 = new EmergencyServiceLevyRateModel()
            {
                Text = "Western Australia",
                Value = ((float)0.00).ToString()
            };
            EmergencyServiceLevyRateModel tr5 = new EmergencyServiceLevyRateModel()
            {
                Text = "South Australia",
                Value = ((float)0.00).ToString()
            };
            EmergencyServiceLevyRateModel tr6 = new EmergencyServiceLevyRateModel()
            {
                Text = "Tasmania",
                Value = ((float)0.28).ToString()
            };
            EmergencyServiceLevyRateModel tr7 = new EmergencyServiceLevyRateModel()
            {
                Text = "Australian Capital Territory",
                Value = ((float)0.00).ToString()
            };
            EmergencyServiceLevyRateModel tr8 = new EmergencyServiceLevyRateModel()
            {
                Text = "Northern Territory",
                Value = ((float)0.00).ToString()
            };
            model.Add(tr1);
            model.Add(tr2);
            model.Add(tr3);
            model.Add(tr4);
            model.Add(tr5);
            model.Add(tr6);
            model.Add(tr7);
            model.Add(tr8);
            ESLRateList = model;
        }
    }
}
