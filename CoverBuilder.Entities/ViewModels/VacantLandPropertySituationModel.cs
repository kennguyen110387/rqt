﻿using CoverBuilder.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities.ViewModels
{
    public class VacantLandPropertySituationModel
    {
        public string SituationNumber { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string LandSize { get; set; }
        public string PremiseUse { get; set; }
        public List<SelectListItemCheckboxes> WallConstruction { get; set; }
        public List<SelectListItemCheckboxes> RoofConstrucion { get; set; }
        public List<SelectListItemCheckboxes> FloorConstruction { get; set; }
        public string PercEPSorPIR { get; set; }
        public string YearPremiseBuilt { get; set; }
        public string YearPremiseRewired { get; set; }
        public List<SelectListItemCheckboxes> FireProtection { get; set; }
        public string PremiseTownWaterConnection { get; set; }
        public string LocalFireBrigade { get; set; }
        public List<SelectListItemCheckboxes> SecurityProtection { get; set; }
        public string BuildingSumInsured { get; set; }
        public string ContentsSumInsured { get; set; }
        public string TheftSumInsured { get; set; }
        public string GlassSumInsured { get; set; }
        public string IsHobbyFarm { get; set; }
        public string HobbyFarmDetail { get; set; }
        public string IsForAgistment { get; set; }
        public string AgistmentDetail { get; set; }
        public string IsForMarketOrParking { get; set; }
        public string MarketOrParkingDetail { get; set; }
        public string HasStructuresOrBuildings { get; set; }
        public string StructureOrBuildingDetail { get; set; }
        public string BuildOrSellLand { get; set; }
        public string CoverExtendPropertyDamage { get; set; }

  
    }
}
