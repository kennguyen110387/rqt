﻿using System.Collections.Generic;
using System.Web.Mvc;
using CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.Enums;

namespace CoverBuilder.Entities.ViewModels
{
    public class ConfigFormulaViewModels
    {
        public IEnumerable<PremiumCalculationReference> FormulaFactors { get; set; }
        public List<SelectListItem> InsuranceTypes { get; set; }
        //public string InsuranceType { get; set; }
    }
}