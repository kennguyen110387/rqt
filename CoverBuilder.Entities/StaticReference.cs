﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Entities
{
    public class StaticReference
    {
        delegate TOut ParamFunc<TIn, TOut>(TIn args);

        static Dictionary<string, string> StatusDictionary = new Dictionary<string, string>
        {
            {"New", "Web Quoted"},
            {"Requote", "Pipeline"}
        };

        static Dictionary<int, string> MultiSituationLiabilities = new Dictionary<int, string>
        {
            {7, "Property Owners"},
            {10, "Vacant Land"}
        };

        static Dictionary<string, string> AccommodationClassType = new Dictionary<string, string>
        {
            {"Backpackers", "BUDGET"},
            {"Boarding Houses", "BUDGET"},
            {"Student Accommodation", "BUDGET"},
            {"Motels", "MOTEL"},
            {"Bed &amp; Breakfast", "MOTEL"}
        };

        static Dictionary<int, ParamFunc<string, string[]>> SplitZoho = new Dictionary<int, ParamFunc<string, string[]>>
            {
             {308, Split},
             {11, Split},
             {18, Split},
             {518, Split},
             {551, Split},
             {620, Split},
             {82, Split},
             {49, Split},
             {132, Split},
             {226, Split},
             {357, Split},
             {359, Split},
             {358, Split},
             {363, Split},
             {366, Split},
             {276, Split}
            };

        public static Dictionary<int, int> ParentChildDetailQuestionnaire = new Dictionary<int, int>
        {
            {109, 108},
            {111, 110},
            {113, 112},
            {115, 114},
            {254, 253},
            {318, 315},
            {319, 316},
            {320, 317},
            {730, 729}, //Hotels
            {728, 727}
        };

        public static Dictionary<int, int> SubContractorPaymentQuestionaire = new Dictionary<int, int>
        {
            {2, 20},
            {5, 51},
            {6, 65},
            {8, 85},
            {9, 99},
            {12, 141},
            {11, 149},
            {18, 774},
            {20, 900}
        };

        public static Dictionary<int, int> SubContractorQuestionaire = new Dictionary<int, int>
        {
            {2, 14},
            {5, 46},
            {6, 61},
            {8, 84},
            {9, 98},
            {12, 140},
            {11, 133},
            {18, 773},
            {20, 799}
        };

        public static string GetLeadStatusByQuoteStatus(string quoteStatus)
        {
            string statusValue = string.Empty;
            if (StatusDictionary.TryGetValue(quoteStatus, out statusValue))
                return statusValue;

            return null;
        }

        public static bool IsMultiSitutationLiability(string liability)
        {
            return MultiSituationLiabilities.ContainsValue(liability);
        }

        public static bool IsCAGroup(int questionId)
        {
            if (questionId == 11)
                return true;
            return false;
        }

        public static bool IsPOService(int questionId)
        {
            if (questionId == 226)
                return true;
            return false;
        }

        public static bool CheckQuestionIdInDictionary(int questionId)
        {
            if (SplitZoho.ContainsKey(questionId))
                return true;
            return false;
        }

        public static string[] SplitResult(int questionId, string zohoResponse)
        {
            if (zohoResponse == null)
                return null;
            else 
            {
                var func = SplitZoho[questionId];
                return func(zohoResponse);
            }
        }

        public static int[] GetAllKeys()
        {
            return SplitZoho.Keys.ToArray();
        }

        public static string[] Split(string textStr)
        {
            return textStr.Split('|').ToArray();
        }

        public static string GetAccommodationBaClassType(string key)
        {
            string BaClassType = string.Empty;
            if (AccommodationClassType.TryGetValue(key, out BaClassType))
                return BaClassType;

            return null;
        }

        public static int GetParentIdQuestionyChildId(int childQuestionId)
        {
            int parentId;
            if (ParentChildDetailQuestionnaire.TryGetValue(childQuestionId, out parentId))
                return parentId;

            return -0;
        }

        public static int GetSubconPaymentQuestionId(int insuranceId)
        {
            int questionId;
            if (SubContractorPaymentQuestionaire.TryGetValue(insuranceId, out questionId))
                return questionId;

            return -0;
        }

        public static int GetSubconQuestionId(int insuranceId)
        {
            int questionId;
            if (SubContractorQuestionaire.TryGetValue(insuranceId, out questionId))
                return questionId;

            return -0;
        }
                
    }
}
