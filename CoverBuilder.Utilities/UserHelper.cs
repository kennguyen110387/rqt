﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.Utilities
{
    public static class UserHelper
    {

        public static PasswordObject HashPassword(string passwordInput)
        {
            byte[] salt;
            byte[] buffer;

            using (Rfc2898DeriveBytes cryptoHasher = new Rfc2898DeriveBytes(passwordInput, 0x10, 0x3e8))
            {
                salt = cryptoHasher.Salt;
                buffer = cryptoHasher.GetBytes(0x20);
            }
            byte[] dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer, 0, dst, 0x11, 0x20);
            var passObj = new PasswordObject()
            {
                Password = passwordInput,
                PasswordHash = Convert.ToBase64String(dst),
                Salt = Convert.ToBase64String(salt)
            };
            return passObj;
        }

        public static bool ValidatePassword(string hashedPassword, string passwordInput)
        {
            byte[] buffer4;
            byte[] src = Convert.FromBase64String(hashedPassword);
            if ((src.Length != 0x31) || (src[0] != 0))
            {
                return false;
            }
            byte[] dst = new byte[0x10];
            Buffer.BlockCopy(src, 1, dst, 0, 0x10);
            byte[] buffer3 = new byte[0x20];
            Buffer.BlockCopy(src, 0x11, buffer3, 0, 0x20);
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(passwordInput, dst, 0x3e8))
            {
                buffer4 = bytes.GetBytes(0x20);
            }
            return ByteArraysEqual(buffer3, buffer4);
        }

        private static bool ByteArraysEqual(byte[] p1, byte[] p2)
        {
            if (p1.Length != p2.Length) return false;
            int i = 0;
            while (i < p1.Length && (p1[i] == p2[i]))
            {
                i++;
            }
            return i == p2.Length;
        }
    }

    public class PasswordObject
    {
        public string Password { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
    }
}
