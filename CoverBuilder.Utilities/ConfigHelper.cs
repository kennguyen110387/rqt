﻿namespace CoverBuilder.Utilities
{
    using System;
    using System.Configuration;
    using System.Text;

    public class ConfigHelper
    {

        //private static string encryptionPassword = "cartTool";
        //public string GetConnectionString
        //private static string connectionString;
        private const string encryptionPassword = "etlWrs";

        public string GetConnectionString(string connectionString, string db)
        {
            //connectionString = connectionString
            connectionString = DecryptConnectionString(connectionString, db);
            connectionString = connectionString.Replace("&quot", "'");
            return connectionString;
        }

        //This will convert the userid and password in connection string
        public static string DecryptConnectionString(string connString, string db)
        {
            string userid, userpwd;
            //if (db == "CoverBuilder")
            //{

            //    userid = ConfigHelper.GetConfigValue("CoverBuilder.Username");
            //    userpwd = ConfigHelper.GetConfigValue("CoverBuilder.Password");
            //}
            //else
            //{
                userid = ConfigHelper.GetConfigValue("CoverBuilder.Username");
                userpwd = ConfigHelper.GetConfigValue("CoverBuilder.Password");
            //}

            string newConnString = connString.Replace("##DB_USERID##", userid).Replace("##DB_PASSWORD##", userpwd);
            return newConnString;
        }

        public static string GetConfigValue(string key)
        {

            if (ConfigurationManager.AppSettings[key] != null)
            {
                return ConfigurationManager.AppSettings[key].ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetDecryptedConfigValue(string key)
        {

            if (ConfigurationManager.AppSettings[key] != null)
            {
                return ConfigurationManager.AppSettings[key].ToString();
            }
            else
            {
                return string.Empty;
            }
        }


        public static string EscapeLargeString(string largeString)
        {
            var value = largeString;
            var limit = 2000;

            var sb = new StringBuilder();
            var loops = value.Length / limit;

            for (int i = 0; i <= loops; i++)
            {
                if (i < loops)
                {
                    sb.Append(Uri.EscapeDataString(value.Substring(limit * i, limit)));
                }
                else
                {
                    sb.Append(Uri.EscapeDataString(value.Substring(limit * i)));
                }
            }

            return value;
        }
    }
}
