﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Web.Script.Serialization;
using ZohoStoreV2.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using CoverBuilder.ZohoStore.Model;
using System.Globalization;
using CoverBuilder.Common.Logging;
using CoverBuilder.Common.Logging.NLog;
using NLog.Config;

namespace ZohoStoreV2.Service
{
    public class ZohoAgentV2
    {
        private const string _USERSMODULE = "users";
        private const string ZohoAPIUrl = "https://www.zohoapis.com/crm/v2/";
        private string ZohoApiKey;
        private static readonly HttpClient _client = new HttpClient();

        private const string CLIENT_ID = "1000.TB5QD9SQA7Y498245QYMJMS27TYB3H";
        private const string CLIENT_SECRET = "dd8eb37db9d2871a2948d44a79e1a2f6e4db5d808b";
        private const string GRANT_TYPE = "refresh_token";
        private const string REFRESH_TOKEN = "1000.9427d08a207df2015be800279f007ed8.02eb85a4ffaf02f4ae3b609624c23250";
        private const string TOKEN_ERROR = "Error generating access token.";
        private const string CODE = "1000.8e73f5e757bd7b7ca100140f44b3bc3c.d155d762bce606278317d8a8fc31c59c";

        private const string ZOHO_TOKEN_REQUEST_URL = "https://accounts.zoho.com/oauth/v2/token";
        public string accessToken;

        private static readonly ILogger Log = new NLogger();

        private string ReplaceSpecialChar(string json)
        {
            if (json.ToString().Contains("&amp;"))
                json = json.ToString().Replace("&amp;", "&");

            if (json.ToString().Contains("&#x0D;"))
                json = json.ToString().Replace("&#x0D;", " ");

            return json;
        }

        public ZohoAgentV2(string token = null)
        {
            if (!string.IsNullOrEmpty(token))
                ZohoApiKey = token;
            else
                ZohoApiKey = System.Configuration.ConfigurationManager.AppSettings["ZohoCRMAuthV2"];
        }

        public void Authenticate(string scope)
        {
            var req = Post();
        }
        private static String Post()
        {
            HttpWebRequest req = WebRequest.Create("https://www.zohoapis.com/crm/v2/users?type=AllUsers") as HttpWebRequest;
            req.Method = "GET";
            if (-1 != 0)
                req.Timeout = -1;
            req.Headers.Set("Authorization", "fa1da4ca75148fce5947ca7dfebfc050");
            req.AutomaticDecompression = DecompressionMethods.GZip;
            String ret = null;
            using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                ret = reader.ReadToEnd();
            }

            return ret;
        }

        private string HTTP(string URL, string data, string POST_GET)
        {
            HttpWebRequest myReq = HttpWebRequest.Create(URL) as HttpWebRequest;
            myReq.Method = POST_GET;
            myReq.ContentType = "application/json";
            myReq.Headers.Add("Authorization", ZohoApiKey);
            //if (data != "")
            //    myReq.GetRequestStream().Write(System.Text.Encoding.UTF8.GetBytes(data), 0, System.Text.Encoding.UTF8.GetBytes(data).Count);

            String ret = null;
            using (HttpWebResponse response = (HttpWebResponse)myReq.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                ret = reader.ReadToEnd();
            }

            return ret;
        }

        public string GenerateTokenv2()
        {
            try
            {
                var values = new Dictionary<string, string>
                {
                    { "refresh_token", REFRESH_TOKEN },
                    { "client_id", CLIENT_ID },
                    { "client_secret", CLIENT_SECRET },
                    { "grant_type", GRANT_TYPE }
                };
                HttpContent content = new FormUrlEncodedContent(values);
                var response = _client.PostAsync(ZOHO_TOKEN_REQUEST_URL, content).Result;
                var responseString = response.Content.ReadAsStringAsync().Result;

                Log.Info(string.Format("\n GenerateToken response \n\t : {0}\n", JsonConvert.SerializeObject(responseString)));

                dynamic results = JsonConvert.DeserializeObject<dynamic>(responseString);
                accessToken = results.access_token;
                return accessToken;
            }
            catch (Exception)
            {
                return TOKEN_ERROR;
            }
        }

       
        public ZohoUsers GetUsers()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var uri = string.Format("{0}{1}", ZohoAPIUrl, _USERSMODULE);
            HttpWebRequest req = WebRequest.Create(uri) as HttpWebRequest;
            req.Method = "GET";
            if (-1 != 0)
                req.Timeout = -1;
            req.Headers.Set("Authorization", ZohoApiKey);
            req.AutomaticDecompression = DecompressionMethods.GZip;
            String ret = null;
            using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                ret = reader.ReadToEnd();
            }
            return DeserializeJsonResponse<ZohoUsers>(ret);
        }

        private T DeserializeJsonResponse<T>(string zohoResponse)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            var obj = JsonConvert.DeserializeObject<T>(zohoResponse);

            return (T)Convert.ChangeType(obj, typeof(T));
        }

        public async Task<ResponseJsonResult> PushAsync(string uri, string data, string token)
        {
            string url = string.Format("{0}{1}", ZohoAPIUrl, uri);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var httpContent = new StringContent(ReplaceSpecialChar(data), Encoding.UTF8, "application/json");
            _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Zoho-oauthtoken", token);

            var response = await _client.PostAsync(url, httpContent);
            var responseString = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<ResponseJsonResult>(responseString);
            return result;

        }

        public async Task<T> SearchSituationRefIdAsync<T>(string module, string referenceId, string token)
        {
           
            string url = string.Format("{0}{1}/search?criteria=(Name:equals:{2})", ZohoAPIUrl, module, referenceId);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Zoho-oauthtoken", token);

            var response = await _client.GetAsync(url);
            var responseString = await response.Content.ReadAsStringAsync();

            var jsonModel = JsonConvert.DeserializeObject<Situations>(responseString);

            var result = (T)Convert.ChangeType(jsonModel.data[0], typeof(T));
            return result;
        }

        public async Task<T> SearchLeadsRefIdAsync<T>(string module, string referenceId, string token)
        {
          
            string url = string.Format("{0}{1}/search?criteria=(Reference_Number:equals:{2})", ZohoAPIUrl, module, referenceId);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Zoho-oauthtoken", token);

            var response = await _client.GetAsync(url);
            var responseString = await response.Content.ReadAsStringAsync();

            var jsonModel = JsonConvert.DeserializeObject<Leads>(responseString);

            var result = (T)Convert.ChangeType(jsonModel.data[0], typeof(T));
            return result;
        }

        public async Task<List<PremiumQuotationInfo>> SearchPremiumInfoByReferenceIdAsync(string referenceId, string module, string token)
        {
           
            string url = string.Format("{0}{1}/search?criteria=(Reference_Number:equals:{2})", ZohoAPIUrl, module, referenceId);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Zoho-oauthtoken", token);

            var response = await _client.GetAsync(url);

            var responseString = await response.Content.ReadAsStringAsync();

            var jsonModel = JsonConvert.DeserializeObject<PremiumQuotationInfos>(responseString);

            return jsonModel.data;
        }
        public async Task<ResponseResult> UploadFileAsync(string uri, string zohoId, string fileName, byte[] fileContent, string token)
        {
          
            string url = string.Format("{0}{1}/{2}/Attachments", ZohoAPIUrl, uri, zohoId);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Zoho-oauthtoken", token);

            string responseString;
            using (var content = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
            {
                content.Add(new StreamContent(new MemoryStream(fileContent)), "file", fileName);

                var response = await _client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
            }

            var result = JsonConvert.DeserializeObject<ResponseResult>(responseString);
            return result;
        }

        public async Task<ResponseJsonResult> UpdateQuotationAsync(string uri, string zohoId, string data, string token)
        {
          
            string url = string.Format("{0}{1}/{2}", ZohoAPIUrl, uri, zohoId);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var httpContent = new StringContent(data, Encoding.UTF8, "application/json");
            _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Zoho-oauthtoken", token);

            var response = await _client.PutAsync(url, httpContent);
            var responseString = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<ResponseJsonResult>(responseString);
            return result;
        }

        public async Task<Situation> SearchRecordsByReferenceIdAsync(string module, string referenceId, string token)
        {
           
            string url = string.Format("{0}{1}/search?criteria=(Reference_Number:equals:{2})", ZohoAPIUrl, module, referenceId);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Zoho-oauthtoken", token);

            Log.Info(string.Format("\n SearchRecordsByReferenceIdAsync \n\t : {0}\n", url));

            var response = await _client.GetAsync(url);

            var responseString = await response.Content.ReadAsStringAsync();

            var jsonModel = JsonConvert.DeserializeObject<Situations>(responseString);

            Log.Info(string.Format("\n SearchRecordsByReferenceIdAsync response \n\t : {0}\n", responseString));

            return jsonModel.data[0];
        }
    }
}
