﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZohoStoreV2.Model
{
    public class ZohoUserInfo
    {
        public string country { get; set; }
        public role role { get; set; }
        public string city { get; set; }
        public string signature { get; set; }
        public string name_format { get; set; }
        public string language { get; set; }
        public string locale { get; set; }
        public string personal_account { get; set; }
        public string default_tab_group { get; set; }
        public string street { get; set; }
        public string alias { get; set; }
        public theme theme { get; set; }
        public string id { get; set; }
        public string state { get; set; }
        public string country_locale { get; set; }
        public string fax { get; set; }
        public string first_name { get; set; }
        public string email { get; set; }
        public string zip { get; set; }
        public string decimal_separator { get; set; }
        public string website { get; set; }
        public string time_format { get; set; }
        public profile profile { get; set; }
        public string mobile { get; set; }
        public string last_name { get; set; }
        public string time_zone { get; set; }
        public string zuid { get; set; }
        public string confirm { get; set; }
        public string full_name { get; set; }
        public List<territories> territories { get; set; }
        public string phone { get; set; }
        public string dob { get; set; }
        public string date_format { get; set; }
        public string status { get; set; }
    }
}
