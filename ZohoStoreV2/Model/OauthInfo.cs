﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZohoStoreV2.Model
{
    public class OauthInfo
    {
        public string Code { get; set; }
        public string Location { get; set; }
        public string Server { get; set; }
    }
}
