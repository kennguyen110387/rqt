﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZohoStoreV2.Model
{
    public class theme
    {
        public normal_tab normal_tab { get; set; }
        public selected_tab selected_tab { get; set; }
        public string new_background { get; set; }
        public string background { get; set; }
        public string screen { get; set; }
        public string type { get; set; }
    }
    public class normal_tab
    {
        public string font_color { get; set; }
        public string background { get; set; }
        
    }
    public class selected_tab
    {
        public string font_color { get; set; }
        public string background { get; set; }
    }

}
