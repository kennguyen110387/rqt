﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZohoStoreV2.Model
{
    public class info
    {
        public string per_page { get; set; }
        public string count { get; set; }
        public string page { get; set; }
        public string more_records { get; set; }
    }
}
