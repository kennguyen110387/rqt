﻿namespace CoverBuilder.Common.Email
{
    using System;
    using System.Configuration;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Web;
    using CoverBuilder.Common.Logging;
    using CoverBuilder.Common.Logging.NLog;
    using NLog.Config;

    public class EmailHandler : IEmailHandler
    {

        private static readonly ILogger Log = new NLogger();

        public string To { get; set; }

        public string From { get; set; }

        public string CarbonCopy { get; set; }

        public string BlindCarbonCopy { get; set; }

        public object RawAttachment { get; set; }

        private string ConvertImageToBase64(string path)
        {
            byte[] imageArray = System.IO.File.ReadAllBytes(path);
            string base64ImageRepresentation = Convert.ToBase64String(imageArray);
            var stringToReturn = "data:image/png;base64," + base64ImageRepresentation;
            return stringToReturn;

        }
        public string Send(string subject, string emailBody, bool isBodyHtml, List<Attachment> files = null)
        {

            // EMBED: Email Images
            string emailHeader = "<table width=\"1000px\" style=\" margin-bottom: 50px;\">" +
                            "<tbody><tr style=\"font-family:  calibri; font-size:11pt;\">" +
                            "<td style=\"font-weight:bold; font-family:  calibri; padding-right: 143px;\">From:</td>" +
                            "<td>+FROM+</td>" +
                            "</tr>" +
                            "<tr style=\"font-family:  calibri; font-size:11pt;\">" +
                            "<td style=\"font-weight:bold;font-family:  calibri;padding-right: 143px;\">Sent:</td>" +
                            "<td>+SENTDATE+</td>" +
                            "</tr>" +
                            "<tr style=\"font-family:  calibri; font-size:11pt;\">" +
                            "<td style=\"font-weight:bold;font-family:  calibri;padding-right: 143px;\">To:</td>" +
                            "<td>+TO+</td>" +
                            "</tr>" +
                            "<tr style=\"font-family:  calibri; font-size:11pt;\">" +
                            "<td style=\"font-weight:bold;font-family:  calibri;padding-right: 143px;\">Cc:</td>" +
                            "<td>+CC+</td>" +
                            "</tr>" +
                            "<tr style=\"font-family:  calibri; font-size:11pt;\">" +
                            "<td style=\"font-weight:bold;font-family:  calibri;padding-right: 143px;\">Subject:</td>" +
                            "<td>" + subject + "</td>" +
                            "</tr>" +
                            "</tbody></table><br /> <br/> <br/> <br/><br/> <br/>";

            string htmlBody = "<a href=\"https://www.facebook.com/RynoInsurance\"><img src=\"cid:RynoFacebook\" /></a>&nbsp;&nbsp;&nbsp;" +
                              "<a href=\"https://www.linkedin.com/company/ryno-insurance\"><img src=\"cid:RynoLinkedin\" /></a><br/><br/> " +
                              "<img src=\"cid:RynoBanner\" /><br/>" +
                              "<p style=\"color:orange;font-family:Arial;font-size:12px;\">A division of East West Insurance Brokers Pty Ltd ABN Number 83 010 630 092   AFS Licence Number 230041</p>";
            var startTag = "underwriting guidelines due to";
            var htmlEmailPdf = emailHeader + emailBody + htmlBody;
            if (emailBody.Contains(startTag))
            {
                int startIndex = emailBody.IndexOf(startTag) + startTag.Length;
                int endIndex = emailBody.IndexOf("</p>", startIndex);
                var newres = emailBody.Substring(startIndex, endIndex - startIndex);
                var encoded = HttpUtility.HtmlEncode(newres);
                htmlEmailPdf = htmlEmailPdf.Replace(newres, encoded);
            }

            AlternateView avHtml = AlternateView.CreateAlternateViewFromString(emailBody + htmlBody, null, MediaTypeNames.Text.Html);

            // Create a LinkedResource object for each embedded image
            var imgBannerPath = System.Web.Hosting.HostingEnvironment.MapPath("~/images/Email/Ryno B2B Email Sig.png");
            var imgFacebookPath = System.Web.Hosting.HostingEnvironment.MapPath("~/images/Email/facebook-icon.png");
            var imgLinkedinPath = System.Web.Hosting.HostingEnvironment.MapPath("~/images/Email/linkedin-icon.png");


            htmlEmailPdf = htmlEmailPdf.Replace("cid:RynoFacebook", imgFacebookPath);
            htmlEmailPdf = htmlEmailPdf.Replace("cid:RynoLinkedin", imgLinkedinPath);
            htmlEmailPdf = htmlEmailPdf.Replace("cid:RynoBanner", imgBannerPath);

            LinkedResource imgBanner = new LinkedResource(imgBannerPath, MediaTypeNames.Image.Jpeg);
            LinkedResource imgFacebook = new LinkedResource(imgFacebookPath, MediaTypeNames.Image.Jpeg);
            LinkedResource imgLinkedin = new LinkedResource(imgLinkedinPath, MediaTypeNames.Image.Jpeg);

            imgBanner.ContentId = "RynoBanner";
            imgFacebook.ContentId = "RynoFacebook";
            imgLinkedin.ContentId = "RynoLinkedin";

            avHtml.LinkedResources.Add(imgBanner);
            avHtml.LinkedResources.Add(imgFacebook);
            avHtml.LinkedResources.Add(imgLinkedin);
            try
            {

                string Host = ConfigurationManager.AppSettings["SmtpServer"];
                int Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);

                NetworkCredential credentials = new NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPwd"]);

                // SET: Mail Addresses
                // CHECK: If there are default template mails set. If no mails are set then use the mails on the web.config file
                if (string.IsNullOrEmpty(To))
                    To = ConfigurationManager.AppSettings["To"];

                if (string.IsNullOrEmpty(From))
                    From = ConfigurationManager.AppSettings["From"];

                if (string.IsNullOrEmpty(CarbonCopy))
                    CarbonCopy = ConfigurationManager.AppSettings["CcAlwaysTo"];

                if (string.IsNullOrEmpty(BlindCarbonCopy))
                    BlindCarbonCopy = ConfigurationManager.AppSettings["BccAlwaysTo"];

                using (SmtpClient client = new SmtpClient(Host, Port))
                using (MailMessage mail = new MailMessage(From, To, subject, emailBody))
                {
                    // ADD: File Attachments
                    foreach (var file in files)
                        mail.Attachments.Add(file);

                    // SET: Mail Properties
                    mail.IsBodyHtml = true;
                    mail.BodyEncoding = System.Text.Encoding.UTF8;
                    mail.SubjectEncoding = System.Text.Encoding.Default;
                    // mail.Priority = MailPriority.High;

                    // ADD: Alternative View For the Mail Footer
                    mail.AlternateViews.Add(avHtml);

                    // ADD: CC/BCC Mail Addresses (Only If Present)
                    if (!string.IsNullOrEmpty(CarbonCopy))
                        mail.CC.Add(CarbonCopy);

                    if (!string.IsNullOrEmpty(BlindCarbonCopy))
                        mail.Bcc.Add(BlindCarbonCopy);

                    // SET: Mail Delivery Properties
                    client.EnableSsl = false;
                    client.Credentials = credentials;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;

                    // REPLACE: Mail Details (PDF Version)
                    string dateSent = String.Format("{0:F}", DateTime.Now);
                    htmlEmailPdf = htmlEmailPdf.Replace("+FROM+", From.Replace("<", "&lt;").Replace(">", "&gt;"));
                    htmlEmailPdf = htmlEmailPdf.Replace("+TO+", To);
                    htmlEmailPdf = htmlEmailPdf.Replace("+SENTDATE+", dateSent);
                    htmlEmailPdf = htmlEmailPdf.Replace("+CC+", CarbonCopy);

                    // SEND: Mail
                    client.Send(mail);
                    Log.Info(string.Format("\n Sending Email Done \n\t : {0}\n", mail.Subject));
                    //return htmlEmailPdf;
                }
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n Sending Email ERROR \n\t : {0}\n", ex.Message));
            }
            return htmlEmailPdf;
        }

        private void CheckAttachmentType(Attachment fileAttachment)
        {

        }
    }
}
