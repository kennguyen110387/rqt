﻿namespace CoverBuilder.Common.Email
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Security;
    using System.Text;

    public interface IEmailHandler
    {
        string To { get; set; }
        string From { get; set; }
        string CarbonCopy { get; set; }
        string BlindCarbonCopy { get; set; }
        object RawAttachment { get; set; }
        string Send(string subject, string emailBody, bool isBodyHtml, List<Attachment> files = null);

    }
}
