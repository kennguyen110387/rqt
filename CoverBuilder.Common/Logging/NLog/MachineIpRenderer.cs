﻿using System.Text;
using System.Web;
using NLog;
using NLog.LayoutRenderers;
using System.Net;

namespace CoverBuilder.Common.Logging.NLog
{
    [LayoutRenderer("aspnet-request-ip")]
    public class AspNetRequestIpLayoutRenderer : LayoutRenderer
    {
        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName()); // `Dns.Resolve()` method is deprecated.
            IPAddress ipAddress = ipHostInfo.AddressList[0];

            builder.Append(ipAddress.ToString());
        }
    }
}
