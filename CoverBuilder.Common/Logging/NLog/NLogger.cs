﻿using CoverBuilder.Entities.Enums;
using NLog;
using System;

namespace CoverBuilder.Common.Logging
{
    public class NLogger : ILogger
    {

        private Logger _logger;
        private LogEventInfo _eventInfo;

        public NLogger()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }
                
        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Warn(string message)
        {
            _logger.Warn(message);
        }

        public void Debug(string message)
        {
            _logger.Debug(message);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Error(Exception ex)
        {
            Error(LogUtility.BuildExceptionMessage(ex));
        }

        public void Error(string message, Exception ex)
        {
            _logger.Error(ex, message);
        }

        public void Fatal(string message)
        {
            _logger.Fatal(message);
        }

        public void Fatal(Exception ex)
        {
            Fatal(LogUtility.BuildExceptionMessage(ex));
        }

        public void AddContext(LogEventInfo logEventInfo)
        {
            _eventInfo = logEventInfo;
        }

        public void Log(LogEventInfo logEventInfo)
        {
            _logger.Log(logEventInfo);
        }

        public void Log(LogTypeEnum logType = LogTypeEnum.Info, string referenceNumber = null, string message = null, Exception ex = null)
        {
            LogLevel logLevel;
            switch (logType)
            {
                case LogTypeEnum.Info:
                    logLevel = LogLevel.Info;
                    break;
                case LogTypeEnum.Warning:
                    logLevel = LogLevel.Warn;
                    break;
                case LogTypeEnum.Error:
                    logLevel = LogLevel.Error;
                    break;
                case LogTypeEnum.Fatal:
                    logLevel = LogLevel.Fatal;
                    break;
                default:
                    throw new ArgumentException("LogLevel not supported");
            }

            LogEventInfo logEventInfo = new LogEventInfo(logLevel, string.Empty, message);
            if (referenceNumber != null)
                logEventInfo.Properties["ReferenceId"] = referenceNumber;

            _logger.Log(logEventInfo);
        }

        private void GuardAgainstNullContext()
        {
            if (_eventInfo == null)
            {
                _eventInfo = new LogEventInfo();
                _eventInfo.Properties["ReferenceId"] = "No Reference";
            }
        }
    }
}
