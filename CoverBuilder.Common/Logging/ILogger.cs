﻿using NLog;
using System;

namespace CoverBuilder.Common.Logging
{
    public interface ILogger
    {
        void Info(string message);

        void Warn(string message);

        void Debug(string message);

        void Error(string message);

        void Error(string message, Exception ex);

        void Error(Exception ex);

        void Fatal(string message);

        void Fatal(Exception ex);

        void Log(CoverBuilder.Entities.Enums.LogTypeEnum logType = Entities.Enums.LogTypeEnum.Info, string referenceNumber = null, string message = null, Exception ex = null);
    }
}
