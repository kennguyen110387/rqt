﻿namespace CoverBuilder.ZohoStore.Service
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using CoverBuilder.ZohoStore.Contract;
    using CoverBuilder.ZohoStore.Model;
    using Newtonsoft.Json.Linq;
    using ZohoStore.ServiceHelper;
    
    public class ZohoAgent : IServiceAgent
    {
        private const string _LEADS = "Leads";  
        private const string _SITUATIONS = "CustomModule10";
        private const string ZohoApiXmlUrl = "https://crm.zoho.com/crm/private/xml/";
        private const string ZohoApiJsonUrl = "https://crm.zoho.com/crm/private/json/";
        private string ZohoApiKey;
        private static readonly HttpClient _client = new HttpClient();

        public ZohoAgent(string authToken = null)
        {
            if (!string.IsNullOrEmpty(authToken))
                ZohoApiKey = authToken;
            else
                ZohoApiKey = System.Configuration.ConfigurationManager.AppSettings["ZohoCRMAuth"];
           
        }
        
        public async Task<ResponseResult> PushAsync(string uri, string action, string webPart)
        {
            string url = string.Format("{0}{1}/{2}", ZohoApiXmlUrl, uri, action);
            Dictionary<string, string> queryParams = new Dictionary<string, string>
            {
                {"authtoken", ZohoApiKey},
                {"scope", "crmpai"},
                {"newFormat", "1"},
                {"wfTrigger", "true"},
                {"xmlData", webPart}
            };

            var encodedContent = new FormUrlEncodedContent(queryParams);
            var response = await _client.PostAsync(url, encodedContent);
            var responseString = await response.Content.ReadAsStringAsync();

            return InsertXmlResponse(responseString);
        }
        
        public async Task<ResponseResult> UpdateQuotationAsync(string uri, string action, string webPart, string zohoId)
        {
            string url = string.Format("{0}{1}/{2}", ZohoApiJsonUrl, uri, action);
            Dictionary<string, string> queryParams = new Dictionary<string, string>
            {
                {"authtoken", ZohoApiKey},
                {"scope", "crmpai"},
                {"id", zohoId},
                {"newFormat", "1"},
                {"wfTrigger", "true"},
                {"xmlData", webPart}
            };

            var encodedContent = new FormUrlEncodedContent(queryParams);
            var response = await _client.PostAsync(url, encodedContent);
            var responseString = await response.Content.ReadAsStringAsync();

            return DeleteJsonResponse(responseString);
        }

        public async Task<ResponseResult> UploadFileAsync(string zohoId, string fileName, byte[] fileContent)
        {
            String url = string.Format("{0}{1}{2}{3}{4}", ZohoApiJsonUrl, "Leads/uploadFile?authtoken=", ZohoApiKey, "&scope=crmapi&id=", zohoId);
            string respString;
                using (var content = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                {
                    content.Add(new StreamContent(new MemoryStream(fileContent)), "content", fileName);

                    var response = await _client.PostAsync(url, content);
                    respString = await response.Content.ReadAsStringAsync();
                }
            return DeleteJsonResponse(respString);
        }

        public async Task<T> SearchByModuleAndRefIdAsync<T>(string module, string referenceId)
        {
            string url = string.Format("{0}{1}/searchRecords?authtoken={2}&fromIndex=1&toIndex=200&scope=crmapi&criteria=(Reference Number:{3})", ZohoApiJsonUrl, module, ZohoApiKey, referenceId);
            HttpResponseMessage result = _client.GetAsync(new System.Uri(url)).Result;
            var response = string.Empty;
            if (result.IsSuccessStatusCode)
            {
                response = await result.Content.ReadAsStringAsync();
            }

            var jsonModel = ParseJsonData<T>(response);

            return (T)Convert.ChangeType(jsonModel, typeof(T)); 
        }

        public async Task<List<PremiumQuotationInfo>> SearchPremiumInfoByReferenceIdAsync(string referenceId)
        {
            string url = string.Format("{0}Leads/searchRecords?authtoken={1}&fromIndex=1&toIndex=200&scope=crmapi&criteria=(Reference Number:{2})", ZohoApiJsonUrl, ZohoApiKey, referenceId);
            HttpResponseMessage result = _client.GetAsync(new System.Uri(url)).Result;
            var response = string.Empty;
            if (result.IsSuccessStatusCode)
            {
                response = await result.Content.ReadAsStringAsync();
            }
            return GetPremiumInfo(response);
        }

        public List<PremiumQuotationInfo> GetPremiumInfo(string zohoRespone)
        {
            JObject myObj = JObject.Parse(zohoRespone);
            IList<JToken> zohoResultIds = myObj["response"]["result"]["Leads"]["row"].Children().ToList();
            PremiumQuotationInfo searchResult = new PremiumQuotationInfo();
            IList<PremiumQuotationInfo> searchResults = new List<PremiumQuotationInfo>();
            if (zohoResultIds[0] is JObject)
            {
                foreach (var zohoRow in zohoResultIds)
                {
                    searchResult = zohoRow["FL"].First.ToObject<PremiumQuotationInfo>();
                    searchResults.Add(searchResult);
                }
            }
            else if (zohoResultIds[0] is JProperty)
            {
                var tokens = zohoResultIds[1].First.ToList();
                foreach (var item in tokens)
                {
                    switch (item["val"].ToString())
                    {
                        case "LEADID":
                            searchResult.LeadId = item["content"].ToString();
                            break;
                        case "Broker Comm":
                            searchResult.BrokerComm = item["content"].ToString();
                            break;
                        case "Broker Comm GST":
                            searchResult.BrokerCommGST = item["content"].ToString();
                            break;
                        case "Stamp Tax":
                            searchResult.StampTax = item["content"].ToString();
                            break;
                        case "Final Premium":
                            searchResult.FinalPremium = item["content"].ToString();
                            break;
                        case "Underwriter Fee GST":
                            searchResult.UnderwriterFeeGST = item["content"].ToString();
                            break;
                        case "Premium Sub Total":
                            searchResult.PremiumSubTotal = item["content"].ToString();
                            break;
                        case "Premium Base":
                            searchResult.PremiumBase = item["content"].ToString();
                            break;
                        case "GST":
                            searchResult.GST = item["content"].ToString();
                            break;
                        case "Underwriter Fee":
                            searchResult.UnderwriterFee = item["content"].ToString();
                            break;
                        default:
                            break;
                    }
                }
                searchResults.Add(searchResult);
            }


            return searchResults as List<PremiumQuotationInfo>;
        }

        public async Task<List<ResponseResult>> SearchRecordsByReferenceIdAsync(string module, string referenceId)
        {
            var refParameter = "Reference Number";
            string url = string.Format("{0}{1}/searchRecords?authtoken={2}&fromIndex=1&toIndex=200&scope=crmapi&criteria=("+refParameter+":{3})", ZohoApiJsonUrl, module, ZohoApiKey, referenceId);

            var response = string.Empty;
            HttpResponseMessage result = await _client.GetAsync(new System.Uri(url));
            if (result.IsSuccessStatusCode)
            {
                response = await result.Content.ReadAsStringAsync();
            }
                return GetZohoId(response, module);
        }
       
        public List<ResponseResult> GetZohoId(string jsonData, string moduleName)
        {
            JObject myObj = JObject.Parse(jsonData);
            IList<JToken> zohoResultIds = myObj["response"]["result"][moduleName]["row"].Children().ToList();
            IList<ResponseResult> searchResults = new List<ResponseResult>();
            ResponseResult result = new ResponseResult();
            if (zohoResultIds[0] is JObject)
            {
                foreach (var zohoRow in zohoResultIds)
                {
                    result = zohoRow["FL"].First.ToObject<ResponseResult>();
                    searchResults.Add(result);
                }
            }
            else if (zohoResultIds[0] is JProperty)
            {
                result = zohoResultIds[1].First.First.ToObject<ResponseResult>();
                searchResults.Add(result);
            }

            return searchResults as List<ResponseResult>;
        }

        private ResponseResult InsertXmlResponse(string zohoResponse)
        {
            var xmlData = XDocument.Parse(zohoResponse);

            var firstNode = xmlData.DescendantNodes().ElementAt(1) as XElement;
            string responseNode = firstNode.Name.LocalName;
            var xmlRoot = xmlData.Root.Element(responseNode);

            if (responseNode == "result")
            {
                if (xmlRoot.Descendants("recorddetail").Count() > 1)
                {
                    return new ResponseResult
                    {
                        Code = 500,
                        Message = "Insert Succeeded for multiple items."
                    };
                }
                else
                {
                    var resultChild = xmlRoot.Descendants("FL");
                    return new ResponseResult
                    {
                        ZohoId = resultChild.Single(x => x.Attribute("val").Value == "Id").Value == null ? string.Empty : resultChild.Single(x => x.Attribute("val").Value == "Id").Value,
                        CreatedTime = resultChild.Single(x => x.Attribute("val").Value == "Created Time").Value == null ? string.Empty : resultChild.Single(x => x.Attribute("val").Value == "Created Time").Value,
                        ModifiedTime = resultChild.Single(x => x.Attribute("val").Value == "Modified Time").Value == null ? string.Empty : resultChild.Single(x => x.Attribute("val").Value == "Modified Time").Value,
                        CreatedBy = resultChild.Single(x => x.Attribute("val").Value == "Created By").Value == null ? string.Empty : resultChild.Single(x => x.Attribute("val").Value == "Created By").Value,
                        ModifiedBy = resultChild.Single(x => x.Attribute("val").Value == "Modified By").Value == null ? string.Empty : resultChild.Single(x => x.Attribute("val").Value == "Modified By").Value,
                        Message = xmlRoot.Descendants("message").Single().Value
                    };
                }
            }
            else
            {
                return new ResponseResult
                {
                    Code = int.Parse(xmlRoot.Descendants("code").Single().Value),
                    Message = xmlRoot.Descendants("message").Single().Value
                };
            }
        }
        
        private ResponseResult DeleteJsonResponse(string zohoResponse)
        {
            JObject myObj = JObject.Parse(zohoResponse);
            IList<JToken> zohoResult = myObj["response"].ToList();

            var resultContent = zohoResult[0].Parent;
            var result = resultContent.First as JProperty;
            return result.Value.ToObject<ResponseResult>();
        }

        private T ParseJsonData<T>(string resp)
        {
            Type typeParam = typeof(T);
            string type = typeParam.Name == "Lead" ? _LEADS : _SITUATIONS;
                        
            JObject o = new JObject();
            JArray jArr = new JArray();

            JObject myJson = JObject.Parse(resp);
            var jToken = myJson["response"]["result"][type]["row"]["FL"];

            for (int i = 0; i < jToken.Count(); i++)
            {
                object value = jToken[i]["content"];
                o.Add(new JProperty(jToken[i]["val"].ToString(), value));
            }

            return o.ToObject<T>();
        }
    }
}
