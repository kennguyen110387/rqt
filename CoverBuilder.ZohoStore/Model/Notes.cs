﻿using System.ComponentModel;
using Newtonsoft.Json;

namespace CoverBuilder.ZohoStore.Model
{
    using System.ComponentModel;

    [DisplayName("Notes")]
    public class Notes
    {
        [DisplayName("id")]
        public string id { get; set; }

        [JsonProperty(PropertyName = "Parent_Id")]
        [DisplayName("entityId")]
        public string LeadId { get; set; }

        [JsonProperty(PropertyName = "Note_Title")]
        [DisplayName("Note Title")]
        public string Title { get; set; }


        [JsonProperty(PropertyName = "Note_Content")]
        [DisplayName("Note Content")]
        public string Content { get; set; }

        [JsonProperty(PropertyName = "se_module")]
        [DisplayName("se module")]
        public string Module { get; set; }

    }
}
