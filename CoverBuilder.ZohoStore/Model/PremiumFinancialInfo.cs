﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace CoverBuilder.ZohoStore.Model
{
    [DisplayName("Leads")]
    public class PremiumFinancialInfo
    {
        [JsonProperty("id")]
        [DisplayName("LEADID")]
        public string LeadId { get; set; }

        [JsonProperty(PropertyName = "Premium_Base")]
        [DisplayName("Premium Base")]
        public decimal PremiumBase { get; set; }

        [JsonProperty(PropertyName = "Stamp_Tax")]
        [DisplayName("Stamp Tax")]
        public decimal StampTax { get; set; }

        [JsonProperty(PropertyName = "GST")]
        [DisplayName("GST")]
        public decimal GST { get; set; }

        [JsonProperty(PropertyName = "Underwriter_Fee")]
        [DisplayName("Underwriter Fee")]
        public decimal UnderwriterFee { get; set; }

        [JsonProperty(PropertyName = "Underwriter_Fee_GST")]
        [DisplayName("Underwriter Fee GST")]
        public decimal UnderwriterFeeGST { get; set; }

        [JsonProperty(PropertyName = "Premium_Sub_Total")]
        [DisplayName("Premium Sub Total")]
        public decimal PremiumSubTotal { get; set; }

        [JsonProperty(PropertyName = "Broker_Comm")]
        [DisplayName("Broker Comm")]
        public decimal BrokerComm { get; set; }

        [JsonProperty(PropertyName = "Broker_Comm_GST")]
        [DisplayName("Broker Comm GST")]
        public decimal BrokerCommGST { get; set; }

        [JsonProperty(PropertyName = "Final_Premium")]
        [DisplayName("Final Premium")]
        public decimal FinalPremium { get; set; }
    }
}
