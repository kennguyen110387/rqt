﻿namespace CoverBuilder.ZohoStore.Model
{
    using System;
    using System.ComponentModel;
    using Newtonsoft.Json;
    using System.Collections.Generic;

    [DisplayName("Leads")]
    public class Lead
    {
        //[DisplayName("Layout")]
        //public string Layout { get; set; }

        #region Policy Information

        [JsonProperty(PropertyName = "id")]
        [DisplayName("LEADID")]
        public string LEADID { get; set; }

        [JsonProperty(PropertyName = "Reference_Number")]
        [DisplayName("Reference Number")]
        public string ReferenceNumber { get; set; }

        [JsonProperty(PropertyName = "Insurance_Type")]
        [DisplayName("Insurance Type")]
        public string LiabilityInsuranceType { get; set; }

        [JsonProperty(PropertyName = "Insured_Name")]
        [DisplayName("Insured Name")]
        public string InsuredName { get; set; }

        [JsonProperty(PropertyName = "Limit_of_Liability")]
        [DisplayName("Limit of Liability")]
        public string LiabilityLimit { get; set; }

        [JsonProperty(PropertyName = "General_Liability")]
        [DisplayName("General Liability")]
        public string GeneralLiability { get; set; }

        [JsonProperty(PropertyName = "Product_Liability")]
        [DisplayName("Product Liability")]
        public string ProductLiability { get; set; }

        [JsonProperty(PropertyName = "Insurance_Period_From")]
        [DisplayName("Insurance Period From")]
        public string InsurancePeriodFrom { get; set; }

        [JsonProperty(PropertyName = "Insurance_Period_To")]
        [DisplayName("Insurance Period To")]
        public string InsurancePeriodTo { get; set; }

        [JsonProperty(PropertyName = "Excess")]
        [DisplayName("Excess")]
        public string Excess { get; set; }

        [JsonProperty(PropertyName = "Business_Desc")]
        [DisplayName("Business Desc")]
        public string BusinessDesc { get; set; }

        [JsonProperty(PropertyName = "Endorsements")]
        [DisplayName("Endorsements")]
        public string Endorsements { get; set; }

        [JsonProperty(PropertyName = "Referral_Triggers")]
        [DisplayName("Referral Triggers")]
        public string ReferralTriggers { get; set; }

        [JsonProperty(PropertyName = "Quote_Received")]
        [DisplayName("Quote Received")]
        public string QuoteReceivedDate { get; set; }

        [JsonProperty(PropertyName = "Quote_Sent")]
        [DisplayName("Quote Sent")]
        public string QuoteSentDate { get; set; }

        #endregion

        #region Lead Information
        [JsonProperty(PropertyName = "SMOWNERID")]
        [DisplayName("SMOWNERID")]
        public string OwnerId { get; set; }

        //[JsonProperty(PropertyName = "Owner")]
        //[DisplayName("Lead Owner")]
        //public long LeadOwner { get; set; }

        [JsonProperty(PropertyName = "Owner")]
        [DisplayName("Lead Owner")]
        public Owner Owner { get; set; }

        [JsonProperty(PropertyName = "BA_Subagent")]
        [DisplayName("BA Subagent")]
        public string SubAgent { get; set; }

        [JsonProperty(PropertyName = "Broker_Name")]
        [DisplayName("Broker Name")]
        public string BrokerageName { get; set; }

        [JsonProperty(PropertyName = "Broker_Postal_Address")]
        [DisplayName("Broker Postal Address")]
        public string BrokerPostalAddress { get; set; }

        [JsonProperty(PropertyName = "Broker_State")]
        [DisplayName("Broker State")]
        public string BrokerState { get; set; }

        [JsonProperty(PropertyName = "Last_Name")]
        [DisplayName("Last Name")]
        public string LeadName { get; set; }

        [JsonProperty(PropertyName = "Company")]
        [DisplayName("Company")]
        public string Company { get; set; }

        [JsonProperty(PropertyName = "Email")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "Phone")]
        [DisplayName("Phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "Lead_Status")]
        [DisplayName("Lead Status")]
        public string LeadStatus { get; set; }

        [JsonProperty(PropertyName = "BA_Class")]
        [DisplayName("BA Class")]
        public string BaClass { get; set; }

        [JsonProperty(PropertyName = "Executive_Code")]
        [DisplayName("Executive Code")]
        public string ExecCode { get; set; }
        #endregion

        #region Risk Information
        [JsonProperty(PropertyName = "Sit_St_Num")]
        [DisplayName("Sit St Num")]
        public string StreetNumber { get; set; }

        [JsonProperty(PropertyName = "Sit_Street")]
        [DisplayName("Sit Street")]
        public string Street { get; set; }

        [JsonProperty(PropertyName = "Sit_State")]
        [DisplayName("Sit State")]
        public string State { get; set; }

        [JsonProperty(PropertyName = "Sit_Suburb")]
        [DisplayName("Sit Suburb")]
        public string Suburb { get; set; }

        [JsonProperty(PropertyName = "Sit_Postcode")]
        [DisplayName("Sit Postcode")]
        public string PostCode { get; set; }

        [JsonProperty(PropertyName = "Comments")]
        [DisplayName("Comments")]
        public string Comments { get; set; }

        [JsonProperty(PropertyName = "Years_in_Business")]
        [DisplayName("Years in Business")]
        public int YearsInBusiness { get; set; }

        [JsonProperty(PropertyName = "Annual_Turnover")]
        [DisplayName("Annual Turnover")]
        public decimal AnnualTurnover { get; set; }

        [JsonProperty(PropertyName = "Num_of_Employees")]
        [DisplayName("Num of Employees")]
        public int NumberOfEmployees { get; set; }

        [JsonProperty(PropertyName = "Annual_Wages")]
        [DisplayName("Annual Wages")]
        public decimal AnnualWages { get; set; }

        [JsonProperty(PropertyName = "Are_Subbies_Used")]
        [DisplayName("Are Subbies Used")]
        public string HasSubContractors { get; set; }

        [JsonProperty(PropertyName = "Paid_to_Subbies")]
        [DisplayName("Paid to Subbies")]
        public decimal TotalAmountForSubContractors { get; set; }

        [JsonProperty(PropertyName = "Activities_Subbies")]
        [DisplayName("Activities Subbies")]
        public string SubContractorActivities { get; set; }

        [JsonProperty(PropertyName = "Subbies_Own_Insurance")]
        [DisplayName("Subbies Own Insurance")]
        public string SubContractorInsurance { get; set; }

        [JsonProperty(PropertyName = "Are_Labour_Used")]
        [DisplayName("Are Labour Used")]
        public string HasLabourHire { get; set; }

        [JsonProperty(PropertyName = "Paid_to_Labour")]
        [DisplayName("Paid to Labour")]
        public decimal TotalAmountForLabourHire { get; set; }

        [JsonProperty(PropertyName = "Activities_Labour")]
        [DisplayName("Activities Labour")]
        public string LabourHireActivities { get; set; }

        [JsonProperty(PropertyName = "Labour_Own_Insurance")]
        [DisplayName("Labour Own Insurance")]
        public string LabourHireInsurance { get; set; }

        [JsonProperty(PropertyName = "Contractual_or_Hold_Harmless")]
        [DisplayName("Contractual or Hold Harmless")]
        public string HasContractOrHoldHarmlessAgreement { get; set; }

        [JsonProperty(PropertyName = "Contractual_or_Hold_Harmless_Details")]
        [DisplayName("Contractual or Hold Harmless Details")]
        public string HasContractOrHoldHarmlessAgreementDetails { get; set; }

        [JsonProperty(PropertyName = "Situation_Count")]
        [DisplayName("Situation Count")]
        public int SituationCount { get; set; }
        #endregion

        #region Financials
        [JsonProperty(PropertyName = "Premium_Base")]
        [DisplayName("Premium Base")]
        public decimal PremiumBase { get; set; }

        [JsonProperty(PropertyName = "ESL")]
        [DisplayName("ESL")]
        public string ESL { get; set; }

        [JsonProperty(PropertyName = "Stamp_Tax")]
        [DisplayName("Stamp Tax")]
        public decimal StampTax { get; set; }

        [JsonProperty(PropertyName = "GST")]
        [DisplayName("GST")]
        public decimal GST { get; set; }

        [JsonProperty(PropertyName = "Underwriter_Fee")]
        [DisplayName("Underwriter Fee")]
        public decimal UnderwriterFee { get; set; }

        [JsonProperty(PropertyName = "Underwriter_Fee_GST")]
        [DisplayName("Underwriter Fee GST")]
        public decimal UnderwriterFeeGST { get; set; }

        [JsonProperty(PropertyName = "Premium_Sub_Total")]
        [DisplayName("Premium Sub Total")]
        public decimal PremiumSubTotal { get; set; }

        [JsonProperty(PropertyName = "Broker_Comm")]
        [DisplayName("Broker Comm")]
        public decimal BrokerComm { get; set; }

        [JsonProperty(PropertyName = "Broker_Comm_GST")]
        [DisplayName("Broker Comm GST")]
        public decimal BrokerCommGST { get; set; }

        [JsonProperty(PropertyName = "Final_Premium")]
        [DisplayName("Final Premium")]
        public decimal FinalPremium { get; set; }

        [JsonProperty(PropertyName = "Rating_Rationale")]
        [DisplayName("Rating Rationale")]
        public string RatingRationale { get; set; }
        #endregion

        #region Accommodation
        [JsonProperty(PropertyName = "Type_of_Accom")]
        [DisplayName("Type of Accom")]
        public string TypeOfAccom { get; set; }

        [JsonProperty(PropertyName = "Number_of_Rooms")]
        [DisplayName("Number of Rooms")] 
        public int NumberOfRooms { get; set; }

        [JsonProperty(PropertyName = "Accom_Category")]
        [DisplayName("Accom Category")]
        public string AccomCategory { get; set; }

        [JsonProperty(PropertyName = "Prop_Owners_Liab")]
        [DisplayName("Prop Owners Liab")]
        public string PropOwnersLiab { get; set; }

        [JsonProperty(PropertyName = "Licensed_Bar")]
        [DisplayName("Licensed Bar")]
        public string LicensedBar { get; set; }

        [JsonProperty(PropertyName = "Licensed_Restaurant")]
        [DisplayName("Licensed Restaurant")]
        public string LicensedRestuarant { get; set; }

        [JsonProperty(PropertyName = "Hardwired_Smoke")]
        [DisplayName("Hardwired Smoke")]
        public string HardwiredSmoke { get; set; }

        [JsonProperty(PropertyName = "Sporting_Facilities")]
        [DisplayName("Sporting Facilities")]
        public string SportingFacility { get; set; }

        [JsonProperty(PropertyName = "Children_s_Playground")]
        [DisplayName("Childrens Playground")]
        public string Playground { get; set; }

        [JsonProperty(PropertyName = "Disabilities")]
        [DisplayName("Disabilities")]
        public string Disabilities { get; set; }
        #endregion

        #region Budget Accommodation (BA & MB)
        //[DisplayName("Number of Rooms")]
        //public int NumberOfRooms { get; set; }

        //[DisplayName("Hardwired Smoke")]
        //public string HardwiredSmoke { get; set; }

        //[DisplayName("Prop Owners Liab")]
        //public string PropOwnersLiab { get; set; }

        //[DisplayName("Disabilities")]
        //public string Disabilities { get; set; }

        //[DisplayName("Sporting Facilities")]
        //public string SportingFacility { get; set; }

        [JsonProperty(PropertyName = "BA_Type_of_Accom")]
        [DisplayName("BA Type of Accom")]
        public string BATypeOfAccom { get; set; }

        [JsonProperty(PropertyName = "BA_Accom_Use")]
        [DisplayName("BA Accom Use")]
        public string BAAccomUse { get; set; }

        //[DisplayName("Licensed Bar")]
        //public string LicensedBar { get; set; }

        [JsonProperty(PropertyName = "MB_Type_of_Accom")]
        [DisplayName("MB Type of Accom")]
        public string MBTypeOfAccom { get; set; }

        [JsonProperty(PropertyName = "MB_Accom_Use")]
        [DisplayName("MB Accom Use")]
        public string MBAccomUse { get; set; }

        //[DisplayName("Licensed Restaurant")]
        //public string LicensedRestuarant { get; set; }

        //[DisplayName("Children's Playground")]
        //public string Playground { get; set; }
        #endregion

        #region Cleaners (CA)
        [JsonProperty(PropertyName = "CA_Group_1")]
        [DisplayName("CA Group 1")]
        public string CAGroup1 { get; set; }

        [JsonProperty(PropertyName = "CA_Group_2")]
        [DisplayName("CA Group 2")]
        public string CAGroup2 { get; set; }

        [JsonProperty(PropertyName = "CA_Group_3")]
        [DisplayName("CA Group 3")]
        public string CAGroup3 { get; set; }

        [JsonProperty(PropertyName = "CA_Group_4")]
        [DisplayName("CA Group 4")]
        public string CAGroup4 { get; set; }

        [JsonProperty(PropertyName = "Loss_of_Keys_Ext")]
        [DisplayName("Loss of Keys Ext")]
        public string LossKeys { get; set; }

        [JsonProperty(PropertyName = "CA_Services")]
        [DisplayName("CA Services")]
        public string CAServices { get; set; }
        #endregion

        #region Restaurants (RS)
        [JsonProperty(PropertyName = "Turnover_Bar")]
        [DisplayName("Turnover Bar")]
        public decimal TurnoverBar { get; set; }

        [JsonProperty(PropertyName = "Turnover_Food")]
        [DisplayName("Turnover Food")]
        public decimal TurnoverFood { get; set; }

        [JsonProperty(PropertyName = "Takeaway_Perc")]
        [DisplayName("Takeaway Perc")]
        public string TakeawaySales { get; set; }

        [JsonProperty(PropertyName = "Seating_Capacity")]
        [DisplayName("Seating Capacity")]
        public int SeatingCapacity { get; set; }

        [JsonProperty(PropertyName = "Trading_Hours")]
        [DisplayName("Trading Hours")]
        public string TradingHours { get; set; }
        #endregion

        #region Transport & Logistics (TL)
        [JsonProperty(PropertyName = "TL_Activities_And_Goods_Carried")]
        [DisplayName("TL Activities And Goods Carried")]
        public string TLActivitiesAndGoods { get; set; }

        [JsonProperty(PropertyName = "TL_Services")]
        [DisplayName("TL Services")]
        public string TLServices { get; set; }
        #endregion

        #region Project Managers (PM)
        [JsonProperty(PropertyName = "PM_Activities")]
        [DisplayName("PM Activities")]
        public string PMActivities { get; set; }

        [JsonProperty(PropertyName = "PM_Services")]
        [DisplayName("PM Services")]
        public string PMServices { get; set; }
        #endregion

        #region Property Owners (PO) & Vacant Land
        [DisplayName("Sit Building Value")]
        public decimal BuildingValue1 { get; set; }

        [DisplayName("Sit Tenants Occupation")]
        public string TenantsOccupation1 { get; set; }

        [DisplayName("Sit Unoccupied")]
        public string SitUnoccupied1 { get; set; }

        [DisplayName("Sit Rental Income")]
        public decimal RentalIncome1 { get; set; }

        [DisplayName("Sit PO Services")]
        public string POServices1 { get; set; }

        [DisplayName("Sit Land Size (sqm)")]
        public int LandSize1 { get; set; }

        [DisplayName("Sit Hobby Farm")]
        public string HobbyFarm1 { get; set; }

        [DisplayName("Sit Agistment")]
        public string Agistment1 { get; set; }

        [DisplayName("Sit CarPark/Markets")]
        public string CarParkOrMarket1 { get; set; }

        [DisplayName("Sit Structures Buildings")]
        public string StructuresBuildings1 { get; set; }
        #endregion

        #region Scaffolders (SF)
        [JsonProperty(PropertyName = "SF_Activities")]
        [DisplayName("SF Activities")]
        public string SFActivities { get; set; }

        [JsonProperty(PropertyName = "SF_Services")]
        [DisplayName("SF Services")]
        public string SFServices { get; set; }

        [JsonProperty(PropertyName = "SF_Services_Turnover")]
        [DisplayName("SF Services Turnover")]
        public string SFServicesTurnover { get; set; }

        [JsonProperty(PropertyName = "Perc_Work_Below_10m")]
        [DisplayName("Perc Work Below 10m")]
        public string WorkUnderTenMetres { get; set; }

        [JsonProperty(PropertyName = "Perc_Work_Above_10m")]
        [DisplayName("Perc Work Above 10m")]
        public string WorkOverTenMetres { get; set; }

        [JsonProperty(PropertyName = "Provides_Labour_Hire_Services")]
        [DisplayName("Provides Labour Hire Services")]
        public string ProvideLabourHireServices { get; set; }

        [JsonProperty(PropertyName = "Turnover_from_Labour_Hire_Services")]
        [DisplayName("Turnover from Labour Hire Services")]
        public string TurnoverForLabourHireServices { get; set; }

        [JsonProperty(PropertyName = "Activities_Labour_Hire_Services")]
        [DisplayName("Activities Labour Hire Services")]
        public string LabourHireServicesActivities { get; set; }

        [JsonProperty(PropertyName = "Perc_of_Residential_Work")]
        [DisplayName("Perc of Residential Work")]
        public string PercResidentialWork { get; set; }

        [JsonProperty(PropertyName = "Perc_of_Commercial_Work")]
        [DisplayName("Perc of Commercial Work")]
        public string PercCommercialWork { get; set; }

        [JsonProperty(PropertyName = "SF_Has_Documents_and_Procedures_for_Inspections")]
        [DisplayName("SF Has Documents and Procedures for Inspections")]
        public string SFDocumentsAndProcedures { get; set; }

        [JsonProperty(PropertyName = "SF_Manufactures_Scaffolding")] //<-- This one
        [DisplayName("SF Manufactures Scaffolding")]
        public string SFManufactureScaffolding { get; set; }

        [JsonProperty(PropertyName = "SF_Manufactures_Scaffolding_Details")] //<-- This one
        [DisplayName("SF Manufactures Scaffolding Details")]
        public string SFManufactureScaffoldingDetails { get; set; }

        //[JsonProperty(PropertyName = "SF_Sells_Second_Hand_Equipment")]
        //[DisplayName("SF Sells Second Hand Equipment")]
        //public string SFSellSecondHandEquipment { get; set; }

        //[JsonProperty(PropertyName = "SF_Sells_Second_Hand_Equipment_Details")]
        //[DisplayName("SF Sells Second Hand Equipment Details")]
        //public string SFSellSecondHandEquipmentDetails { get; set; }

        [JsonProperty(PropertyName = "SF_Hires_Scaffolding")]
        [DisplayName("SF Hires Scaffolding")]
        public string SFHireScaffolding { get; set; }

        [JsonProperty(PropertyName = "SF_Hires_Scaffolding_Details")]
        [DisplayName("SF Hires Scaffolding Details")]
        public string SFHireScaffoldingDetails { get; set; }

        [JsonProperty(PropertyName = "SF_Tested_and_Compliant_with_Standards")] //<-- This one
        [DisplayName("SF Tested and Compliant with Standards")]
        public string SFTestedAndCompliant { get; set; }

        //[JsonProperty(PropertyName = "SF_Tested_and_Compliant_with_Standards_Details")]
        //[DisplayName("SF Tested and Compliant with Standards Details")]
        //public string SFTestedAndCompliantDetails { get; set; }

        #endregion

        #region Welders (WB)
        [JsonProperty(PropertyName = "WB_Activities")]
        [DisplayName("WB Activities")]
        public string WBActivities { get; set; }
        #endregion

        #region Shopping Centres (SC)
        [JsonProperty(PropertyName = "Num_Anchor_Tenants")]
        [DisplayName("Num Anchor Tenants")]
        public string AnchorTenants { get; set; }

        [JsonProperty(PropertyName = "Num_Retail_Tenants")]
        [DisplayName("Num Retail Tenants")]
        public string RetailTenants { get; set; }
        #endregion

        #region Events and Markets (EM)

        [JsonProperty(PropertyName = "Event_Type")]
        [DisplayName("Event Type")]
        public string EventType { get; set; }

        //Event Specific Fields
        [JsonProperty(PropertyName = "Event_Activities")]
        [DisplayName("Event Activities")]
        public string EVActivities { get; set; }

        [JsonProperty(PropertyName = "Event_Times")]
        [DisplayName("Event Times")]
        public string EventTimes { get; set; }

        [JsonProperty(PropertyName = "Event_Ticket_Price")]
        [DisplayName("Event Ticket Price")]
        public int EventTicketPrice { get; set; }

        //Market Specific Fields
        [JsonProperty(PropertyName = "Market_Frequency")]
        [DisplayName("Market Frequency")]
        public string MarketFrequency { get; set; }

        [JsonProperty(PropertyName = "Market_Operating_Times")]
        [DisplayName("Market Operating Times")]
        public string MarketOperatingTime { get; set; }

        //Common Fields
        [JsonProperty(PropertyName = "Cover_Required")]
        [DisplayName("Cover Required")]
        public string CoverRequired { get; set; }

        [JsonProperty(PropertyName = "Number_of_Attendees")]
        [DisplayName("Number of Attendees")]
        public int Attendees { get; set; }

        [JsonProperty(PropertyName = "Indoor_or_Outdoor")]
        [DisplayName("Indoor or Outdoor")]
        public string IndoorOutdoor { get; set; }

        [JsonProperty(PropertyName = "Number_of_Food_Stalls")]
        [DisplayName("Number of Food Stalls")]
        public int NumberOfFoodStalls { get; set; }

        [JsonProperty(PropertyName = "Number_of_Market_Stalls")]
        [DisplayName("Number of Market Stalls")]
        public int NumberOfMarketStalls { get; set; }

        [JsonProperty(PropertyName = "Stall_Holder_owns_Insurance")]
        [DisplayName("Stall Holder owns Insurance")]
        public string StallHolderInsurance { get; set; }

        [JsonProperty(PropertyName = "Event_Has")]
        [DisplayName("Event Has")]
        public string EventHaveAny { get; set; }

        //Alcohol Details
        [JsonProperty(PropertyName = "Alcohol_Served")]
        [DisplayName("Alcohol Served")]
        public string HasAlcoholServed { get; set; }

        [JsonProperty(PropertyName = "Alcohol_Seller")]
        [DisplayName("Alcohol Seller")]
        public string AlcoholSeller { get; set; }

        [JsonProperty(PropertyName = "Alcohol_Insurance")]
        [DisplayName("Alcohol Insurance")]
        public string AlcoholInsurance { get; set; }

        [JsonProperty(PropertyName = "Alcohol_License")]
        [DisplayName("Alcohol License")]
        public string AlcoholLicense { get; set; }

        #endregion

        #region Market Stall Holders (SH)

        [JsonProperty(PropertyName = "SH_Activities_and_Products")]
        [DisplayName("SH Activities and Products")]
        public string SHActivitiesAndProducts { get; set; }

        #endregion

        #region Hotel (HL)

        [JsonProperty(PropertyName = "HL_Activities")]
        [DisplayName("HL Capacity")]
        public string HLCapacity { get; set; }

        [JsonProperty(PropertyName = "HL_Scope")]
        [DisplayName("HL Scope")]
        public string HLScope { get; set; }

        [JsonProperty(PropertyName = "Turnover_Accommodation1")]
        [DisplayName("Turnover Accommodation")]
        public decimal TurnoverAccommodation { get; set; }

        [JsonProperty(PropertyName = "Turnover_Bottleshop")]
        [DisplayName("Turnover Bottleshop")]
        public decimal TurnoverBottleShop { get; set; }

        [JsonProperty(PropertyName = "Turnover_Gaming")]
        [DisplayName("Turnover Gaming")]
        public decimal TurnoverGaming { get; set; }

        [JsonProperty(PropertyName = "Turnover_Other")]
        [DisplayName("Turnover Other")]
        public int TurnoverOther { get; set; }

        [JsonProperty(PropertyName = "HL_Live_Entertainment")]
        [DisplayName("HL Live Entertainment")]
        public string LiveEntertainment { get; set; }

        [JsonProperty(PropertyName = "Has_Dance_Floor")]
        [DisplayName("Has Dance Floor")]
        public string HasDanceFloor { get; set; }

        [JsonProperty(PropertyName = "Dance_Floor_Size_sqm")]
        [DisplayName("Dance Floor Size (sqm)")]
        public string DanceFloorSize { get; set; }

        [JsonProperty(PropertyName = "Dance_Frequency")]
        [DisplayName("Dance Frequency")]
        public string DanceFrequency { get; set; }

        [JsonProperty(PropertyName = "Are_Security_Subbies_Used")]
        [DisplayName("Are Security Subbies Used")]
        public string HasSubContractorsCC { get; set; }

        [JsonProperty(PropertyName = "Paid_to_Security_Subbies")]
        [DisplayName("Paid to Security Subbies")]
        public string TotalAmountForSubContractorsCC { get; set; }

        [JsonProperty(PropertyName = "Security_Subbies_Own_Insurance")]
        [DisplayName("Security Subbies Own Insurance")]
        public string SubContractorInsuranceCC { get; set; }

        [JsonProperty(PropertyName = "Employ_Security_Services")]
        [DisplayName("Employ Security Services")]
        public string EmployCC { get; set; }

        [JsonProperty(PropertyName = "CCTV_Details")]
        [DisplayName("CCTV Details")]
        public string CCTVDetails { get; set; }

        [JsonProperty(PropertyName = "Number_of_Accommodation_Rooms")]
        [DisplayName("Number of Accommodation Rooms")]
        public string NumberOfAccommodationRooms { get; set; }

        [JsonProperty(PropertyName = "Website")]
        [DisplayName("Website")]
        public string Website { get; set; }

        #endregion

        #region Trades (TR)

        [JsonProperty(PropertyName = "Trade_Niche")]
        [DisplayName("TR Niche")]
        public string TRNiche { get; set; }

        [JsonProperty(PropertyName = "TR_Work_Performed")]
        [DisplayName("TR Work Performed")]
        public string TRWorkPerformed { get; set; }

        [JsonProperty(PropertyName = "TR_Work_Performed_Details_and_Turnover")]
        [DisplayName("TR Work Performed Details and Turnover")]
        public string TRWorkPerformedTurnover { get; set; }

        [JsonProperty(PropertyName = "TR_Hot_Works")]
        [DisplayName("TR Hot Works")]
        public string TRHotWorks { get; set; }

        #endregion

        #region General Liability (GL)

        [JsonProperty(PropertyName = "GL_Activities")]
        [DisplayName("GL Activities")]
        public string GLActivities { get; set; }

        [JsonProperty(PropertyName = "GL_Work_Performed")]
        [DisplayName("GL Work Performed")]
        public string GLWorkPerformed { get; set; }

        [JsonProperty(PropertyName = "GL_Work_Performed_Details_and_Turnover")]
        [DisplayName("GL Work Performed Details and Turnover")]
        public string GLWorkPerformedTurnover { get; set; }

        #endregion

        #region Handmade (HM)

        [JsonProperty(PropertyName = "HM_Products")]
        [DisplayName("HM Products")]
        public string HMProducts { get; set; }

        [JsonProperty(PropertyName = "HM_Products_Tested")]
        [DisplayName("HM Products Tested")]
        public string HMProductsTested { get; set; }

        [JsonProperty(PropertyName = "HM_Has_Bisphenol")]
        [DisplayName("HM Has Bisphenol")]
        public string HMHasBisphenol { get; set; }

        #endregion

        #region Import/Export

        [JsonProperty(PropertyName = "Is_Importing_Products")]
        [DisplayName("Is Importing Products")]
        public string ImportProduct { get; set; }

        [JsonProperty(PropertyName = "Import_Product_List")]
        [DisplayName("Import Product List")]
        public string ImportProductList { get; set; }

        [JsonProperty(PropertyName = "Export_Country")]
        [DisplayName("Export Country")]
        public string ExportCountry { get; set; }

        [JsonProperty(PropertyName = "Export_Turnover")]
        [DisplayName("Export Turnover")]
        public string ExportTurnover { get; set; }

        [JsonProperty(PropertyName = "Import_QC_Procedure")]
        [DisplayName("Import QC Procedure")]
        public string ImportQCProcedure { get; set; }

        [JsonProperty(PropertyName = "Is_Exporting_Products")]
        [DisplayName("Is Exporting Products")]
        public string ExportProduct { get; set; }

        [JsonProperty(PropertyName = "Export_Product_List")]
        [DisplayName("Export Product List")]
        public string ExportProductList { get; set; }

        [JsonProperty(PropertyName = "Import_Country")]
        [DisplayName("Import Country")]
        public string ImportCountry { get; set; }

        [JsonProperty(PropertyName = "Import_Turnover")]
        [DisplayName("Import Turnover")]
        public string ImportTurnover { get; set; }

        [JsonProperty(PropertyName = "Export_QC_Procedure")]
        [DisplayName("Export QC Procedure")]
        public string ExportQCProcedure { get; set; }

        #endregion

        #region Disclosure Information
        [JsonProperty(PropertyName = "Claims_5_Years")]
        [DisplayName("Claims 5 Years")]
        public string DiscInfoClaimDetail { get; set; }

        [JsonProperty(PropertyName = "Insurance_Declined")]
        [DisplayName("Insurance Declined")]
        public string DiscInfoDeclineDetail { get; set; }

        [JsonProperty(PropertyName = "Insurance_Refused")]
        [DisplayName("Insurance Refused")]
        public string DiscInfoRenewalDetail { get; set; }

        [JsonProperty(PropertyName = "Excess_Imposed")]
        [DisplayName("Excess Imposed")]
        public string DiscInfoExcessDetail { get; set; }

        [JsonProperty(PropertyName = "Claim_Rejected")]
        [DisplayName("Claim Rejected")]
        public string DiscInfoClaimRejectedDetail { get; set; }

        [JsonProperty(PropertyName = "Bankruptcy")]
        [DisplayName("Bankruptcy")]
        public string DiscInfoBankruptDetail { get; set; }

        [JsonProperty(PropertyName = "Criminal_Charge")]
        [DisplayName("Criminal Charge")]
        public string DiscInfoConvictedDetail { get; set; }
        #endregion

    }

    public class Leads
    {
        public List<Lead> data { get; set; }
    }
}
