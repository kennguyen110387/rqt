﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace CoverBuilder.ZohoStore.Model
{
    [DisplayName("Leads")]
    public class PremiumQuotationInfo
    {
        [JsonProperty("LEADID")]
        [DisplayName("LEADID")]
        public string LeadId { get; set; }

        [JsonProperty(PropertyName = "Premium Base")]
        [DisplayName("Premium Base")]
        public string PremiumBase { get; set; }

        [JsonProperty(PropertyName = "Stamp Tax")]
        [DisplayName("Stamp Tax")]
        public string StampTax { get; set; }

        [JsonProperty(PropertyName = "GST")]
        [DisplayName("GST")]
        public string GST { get; set; }

        [JsonProperty(PropertyName = "Underwriter Fee")]
        [DisplayName("Underwriter Fee")]
        public string UnderwriterFee { get; set; }

        [JsonProperty(PropertyName = "Underwriter Fee GST")]
        [DisplayName("Underwriter Fee GST")]
        public string UnderwriterFeeGST { get; set; }

        [JsonProperty(PropertyName = "Premium Sub Total")]
        [DisplayName("Premium Sub Total")]
        public string PremiumSubTotal { get; set; }

        [JsonProperty(PropertyName = "Broker Comm")]
        [DisplayName("Broker Comm")]
        public string BrokerComm { get; set; }

        [JsonProperty(PropertyName = "Broker Comm GST")]
        [DisplayName("Broker Comm GST")]
        public string BrokerCommGST { get; set; }

        [JsonProperty(PropertyName = "Final Premium")]
        [DisplayName("Final Premium")]
        public string FinalPremium { get; set; }
    }

    public class PremiumQuotationInfos
    {
        public List<PremiumQuotationInfo> data { get; set; }
    }
}
