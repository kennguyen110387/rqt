﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.ZohoStore.Model
{
    [DisplayName("CustomModule4")]
    public class LiabilityQuotationHeader
    {
        //Lead Information
        [DisplayName("Brokerage Name")]
        public string BrokerageName { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Phone")]
        public string Phone { get; set; }

        [DisplayName("Insurance Period From")]
        public string InsurancePeriodFrom { get; set; }

        [DisplayName("Insurance Period To")]
        public string InsurancePeriodTo { get; set; }

        [DisplayName("Insured Name")]
        public string InsuredName { get; set; }

        [DisplayName("Liability Limit")]
        public string LiabilityLimit { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Reference No")]
        public string ReferenceNo { get; set; }

        [DisplayName("Insurance Type")]
        public string InsuranceType { get; set; }

        [DisplayName("Active")]
        public string Active { get; set; }

        [DisplayName("Send PDF")]
        public string SendPDF { get; set; }

        [DisplayName("Lead Status")]
        public string LeadStatus { get; set; }

        [DisplayName("Address")]
        public string Address { get; set; }

        [DisplayName("Suburb")]
        public string Suburb { get; set; }

        [DisplayName("State")]
        public string State { get; set; }

        [DisplayName("Post Code")]
        public string PostCode { get; set; }

        [DisplayName("Disclosure 1")]
        public string Disclosure1 { get; set; }

        [DisplayName("Disclosure 1 Details")]
        public string Disclosure1Details { get; set; }

        [DisplayName("Disclosure 2")]
        public string Disclosure2 { get; set; }

        [DisplayName("Disclosure 2 Details")]
        public string Disclosure2Details { get; set; }

        [DisplayName("Disclosure 3")]
        public string Disclosure3 { get; set; }

        [DisplayName("Disclosure 3 Details")]
        public string Disclosure3Details { get; set; }

        [DisplayName("Disclosure 4")]
        public string Disclosure4 { get; set; }

        [DisplayName("Disclosure 4 Details")]
        public string Disclosure4Details { get; set; }

        [DisplayName("Disclosure 5")]
        public string Disclosure5 { get; set; }

        [DisplayName("Disclosure 5 Details")]
        public string Disclosure5Details { get; set; }

        [DisplayName("Disclosure 6")]
        public string Disclosure6 { get; set; }

        [DisplayName("Disclosure 6 Details")]
        public string Disclosure6Details { get; set; }

        [DisplayName("Disclosure 7")]
        public string Disclosure7 { get; set; }

        [DisplayName("Disclosure 7 Details")]
        public string Disclosure7Details { get; set; }

        [DisplayName("Final Premium")]
        public decimal FinalPremium { get; set; }

        [DisplayName("GST")]
        public decimal GST { get; set; }

        [DisplayName("Premium Base")]
        public decimal PremiumBase { get; set; }

        [DisplayName("Stamp Tax")]
        public decimal StampTax { get; set; }

        [DisplayName("Underwriter Fee")]
        public decimal UnderwriterFee { get; set; }

        [DisplayName("Underwriter Fee GST")]
        public decimal UnderwriterFeeGST { get; set; }

        [DisplayName("Broker Comm")]
        public decimal BrokerComm { get; set; }

        [DisplayName("Broker Comm GST")]
        public decimal BrokerCommGST { get; set; }

        [DisplayName("Premium Sub Total")]
        public decimal PremiumSubTotal { get; set; }
    }
}
