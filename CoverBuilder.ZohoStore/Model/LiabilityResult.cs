﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoverBuilder.ZohoStore.Model
{
    [DisplayName("CustomModule8")]
    public class LiabilityResult
    {
        [DisplayName("Quotation Header")]
        public string QuotationHeader { get; set; }
        [DisplayName("Reference No")]
        public string ReferenceNo { get; set; }

        [DisplayName("Question")]
        public string Question { get; set; }

        [DisplayName("User Response")]
        public string Response { get; set; }

        [DisplayName("Trigger to Underwriter")]
        public bool TriggerToUnderWriter { get; set; }

        [DisplayName("Group Id")]
        public int? GroupId { get; set; }
    }
}
