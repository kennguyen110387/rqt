﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoverBuilder.ZohoStore.Model
{
    public class ResponseResult
    {

        public int? Code { get; set; }

        //[JsonProperty("content")]
        [JsonProperty("Id")]
        public string ZohoId { get; set; }
        public string CreatedTime { get; set; }
        public string ModifiedTime { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Message { get; set; }
    }

    public class ResponseJsonResult
    {
        public List<Data> data { get; set; }
    }
    public class Data
    {

        public string code { get; set; }
        public Details details { get; set; }
        public string message { get; set; }
        public string status { get; set; }
        public string api_name { get; set; }
    }
    public class Details
    {
        public string Modified_Time { get; set; }
        public Owner Modified_By { get; set; }
        public string Created_Time { get; set; }
        public Owner Created_By { get; set; }
        public string id { get; set; }
        public string expected_data_type { get; set; }
        public string api_name { get; set; }
    }
    public class Owner
    {
        public string name { get; set; }
        public long id { get; set; }
    }
}
