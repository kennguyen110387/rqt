﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.ZohoStore.Model
{
    public class LeadResult
    {
        public LeadResult()
        {

        }

        public LeadResult(int tQuestionId, string tResponse, string tLeadFieldName, int? tGroupId = null, int? tActivityGroup = null)
        {
            this.QuestionId = tQuestionId;
            this.Response = tResponse;
            this.LeadFieldName = tLeadFieldName;
            this.GroupId = tGroupId;
            this.ActivityGroup = tActivityGroup;
        }

        public int QuestionId { get; set; }
        public string Response { get; set; }
        public string LeadFieldName { get; set; }
        public int? GroupId { get; set; }
        public int? ActivityGroup { get; set; }
    }
}
