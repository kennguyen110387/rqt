﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZohoStore.Model
{
    [DisplayName("CustomModule2")]
    public class PremiumHeader
    {
        [DisplayName("Id")]
        public string CustomModule2Name { get; set; }
        [DisplayName("LiabilityQuotation Owner")]
        public string CustomModule2Owner { get; set; }
        [DisplayName("Created By")]
        public string CreatedBy { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Modified By")]
        public string ModifiedBy { get; set; }
        [DisplayName("Brokerage Name")]
        public string BrokerageName { get; set; }
        [DisplayName("Active")]
        public string Active { get; set; }
        [DisplayName("Last Activity Time")]
        public string LastActivityTime { get; set; }
        [DisplayName("Insured Name")]
        public string InsuredName { get; set; }
        [DisplayName("Send Pdf")]
        public string SendPdf { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Secondary Email")]
        public string SecondaryEmail { get; set; }
        [DisplayName("Liability Limit")]
        public string LiabilityLimit { get; set; }
        [DisplayName("Street No")]
        public string StreetNo { get; set; }
        [DisplayName("Disclosure 1")]
        public string Disclosure1 { get; set; }
        [DisplayName("Street Name")]
        public string StreetName { get; set; }
        [DisplayName("Disclosure 1 Details")]
        public string Disclosure1Details { get; set; }
        [DisplayName("Suburb")]
        public string Suburb { get; set; }
        [DisplayName("Disclosure 2")]
        public string Disclosure2 { get; set; }
        [DisplayName("State")]
        public string State { get; set; }
        [DisplayName("Disclosure 2 Details")]
        public string Disclosure2Details { get; set; }
        [DisplayName("Post Code")]
        public string PostCode { get; set; }
        [DisplayName("Disclosure 3")]
        public string Disclosure3 { get; set; }
        [DisplayName("Disclosure 4")]
        public string Disclosure4 { get; set; }
        [DisplayName("Disclosure 5")]
        public string Disclosure5 { get; set; }
        [DisplayName("Disclosure 6")]
        public string Disclosure6 { get; set; }
        [DisplayName("Disclosure 7")]
        public string Disclosure7 { get; set; }
        [DisplayName("Premium Base")]
        public string PremiumBase { get; set; }
        [DisplayName("GST")]
        public string GST { get; set; }
        [DisplayName("Stamp Tax")]
        public string StampTax { get; set; }
        [DisplayName("Underwriter Fee")]
        public string UnderwriterFee { get; set; }
        [DisplayName("Final Premium")]
        public string FinalPremium { get; set; }
        

    }
}
