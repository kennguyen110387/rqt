﻿namespace CoverBuilder.ZohoStore.Model
{
    using System;

    public class Potentials
    {
        public string POTENTIALID { get; set; }                     
        public string SMOWNERID { get; set; }                       
        public string PotentialOwner { get; set; }                  
        public decimal Amount { get; set; }                         
        public string PotentialName { get; set; }                   
        public string ClosingDate { get; set; }                     
        public string ACCOUNTID { get; set; }                       
        public string AccountName { get; set; }                     
        public string Stage { get; set; }                           
        public string Type { get; set; }                            
        public int Probability { get; set; }                        
        public string SMCREATORID { get; set; }
        public string CONTACTID { get; set; }                       
        public string ContactName { get; set; }                     
        public decimal ExpectedRevenue { get; set; }                
        public string SalesCycleDuration { get; set; }
        public string OverAllSalesDuration { get; set; }            
        public string Description { get; set; }                                                 
    }                                                               
}