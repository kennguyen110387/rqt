﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.ZohoStore.Model
{
    [DisplayName("CustomModule5")]
    public class LiabilityQuotationDetail
    {

        //ID Details
        //[DisplayName("Id")]
        //public string Id { get; set; }

        [DisplayName("Reference No")]
        public string ReferenceNo { get; set; }

        //[DisplayName("Header Id")]
        //public string HeaderId { get; set; }

        //Premium Details
        [DisplayName("Base Premium")]
        public Decimal BasePremium { get; set; }

        [DisplayName("GST")]
        public Decimal GST { get; set; }

        [DisplayName("Stamp Tax")]
        public Decimal StampTax { get; set; }

        [DisplayName("Excess")]
        public Decimal Excess { get; set; }

        //Address
        [DisplayName("Situation Id")]
        public int SituationId { get; set; }

        [DisplayName("State")]
        public string State { get; set; }

        [DisplayName("Street Name")]
        public string StreetName { get; set; }

        [DisplayName("Street No")]
        public string StreetNo { get; set; }

        [DisplayName("Suburb")]
        public string Suburb { get; set; }

        [DisplayName("Postal Code")]
        public string PostalCode { get; set; }

        //Record Info
        [DisplayName("Liability Quote")]
        public string LiabilityQuote { get; set; }

    }
}
