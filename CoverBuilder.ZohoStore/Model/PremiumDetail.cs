﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.ZohoStore.Model
{
    [DisplayName("CustomModule5")]
    public class PremiumDetail
    {
        [DisplayName("Reference No")]
        public string ReferenceNo { get; set; }
        [DisplayName("Liability Quote")]
        public string LiabilityQuote { get; set; }
        [DisplayName("Base Premium")]
        public string BasePremium { get; set; }
        [DisplayName("LiabilityQuotationDetail Owner")]
        public string LIabilityQuotationDetailOwner { get; set; }
        [DisplayName("Stamp Tax")]
        public string StampTax { get; set; }
        [DisplayName("Created By")]
        public string CreatedBy { get; set; }
        [DisplayName("GST")]
        public string GST { get; set; }
        [DisplayName("Modified By")]
        public string ModifiedBy { get; set; }
        [DisplayName("Last Activity Time")]
        public string LastActivityTime { get; set; }
        [DisplayName("Excess")]
        public string Excess { get; set; }
        [DisplayName("Email Opt Out")]
        public string EmailOptOut { get; set; }
        [DisplayName("Street No")]
        public string StreetNo { get; set; }
        [DisplayName("Street Name")]
        public string StreetName { get; set; }
        [DisplayName("Suburb")]
        public string Suburb { get; set; }
        [DisplayName("State")]
        public string State { get; set; }
        [DisplayName("Postal Code")]
        public string PostalCode { get; set; }
    }
}
