﻿using System.ComponentModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace CoverBuilder.ZohoStore.Model
{
    [DisplayName("Situation Details")]
    public class Situation
    {
        [JsonProperty(PropertyName = "Owner")]
        [DisplayName("Situation Detail Owner")]
        public SituationOwner Owner { get; set; }

        [JsonProperty(PropertyName = "Name")]
        [DisplayName("Reference Number")]
        public string ReferenceNumber { get; set; }

        [JsonProperty(PropertyName = "Email")]
        [DisplayName("Email")]
        public string BrokerEmail { get; set; }

        [JsonProperty(PropertyName = "Situation_Count")]
        [DisplayName("Situation Count")]
        public string SituationCount { get; set; }

        [JsonProperty(PropertyName = "Lead_Reference_ID")]
        [DisplayName("Lead Reference_ID")]
        public string LeadReference_ID { get; set; }

        [JsonProperty(PropertyName = "Lead_Reference")]
        [DisplayName("Lead Reference")]
        public LeadReference LeadReference { get; set; }

        #region Situation 1 Details
        [JsonProperty(PropertyName = "Sit_St_Num")]
        [DisplayName("Sit1 St Num")]
        public string StreetNumber1 { get; set; }

        [JsonProperty(PropertyName = "Sit_Street")]
        [DisplayName("Sit1 Street")]
        public string Street1 { get; set; }

        [JsonProperty(PropertyName = "Sit_State")]
        [DisplayName("Sit1 State")]
        public string State1 { get; set; }

        [JsonProperty(PropertyName = "Sit_Suburb")]
        [DisplayName("Sit1 Suburb")]
        public string Suburb1 { get; set; }

        [JsonProperty(PropertyName = "Sit_Postcode")]
        [DisplayName("Sit1 Postcode")]
        public string PostCode1 { get; set; }

        [JsonProperty(PropertyName = "Sit_Building_Value")]
        [DisplayName("Sit1 Building Value")]
        public decimal BuildingValue1 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_6")]
        [DisplayName("Sit1 Tenants Occupation")]
        public string TenantsOccupation1 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_7")]
        [DisplayName("Sit1 Unoccupied")]
        public string Unoccupied1 { get; set; }

        [JsonProperty(PropertyName = "Sit_Rental_Income")]
        [DisplayName("Sit1 Rental Income")]
        public decimal RentalIncome1 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_8")]
        [DisplayName("Sit1 PO Services")]
        public string POServices1 { get; set; }

        [JsonProperty(PropertyName = "Sit_Land_Size")]
        [DisplayName("Sit1 Land Size (sqm)")]
        public int LandSize1 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_9")]
        [DisplayName("Sit1 Hobby Farm")]
        public string HobbyFarm1 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_10")]
        [DisplayName("Sit1 Agistment")]
        public string Agistment1 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_11")]
        [DisplayName("Sit1 CarPark/Markets")]
        public string CarParkOrMarket1 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_12")]
        [DisplayName("Sit1 Structures Buildings")]
        public string StructuresBuildings1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_BuildOn_Sell_Land")]
        [DisplayName("Sit1 BuildOn/Sell Land")]
        public string BuildSellLand1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Cover_Extend_Property_Damage")]
        [DisplayName("Sit1 Cover Extend Property Damage")]
        public string CoverExtendPropertyDamage1 { get; set; }
        #endregion

        #region Situation 2 Details
        [JsonProperty(PropertyName = "Single_Line_20")]
        [DisplayName("Sit2 St Num")]
        public string StreetNumber2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_24")]
        [DisplayName("Sit2 Street")]
        public string Street2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_21")]
        [DisplayName("Sit2 State")]
        public string State2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_23")]
        [DisplayName("Sit2 Suburb")]
        public string Suburb2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_22")]
        [DisplayName("Sit2 Postcode")]
        public string PostCode2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Building_Value")]
        [DisplayName("Sit2 Building Value")]
        public decimal BuildingValue2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_13")]
        [DisplayName("Sit2 Tenants Occupation")]
        public string TenantsOccupation2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_14")]
        [DisplayName("Sit2 Unoccupied")]
        public string Unoccupied2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Rental_Income")]
        [DisplayName("Sit2 Rental Income")]
        public decimal RentalIncome2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_15")]
        [DisplayName("Sit2 PO Services")]
        public string POServices2 { get; set; }

        [JsonProperty(PropertyName = "Number_2")]
        [DisplayName("Sit2 Land Size (sqm)")]
        public int LandSize2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_16")]
        [DisplayName("Sit2 Hobby Farm")]
        public string HobbyFarm2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_17")]
        [DisplayName("Sit2 Agistment")]
        public string Agistment2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_18")]
        [DisplayName("Sit2 CarPark/Markets")]
        public string CarParkOrMarket2 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_19")]
        [DisplayName("Sit2 Structures Buildings")]
        public string StructuresBuildings2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_BuildOn_Sell_Land")]
        [DisplayName("Sit2 BuildOn/Sell Land")]
        public string BuildSellLand2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Cover_Extend_Property_Damage")]
        [DisplayName("Sit2 Cover Extend Property Damage")]
        public string CoverExtendPropertyDamage2 { get; set; }

        #endregion

        #region Situation 3 Details
        [JsonProperty(PropertyName = "Sit3_St_Num")]
        [DisplayName("Sit3 St Num")]
        public string StreetNumber3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Street")]
        [DisplayName("Sit3 Street")]
        public string Street3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_State")]
        [DisplayName("Sit3 State")]
        public string State3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Suburb")]
        [DisplayName("Sit3 Suburb")]
        public string Suburb3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Postcode")]
        [DisplayName("Sit3 Postcode")]
        public string PostCode3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Building_Value")]
        [DisplayName("Sit3 Building Value")]
        public decimal BuildingValue3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Tenants_Occupation")]
        [DisplayName("Sit3 Tenants Occupation")]
        public string TenantsOccupation3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Unoccupied")]
        [DisplayName("Sit3 Unoccupied")]
        public string Unoccupied3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Rental_Income")]
        [DisplayName("Sit3 Rental Income")]
        public decimal RentalIncome3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_PO_Services")]
        [DisplayName("Sit3 PO Services")]
        public string POServices3 { get; set; }

        [JsonProperty(PropertyName = "Number_3")]
        [DisplayName("Sit3 Land Size (sqm)")]
        public int LandSize3 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_57")]
        [DisplayName("Sit3 Hobby Farm")]
        public string HobbyFarm3 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_58")]
        [DisplayName("Sit3 Agistment")]
        public string Agistment3 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_59")]
        [DisplayName("Sit3 CarPark/Markets")]
        public string CarParkOrMarket3 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_60")]
        [DisplayName("Sit3 Structures Buildings")]
        public string StructuresBuildings3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_BuildOn_Sell_Land")]
        [DisplayName("Sit3 BuildOn/Sell Land")]
        public string BuildSellLand3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Cover_Extend_Property_Damage")]
        [DisplayName("Sit3 Cover Extend Property Damage")]
        public string CoverExtendPropertyDamage3 { get; set; }
        #endregion

        #region Situation 4 Details
        [JsonProperty(PropertyName = "Sit4_St_Num")]
        [DisplayName("Sit4 St Num")]
        public string StreetNumber4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Street")]
        [DisplayName("Sit4 Street")]
        public string Street4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_State")]
        [DisplayName("Sit4 State")]
        public string State4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Suburb")]
        [DisplayName("Sit4 Suburb")]
        public string Suburb4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Postcode")]
        [DisplayName("Sit4 Postcode")]
        public string PostCode4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Building_Value")]
        [DisplayName("Sit4 Building Value")]
        public decimal BuildingValue4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Tenants_Occupation")]
        [DisplayName("Sit4 Tenants Occupation")]
        public string TenantsOccupation4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Unoccupied")]
        [DisplayName("Sit4 Unoccupied")]
        public string Unoccupied4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Rental_Income")]
        [DisplayName("Sit4 Rental Income")]
        public decimal RentalIncome4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_PO_Services")]
        [DisplayName("Sit4 PO Services")]
        public string POServices4 { get; set; }

        [JsonProperty(PropertyName = "Number_4")]
        [DisplayName("Sit4 Land Size (sqm)")]
        public int LandSize4 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_45")]
        [DisplayName("Sit4 Hobby Farm")]
        public string HobbyFarm4 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_46")]
        [DisplayName("Sit4 Agistment")]
        public string Agistment4 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_47")]
        [DisplayName("Sit4 CarPark/Markets")]
        public string CarParkOrMarket4 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_48")]
        [DisplayName("Sit4 Structures Buildings")]
        public string StructuresBuildings4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_BuildOn_Sell_Land")]
        [DisplayName("Sit4 BuildOn/Sell Land")]
        public string BuildSellLand4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Cover_Extend_Property_Damage")]
        [DisplayName("Sit4 Cover Extend Property Damage")]
        public string CoverExtendPropertyDamage4 { get; set; }
        #endregion

        #region Situation 5 Details
        [JsonProperty(PropertyName = "Sit5_St_Num")]
        [DisplayName("Sit5 St Num")]
        public string StreetNumber5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Street")]
        [DisplayName("Sit5 Street")]
        public string Street5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_State")]
        [DisplayName("Sit5 State")]
        public string State5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Suburb")]
        [DisplayName("Sit5 Suburb")]
        public string Suburb5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Postcode")]
        [DisplayName("Sit5 Postcode")]
        public string PostCode5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Building_Value")]
        [DisplayName("Sit5 Building Value")]
        public decimal BuildingValue5 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_25")]
        [DisplayName("Sit5 Tenants Occupation")]
        public string TenantsOccupation5 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_26")]
        [DisplayName("Sit5 Unoccupied")]
        public string Unoccupied5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Rental_Income")]
        [DisplayName("Sit5 Rental Income")]
        public decimal RentalIncome5 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_27")]
        [DisplayName("Sit5 PO Services")]
        public string POServices5 { get; set; }

        [JsonProperty(PropertyName = "Number_5")]
        [DisplayName("Sit5 Land Size (sqm)")]
        public int LandSize5 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_28")]
        [DisplayName("Sit5 Hobby Farm")]
        public string HobbyFarm5 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_29")]
        [DisplayName("Sit5 Agistment")]
        public string Agistment5 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_30")]
        [DisplayName("Sit5 CarPark/Markets")]
        public string CarParkOrMarket5 { get; set; }

        [JsonProperty(PropertyName = "Single_Line_31")]
        [DisplayName("Sit5 Structures Buildings")]
        public string StructuresBuildings5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_BuildOn_Sell_Land")]
        [DisplayName("Sit5 BuildOn/Sell Land")]
        public string BuildSellLand5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Cover_Extend_Property_Damage")]
        [DisplayName("Sit5 Cover Extend Property Damage")]
        public string CoverExtendPropertyDamage5 { get; set; }
        #endregion

        #region VLP Details 1

        [JsonProperty(PropertyName = "Sit1_Premise_Use")]
        [DisplayName("Sit1 Premise Use")]
        public string PremiseUse1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Wall_Construction")]
        [DisplayName("Sit1 Wall Construction")]
        public string WallConstruction1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Roof_Construction")]
        [DisplayName("Sit1 Roof Construction")]
        public string RoofConstruction1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Floor_Construction")]
        [DisplayName("Sit1 Floor Construction")]
        public string FloorConstruction1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_EPS_or_PIS_Perc")]
        [DisplayName("Sit1 EPS or PIS Perc")]
        public string PercEPSorPIR1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Year_Premise_Built")]
        [DisplayName("Sit1 Year Premise Built")]
        public string YearPremiseBuilt1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Year_Premise_Rewired")]
        [DisplayName("Sit1 Year Premise Rewired")]
        public string YearPremiseRewired1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Fire_Protection")]
        [DisplayName("Sit1 Fire Protection")]
        public string FireProtection1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Premise_Town_Water_Connection")]
        [DisplayName("Sit1 Premise Town Water Connection")]
        public string PremiseTownWaterConnection1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Local_Fire_Brigade")]
        [DisplayName("Sit1 Local Fire Brigade")]
        public string LocalFireBrigade1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Security_Protection")]
        [DisplayName("Sit1 Security Protection")]
        public string SecurityProtection1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Building_Sum_Insured")]
        [DisplayName("Sit1 Building Sum Insured")]
        public int BuildingSumInsured1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Contents_Sum_InsuredNumber_10")]
        [DisplayName("Sit1 Contents Sum Insured")]
        public int ContentsSumInsured1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Theft_Sum_Insured")]
        [DisplayName("Sit1 Theft Sum Insured")]
        public int TheftSumInsured1 { get; set; }

        [JsonProperty(PropertyName = "Sit1_Glass_Sum_Insured")]
        [DisplayName("Sit1 Glass Sum Insured")]
        public string GlassSumInsured1 { get; set; }

        #endregion

        #region VLP Details 2

        [JsonProperty(PropertyName = "Sit2_Premise_Use")]
        [DisplayName("Sit2 Premise Use")]
        public string PremiseUse2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Wall_Construction")]
        [DisplayName("Sit2 Wall Construction")]
        public string WallConstruction2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Roof_Construction")]
        [DisplayName("Sit2 Roof Construction")]
        public string RoofConstruction2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Floor_Construction")]
        [DisplayName("Sit2 Floor Construction")]
        public string FloorConstruction2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_EPS_or_PIS_Perc")]
        [DisplayName("Sit2 EPS or PIS Perc")]
        public string PercEPSorPIR2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Year_Premise_Built")]
        [DisplayName("Sit2 Year Premise Built")]
        public string YearPremiseBuilt2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Year_Premise_Rewired")]
        [DisplayName("Sit2 Year Premise Rewired")]
        public string YearPremiseRewired2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Fire_Protection")]
        [DisplayName("Sit2 Fire Protection")]
        public string FireProtection2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Premise_Town_Water_Connection")]
        [DisplayName("Sit2 Premise Town Water Connection")]
        public string PremiseTownWaterConnection2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Local_Fire_Brigade")]
        [DisplayName("Sit2 Local Fire Brigade")]
        public string LocalFireBrigade2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Security_Protection")]
        [DisplayName("Sit2 Security Protection")]
        public string SecurityProtection2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Building_Sum_Insured")]
        [DisplayName("Sit2 Building Sum Insured")]
        public int BuildingSumInsured2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Contents_Sum_InsuredNumber_15")]
        [DisplayName("Sit2 Contents Sum Insured")]
        public int ContentsSumInsured2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Theft_Sum_Insured")]
        [DisplayName("Sit2 Theft Sum Insured")]
        public int TheftSumInsured2 { get; set; }

        [JsonProperty(PropertyName = "Sit2_Glass_Sum_Insured")]
        [DisplayName("Sit2 Glass Sum Insured")]
        public string GlassSumInsured2 { get; set; }

        #endregion

        #region VLP Details 3

        [JsonProperty(PropertyName = "Sit3_Premise_Use")]
        [DisplayName("Sit3 Premise Use")]
        public string PremiseUse3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Wall_Construction")]
        [DisplayName("Sit3 Wall Construction")]
        public string WallConstruction3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Roof_Construction")]
        [DisplayName("Sit3 Roof Construction")]
        public string RoofConstruction3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Floor_Construction")]
        [DisplayName("Sit3 Floor Construction")]
        public string FloorConstruction3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_EPS_or_PIS_Perc")]
        [DisplayName("Sit3 EPS or PIS Perc")]
        public string PercEPSorPIR3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Year_Premise_Built")]
        [DisplayName("Sit3 Year Premise Built")]
        public string YearPremiseBuilt3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Year_Premise_Rewired")]
        [DisplayName("Sit3 Year Premise Rewired")]
        public string YearPremiseRewired3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Fire_Protection")]
        [DisplayName("Sit3 Fire Protection")]
        public string FireProtection3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Premise_Town_Water_Connection")]
        [DisplayName("Sit3 Premise Town Water Connection")]
        public string PremiseTownWaterConnection3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Local_Fire_Brigade")]
        [DisplayName("Sit3 Local Fire Brigade")]
        public string LocalFireBrigade3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Security_Protection")]
        [DisplayName("Sit3 Security Protection")]
        public string SecurityProtection3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Building_Sum_Insured")]
        [DisplayName("Sit3 Building Sum Insured")]
        public int BuildingSumInsured3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Contents_Sum_Insured")]
        [DisplayName("Sit3 Contents Sum Insured")]
        public int ContentsSumInsured3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Theft_Sum_Insured")]
        [DisplayName("Sit3 Theft Sum Insured")]
        public int TheftSumInsured3 { get; set; }

        [JsonProperty(PropertyName = "Sit3_Glass_Sum_Insured")]
        [DisplayName("Sit3 Glass Sum Insured")]
        public string GlassSumInsured3 { get; set; }

        #endregion

        #region VLP Details 4

        [JsonProperty(PropertyName = "Sit4_Premise_Use")]
        [DisplayName("Sit4 Premise Use")]
        public string PremiseUse4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Wall_Construction")]
        [DisplayName("Sit4 Wall Construction")]
        public string WallConstruction4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Roof_Construction")]
        [DisplayName("Sit4 Roof Construction")]
        public string RoofConstruction4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Floor_Construction")]
        [DisplayName("Sit4 Floor Construction")]
        public string FloorConstruction4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_EPS_or_PIS_Perc")]
        [DisplayName("Sit4 EPS or PIS Perc")]
        public string PercEPSorPIR4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Year_Premise_Built")]
        [DisplayName("Sit4 Year Premise Built")]
        public string YearPremiseBuilt4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Year_Premise_Rewired")]
        [DisplayName("Sit4 Year Premise Rewired")]
        public string YearPremiseRewired4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Fire_Protection")]
        [DisplayName("Sit4 Fire Protection")]
        public string FireProtection4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Premise_Town_Water_Connection")]
        [DisplayName("Sit4 Premise Town Water Connection")]
        public string PremiseTownWaterConnection4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Local_Fire_Brigade")]
        [DisplayName("Sit4 Local Fire Brigade")]
        public string LocalFireBrigade4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Security_Protection")]
        [DisplayName("Sit4 Security Protection")]
        public string SecurityProtection4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Building_Sum_Insured")]
        [DisplayName("Sit4 Building Sum Insured")]
        public int BuildingSumInsured4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Contents_Sum_Insured")]
        [DisplayName("Sit4 Contents Sum Insured")]
        public int ContentsSumInsured4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Theft_Sum_Insured")]
        [DisplayName("Sit4 Theft Sum Insured")]
        public int TheftSumInsured4 { get; set; }

        [JsonProperty(PropertyName = "Sit4_Glass_Sum_Insured")]
        [DisplayName("Sit4 Glass Sum Insured")]
        public string GlassSumInsured4 { get; set; }

        #endregion

        #region VLP Details 5

        [JsonProperty(PropertyName = "Sit5_Premise_Use")]
        [DisplayName("Sit5 Premise Use")]
        public string PremiseUse5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Wall_Construction")]
        [DisplayName("Sit5 Wall Construction")]
        public string WallConstruction5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Roof_Construction")]
        [DisplayName("Sit5 Roof Construction")]
        public string RoofConstruction5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Floor_Construction")]
        [DisplayName("Sit5 Floor Construction")]
        public string FloorConstruction5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_EPS_or_PIS_Perc")]
        [DisplayName("Sit5 EPS or PIS Perc")]
        public string PercEPSorPIR5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Year_Premise_Built")]
        [DisplayName("Sit5 Year Premise Built")]
        public string YearPremiseBuilt5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Year_Premise_Rewired")]
        [DisplayName("Sit5 Year Premise Rewired")]
        public string YearPremiseRewired5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Fire_Protection")]
        [DisplayName("Sit5 Fire Protection")]
        public string FireProtection5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Premise_Town_Water_Connection")]
        [DisplayName("Sit5 Premise Town Water Connection")]
        public string PremiseTownWaterConnection5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Local_Fire_Brigade")]
        [DisplayName("Sit5 Local Fire Brigade")]
        public string LocalFireBrigade5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Security_Protection")]
        [DisplayName("Sit5 Security Protection")]
        public string SecurityProtection5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Building_Sum_Insured")]
        [DisplayName("Sit5 Building Sum Insured")]
        public int BuildingSumInsured5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Contents_Sum_Insured")]
        [DisplayName("Sit5 Contents Sum Insured")]
        public int ContentsSumInsured5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Theft_Sum_Insured")]
        [DisplayName("Sit5 Theft Sum Insured")]
        public int TheftSumInsured5 { get; set; }

        [JsonProperty(PropertyName = "Sit5_Glass_Sum_Insured")]
        [DisplayName("Sit5 Glass Sum Insured")]
        public string GlassSumInsured5 { get; set; }

        #endregion
    }


    public class SituationOwner
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class LeadReference
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Situations
    {
        public List<Situation> data { get; set; }
    }
}
