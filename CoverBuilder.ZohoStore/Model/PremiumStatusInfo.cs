﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoverBuilder.ZohoStore.Model
{
    [DisplayName("Leads")]
    public class PremiumStatusInfo
    {

        [JsonProperty("id")]
        [DisplayName("LEADID")]
        public string LeadId { get; set; }

        [JsonProperty(PropertyName = "Lead_Status")]
        [DisplayName("Lead Status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "Decline_Comment")]
        [DisplayName("Decline Comment")]
        public string Remarks { get; set; }

        [JsonProperty(PropertyName = "Insurance_Period_From")]
        [DisplayName("Insurance Period From")]
        public string InsuranceDateFrom { get; set; }

        [JsonProperty(PropertyName = "Insurance_Period_To")]
        [DisplayName("Insurance Period To")]
        public string InsuranceDateTo { get; set; }

        [JsonProperty(PropertyName = "Quote_Lost")]
        [DisplayName("Quote Lost")]
        public string QuoteLost { get; set; }

    }
}
