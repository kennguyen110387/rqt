﻿namespace CoverBuilder.ZohoStore.ServiceHelper
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    public static class ZohoApiHelper
    {
        public static string DataToXmlString<T>(T model)
        {
            Type modelType = typeof(T);
            try
            {
                List<XElement> xmlElements = new List<XElement>();
                foreach (var item in modelType.GetProperties())
                {
                    var attributeDisplayName = item.GetCustomAttributes(typeof(DisplayNameAttribute), true).FirstOrDefault() as DisplayNameAttribute;
                    var itemValue = item.GetValue(model, null);
                    if (itemValue == null)
                        itemValue = String.Empty;

                    // --> Special Characters Correction [Add Characters as you see fit]
                    if (itemValue.ToString().Contains("&amp;"))
                        itemValue = itemValue.ToString().Replace("&amp;", "&");

                    if (itemValue.ToString().Contains("&#x0D;"))
                        itemValue = itemValue.ToString().Replace("&#x0D;", " ");

                    if (item.Name != "Content")
                        itemValue = new XCData(itemValue.ToString());

                    xmlElements.Add(new XElement("FL", new XAttribute("val", attributeDisplayName.DisplayName), itemValue));
                }
                var headerDisplayName = modelType.GetCustomAttributes(typeof(DisplayNameAttribute), true).FirstOrDefault() as DisplayNameAttribute;

                XDocument xmlData = new XDocument(
                new XElement(headerDisplayName.DisplayName, new XElement("row",
                            new XAttribute("no", "1"), xmlElements)));

                //return xmlData.ToString().Replace("&", "%26");
                return xmlData.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string DataToXmlString<T>(List<T> model)
        {
            Type modelType = typeof(T);
            try
            {
                List<XElement> xmlRowElements = new List<XElement>();
                List<XElement> xmlElements = new List<XElement>();
                for (int i = 0; i < model.Count; i++)
                {
                    foreach (var item in modelType.GetProperties())
                    {
                        var attributeDisplayName = item.GetCustomAttributes(typeof(DisplayNameAttribute), true).FirstOrDefault() as DisplayNameAttribute;
                        xmlElements.Add(new XElement("FL", new XAttribute("val", attributeDisplayName.DisplayName), item.GetValue(model[i], null)));
                    }

                    xmlRowElements.Add(new XElement("row",
                            new XAttribute("no", i + 1), xmlElements));
                    xmlElements.Clear();
                }

                var headerDisplayName = modelType.GetCustomAttributes(typeof(DisplayNameAttribute), true).FirstOrDefault() as DisplayNameAttribute;
                XDocument xmlData = new XDocument(
                new XElement(headerDisplayName.DisplayName, xmlRowElements));

                return xmlData.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
