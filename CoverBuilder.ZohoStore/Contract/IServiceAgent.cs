﻿namespace CoverBuilder.ZohoStore.Contract
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CoverBuilder.ZohoStore.Model;

    public interface IServiceAgent
    {
        Task<ResponseResult> PushAsync(string url, string action, string webPart);

        Task<List<ResponseResult>> SearchRecordsByReferenceIdAsync(string module, string referenceId);

        Task<ResponseResult> UpdateQuotationAsync(string uri, string action, string webPart, string zohoId);

        Task<ResponseResult> UploadFileAsync(string zohoId, string fileName, byte[] fileByteContent);
    }
}
