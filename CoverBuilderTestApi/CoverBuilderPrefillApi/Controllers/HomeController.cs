﻿using CoverBuilderPrefillApi.Models;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace CoverBuilderPrefillApi.Controllers
{
    public class HomeController : ApiController
    {
        [HttpPost]
        public IHttpActionResult BrokerPrefill([FromUri] BrokerData brokerModel)
        {
            if (ModelState.IsValid)
                return Json(new { Status = "success" });
            else
            {
                Error err = new Error();
                var errorList = ModelState.ToDictionary(
                                kvp => kvp.Key,
                                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                            );
                err.Status = "failed";
                err.ModelError = errorList;     
                return new ErrorResult(err, Request);
            }

        }
    }

    public class ErrorResult : IHttpActionResult
    {
        Error _error;
        HttpRequestMessage _request;

        public ErrorResult(Error error, HttpRequestMessage request)
        {
            _error = error;
            _request = request;
        }
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = new HttpResponseMessage()
            {
                Content = new ObjectContent<Error>(_error, new JsonMediaTypeFormatter()),
                RequestMessage = _request
            };
            return Task.FromResult(response);
        }
    }

    public class Error
    {
        public string Status { get; set; }
        public string Message { get; set; }

        public Dictionary<string, string[]> ModelError { get; set; }
    }

}
