﻿using System.ComponentModel.DataAnnotations;

namespace CoverBuilderPrefillApi.Models
{
    public class BrokerData
    {
        [Required(ErrorMessage = "BASubAgent is required")]
        public string BASubAgent { get; set; }

        [Required]
        public string Brokerage { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "Phone must have maximum length of 10")]
        [Range(0, long.MaxValue, ErrorMessage = "Please enter valid Number")]
        public string Phone { get; set; }

        [Required]
        public string Street { get; set; }

        [Required]
        public string Suburb { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Invalid Post Code")]
        [StringLength(5, ErrorMessage = "PostCode must be numeric and has maximum length of 5")]
        public string PostCode { get; set; }
    }
}