﻿namespace CoverBuilder.BLL.BusinessLogics
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CoverBuilder.BLL.Interfaces;
    using CoverBuilder.Entities.Entities;
    using Hangfire;
    using Hangfire.Server;
    using CoverBuilder.Entities.Enums;
    using ZohoStoreV2.Model;

    public class WorkerService : IWorkerService
    {
        private readonly IZohoService _zohoService;
        private readonly ILiabilityQuotationHeaderBusinessLogic _liabilityQuotationHeaderService;
        private readonly IPremiumDetailBusinessLogic _premiumDetailService;
        private readonly ILiabilityInsuranceResultBusinessLogic _liabilityInsuranceResultService;

        const string NOTE_FORMTITLE = "Forms - Web Lead";
        const string NOTE_POLICYDETAILSANDENDORSEMENTS = "Endorsements";

        public WorkerService()
        {
            IZohoService zohoService = new ZohoService();
            ILiabilityQuotationHeaderBusinessLogic liabilityQuotationService = new LiabilityQuotationHeaderBusinessLogic();
            IPremiumDetailBusinessLogic premiumDetailService = new PremiumDetailBusinessLogic();
            ILiabilityInsuranceResultBusinessLogic liabilityInsuranceResultService = new LiabilityInsuranceResultBusinessLogic();

            _zohoService = zohoService;
            _liabilityQuotationHeaderService = liabilityQuotationService;
            _premiumDetailService = premiumDetailService;
            _liabilityInsuranceResultService = liabilityInsuranceResultService;
        }


        public async Task SaveAsync(string referenceNumber, string quoteStatus, PerformContext context, string access_token)
        {
            var liabilityQuotationHeader = _liabilityQuotationHeaderService.GetUserInfoByReferenceNumber(referenceNumber);

            if (liabilityQuotationHeader != null)
            {
                var dsLiabilityResult = _liabilityInsuranceResultService.GetFlattenedQuestionnaireResults(liabilityQuotationHeader.Id);
                var leadId = string.Empty;
                if (quoteStatus.Contains(UserResponseEnum.New.ToString()))
                {
                    var saveTaskReturn = await SaveLeadAsync(liabilityQuotationHeader, dsLiabilityResult, access_token);
                    leadId = saveTaskReturn;
                }
                else
                {
                    await UpdateLeadAsync(liabilityQuotationHeader, dsLiabilityResult, access_token);
                }

                //await UploadFilesAsync(leadId, liabilityQuotationHeader).ConfigureAwait(false);
            }
        }

        public async Task<string> SaveLeadAsync(LiabilityQuotationHeader liabilityHeader, DataSet dsLiabilityResult, string access_token)
        {
            var leadLiabilityResult = ConvertToLiabilityResultType(dsLiabilityResult.Tables[0]);

            var taskSavedLeadId = await _zohoService.SaveLeadsAsync(liabilityHeader, leadLiabilityResult, access_token);

            if (dsLiabilityResult.Tables.Count > 1)
            {
                var multiSituationLiabilityResult = ConvertToLiabilityResultType(dsLiabilityResult.Tables[1]);
                await _zohoService.SaveCustomModuleAsync(liabilityHeader, multiSituationLiabilityResult, access_token);
            }

            return taskSavedLeadId;
        }

        public async Task UpdateLeadAsync(LiabilityQuotationHeader liabilityHeader, DataSet dsLiabilityResult, string access_token)
        {
            var leadLiabilityResult = ConvertToLiabilityResultType(dsLiabilityResult.Tables[0]);

            await _zohoService.UpdateLeadsAsync(liabilityHeader, leadLiabilityResult, access_token);

            if (dsLiabilityResult.Tables.Count > 1)
            {
                var customModuleZohoId = await _zohoService.SearchCustomModuleByReferenceIdAsync(liabilityHeader.ReferenceNumber, access_token);
                string zohoId = customModuleZohoId;// customModuleZohoId.Select(o => o.ZohoId).FirstOrDefault();
                var multiSituationLiabilityResult = ConvertToLiabilityResultType(dsLiabilityResult.Tables[1]);
                await _zohoService.UpdateCustomModuleAsync(zohoId, liabilityHeader, multiSituationLiabilityResult, access_token);
            }
        }

        public async Task UploadFilesAsync(string referenceNumber, List<Attachment> fileUploadList, string access_token)
        {
            //Get files to upload
            ILiabilityQuotationHeaderBusinessLogic userInfo = new LiabilityQuotationHeaderBusinessLogic();
            var userHeader = userInfo.GetUserInfoByReferenceNumber(referenceNumber);
            IZohoService zohoService = new ZohoService();
            await zohoService.UploadFilesAsync(userHeader.LeadId, fileUploadList, access_token);
        }

        private List<LiabilityQuestionnaireResult> ConvertToLiabilityResultType(DataTable dsResult)
        {
            var liabilityResults = new List<LiabilityQuestionnaireResult>();

            foreach (DataRow row in dsResult.Rows)
            {
                var result = new LiabilityQuestionnaireResult
                {
                    LeadFieldName = row.Field<string>("LeadFieldName"),
                    Question = row.Field<string>("Question"),
                    Response = row.Field<string>("Response")
                };
                liabilityResults.Add(result);
            }
            return liabilityResults;
        }

        #region %%%%%%%%%%%%%%%%%%%%%%%%%%% ZOHO NOTES %%%%%%%%%%%%%%%%%%%%%%%%%%%
        public void SaveRationaleNoteAsync(string referenceNumber, string access_token)
        {
            //Get files to upload
            ILiabilityQuotationHeaderBusinessLogic userInfo = new LiabilityQuotationHeaderBusinessLogic();
            var userHeader = userInfo.GetUserInfoByReferenceNumber(referenceNumber);
            _zohoService.SaveRationaleNoteAsync(userHeader.LeadId, userHeader.RatingRationale, access_token);
        }
        #endregion


    }
}
