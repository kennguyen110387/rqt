﻿using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using CoverBuilder.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Threading;

namespace CoverBuilder.BLL.BusinessLogics
{
    public class EmailTransactionBusinessLogic : IEmailTransactionBusinessLogic
    {
        public List<EmailTransaction> GetEmailTransaction()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.EmailTransaction.FindAll().ToList();
            }
        }

        //public List<EmailTransaction> GetEmailTransactionByTransactionID(int ID)
        //{
        //    using (IUnitOfWork UoW = new UnitOfWork())
        //    {
        //        return UoW.EmailTransaction.Find(x => x.TransactionId == ID).ToList();
        //    }
        //}

        public void CreateEmailTransaction(EmailTransaction vm)
        {
            using (IUnitOfWork Uow = new UnitOfWork())
            {
                Uow.EmailTransaction.Add(vm);
                Uow.Commit();
            }
        }

        public void CreateEmailTransactions(List<EmailTransaction> emailTransactionList)
        {
            using (IUnitOfWork Uow = new UnitOfWork())
            {
                foreach (var transaction in emailTransactionList)
                {
                    Uow.EmailTransaction.Add(transaction);
                }
                Uow.Commit();
            }
        }
        public async Task<int> CreateEmailTransactionsAsync(List<EmailTransaction> emailTransactionList, CancellationToken cancellationToken)
        {
            using (IUnitOfWork Uow = new UnitOfWork())
            {
                try
                {
                    foreach (var transaction in emailTransactionList)
                    {
                        Uow.EmailTransaction.Add(transaction);
                    }
                
                    return await Uow.CommitAsync(cancellationToken);
                }
                catch (Exception)
                {
                    //log
                    throw;
                }
                finally
                {
                    Uow.Dispose();
                }
            }
           
        }
        public void UpdateEmailStatus(int emailId, string emailStatus)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string query = "usp_UpdateEmailStatus";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@emailId",
                    Value = emailId,
                    SqlDbType = System.Data.SqlDbType.Int
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@emailStatus",
                    Value = emailStatus,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                
                uow.ExecuteNonQuery(query, param, CommandType.StoredProcedure);
            }
        }


    }
}
