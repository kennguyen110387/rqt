﻿using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using CoverBuilder.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoverBuilder.BLL.BusinessLogics
{
    public class DropDownValueBusinessLogic : IDropDownValueBusinessLogic
    {
        public List<DropDownValue> GetDropDownValue()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.DropDownValue.FindAll().ToList();
            }
        }

        public List<DropDownValue> GetDropdownValueByInsuranceID(int insuranceId)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.DropDownValue.Find(x => x.InsuranceID == insuranceId).ToList();
            }
        }
    }
}
