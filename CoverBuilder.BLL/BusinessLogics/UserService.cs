﻿namespace CoverBuilder.BLL.BusinessLogics
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CoverBuilder.BLL.Interfaces;
    using CoverBuilder.DAL.Infrastructure;
    using CoverBuilder.Entities.Entities;
    using CoverBuilder.Entities.Enums;
    using CoverBuilder.Utilities;
    public class UserService : IUserService
    {
        public User GetByUsername(string userName)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                return uow.User.Find(x => x.IsActive == true && x.Username.ToLower() == userName.ToLower()).FirstOrDefault();
            }
        }

        public bool Authenticate(string userName, string password)
        {
            User user = GetByUsername(userName);
            return Validate(user.Password, password) != PasswordVerificationResult.Failed;
        }
        public UserVerificationResult Register(string userName, string password)
        {
            var encryptedPass = HashPassword(password);

            User user = new User()
            {
                Username = userName,
                Password = encryptedPass.PasswordHash,
                Salt = encryptedPass.Salt,
                IsActive = true
            };
            var created = CreateNew(user);
            return created ? UserVerificationResult.Success : UserVerificationResult.Failed;
        }

        public void ChangePassword()
        {
            throw new NotImplementedException();
        }

        public bool CreateNew(User userModel)
        {
            //return _userRepository.CreateNew(userModel);
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetList(bool hasPaging, int pageIndex, int pageSize)
        {
            //return _userRepository.GetList(hasPaging, pageIndex, pageSize);
            throw new NotImplementedException();
        }

        public bool Update(User userModel)
        {
            throw new NotImplementedException();
        }

        public void ResetPassword()
        {
            throw new NotImplementedException();
        }

        public PasswordObject HashPassword(string passwordInput)
        {
            return UserHelper.HashPassword(passwordInput);
        }

        private static PasswordVerificationResult Validate(string hashedPassword, string passwordInput)
        {
            return UserHelper.ValidatePassword(hashedPassword, passwordInput) ? PasswordVerificationResult.Success : PasswordVerificationResult.Failed;
        }
    }
}
