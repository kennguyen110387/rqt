﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using CoverBuilder.Entities.Entities;
using CoverBuilder.ZohoStore.Model;
using System.Threading;
using CoverBuilder.Common.Logging;

namespace CoverBuilder.BLL.BusinessLogics
{
    public class LiabilityInsuranceResultBusinessLogic : ILiabilityInsuranceResultBusinessLogic
    {
        private static ILogger logger = new NLogger();
        public List<LiabilityInsuranceResult> GetLiabilityInsuranceResult()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.LiabilityInsuranceResult.FindAll().ToList();
            }
        }

        public int CreateLiabilityInsuranceResult(Guid id, int liabilityInsuranceId, List<LiabilityInsuranceResult> LiabilityInsuranceResultViewModel)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                for (int i = 0; i < LiabilityInsuranceResultViewModel.Count; i++)
                {
                    LiabilityInsuranceResultViewModel[i].UserInfoId = id;
                    UoW.LiabilityInsuranceResult.Add(LiabilityInsuranceResultViewModel[i]);
                }

                return UoW.Commit();
            }
        }

        public async Task<int> CreateLiabilityInsuranceResultAsync(Guid id, int liabilityInsuranceId, List<LiabilityInsuranceResult> LiabilityInsuranceResultViewModel, CancellationToken cancellationToken)
        {
            try
            {
                logger.Log(CoverBuilder.Entities.Enums.LogTypeEnum.Info, id.ToString(), "Save Liability Result");
                using (IUnitOfWork UoW = new UnitOfWork())
                {
                    for (int i = 0; i < LiabilityInsuranceResultViewModel.Count; i++)
                    {
                        LiabilityInsuranceResultViewModel[i].UserInfoId = id;
                        UoW.LiabilityInsuranceResult.Add(LiabilityInsuranceResultViewModel[i]);
                    }
                    var saveTask = UoW.CommitAsync(cancellationToken);
                    return await saveTask;
                }
            }
            catch (Exception)
            {
                //log
                throw;
            }
        }

        public async Task<int> UpdateAsync(List<LiabilityInsuranceResult> liabilityInsuranceResultModel, CancellationToken cancellationToken)
        {
            try
            {
                using (IUnitOfWork UoW = new UnitOfWork())
                {
                    for (int i = 0; i < liabilityInsuranceResultModel.Count; i++)
                    {
                        UoW.LiabilityInsuranceResult.Update(liabilityInsuranceResultModel[i]);
                    }
                    var saveTask = UoW.CommitAsync(cancellationToken);
                    return await saveTask;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public System.Data.DataSet GetReportDataSource(Guid userId)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string query = "usp_ReportQuotation";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                    {
                        ParameterName = "@userId",
                        Value = userId,
                        SqlDbType = System.Data.SqlDbType.UniqueIdentifier
                    });

                return uow.ExecuteQuery(query, param, CommandType.StoredProcedure);
            }
        }
        
        public List<LiabilityInsuranceResult> GetByUserId(Guid id)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.LiabilityInsuranceResult.Find(itm => itm.UserInfoId == id && itm.IsActive == true).ToList();
            }
        }

        public int SetToInactive(Guid userId)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string query = "usp_UpdateLiabilityInsuranceResult";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@UserInfoId",
                    Value = userId.ToString(),
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                return uow.ExecuteNonQuery(query, param, CommandType.StoredProcedure);
            }
        }

        
        public void SetPolicyDetails(Guid userId)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string query = "usp_SetEndorsements";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@userId",
                    Value = userId,
                    SqlDbType = System.Data.SqlDbType.UniqueIdentifier
                });

                uow.ExecuteNonQuery(query, param, CommandType.StoredProcedure);

            }
        }

        public DataSet GetFlattenedQuestionnaireResults(Guid id)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string storedProc = "usp_GetLiabilityQuestionnaireResult";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@userId",
                    Value = id,
                    SqlDbType = System.Data.SqlDbType.UniqueIdentifier
                });

                return uow.ExecuteQuery(storedProc, param, CommandType.StoredProcedure);
            }
        }

    }
}
