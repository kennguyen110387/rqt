﻿using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using CoverBuilder.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoverBuilder.BLL.BusinessLogics
{
    public class PremiumCalculationReferenceBusinessLogic : IPremiumCalculationReferenceBusinessLogic
    {
        public List<PremiumCalculationReference> GetPremiumCalculationReference()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.PremiumCalculationReference.FindAll().ToList();
            }
        }

        public PremiumCalculationReference GetPremiumBaseByLandSize(int sizeOfLand, int insuranceTypeId)
        {
            
            using (IUnitOfWork uow = new UnitOfWork())
            {
                IQueryable<PremiumCalculationReference> rateList = uow.PremiumCalculationReference.Find(x => x.InsuranceTypeID == insuranceTypeId).ToList().AsQueryable();
                var maxLandArea = rateList.Max(mx => mx.ConditionB).Value;
                if (sizeOfLand > maxLandArea)
                {
                    return rateList.First(mv => mv.ConditionB == maxLandArea);
                }

                return rateList.Where(x => sizeOfLand >= x.ConditionA && sizeOfLand <= x.ConditionB).First();
            }
        }

        public PremiumCalculationReference GetByGroupId(int groupId, int insuranceTypeId)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                return uow.PremiumCalculationReference.Find(x => x.ConditionA == groupId && x.InsuranceTypeID == insuranceTypeId).FirstOrDefault();
            }
        }



        public List<PremiumCalculationReference> GetByConditionType(string conditionType)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                return uow.PremiumCalculationReference.Find(x => x.ConditionType == conditionType).ToList();
            }
        }


        public List<PremiumCalculationReference> GetByInsuranceTypeId(int insuranceTypeId)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                return uow.PremiumCalculationReference.Find(x => x.InsuranceTypeID == insuranceTypeId).ToList();
            }
        }
    }
}
