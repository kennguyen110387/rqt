﻿namespace CoverBuilder.BLL.BusinessLogics
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CoverBuilder.BLL.Interfaces;
    using CoverBuilder.DAL.Infrastructure;
    using CoverBuilder.Entities.Entities;

    public class LookupBusinessLogic : ILookupBusinessLogic
    {
        public List<Lookup> GetReferenceByCode(string code)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                return uow.Lookup.Find(x => x.Code == code).ToList();
            }
        }
        
        public Lookup GetByCodeAndText(string code, string text)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                return uow.Lookup.Find(x => x.Code == code && x.Text == text).FirstOrDefault();
            }
        }

        public Lookup GetByValue(string value)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                return uow.Lookup.Find(x => x.Value == value).FirstOrDefault();
            }
        }
    }
}
