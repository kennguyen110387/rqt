﻿using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using Entity = CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CoverBuilder.BLL.BusinessLogics
{
    public class UnderwriterBusinessLogic : IUnderwriterBusinessLogic
    {
        public List<Entity.Underwriter> GetUnderwriterList()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.Underwriter.FindAll().ToList();
            }
        }
        public int CreateUnderwriter(List<Underwriter> UnderwriterList)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                for (int i = 0; i < UnderwriterList.Count; i++)
                {
                    UnderwriterList[i].IsActive = true;
                    UnderwriterList[i].CreatedBy = "WebApp";
                    UnderwriterList[i].CreatedDate = DateTime.Now;
                    UoW.Underwriter.Add(UnderwriterList[i]);
                }

                return UoW.Commit();
            }
        }

        public Entity.Underwriter GetById(int underwriterId)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.Underwriter.Find(x => x.Id == underwriterId).Where(act => act.IsActive == true).FirstOrDefault();
            }
        }
        public Entity.Underwriter GetByUserId(string underwriterId)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.Underwriter.Find(x => x.UserId == underwriterId).Where(act => act.IsActive == true).FirstOrDefault();
            }
        }
        public Entity.Underwriter GetByExecutiveCode(string code)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.Underwriter.Find(x => x.ExecutiveCode == code && x.IsActive == true).FirstOrDefault();
            }
        }

        public Entity.Underwriter GetByEmail(string email)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.Underwriter.Find(x => x.Email == email && x.IsActive == true).FirstOrDefault();
            }
        }
        public int SetUnderwriterInactive()
        {
            try
            {
                var underwriterList = GetUnderwriterList();
                underwriterList.ForEach(x => { x.IsActive = false; x.ModifiedDate = DateTime.Now; });
                using (IUnitOfWork UoW = new UnitOfWork())
                {
                    for (int i = 0; i < underwriterList.Count; i++)
                    {
                        UoW.Underwriter.UpdateUnderwriterIsActive(underwriterList[i]);
                    }
                    var saveTask = UoW.Commit();
                    return saveTask;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        
        public string IgnoreEmailIfPrimaryUnderwriter(int insuranceId, string code)
        {
            ILiabilityInsuranceTypeBusinessLogic liabilityInsuranceService = new LiabilityInsuranceTypeBusinessLogic();
            var liabilityUwDetails = liabilityInsuranceService.GetLiabilityInsuranceTypeById(insuranceId);

            var underwriterDetail = this.GetByExecutiveCode(code);

            return liabilityUwDetails.UnderwriterId != underwriterDetail.UserId ? underwriterDetail.Email : null;
        }
    }
}
