﻿using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using CoverBuilder.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoverBuilder.BLL.BusinessLogics
{
    public class CheckBoxValueBusinessLogic : ICheckBoxValueBusinessLogic
    {
        public List<CheckBoxValue> GetCheckBoxValue()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.CheckBoxValue.FindAll().ToList();
            }
        }

        public List<CheckBoxValue> GetCheckBoxValueByInsuranceID(int ID)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.CheckBoxValue.Find(x => x.InsuranceID == ID).ToList();
            }
        }


    }
}
