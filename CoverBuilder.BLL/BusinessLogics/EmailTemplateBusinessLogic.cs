﻿using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using CoverBuilder.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoverBuilder.BLL.BusinessLogics
{
    public class EmailTemplateBusinessLogic : IEmailTemplateBusinessLogic
    {
        public List<EmailTemplate> GetEmailTemplate()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.EmailTemplate.FindAll().ToList();
            }
        }

        public List<EmailTemplate> GetEmailTemplateByTemplateID(int ID)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.EmailTemplate.Find(x => x.TemplateId == ID).ToList();
            }
        }

        public int UpdateCc(int templateId, string mailCc)
        {
            EmailTemplate templateInfo = GetEmailTemplateByTemplateID(templateId).FirstOrDefault();
            templateInfo.Cc = mailCc;

            int result;
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                UoW.EmailTemplate.UpdateCc(templateInfo);
                result = UoW.Commit();
            }
            return result;
        }

        public int UpdateBcc(int templateId, string mailBcc)
        {
            EmailTemplate templateInfo = GetEmailTemplateByTemplateID(templateId).FirstOrDefault();
            templateInfo.Bcc = mailBcc;

            int result;
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                UoW.EmailTemplate.UpdateBcc(templateInfo);
                result = UoW.Commit();
            }
            return result;
        }

    }
}
