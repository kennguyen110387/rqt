﻿namespace CoverBuilder.BLL.BusinessLogics
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using CoverBuilder.Entities;
    using Entity = CoverBuilder.Entities.Entities;
    using ZohoModel = CoverBuilder.ZohoStore.Model;
    //using CoverBuilder.ZohoStore.Service;
    using CoverBuilder.ZohoStore.ServiceHelper;
    using CoverBuilder.BLL.Interfaces;
    using CoverBuilder.Entities.Enums;
    using ZohoStoreV2.Service;
    using System.IO;
    using System.Text.RegularExpressions;
    using ZohoStoreV2.Model;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using CoverBuilder.Common.Logging;
    using CoverBuilder.Common.Logging.NLog;
    using NLog.Config;

    public class ZohoService : IZohoService
    {
        //private static readonly ILog Log = LogManager.GetLogger(typeof(ZohoService));
        private static readonly ILogger Log = new NLogger();

        private const string _LEADS = "Leads";
        private const string _SITUATIONS = "Situation_Details";//"CustomModule10";
        private const string _USERSCOPE = "ZohoCRM.users.All";
        private const string _NOTES = "Notes";

        public async Task<string> SaveLeadsAsync(Entity.LiabilityQuotationHeader liabilityQuotationHeaderModel, List<Entity.LiabilityQuestionnaireResult> situation, string access_token)
        {
            string result = "";
            var UserId = new LiabilityInsuranceTypeBusinessLogic().GetWithUnderwriterById(liabilityQuotationHeaderModel.InsuranceId.Value).Underwriter.UserId;
            var OwnerEmail = new LiabilityInsuranceTypeBusinessLogic().GetWithUnderwriterById(liabilityQuotationHeaderModel.InsuranceId.Value).Underwriter.Email;
            var model = ConvertToLeadModel(liabilityQuotationHeaderModel, situation, UserId, OwnerEmail);
            var models = new List<ZohoModel.Lead>();
            models.Add(model);
            //var zohoAgent = new ZohoAgent();
            //string xmlPart = ZohoApiHelper.DataToXmlString<ZohoModel.Lead>(model);
            //var response = await zohoAgent.PushAsync(_LEADS, ZohoApiCommand.insertRecords.ToString(), xmlPart);

            var zohoAgent = new ZohoAgentV2();
            var triggers = new string[1] { "workflow" };
            var json = JsonConvert.SerializeObject(new { data = models, trigger = triggers });

            Log.Info(string.Format("\n SaveLeadsAsync json \n\t : {0}\n", json));

            //var token = await zohoAgent.GenerateToken();
            var response = await zohoAgent.PushAsync(_LEADS, json, access_token);

            Log.Info(string.Format("\n SaveLeadsAsync response \n\t : {0}\n", JsonConvert.SerializeObject(response)));

            if (response.data[0].details.id != null)
            {
                ILiabilityQuotationHeaderBusinessLogic liabilityQuotationHeaderService = new LiabilityQuotationHeaderBusinessLogic();
                liabilityQuotationHeaderService.UpdateLeadId(liabilityQuotationHeaderModel.Id, response.data[0].details.id);
            }

            return response.data[0].details.id;
        }

        public async Task<string> UpdateLeadsAsync(Entity.LiabilityQuotationHeader liabilityQuotationHeaderModel, List<Entity.LiabilityQuestionnaireResult> situation, string access_token)
        {
            var UserId = liabilityQuotationHeaderModel.UnderwriterId;
            var OwnerEmail = new UnderwriterBusinessLogic().GetByUserId(UserId).Email;
            var model = ConvertToLeadModel(liabilityQuotationHeaderModel, situation, UserId, OwnerEmail);

            var zohoAgent = new ZohoAgentV2();

            var models = new List<ZohoModel.Lead>();
            models.Add(model);

            var json = JsonConvert.SerializeObject(new { data = models });

            Log.Info(string.Format("\n UpdateLeadsAsync json \n\t : {0}\n", json));

            JObject rss = JObject.Parse(json);
            JObject data = (JObject)rss["data"][0];

            if (liabilityQuotationHeaderModel.InsuranceId != 16)
                data.Property("BA_Class").Remove();

            if (liabilityQuotationHeaderModel.IsSynced.Value)
            {
                data.Property("Premium_Base").Remove();
                data.Property("Underwriter_Fee").Remove();
                data.Property("Business_Desc").Remove();
                data.Property("Excess").Remove();
                data.Property("Endorsements").Remove();
                if (liabilityQuotationHeaderModel.GeneralLiability < 1)
                    data.Property("General_Liability").Remove();
                if (liabilityQuotationHeaderModel.ProductLiability < 1)
                    data.Property("Product_Liability").Remove();

                json = rss.ToString();
            }

            //var token = await zohoAgent.GenerateToken();

            var response = await zohoAgent.UpdateQuotationAsync(_LEADS, liabilityQuotationHeaderModel.LeadId, json, access_token);

            Log.Info(string.Format("\n UpdateLeadsAsync response \n\t : {0}\n", JsonConvert.SerializeObject(response)));

            return response.data[0].details.id;
        }

        private ZohoModel.Lead ConvertToLeadModel(Entity.LiabilityQuotationHeader liabilityQuotationHeaderModel, List<Entity.LiabilityQuestionnaireResult> liabilityQuestionnaireResult, string userId = null, string ownerEmail = null)
        {
            try
            {
                var objInsuranceType = new LiabilityInsuranceTypeBusinessLogic().GetWithUnderwriterById(liabilityQuotationHeaderModel.InsuranceId.Value);

                var _liabilityQuotationHeaderService = new LiabilityQuotationHeaderBusinessLogic();

                if (objInsuranceType.InsuranceId == 16)
                {
                    objInsuranceType.Class = StaticReference.GetAccommodationBaClassType(liabilityQuestionnaireResult.Where(p => p.LeadFieldName == "TypeOfAccom").Select(x => x.Response).FirstOrDefault());
                }

                if (liabilityQuotationHeaderModel.UnderwriterEmail != null)
                {
                    IUnderwriterBusinessLogic uwService = new UnderwriterBusinessLogic();
                    var newUwCode = uwService.GetByEmail(liabilityQuotationHeaderModel.UnderwriterEmail).ExecutiveCode;
                    objInsuranceType.Underwriter.ExecutiveCode = newUwCode;
                }
                var GeneralLiability = liabilityQuotationHeaderModel.LiabilityLimit;
                var ProductLiability = liabilityQuotationHeaderModel.LiabilityLimit;
                // Get Endorsements
                var dsEndorsements = _liabilityQuotationHeaderService.GetEndorsementList(liabilityQuotationHeaderModel.Id);
                var endorsements = "";
                if (dsEndorsements.Tables.Count != 0)
                    endorsements = ConstructEndorsements(dsEndorsements);

                // Get Referral Triggers
                var dsReferralTriggers = _liabilityQuotationHeaderService.GetReferralTriggers(liabilityQuotationHeaderModel.Id);
                var referralTriggers = liabilityQuotationHeaderModel.TriggerDescription ?? string.Empty;
                if (dsReferralTriggers.Tables.Count != 0)
                {
                    referralTriggers += "\n";
                    referralTriggers += ConstructReferralTriggers(dsReferralTriggers);
                }

                // Get Additional Details
                var situationCount = liabilityQuestionnaireResult.Where(g => g.Question == "State").Count();

                var quoteStatus = string.Empty;
                if (liabilityQuotationHeaderModel.HasTriggerReferral.Value)
                    quoteStatus = "Referral Triggered";
                else
                    quoteStatus = "Web Quoted";

                // Get Policy Details
                ZohoModel.Lead model = new ZohoModel.Lead
                {
                    //Layout = 0,
                    OwnerId = userId,
                    Owner = new ZohoModel.Owner() { id = Convert.ToInt64(userId) },
                    SubAgent = liabilityQuotationHeaderModel.BASubAgent,
                    BaClass = objInsuranceType.Class,
                    ReferenceNumber = liabilityQuotationHeaderModel.ReferenceNumber,
                    ExecCode = liabilityQuotationHeaderModel.Status == "Requote" || liabilityQuotationHeaderModel.Status == "Synced" ? new UnderwriterBusinessLogic().GetByUserId(userId).ExecutiveCode : objInsuranceType.Underwriter.ExecutiveCode,
                    LiabilityInsuranceType = objInsuranceType.LiabilityInsurance,
                    InsurancePeriodFrom = liabilityQuotationHeaderModel.InsurancePeriodFrom.Value.ToString("yyyy-MM-dd"),
                    InsurancePeriodTo = liabilityQuotationHeaderModel.InsurancePeriodTo.Value.ToString("yyyy-MM-dd"),
                    LiabilityLimit = liabilityQuotationHeaderModel.LiabilityLimit.ToString(),
                    GeneralLiability = GeneralLiability.ToString(),
                    ProductLiability = ProductLiability.ToString(),
                    InsuredName = liabilityQuotationHeaderModel.InsuredFullName,
                    Company = liabilityQuotationHeaderModel.BrokerageName,
                    BrokerageName = liabilityQuotationHeaderModel.FullName,
                    LeadName = liabilityQuotationHeaderModel.InsuredFullName,
                    Email = liabilityQuotationHeaderModel.Email,
                    Phone = liabilityQuotationHeaderModel.PhoneNo,
                    LeadStatus = quoteStatus,
                    PremiumBase = liabilityQuotationHeaderModel.TotalPremiumBase.Value,
                    ESL = liabilityQuotationHeaderModel.TotalESL != null ? liabilityQuotationHeaderModel.TotalESL.ToString() : "",
                    GST = liabilityQuotationHeaderModel.TotalGST.Value,
                    StampTax = liabilityQuotationHeaderModel.TotalStampTax.Value,
                    UnderwriterFee = liabilityQuotationHeaderModel.UWFee.Value,
                    UnderwriterFeeGST = (liabilityQuotationHeaderModel.UWFeeGSTRate.Value * liabilityQuotationHeaderModel.UWFee.Value),
                    PremiumSubTotal = liabilityQuotationHeaderModel.PremiumSubTotal.Value,
                    BrokerComm = liabilityQuotationHeaderModel.BrokerComm.Value,
                    BrokerCommGST = liabilityQuotationHeaderModel.BrokerCommGST.Value,
                    FinalPremium = liabilityQuotationHeaderModel.TotalFinalPremium.Value,
                    BusinessDesc = liabilityQuotationHeaderModel.PolicyBusinessDescription,
                    Excess = liabilityQuotationHeaderModel.PolicyExcess,
                    Endorsements = endorsements ?? string.Empty,
                    ReferralTriggers = referralTriggers ?? string.Empty,
                    DiscInfoBankruptDetail = liabilityQuotationHeaderModel.DiscInfoBankruptDetail == null ? string.Empty : liabilityQuotationHeaderModel.DiscInfoBankruptDetail,
                    DiscInfoClaimDetail = liabilityQuotationHeaderModel.DiscInfoClaimDetail == null ? string.Empty : liabilityQuotationHeaderModel.DiscInfoClaimDetail,
                    DiscInfoClaimRejectedDetail = liabilityQuotationHeaderModel.DiscInfoClaimRejectedDetail == null ? string.Empty : liabilityQuotationHeaderModel.DiscInfoClaimRejectedDetail,
                    DiscInfoConvictedDetail = liabilityQuotationHeaderModel.DiscInfoConvictedDetail == null ? string.Empty : liabilityQuotationHeaderModel.DiscInfoConvictedDetail,
                    DiscInfoDeclineDetail = liabilityQuotationHeaderModel.DiscInfoDeclineDetail == null ? string.Empty : liabilityQuotationHeaderModel.DiscInfoDeclineDetail,
                    DiscInfoExcessDetail = liabilityQuotationHeaderModel.DiscInfoExcessDetail == null ? string.Empty : liabilityQuotationHeaderModel.DiscInfoExcessDetail,
                    DiscInfoRenewalDetail = liabilityQuotationHeaderModel.DiscInfoRenewalDetail == null ? string.Empty : liabilityQuotationHeaderModel.DiscInfoRenewalDetail,
                    QuoteReceivedDate = liabilityQuotationHeaderModel.CreateDate.HasValue ? liabilityQuotationHeaderModel.CreateDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd"),
                    QuoteSentDate = liabilityQuotationHeaderModel.QuoteSent.HasValue ? liabilityQuotationHeaderModel.QuoteSent.Value.ToString("yyyy-MM-dd") : null,
                    RatingRationale = liabilityQuotationHeaderModel.RatingRationale,
                    BrokerPostalAddress = liabilityQuotationHeaderModel.BrokerFullAddress,
                    BrokerState = liabilityQuotationHeaderModel.SituationState,
                    SituationCount = situationCount
                };

                // Get Result Details
                Type objModel = model.GetType();
                foreach (var res in liabilityQuestionnaireResult)
                {
                    if (res.LeadFieldName != null && res.Response != null)
                    {
                        PropertyInfo propInfo = objModel.GetProperty(res.LeadFieldName);
                        propInfo.SetValue(model, Convert.ChangeType(res.Response, propInfo.PropertyType));
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n ConvertToLeadModel exception \n\t : {0}\n", ex.ToString()));
            }
            return new ZohoModel.Lead();
        }

        public async Task<string> SaveCustomModuleAsync(Entity.LiabilityQuotationHeader liabilityQuotationHeaderModel, List<Entity.LiabilityQuestionnaireResult> situation, string access_token)
        {
            var ownerId = new LiabilityInsuranceTypeBusinessLogic().GetWithUnderwriterById(liabilityQuotationHeaderModel.InsuranceId.Value).Underwriter.UserId;
            var model = ConvertToSituationModel(liabilityQuotationHeaderModel.Id, situation, ownerId); //Need to be updated

            var models = new List<ZohoModel.Situation>();
            models.Add(model);

            var zohoAgent = new ZohoAgentV2();
            var triggers = new string[1] { "workflow" };
            var json = JsonConvert.SerializeObject(new { data = models, trigger = triggers });

            Log.Info(string.Format("\n SaveCustomModuleAsync json \n\t : {0}\n", json));

            var response = await zohoAgent.PushAsync(_SITUATIONS, json, access_token);

            Log.Info(string.Format("\n SaveCustomModuleAsync response \n\t : {0}\n", JsonConvert.SerializeObject(response)));

            return response.data[0].details.id;
        }

        public async Task<string> UpdateCustomModuleAsync(string customModuleZohoId, Entity.LiabilityQuotationHeader liabilityQuotationHeaderModel, List<Entity.LiabilityQuestionnaireResult> situation, string access_token)
        {
            var ownerId = liabilityQuotationHeaderModel.UnderwriterId;
            var model = ConvertToSituationModel(liabilityQuotationHeaderModel.Id, situation, ownerId);

            var models = new List<ZohoModel.Situation>();
            models.Add(model);

            var zohoAgent = new ZohoAgentV2();
            var triggers = new string[1] { "workflow" };
            var json = JsonConvert.SerializeObject(new { data = models, trigger = triggers });

            Log.Info(string.Format("\n UpdateCustomModuleAsync json \n\t : {0}\n", json));

            var response = await zohoAgent.UpdateQuotationAsync(_SITUATIONS, customModuleZohoId, json, access_token);

            Log.Info(string.Format("\n UpdateCustomModuleAsync response \n\t : {0}\n", response.data[0].ToString()));

            return response.data[0].details.id;
        }

        private ZohoModel.Situation ConvertToSituationModel(Guid liabilityQuotationId, List<Entity.LiabilityQuestionnaireResult> liabilityQuestionnaireResult, string ownerId = null)
        {
            try
            {
                var quotationModel = new LiabilityQuotationHeaderBusinessLogic().GetUserInfoById(liabilityQuotationId);

                string insuranceType = new LiabilityInsuranceTypeBusinessLogic().GetLiabilityInsuranceType()
                                                                                             .Where(x => x.InsuranceId == quotationModel.InsuranceId)
                                                                                             .Select(x => x.LiabilityInsurance).FirstOrDefault();

                var situationCount = liabilityQuestionnaireResult.Where(g => g.Question == "State").Count();

                ZohoModel.Situation model = new ZohoModel.Situation
                {
                    Owner = new ZohoModel.SituationOwner { id = ownerId },
                    ReferenceNumber = quotationModel.ReferenceNumber,
                    LeadReference_ID = quotationModel.LeadId,
                    LeadReference = new ZohoModel.LeadReference { id = quotationModel.LeadId },
                    SituationCount = situationCount.ToString(),
                    BrokerEmail = quotationModel.Email,
                };

                Type objModel = model.GetType();
                foreach (var res in liabilityQuestionnaireResult)
                {
                    if (res.LeadFieldName != null && res.Response != null)
                    {
                        PropertyInfo propInfo = objModel.GetProperty(res.LeadFieldName);
                        propInfo.SetValue(model, Convert.ChangeType(res.Response, propInfo.PropertyType));
                    }
                }

                return model;
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n ConvertToSituationModel exception \n\t : {0}\n", ex.ToString()));
            }
            return new ZohoModel.Situation();
        }

        private string ConstructEndorsements(System.Data.DataSet endorsements)
        {
            try
            {
                StringBuilder noteContent = new StringBuilder();
                string breakLine = "\n";

                var dt = endorsements.Tables[0];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //noteContent.Append(string.Format("{0}.  {1}", i + 1, dt.Rows[i].Field<string>("Name")));
                    noteContent.Append(string.Format("{0}", dt.Rows[i].Field<string>("Name")));
                    noteContent.Append(breakLine);
                }

                noteContent.Append(breakLine);
                noteContent.Append(breakLine);

                foreach (System.Data.DataRow row in dt.Rows)
                {
                    noteContent.Append(row.Field<string>("Name"));
                    noteContent.Append(breakLine);
                    noteContent.Append(row.Field<string>("Description"));
                    noteContent.Append(breakLine);
                    noteContent.Append(breakLine);
                }
                return noteContent.ToString();
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n ConstructEndorsements exception \n\t : {0}\n", ex.ToString()));
            }
            return "";
        }

        private string ConstructReferralTriggers(System.Data.DataSet dsReferralTriggers)
        {
            try
            {
                StringBuilder noteContent = new StringBuilder();
                string breakLine = "\n";

                var dt = dsReferralTriggers.Tables[0];

                foreach (System.Data.DataRow row in dt.Rows)
                {
                    noteContent.Append(row.Field<string>("TriggerDesc"));
                    noteContent.Append(breakLine);
                }

                return noteContent.ToString();
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n ConstructReferralTriggers exception \n\t : {0}\n", ex.ToString()));
            }
            return "";
        }

        public async Task<Entity.LiabilityQuotationData> SyncZohoData(string referenceId, string access_token)
        {
            Entity.LiabilityQuotationData liabilityResultModel = new Entity.LiabilityQuotationData();
            try
            {
                var insuranceId = 0;
                var apiLeadResult = await GetLeadsByReferenceIdAsync(referenceId, access_token);
                var liabilityQuoteModel = ConvertToLeadEntity(apiLeadResult);

                liabilityResultModel.LiabilityQuotationHeader = liabilityQuoteModel;

                var questionnaireModel = new QuestionaireBusinessLogic().GetByInsuranceId(liabilityQuoteModel.InsuranceId.Value);

                Entity.LiabilityQuotationData handle = new Entity.LiabilityQuotationData();
                if (StaticReference.IsMultiSitutationLiability(apiLeadResult.LiabilityInsuranceType))
                {
                    var apiSituationResult = await GetSituationModuleDataByRefIdAsync(referenceId, access_token);
                    handle = MapSituationToResultEntity(liabilityQuoteModel.Id, questionnaireModel, apiSituationResult);
                }
                else
                {
                    handle = MapLeadToResultEntity(liabilityQuoteModel.Id, questionnaireModel, apiLeadResult);
                }

                liabilityResultModel.LiabilityResultList = handle.LiabilityResultList;
                liabilityResultModel.DeserialisedLiabilityResultList = handle.DeserialisedLiabilityResultList;

            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n SyncZohoData exception \n\t : {0}\n", ex.ToString()));
            }
            return liabilityResultModel;
        }

        public async Task<ZohoModel.Lead> GetLeadsByReferenceIdAsync(string referenceId, string access_token)
        {
            var zohoAgent = new ZohoAgentV2();
            //var token = await zohoAgent.GenerateToken();
            var model = await zohoAgent.SearchLeadsRefIdAsync<ZohoModel.Lead>(_LEADS, referenceId, access_token);

            return model;
        }

        public async Task<ZohoModel.Situation> GetSituationModuleDataByRefIdAsync(string referenceId, string access_token)
        {
            var zohoAgent = new ZohoAgentV2();
            //var token = await zohoAgent.GenerateToken();
            var model = await zohoAgent.SearchSituationRefIdAsync<ZohoModel.Situation>(_SITUATIONS, referenceId, access_token);
            return model;
        }

        private Entity.LiabilityQuotationData MapLeadToResultEntity(Guid quoteId, List<Entity.Questionaire> questionnaireModel, ZohoModel.Lead apiResult)
        {
            List<Entity.LiabilityInsuranceResult> liabilityResultForReinsertion = new List<Entity.LiabilityInsuranceResult>();
            var liabilityResultSvc = new LiabilityInsuranceResultBusinessLogic();
            var liabilityResultModel = liabilityResultSvc.GetByUserId(quoteId);

            try
            {
                Type objType = typeof(ZohoModel.Lead);

                var leadResultModel = MapReadableLeadResult(questionnaireModel, apiResult);

                int[] concatenatedQuestions = StaticReference.GetAllKeys();
                liabilityResultModel.Where(p => concatenatedQuestions.Contains(p.QuestionId)).ToList().ForEach(s => s.IsActive = false);
                foreach (var genQuestions in leadResultModel.Where(p => !concatenatedQuestions.Contains(p.QuestionId)))
                {
                    // ==> HANDLE: Check if Zoho Lead Question does not exist in current db lead result record
                    if (!liabilityResultModel.Select(s => s.QuestionId).Contains(genQuestions.QuestionId))
                    {
                        break;
                    }
                    else
                    {
                        if (StaticReference.ParentChildDetailQuestionnaire.Keys.ToArray().Contains(genQuestions.QuestionId))
                        {
                            int parentId = StaticReference.GetParentIdQuestionyChildId(genQuestions.QuestionId);
                            if (string.IsNullOrEmpty(genQuestions.Response))
                                liabilityResultModel.Where(f => f.QuestionId == parentId && f.GroupId == genQuestions.GroupId).FirstOrDefault().Response = "No";
                            else
                                liabilityResultModel.Where(f => f.QuestionId == parentId && f.GroupId == genQuestions.GroupId).FirstOrDefault().Response = "Yes";
                            liabilityResultModel.Where(f => f.QuestionId == genQuestions.QuestionId && f.GroupId == genQuestions.GroupId).FirstOrDefault().Response = genQuestions.Response;
                        }
                        else
                            liabilityResultModel.Where(f => f.QuestionId == genQuestions.QuestionId).FirstOrDefault().Response = genQuestions.Response;
                    }
                }

                foreach (var resp in leadResultModel.Where(p => concatenatedQuestions.Contains(p.QuestionId)))
                {
                    string[] splitResponse = StaticReference.SplitResult(resp.QuestionId, resp.Response);
                    for (int i = 0; i < splitResponse.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(splitResponse[i]))
                        {
                            liabilityResultForReinsertion.Add(new Entity.LiabilityInsuranceResult
                            {
                                UserInfoId = quoteId,
                                QuestionId = resp.QuestionId,
                                Response = splitResponse[i].Trim(),
                                IsActive = true,
                                ActivityGroup = resp.ActivityGroup,
                                ModifiedDate = DateTime.Now
                            });
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n MapLeadToResultEntity exception \n\t : {0}\n", ex.ToString()));
            }

            return new Entity.LiabilityQuotationData
            {
                LiabilityResultList = liabilityResultModel,
                DeserialisedLiabilityResultList = liabilityResultForReinsertion
            };
        }

        private Entity.LiabilityQuotationData MapSituationToResultEntity(Guid quoteId, List<Entity.Questionaire> questionnaireModel, ZohoModel.Situation apiResult)
        {
            var liabilityResultSvc = new LiabilityInsuranceResultBusinessLogic();
            var liabilityResultModel = liabilityResultSvc.GetByUserId(quoteId);
            List<Entity.LiabilityInsuranceResult> liabilityResultForReinsertion = new List<Entity.LiabilityInsuranceResult>();

            try
            {

                var situationResultModel = MapReadableSituationResult(questionnaireModel, apiResult);
                int poServiceId = 226;

                liabilityResultModel.Where(p => p.QuestionId == poServiceId).ToList().ForEach(l => l.IsActive = false);
                foreach (var res in situationResultModel.Where(x => x.QuestionId != poServiceId))
                {
                    if (StaticReference.ParentChildDetailQuestionnaire.Keys.ToArray().Contains(res.QuestionId))
                    {
                        int parentId = StaticReference.GetParentIdQuestionyChildId(res.QuestionId);
                        if (string.IsNullOrEmpty(res.Response))
                            liabilityResultModel.Where(f => f.QuestionId == parentId && f.GroupId == res.GroupId).FirstOrDefault().Response = "No";
                        else
                            liabilityResultModel.Where(f => f.QuestionId == parentId && f.GroupId == res.GroupId).FirstOrDefault().Response = "Yes";
                        liabilityResultModel.Where(f => f.QuestionId == res.QuestionId && f.GroupId == res.GroupId).FirstOrDefault().Response = res.Response;
                    }
                    else
                        liabilityResultModel.Where(f => f.QuestionId == res.QuestionId && f.GroupId == res.GroupId).FirstOrDefault().Response = res.Response;
                }

                foreach (var poService in situationResultModel.Where(x => x.QuestionId == poServiceId))
                {
                    string[] splitResponse = StaticReference.SplitResult(poService.QuestionId, poService.Response);
                    for (int i = 0; i < splitResponse.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(splitResponse[i]))
                        {
                            liabilityResultForReinsertion.Add(new Entity.LiabilityInsuranceResult
                            {
                                UserInfoId = quoteId,
                                QuestionId = poService.QuestionId,
                                Response = splitResponse[i].Trim(),
                                IsActive = true,
                                GroupId = poService.GroupId,
                                ModifiedDate = DateTime.Now
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n MapSituationToResultEntity exception \n\t : {0}\n", ex.ToString()));
            }

            return new Entity.LiabilityQuotationData
            {
                LiabilityResultList = liabilityResultModel,
                DeserialisedLiabilityResultList = liabilityResultForReinsertion
            };
        }

        public List<ZohoModel.LeadResult> MapReadableSituationResult(List<Entity.Questionaire> questionnaireData, ZohoModel.Situation apiSituationResult)
        {
            List<ZohoModel.LeadResult> model = new List<ZohoModel.LeadResult>();
            try
            {
                var excludeComments = "Comments";
                var obj = typeof(ZohoModel.Situation);
                var objPropList = obj.GetProperties().Where(x => x.Name != excludeComments).Select(p => p.Name).AsQueryable();

                foreach (var item in questionnaireData.Where(p => p.LeadFieldName != null && p.LeadFieldName != excludeComments).GroupBy(p => p.QuestionId).Select(y => y.First()))
                {
                    for (int i = 1; i <= Convert.ToInt32(apiSituationResult.SituationCount); i++)
                    {
                        string completeFieldName = string.Format("{0}{1}", item.LeadFieldName, i);
                        var objProp = obj.GetProperty(completeFieldName);
                        var objPropVal = objProp.GetValue(apiSituationResult);
                        model.Add(new ZohoModel.LeadResult(item.QuestionId, objPropVal == null ? string.Empty : objPropVal.ToString(), completeFieldName, i));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n MapReadableSituationResult exception \n\t : {0}\n", ex.ToString()));
            }

            return model;
        }

        public List<ZohoModel.LeadResult> MapReadableLeadResult(List<Entity.Questionaire> questionnaireData, ZohoModel.Lead apiResult)
        {
            List<ZohoModel.LeadResult> model = new List<ZohoModel.LeadResult>();
            try
            {
                var obj = typeof(ZohoModel.Lead);
                var objPropList = obj.GetProperties().Select(p => p.Name).AsQueryable();


                foreach (var question in questionnaireData.Where(x => x.LeadFieldName != null).GroupBy(p => p.QuestionId).Select(y => y.First()))
                {
                    if (!StaticReference.IsCAGroup(question.QuestionId))
                    {
                        var objProp = obj.GetProperty(question.LeadFieldName);
                        var objPropVal = objProp.GetValue(apiResult);
                        model.Add(new ZohoModel.LeadResult(question.QuestionId, objPropVal == null ? string.Empty : objPropVal.ToString(), question.LeadFieldName));
                    }
                    else
                    {
                        for (int i = 1; i <= 4; i++)
                        {
                            var caGroupName = string.Format("{0}{1}", question.LeadFieldName, i);
                            var objProp = obj.GetProperty(caGroupName);
                            var objPropVal = objProp.GetValue(apiResult);
                            model.Add(new ZohoModel.LeadResult(question.QuestionId, objPropVal == null ? string.Empty : objPropVal.ToString(), caGroupName, null, i));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n MapReadableLeadResult exception \n\t : {0}\n", ex.ToString()));
            }

            return model;
        }

        public Entity.LiabilityQuotationHeader ConvertToLeadEntity(ZohoModel.Lead leadModel)
        {
            ILiabilityQuotationHeaderBusinessLogic liabilityQuoteSvc = new LiabilityQuotationHeaderBusinessLogic();
            Entity.LiabilityQuotationHeader model = liabilityQuoteSvc.GetUserInfoByReferenceNumber(leadModel.ReferenceNumber);
            try
            {

                double LiabilityLimit = 0.0;
                if (Convert.ToDouble(leadModel.GeneralLiability) != 0 && Convert.ToDouble(leadModel.ProductLiability) == 0)
                {
                    LiabilityLimit = Convert.ToDouble(leadModel.GeneralLiability);
                }
                else if (Convert.ToDouble(leadModel.ProductLiability) != 0 && Convert.ToDouble(leadModel.GeneralLiability) == 0)
                {
                    LiabilityLimit = Convert.ToDouble(leadModel.ProductLiability);
                }
                else
                {
                    LiabilityLimit = Convert.ToDouble(leadModel.LiabilityLimit);
                }
                var uwService = new UnderwriterBusinessLogic();
                var secondaryLeadOwnerEmail = uwService.IgnoreEmailIfPrimaryUnderwriter(model.InsuranceId.Value, leadModel.ExecCode);

                DateTime quoteSentDate;
                if (DateTime.TryParse(leadModel.QuoteSentDate, out quoteSentDate))
                {
                    model.QuoteSent = quoteSentDate;
                }

                model.BASubAgent = leadModel.SubAgent;
                model.ReferenceNumber = leadModel.ReferenceNumber;
                model.InsurancePeriodFrom = Convert.ToDateTime(leadModel.InsurancePeriodFrom);
                model.InsurancePeriodTo = Convert.ToDateTime(leadModel.InsurancePeriodTo);
                model.LiabilityLimit = LiabilityLimit;
                model.GeneralLiability = Convert.ToDouble(leadModel.GeneralLiability);
                model.ProductLiability = Convert.ToDouble(leadModel.ProductLiability);
                model.ExecCode = leadModel.ExecCode;
                model.InsuredFullName = leadModel.LeadName;
                model.BrokerageName = leadModel.Company;
                model.FullName = leadModel.BrokerageName;
                model.Email = leadModel.Email;
                model.PhoneNo = leadModel.Phone;
                model.TotalPremiumBase = leadModel.PremiumBase;
                model.TotalESL = Convert.ToDecimal(leadModel.ESL);
                model.TotalGST = leadModel.GST;
                model.TotalStampTax = leadModel.StampTax;
                model.UWFee = leadModel.UnderwriterFee;
                model.TotalUwFee = leadModel.UnderwriterFeeGST;
                model.PremiumSubTotal = leadModel.PremiumSubTotal;
                model.BrokerComm = leadModel.BrokerComm;
                model.BrokerCommGST = leadModel.BrokerCommGST;
                model.TotalFinalPremium = leadModel.FinalPremium;
                model.PolicyBusinessDescription = leadModel.BusinessDesc;
                model.PolicyExcess = leadModel.Excess;
                model.PolicyZoho = leadModel.Endorsements;
                model.DiscInfoBankrupt = !string.IsNullOrEmpty(leadModel.DiscInfoBankruptDetail) ? true : false;
                model.DiscInfoBankruptDetail = leadModel.DiscInfoBankruptDetail;
                model.DiscInfoClaim = !string.IsNullOrEmpty(leadModel.DiscInfoClaimDetail) ? true : false;
                model.DiscInfoClaimDetail = leadModel.DiscInfoClaimDetail;
                model.DiscInfoClaimRejected = !string.IsNullOrEmpty(leadModel.DiscInfoClaimRejectedDetail) ? true : false;
                model.DiscInfoClaimRejectedDetail = leadModel.DiscInfoClaimRejectedDetail;
                model.DiscInfoConvicted = !string.IsNullOrEmpty(leadModel.DiscInfoConvictedDetail) ? true : false;
                model.DiscInfoConvictedDetail = leadModel.DiscInfoConvictedDetail;
                model.DiscInfoDecline = !string.IsNullOrEmpty(leadModel.DiscInfoDeclineDetail) ? true : false;
                model.DiscInfoDeclineDetail = leadModel.DiscInfoDeclineDetail;
                model.DiscInfoExcess = !string.IsNullOrEmpty(leadModel.DiscInfoExcessDetail) ? true : false;
                model.DiscInfoExcessDetail = leadModel.DiscInfoExcessDetail;
                model.DiscInfoRenewal = !string.IsNullOrEmpty(leadModel.DiscInfoRenewalDetail) ? true : false;
                model.DiscInfoRenewalDetail = leadModel.DiscInfoRenewalDetail;
                model.UnderwriterEmail = string.IsNullOrEmpty(secondaryLeadOwnerEmail) ? model.UnderwriterEmail : secondaryLeadOwnerEmail;
                model.TriggerDescription = leadModel.ReferralTriggers;
                model.ModifiedDate = DateTime.Now;

            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n ConvertToLeadEntity exception \n\t : {0}\n", ex.ToString()));
            }

            return model;
        }

        public async Task<string> SearchCustomModuleByReferenceIdAsync(string referenceId, string access_token)
        {
            var zohoAgent = new ZohoAgentV2();
            //var token = await zohoAgent.GenerateToken();
            var apiResult = await zohoAgent.SearchRecordsByReferenceIdAsync(_SITUATIONS, referenceId, access_token);
            return apiResult.LeadReference_ID;
        }

        public async Task UpdateLeadStatusAsync(Entity.PremiumStatusInfo statusInfo, string zohoId, string access_token)
        {
            var model = ConvertStatusInfo(statusInfo);
            var zohoAgent = new ZohoAgentV2();

            var models = new List<ZohoModel.PremiumStatusInfo>();
            models.Add(model);

            var triggers = new string[1] { "workflow" };
            var json = JsonConvert.SerializeObject(new { data = models });

            Log.Info(string.Format("\n UpdateLeadStatusAsync json \n\t : {0}\n", json));

            //var token = await zohoAgent.GenerateToken();

            var response = await zohoAgent.UpdateQuotationAsync(_LEADS, zohoId, json, access_token);

            Log.Info(string.Format("\n UpdateLeadStatusAsync response \n\t : {0}\n", JsonConvert.SerializeObject(response)));
        }

        private ZohoModel.PremiumStatusInfo ConvertStatusInfo(Entity.PremiumStatusInfo statusInfo)
        {
            return new ZohoModel.PremiumStatusInfo()
            {
                LeadId = statusInfo.LeadId,
                Status = statusInfo.Status,
                Remarks = statusInfo.Remarks,
                InsuranceDateFrom = statusInfo.InsuranceDateFrom,
                InsuranceDateTo = statusInfo.InsuranceDateTo,
                QuoteLost = statusInfo.QuoteLostDate
            };
        }

        public async Task<List<Entity.PremiumQuotationInfo>> SearchPremiumInfoByReferenceId(string referenceId, string access_token)
        {
            var zohoAgent = new ZohoAgentV2();
            //var token = await zohoAgent.GenerateToken();
            var apiResult = await zohoAgent.SearchPremiumInfoByReferenceIdAsync(referenceId, _LEADS, access_token);
            var entity = new List<Entity.PremiumQuotationInfo>();
            entity.AddRange(apiResult.Select(e => new Entity.PremiumQuotationInfo { LeadId = e.LeadId, BrokerComm = e.BrokerComm, BrokerCommGst = e.BrokerCommGST, StampTax = e.StampTax, FinalPremium = e.FinalPremium, UnderwriterFeeGst = e.UnderwriterFeeGST, UnderwriterFee = e.UnderwriterFee, PremiumSubTotal = e.PremiumSubTotal, PremiumBase = e.PremiumBase, Gst = e.GST }));
            return entity;
        }
        // Update Financials
        public async Task UpdateLeadFinancesAsync(Entity.PremiumFinancialInfo financialInfo, string zohoId, string access_token)
        {
            var model = ConvertFinancialInfo(financialInfo);
            var zohoAgent = new ZohoAgentV2();
            var models = new List<ZohoModel.PremiumFinancialInfo>();
            models.Add(model);

            var triggers = new string[1] { "workflow" };
            var json = JsonConvert.SerializeObject(new { data = models, trigger = triggers });

            Log.Info(string.Format("\n UpdateLeadFinancesAsync json \n\t : LeadId: {0}, {1}\n", zohoId, json));

            var response = await zohoAgent.UpdateQuotationAsync(_LEADS, zohoId, json, access_token);

            Log.Info(string.Format("\n UpdateLeadFinancesAsync response \n\t : LeadId: {0}, {1}\n", zohoId, JsonConvert.SerializeObject(response)));

        }
        private ZohoModel.PremiumFinancialInfo ConvertFinancialInfo(Entity.PremiumFinancialInfo financialInfo)
        {
            return new ZohoModel.PremiumFinancialInfo()
            {
                LeadId = financialInfo.LeadId,
                PremiumBase = financialInfo.PremiumBase,
                StampTax = financialInfo.StampTax,
                GST = financialInfo.GST,
                UnderwriterFee = financialInfo.UnderwriterFee,
                UnderwriterFeeGST = financialInfo.UnderwriterFeeGST,
                PremiumSubTotal = financialInfo.PremiumSubTotal,
                BrokerComm = financialInfo.BrokerComm,
                BrokerCommGST = financialInfo.BrokerCommGST,
                FinalPremium = financialInfo.FinalPremium
            };
        }
        public List<Entities.Entities.Underwriter> GetZohoUsers()
        {
            List<Entities.Entities.Underwriter> underwriterList = new List<Entity.Underwriter>();
            var zohov2 = new ZohoAgentV2();
            var userlist = zohov2.GetUsers();

            foreach (var item in userlist.users.Where(x => x.status == "active"))
            {
                underwriterList.Add(new Entities.Entities.Underwriter()
                {
                    Name = item.full_name,
                    Email = item.email,
                    UserId = item.id,
                    ExecutiveCode = item.alias == null ? null : item.alias.ToUpper(),
                    Role = item.profile.name
                });
            }
            return underwriterList;
        }


        #region Create Lead Notes
        //public async Task SaveNoteAsync(int insuranceTypeId, string leadId, List<Entity.LiabilityInsuranceResult> situation, System.Data.DataSet genericInfo)
        //{
        //    var noteContent = ConvertLiabilityResultToNote(insuranceTypeId, situation, genericInfo);
        //    ZohoModel.Notes model = new ZohoModel.Notes
        //    {
        //        LeadId = leadId,
        //        Title = "Form Details",
        //        Content = noteContent
        //    };

        //    await SaveNoteAsync(model);
        //}

        //public async Task SaveNoteAsync(Entity.LiabilityQuotationHeader liabilityQuoteHeader, System.Data.DataSet endorsements)
        //{
        //    List<ZohoModel.Notes> listModel = new List<ZohoModel.Notes>();

        //    ILookupBusinessLogic lookupService = new LookupBusinessLogic();

        //    var noteContent = ConvertEndorsementsToNote(liabilityQuoteHeader.InsuranceId.Value, endorsements);
        //    listModel.Add(new ZohoModel.Notes
        //    {
        //        LeadId = liabilityQuoteHeader.LeadId,
        //        Title = "Endorsements",
        //        Content = noteContent
        //    });



        //    StringBuilder policyNoteContent = new StringBuilder();
        //    policyNoteContent.Append(lookupService.GetByValue(liabilityQuoteHeader.InsuranceId.ToString()).Description);


        //    listModel.Add(new ZohoModel.Notes
        //    {
        //        LeadId = liabilityQuoteHeader.LeadId,
        //        Title = "Policy Details",
        //        Content = policyNoteContent.ToString().Replace("{BusinessDescription}", liabilityQuoteHeader.PolicyBusinessDescription).Replace("{LiabilityLimit}", liabilityQuoteHeader.LiabilityLimit.ToString()).Replace("{Excess}", liabilityQuoteHeader.PolicyExcess)
        //    });


        //    await SaveMultiNoteAsync(listModel);
        //}

        //public async Task SaveNoteAsync<T>(T obj)
        //{
        //    Type objType = obj.GetType();
        //    if (objType.Name != "Notes")
        //    {
        //        objType = objType.GetGenericArguments()[0];
        //    }

        //    string xmlPart = ZohoApiHelper.DataToXmlString<T>(obj);
        //    var zohoAgent = new ZohoAgent();
        //    var response = await zohoAgent.PushAsync(_NOTES, ZohoApiCommand.insertRecords.ToString(), xmlPart);
        //}

        //public async Task SaveMultiNoteAsync(List<ZohoModel.Notes> model)
        //{
        //    string xmlPart = ZohoApiHelper.DataToXmlString<ZohoModel.Notes>(model);
        //    var zohoAgent = new ZohoAgent();
        //    var response = await zohoAgent.PushAsync(_NOTES, ZohoApiCommand.insertRecords.ToString(), xmlPart);
        //}
        #endregion

        #region Update Zoho Lead Notes
        //public async Task UpdateNoteAsync(int insuranceTypeId, Entity.Notes noteEntity, List<Entity.LiabilityInsuranceResult> situation, System.Data.DataSet genericInfo)
        //{
        //    var noteContent = ConvertLiabilityResultToNote(insuranceTypeId, situation, genericInfo);
        //    var noteModel = new ZohoModel.Notes
        //    {
        //        Title = "Forms - Web Lead (Requote)",
        //        Content = noteContent
        //    };
        //    await UpdateNoteAsync(noteModel, noteEntity.id);
        //}

        //public async Task UpdateNoteAsync(int insuranceTypeId, Entity.Notes noteEntity, System.Data.DataSet endorsements)
        //{
        //    var noteContent = ConvertEndorsementsToNote(insuranceTypeId, endorsements);
        //    var noteModel = new ZohoModel.Notes
        //    {
        //        Title = "Policy Details and Endorsements",
        //        Content = noteContent
        //    };

        //    await UpdateNoteAsync(noteModel, noteEntity.id);
        //}

        //public async Task UpdateNoteAsync<T>(T obj, string noteId)
        //{
        //    var zohoAgent = new ZohoAgent();
        //    string xmlPart = ZohoApiHelper.DataToXmlString<T>(obj);
        //    var response = await zohoAgent.UpdateNotesAsync(_NOTES, ZohoApiCommand.updateRecords.ToString(), xmlPart, noteId);
        //}
        #endregion

        #region Generate Note Content
        private string ConvertLiabilityResultToNote(int insuranceTypeId, List<Entity.LiabilityInsuranceResult> model, System.Data.DataSet genericInfo)
        {
            StringBuilder noteContent = new StringBuilder();
            string breakLine = "\n";
            string trailingSpace = "     ";
            IQuestionaireBusinessLogic questionnaire = new QuestionaireBusinessLogic();

            foreach (System.Data.DataRow item in genericInfo.Tables[0].Rows)
            {
                noteContent.Append(item.Field<string>("Question"));
                noteContent.Append(trailingSpace);
                noteContent.Append(item.Field<string>("Response"));
                noteContent.Append(breakLine);
            }
            noteContent.Append(breakLine);
            foreach (var item in model)
            {
                var questionList = questionnaire.GetByInsuranceId(insuranceTypeId);
                var question = questionList.Find(x => x.QuestionId == item.QuestionId).Question;

                noteContent.Append(question);
                noteContent.Append(trailingSpace);
                noteContent.Append(item.Response);
                noteContent.Append(breakLine);
            }

            return noteContent.ToString();
        }

        #endregion

        #region Create Lead Notes - Rating Rationale

        public async Task SaveRationaleNoteAsync(string leadId, string ratingRationale, string access_token)
        {
            var noteContent = ratingRationale;
            ZohoModel.Notes model = new ZohoModel.Notes
            {
                LeadId = leadId,
                Title = "Rating Rationale " + System.DateTime.Now.Date,
                Content = noteContent,
                Module = "Leads"
            };

            var models = new List<ZohoModel.Notes>();
            models.Add(model);

            var zohoAgent = new ZohoAgentV2();
            var json = JsonConvert.SerializeObject(new { data = models });
            //var token = await zohoAgent.GenerateToken();
            var response = await zohoAgent.PushAsync(_NOTES, json, access_token);

            Log.Info(string.Format("\n SaveRationaleNoteAsync response \n\t : {0}\n", JsonConvert.SerializeObject(response)));
        }

        #endregion

        //public async Task<List<Entity.Notes>> GetAllNotesByLeadIDAsync(string leadId)
        //{
        //    var zohoAgent = new ZohoAgent();
        //    var result = await zohoAgent.GetAllLeadNotes(leadId);

        //    return result.Select(x => new Entity.Notes
        //    {
        //        id = x.id,
        //        Content = x.Content,
        //        Title = x.Title
        //    }).ToList();
        //}

        public async Task UploadFilesAsync(string leadId, IEnumerable<Entity.Attachment> attachmentList, string access_token)
        {
            try
            {
                var zohoAgent = new ZohoAgentV2();
                var details = new StringBuilder();

                //var token = await zohoAgent.GenerateToken();
                var taskList = new List<Task<ZohoModel.ResponseResult>>();

                details.AppendFormat("UploadFilesAsync \n\tLeadID: {0}", leadId);

                foreach (var attachment in attachmentList)
                {
                    taskList.Add(zohoAgent.UploadFileAsync(_LEADS, leadId, attachment.FileName, attachment.File, access_token));
                    details.AppendFormat("Filename: \t{0}", attachment.FileName);
                }

                var taskResult = await Task.WhenAll(taskList);
                var markedAttachmentList = attachmentList.ToList();
                for (int i = 0; i < taskResult.Length; i++)
                {
                    markedAttachmentList[i].Remarks = taskResult[i].Message;
                    details.AppendFormat("UploadFilesAsync response \n\tLeadID: {0}", JsonConvert.SerializeObject(taskResult[i]));
                }

                Log.Info(string.Format("\n UploadFilesAsync response \n\t : {0}\n", details.ToString()));

            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n UploadFilesAsync exception \n\t : {0}\n", ex.ToString()));
            }
        }
    }
}