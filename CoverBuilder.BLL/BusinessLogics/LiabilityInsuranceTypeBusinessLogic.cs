﻿using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using CoverBuilder.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoverBuilder.BLL.BusinessLogics
{
    public class LiabilityInsuranceTypeBusinessLogic : ILiabilityInsuranceTypeBusinessLogic
    {
        public List<LiabilityInsuranceType> GetLiabilityInsuranceType()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.LiabilityInsuranceType.Find(act => act.IsActive == true).ToList();
            }
        }

        public LiabilityInsuranceType GetLiabilityInsuranceTypeById(int insuranceId)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.LiabilityInsuranceType.Find(x => x.InsuranceId == insuranceId).Where(act => act.IsActive == true).FirstOrDefault();
            }
        }

        public async Task<LiabilityInsuranceType> GetLiabilityInsuranceTypeByIdAsync(int liabilityInsuranceId)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                var findTask = uow.LiabilityInsuranceType.FindAsync(act => act.IsActive == true && act.InsuranceId == liabilityInsuranceId).ConfigureAwait(false);
                var result = await findTask;

                return result.FirstOrDefault();
            }
        }

        public void UpdateAssignedUnderwriter(LiabilityInsuranceType liabilityInsuranceType)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                uow.LiabilityInsuranceType.UpdateLiabilityTypeUw(liabilityInsuranceType);
                uow.Commit();
            }
        }

        public List<LiabilityInsuranceType> GetWithUnderwriter()
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                var liabilityTypeList = GetLiabilityInsuranceType();
                return liabilityTypeList
                    .Join(uow.Underwriter.FindAll().AsEnumerable(), p => p.UnderwriterId, u => u.UserId, (p, u) => new { p, u })
                    .Where(pu => pu.u.IsActive == true)
                    .Select(pu => new LiabilityInsuranceType
                    {
                        InsuranceId = pu.p.InsuranceId,
                        LiabilityInsurance = pu.p.LiabilityInsurance,
                        Code = pu.p.Code,
                        OrderId = pu.p.OrderId,
                        UnderwriterId = pu.p.UnderwriterId,
                        Class = pu.p.Class,
                        Underwriter = pu.u,
                        DisplayName = pu.p.DisplayName
                    }).ToList();
            }
        }

        public LiabilityInsuranceType GetWithUnderwriterById(int insuranceId)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                var liabilityTypeList = GetLiabilityInsuranceType();
                return liabilityTypeList
                    .Join(uow.Underwriter.FindAll().AsEnumerable(), p => p.UnderwriterId, u => u.UserId , (p, u) => new { p, u })
                    .Where(pu => pu.p.InsuranceId == insuranceId && pu.u.IsActive == true)
                    .Select(pu => new LiabilityInsuranceType
                    {
                        InsuranceId = pu.p.InsuranceId,
                        LiabilityInsurance = pu.p.LiabilityInsurance,
                        Code = pu.p.Code,
                        OrderId = pu.p.OrderId,
                        UnderwriterId = pu.p.UnderwriterId,
                        Class = pu.p.Class,
                        Underwriter = pu.u
                    }).FirstOrDefault();
            }
        }
    }
}
