﻿using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using Entity = CoverBuilder.Entities.Entities;
//using CoverBuilder.ZohoStore.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CoverBuilder.BLL.BusinessLogics
{
    public class PremiumDetailBusinessLogic : IPremiumDetailBusinessLogic
    {
        public List<Entity.PremiumDetail> GetPremiumDetail()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.PremiumDetail.FindAll().ToList();
            }
        }

        public int CreatePremiumDetail(List<Entity.PremiumDetail> premDtlList)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                foreach (var item in premDtlList)
                {
                    UoW.PremiumDetail.Add(item);
                }
                UoW.Commit();
            }

            return premDtlList.Count;
        }

        public async Task<int> CreatePremiumDetailAsync(List<Entity.PremiumDetail> premDtlList, System.Threading.CancellationToken cancellationToken)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork())
                {
                    foreach (var item in premDtlList)
                    {
                        uow.PremiumDetail.Add(item);
                    }
                    var commitTask = uow.CommitAsync(cancellationToken);
                    return await commitTask;
                }
            }
            catch (Exception)
            {
                //log ex
                throw;
            }
        }

        public int SetToInactive(Guid userId)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string query = "usp_UpdatePremiumDetail";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@UserInfoId",
                    Value = userId.ToString(),
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                return uow.ExecuteNonQuery(query, param, CommandType.StoredProcedure);
            }
        }
        
        public List<Entity.PremiumDetail> GetPremiumDetailByReferenceId(Guid id)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.PremiumDetail.Find(x => x.HeaderId == id && x.IsActive == true).ToList();
            }
        }

    }
}
