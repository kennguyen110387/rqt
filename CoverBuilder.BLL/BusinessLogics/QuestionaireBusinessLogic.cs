﻿using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using CoverBuilder.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoverBuilder.BLL.BusinessLogics
{
    public class QuestionaireBusinessLogic : IQuestionaireBusinessLogic
    {
        public List<Questionaire> GetQuestionaire()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.Questionaire.FindAll().ToList();
            }
        }
        public Questionaire SelectQuestoinaire(int id)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.Questionaire.Find(x => x.QuestionId == id).FirstOrDefault();
            }
        }
        
        public List<Questionaire> GetByInsuranceId(int liabilityInsuranceId)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                return uow.Questionaire.Find(x => x.InsuranceId == liabilityInsuranceId && x.IsActive == true).ToList();
            }
        }
     
    }
}
