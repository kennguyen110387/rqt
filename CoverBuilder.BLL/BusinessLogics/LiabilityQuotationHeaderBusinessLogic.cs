﻿using CoverBuilder.BLL.Interfaces;
using CoverBuilder.DAL;
using CoverBuilder.DAL.Infrastructure;
using Entity = CoverBuilder.Entities.Entities;
using CoverBuilder.ZohoStore.Contract;
//using CoverBuilder.ZohoStore.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoverBuilder.Entities.Entities;
using System.Data;
using System.Threading;
namespace CoverBuilder.BLL.BusinessLogics
{
    public class LiabilityQuotationHeaderBusinessLogic : ILiabilityQuotationHeaderBusinessLogic
    {
        public List<Entity.LiabilityQuotationHeader> GetUserInfo()
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.LiabilityQuotationHeader.FindAll().ToList();
            }
        }
        
        public Guid CreateLiabilityQuotationHeader(Entity.LiabilityQuotationHeader header)
        {
            LiabilityInsuranceTypeBusinessLogic blInsuranceType = new LiabilityInsuranceTypeBusinessLogic();
            header.IsSynced = false;
            var insuranceType = blInsuranceType.GetLiabilityInsuranceType().Where(x => x.InsuranceId == header.InsuranceId).FirstOrDefault();
            var referenceNumber = String.Format("{0}-{1}", insuranceType.Code,header.Id.ToString().Replace("-", "").ToUpper().Substring(0, 10));
            header.ReferenceNumber = referenceNumber;
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                UoW.LiabilityQuotationHeader.Add(header);
                UoW.Commit();
            }

            return header.Id;
        }

        public async Task<int> CreateLiabilityQuotationHeaderAsync(LiabilityQuotationHeader liabilityQuotationHeader, CancellationToken cancellationToken)
        {
            IUnitOfWork uow = new UnitOfWork();
            LiabilityInsuranceTypeBusinessLogic blInsuranceType = new LiabilityInsuranceTypeBusinessLogic();
            liabilityQuotationHeader.IsSynced = false;
            var insuranceType = blInsuranceType.GetLiabilityInsuranceType().Where(x => x.InsuranceId == liabilityQuotationHeader.InsuranceId).FirstOrDefault();
            var referenceNumber = String.Format("{0}-{1}", insuranceType.Code, liabilityQuotationHeader.Id.ToString().Replace("-", "").ToUpper().Substring(0, 10));
            liabilityQuotationHeader.ReferenceNumber = referenceNumber;

            if (!liabilityQuotationHeader.HasTriggerReferral.Value && liabilityQuotationHeader.Status == "New")
            {
                liabilityQuotationHeader.QuoteSent = DateTime.Now;
            }
            else if (liabilityQuotationHeader.Status == "Requote")
            {
                liabilityQuotationHeader.QuoteSent = liabilityQuotationHeader.QuoteSent.Value;
            }

            try
            {
                uow.LiabilityQuotationHeader.Add(liabilityQuotationHeader);

                return await uow.CommitAsync(cancellationToken);
            }
            catch (Exception)
            {
                //log
                throw;
            }
            finally
            {
                uow.Dispose();
            }            
        }
        public int UpdateStatus(string id, string status)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string query = "usp_UpdateStatus";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@id",
                    Value = id,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@status",
                    Value = status,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                
                return uow.ExecuteNonQuery(query, param, CommandType.StoredProcedure);
            }
        }
        public int UpdateStatus(string id, string status, string comments)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string query = "usp_UpdateStatus";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@id",
                    Value = id,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@status",
                    Value = status,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@comments",
                    Value = comments,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                return uow.ExecuteNonQuery(query, param, CommandType.StoredProcedure);
            }
        }

        public int UpdatePeriod(string id, string status, string periodFrom, string periodTo)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string query = "usp_UpdatePeriod";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@id",
                    Value = id,
                    SqlDbType = System.Data.SqlDbType.VarChar
                }); 
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@status",
                    Value = status,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@periodFrom",
                    Value = Convert.ToDateTime(periodFrom),
                    SqlDbType = System.Data.SqlDbType.DateTime
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@periodTo",
                    Value = Convert.ToDateTime(periodTo),
                    SqlDbType = System.Data.SqlDbType.DateTime
                });
                return uow.ExecuteNonQuery(query, param, CommandType.StoredProcedure);
            }
        }

        public int UpdateOwnerId(string underwriterId, string referenceNumber)
        {
            var userInfo = GetUserInfoByReferenceNumber(referenceNumber);
            userInfo.UnderwriterId = underwriterId;
            int result;
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                UoW.LiabilityQuotationHeader.UpdateUnderwriterId(userInfo);
                result = UoW.Commit();
            }
            return result;
        }
        public int UpdateIsSynced(string referenceNumber)
        {
            var userInfo = GetUserInfoByReferenceNumber(referenceNumber);
            userInfo.IsSynced = true;
            int result;
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                UoW.LiabilityQuotationHeader.UpdateIsSynced(userInfo);
                result = UoW.Commit();
            }
            return result;
        }
        public int UpdateRemarks(string id, string remarks)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string query = "usp_UpdateRemarks";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@id",
                    Value = id,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@remarks",
                    Value = remarks,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                return uow.ExecuteNonQuery(query, param, CommandType.StoredProcedure);
            }
        }
        public int UpdatePremiumInfo(PremiumQuotationInfo premInfo, string referenceNo)
        {
            using(IUnitOfWork uow = new UnitOfWork())
            {
                string query = "usp_UpdatePremiumInfo";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
               param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@referenceNo",
                    Value = referenceNo,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@brokerComm",
                    Value = premInfo.BrokerComm,
                    SqlDbType = System.Data.SqlDbType.Decimal
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@brokerCommGst",
                    Value = premInfo.BrokerCommGst,
                    SqlDbType = System.Data.SqlDbType.Decimal
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@stampTax",
                    Value = premInfo.StampTax,
                    SqlDbType = System.Data.SqlDbType.Decimal
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@finalPremium",
                    Value = premInfo.FinalPremium,
                    SqlDbType = System.Data.SqlDbType.Decimal
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@underwriterFeeGst",
                    Value = premInfo.UnderwriterFeeGst,
                    SqlDbType = System.Data.SqlDbType.Decimal
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@underwriterFee",
                    Value = premInfo.UnderwriterFee,
                    SqlDbType = System.Data.SqlDbType.Decimal
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@premiumSubTotal",
                    Value = premInfo.PremiumSubTotal,
                    SqlDbType = System.Data.SqlDbType.Decimal
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@premiumBase",
                    Value = premInfo.PremiumBase,
                    SqlDbType = System.Data.SqlDbType.Decimal
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@gst",
                    Value = premInfo.Gst,
                    SqlDbType = System.Data.SqlDbType.Decimal
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@endorsements",
                    Value = premInfo.Endorsements == null ? string.Empty : premInfo.Endorsements,
                    SqlDbType = System.Data.SqlDbType.NVarChar
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@excess",
                    Value = premInfo.StdExcess == null ? string.Empty : premInfo.StdExcess,
                    SqlDbType = System.Data.SqlDbType.NVarChar
                });
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@businessDesc",
                    Value = premInfo.BusinessDesc == null ? string.Empty : premInfo.BusinessDesc,
                    SqlDbType = System.Data.SqlDbType.NVarChar
                });
                return uow.ExecuteNonQuery(query, param, CommandType.StoredProcedure);
            } 

	
        }
        
        public Entity.LiabilityQuotationHeader GetUserInfoById(Guid userId)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.LiabilityQuotationHeader.Find(x => x.Id == userId).FirstOrDefault();
            }
        }
        public Entity.LiabilityQuotationHeader GetUserInfoByReferenceNumber(string refNum)
        {
            using (IUnitOfWork UoW = new UnitOfWork())
            {
                return UoW.LiabilityQuotationHeader.Find(x => x.ReferenceNumber == refNum).FirstOrDefault();
            }
        }
        public List<UserDetails> GetReportInfo(Guid userId, int insuranceId)
        {
            var liabilityInsuranceType = new LiabilityInsuranceTypeBusinessLogic()
                .GetLiabilityInsuranceType()
                .Where(x => x.InsuranceId == insuranceId).FirstOrDefault();
            var userInfo = GetUserInfoById(userId);
            List<UserDetails> userDetail = new List<UserDetails>();

            userDetail.Add(new UserDetails
            {
                UserInfoId = userInfo.Id
             ,
                InsuranceId = userInfo.InsuranceId.Value
             ,
                FullName = userInfo.FullName
             ,
                LiabilityInsurance = liabilityInsuranceType.LiabilityInsurance
             ,
                InsuredFullName = userInfo.InsuredFullName
             ,
                BrokerageName = userInfo.BrokerageName
             ,
                SituationStreetNo = userInfo.SituationStreetNo
             ,
                SituationStreetName = userInfo.SitutationStreetName
             ,
                SituationSuburb = userInfo.SituationSuburb
             ,
                SituationState = userInfo.SituationState
             ,
                SituationPostCode = userInfo.SituationPostCode
             ,
                InsurancePeriodFrom = userInfo.InsurancePeriodFrom.Value
             ,
                InsurancePeriodTo = userInfo.InsurancePeriodTo.Value
             ,
                DateCreated = userInfo.CreateDate.Value
            });

            return userDetail;
        }
        
        public void UpdateLeadId(Guid id, string zohoId)
        {
            var model = new LiabilityQuotationHeader
            {
                Id = id,
                LeadId = zohoId
            };

            using (var uow = new UnitOfWork())
            {
                uow.LiabilityQuotationHeader.UpdateLiabilityZohoLeadId(model);
                uow.Commit();
            }
        }
        
        public void UpdateQuotationHeader(LiabilityQuotationHeader userInfo)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                uow.LiabilityQuotationHeader.Update(userInfo);
                uow.Commit();
            }
        }

        public void UpdateInsuranceDates(LiabilityQuotationHeader model)
        {

            using (var uow = new UnitOfWork())
            {
                uow.LiabilityQuotationHeader.UpdateLiabilityInsuranceDates(model);
                uow.Commit();
            }
        }

        public async Task<int> UpdateQuotationHeaderAsync(LiabilityQuotationHeader userInfo, CancellationToken token)
        {
            userInfo.ModifiedDate = DateTime.UtcNow;
            using (IUnitOfWork uow = new UnitOfWork())
            {
                uow.LiabilityQuotationHeader.Update(userInfo);
                return await uow.CommitAsync(token);
            }
        }

        public DataSet GetQuotationResponse(string referenceNumber)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string storedProc = "usp_GetGenericQuestions";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@ReferenceNumber",
                    Value = referenceNumber,
                    SqlDbType = System.Data.SqlDbType.VarChar
                });

                return uow.ExecuteQuery(storedProc, param, CommandType.StoredProcedure);
            }
        }

        public DataSet GetEndorsementList(Guid id)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string storedProc = "usp_GetEndorsements";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@userId",
                    Value = id,
                    SqlDbType = System.Data.SqlDbType.UniqueIdentifier
                });

                return uow.ExecuteQuery(storedProc, param, CommandType.StoredProcedure);
            }
        }

        public DataSet GetReferralTriggers(Guid id)
        {
            using (IUnitOfWork uow = new UnitOfWork())
            {
                string storedProc = "usp_GetReferralTriggers";
                List<System.Data.SqlClient.SqlParameter> param = new List<System.Data.SqlClient.SqlParameter>();
                param.Add(new System.Data.SqlClient.SqlParameter()
                {
                    ParameterName = "@userId",
                    Value = id,
                    SqlDbType = System.Data.SqlDbType.UniqueIdentifier
                });

                return uow.ExecuteQuery(storedProc, param, CommandType.StoredProcedure);
            }
        }
    }
}
