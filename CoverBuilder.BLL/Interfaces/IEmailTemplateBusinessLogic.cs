﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoverBuilder.Entities.Entities;

namespace CoverBuilder.BLL.Interfaces
{
    public interface IEmailTemplateBusinessLogic
    {
        List<EmailTemplate> GetEmailTemplate();
        List<EmailTemplate> GetEmailTemplateByTemplateID(int ID);
        int UpdateCc(int templateId, string mailCc);
        int UpdateBcc(int templateId, string mailBcc);
    }
}
