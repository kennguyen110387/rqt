﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZohoModel = CoverBuilder.ZohoStore.Model;
using Entity = CoverBuilder.Entities.Entities;

namespace CoverBuilder.BLL.BusinessLogics
{
    public interface IZohoService
    {
        Task UpdateLeadStatusAsync(Entity.PremiumStatusInfo statusInfo, string zohoId, string access_token);
        Task UpdateLeadFinancesAsync(Entity.PremiumFinancialInfo financialInfo, string zohoId, string access_token);
        Task<Entity.LiabilityQuotationData> SyncZohoData(string referenceId, string access_token);
        Task<string> SearchCustomModuleByReferenceIdAsync(string referenceId, string access_token);
        Task<string> SaveLeadsAsync(Entity.LiabilityQuotationHeader liabilityQuotationHeaderModel, List<Entity.LiabilityQuestionnaireResult> situation, string access_token);
        Task<string> UpdateLeadsAsync(Entity.LiabilityQuotationHeader liabilityQuotationHeaderModel, List<Entity.LiabilityQuestionnaireResult> situation, string access_token);
        Task<string> SaveCustomModuleAsync(Entity.LiabilityQuotationHeader liabilityQuotationHeaderModel, List<Entity.LiabilityQuestionnaireResult> situation, string access_token);
        Task<string> UpdateCustomModuleAsync(string customModuleZohoId, Entity.LiabilityQuotationHeader liabilityQuotationHeaderModel, List<Entity.LiabilityQuestionnaireResult> situation, string access_token);
        Task<List<Entity.PremiumQuotationInfo>> SearchPremiumInfoByReferenceId(string referenceId, string access_token);
        Task UploadFilesAsync(string leadId, IEnumerable<Entity.Attachment> filePathList, string access_token);
        List<Entities.Entities.Underwriter> GetZohoUsers();
        Task SaveRationaleNoteAsync(string leadId, string ratingRationale, string access_token);

    }
}
