﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoverBuilder.Entities.Entities;
using System.Threading.Tasks;
using System.Threading;

namespace CoverBuilder.BLL.Interfaces
{
    public interface IEmailTransactionBusinessLogic
    {
        List<EmailTransaction> GetEmailTransaction();
        void CreateEmailTransaction(EmailTransaction vm);
        void CreateEmailTransactions(List<EmailTransaction> emailTransactionList);
        void UpdateEmailStatus(int emailId, string emailStatus);
        Task<int> CreateEmailTransactionsAsync(List<EmailTransaction> emailTransactionList, CancellationToken cancellationToken);
    }
}
