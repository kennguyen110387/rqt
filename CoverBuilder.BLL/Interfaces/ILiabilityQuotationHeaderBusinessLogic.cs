﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoverBuilder.Entities.Entities;
using System.Threading.Tasks;
using System.Threading;
using System.Data;

namespace CoverBuilder.BLL.Interfaces
{
    public interface ILiabilityQuotationHeaderBusinessLogic
    {
        List<LiabilityQuotationHeader> GetUserInfo();
        Guid CreateLiabilityQuotationHeader(LiabilityQuotationHeader userInfo);
        Task<int> CreateLiabilityQuotationHeaderAsync(LiabilityQuotationHeader liabilityQuotationHeader, CancellationToken cancellationToken);
        void UpdateQuotationHeader(LiabilityQuotationHeader userInfo);
        Task<int> UpdateQuotationHeaderAsync(LiabilityQuotationHeader userInfo, CancellationToken token);
        LiabilityQuotationHeader GetUserInfoById(Guid userId);
        LiabilityQuotationHeader GetUserInfoByReferenceNumber(string refNum);
        int UpdateStatus(string id, string status);
        int UpdateStatus(string id, string status, string comments);
        List<UserDetails> GetReportInfo(Guid userId, int insuranceId);
        int UpdatePremiumInfo(PremiumQuotationInfo premInfo, string referenceNo);
        int UpdateRemarks(string id, string remarks);
        void UpdateLeadId(Guid userId, string zohoId);
        int UpdateOwnerId(string underwriterId, string referenceNumber);
        void UpdateInsuranceDates(LiabilityQuotationHeader userInfo);
        DataSet GetQuotationResponse(string referenceNumber);
        DataSet GetEndorsementList(Guid id);
        DataSet GetReferralTriggers(Guid id);
        int UpdateIsSynced(string referenceNumber);
        int UpdatePeriod(string id, string status, string periodFrom, string periodTo);
    }
}
