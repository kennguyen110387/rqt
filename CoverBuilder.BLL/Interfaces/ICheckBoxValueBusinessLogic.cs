﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoverBuilder.Entities.Entities;

namespace CoverBuilder.BLL.Interfaces
{
    public interface ICheckBoxValueBusinessLogic
    {
        List<CheckBoxValue> GetCheckBoxValue();
        List<CheckBoxValue> GetCheckBoxValueByInsuranceID(int ID);
    }
}
