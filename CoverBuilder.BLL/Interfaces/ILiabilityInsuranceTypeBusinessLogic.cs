﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoverBuilder.Entities.Entities;
using System.Threading.Tasks;

namespace CoverBuilder.BLL.Interfaces
{
    public interface ILiabilityInsuranceTypeBusinessLogic
    {
        List<LiabilityInsuranceType> GetLiabilityInsuranceType();
        LiabilityInsuranceType GetLiabilityInsuranceTypeById(int insuranceId);
        Task<LiabilityInsuranceType> GetLiabilityInsuranceTypeByIdAsync(int liabilityInsuranceId);
        List<LiabilityInsuranceType> GetWithUnderwriter();
        LiabilityInsuranceType GetWithUnderwriterById(int liabilityInsuranceId);
        void UpdateAssignedUnderwriter(LiabilityInsuranceType liabilityInsuranceType);
    }
}
