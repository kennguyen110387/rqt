﻿namespace CoverBuilder.BLL.Interfaces
{
    using CoverBuilder.Entities.Entities;
    using Hangfire.Server;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    
    public interface IWorkerService
    {
        Task SaveAsync(string referenceNumber, string quoteStatus, PerformContext context, string access_token);
        Task UploadFilesAsync(string referenceNumber, List<Attachment> fileUploadList, string access_token);
        void SaveRationaleNoteAsync(string referenceNumber, string access_token);

    }
}
