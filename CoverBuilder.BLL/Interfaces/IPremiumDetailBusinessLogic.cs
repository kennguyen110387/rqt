﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoverBuilder.Entities.Entities;
using System.Threading.Tasks;
using System.Threading;

namespace CoverBuilder.BLL.Interfaces
{
    public interface IPremiumDetailBusinessLogic
    {
        List<PremiumDetail> GetPremiumDetail();
        int CreatePremiumDetail(List<PremiumDetail> premDtlList);
        Task<int> CreatePremiumDetailAsync(List<PremiumDetail> premDtlList, CancellationToken cancellationToken);
        int SetToInactive(Guid userId);

        List<PremiumDetail> GetPremiumDetailByReferenceId(Guid id);
    }
}
