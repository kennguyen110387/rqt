﻿namespace CoverBuilder.BLL.Interfaces
{
    using System.Collections.Generic;
    using CoverBuilder.Entities.Entities;

    public interface ILookupBusinessLogic
    {
        List<Lookup> GetReferenceByCode(string code);
        Lookup GetByCodeAndText(string code, string text);
        Lookup GetByValue(string value);
    }
}
