﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoverBuilder.Entities.Entities;

namespace CoverBuilder.BLL.Interfaces
{
    public interface IUnderwriterBusinessLogic
    {
        List<Underwriter> GetUnderwriterList();
        Underwriter GetById(int underwriterId);
        Underwriter GetByExecutiveCode(string code);
        string IgnoreEmailIfPrimaryUnderwriter(int insuranceId, string code);
        Underwriter GetByEmail(string email);
        int SetUnderwriterInactive();
        int CreateUnderwriter(List<Underwriter> UnderwriterList);
        Underwriter GetByUserId(string underwriterId);
    }
}
