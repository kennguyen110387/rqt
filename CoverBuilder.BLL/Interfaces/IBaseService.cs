﻿namespace CoverBuilder.BLL.Interfaces
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface IBaseService<T> : IDisposable
        where T : class
    {
        void Add(T newEntity);
        void Remove(T newEntity);
        IQueryable<T> Find(Expression<Func<T, bool>> predicate);
        Task<IQueryable<T>> FindAsync(Expression<Func<T, bool>> predicate);
        IQueryable<T> FindAll();
        void Update(T newEntity);
        void UpdateLiabilityZohoLeadId(T entity);
        IQueryable<T> AdvanceSearch(Expression<Func<T, bool>> predicate);
    }
}
