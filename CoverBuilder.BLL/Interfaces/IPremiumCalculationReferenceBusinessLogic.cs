﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoverBuilder.Entities.Entities;

namespace CoverBuilder.BLL.Interfaces
{
    public interface IPremiumCalculationReferenceBusinessLogic
    {
        List<PremiumCalculationReference> GetPremiumCalculationReference();
        PremiumCalculationReference GetPremiumBaseByLandSize(int sizeOfLand, int insuranceTypeId);
        PremiumCalculationReference GetByGroupId(int groupId, int insuranceTypeId);
        List<PremiumCalculationReference> GetByInsuranceTypeId(int insuranceTypeId);
        List<PremiumCalculationReference> GetByConditionType(string conditionType);
    }
}
