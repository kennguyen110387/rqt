﻿using CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.Enums;

namespace CoverBuilder.BLL.Interfaces
{
    public interface IUserService
    {
        User GetByUsername(string userName);
        bool Authenticate(string userName, string password);
        UserVerificationResult Register(string userName, string password);
    }
}
