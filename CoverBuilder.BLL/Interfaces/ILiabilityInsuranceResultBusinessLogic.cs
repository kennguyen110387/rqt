﻿namespace CoverBuilder.BLL.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CoverBuilder.Entities.Entities;
    using System.Threading;

    public interface ILiabilityInsuranceResultBusinessLogic
    {
        List<LiabilityInsuranceResult> GetLiabilityInsuranceResult();
        int CreateLiabilityInsuranceResult(Guid id, int liabilityInsuranceId, List<LiabilityInsuranceResult> LiabilityInsuranceResultViewModel);
        Task<int> CreateLiabilityInsuranceResultAsync(Guid id, int liabilityInsuranceId, List<LiabilityInsuranceResult> LiabilityInsuranceResultViewModel, CancellationToken cancellationToken);
        DataSet GetReportDataSource(Guid userId);
        List<LiabilityInsuranceResult> GetByUserId(Guid id);
        int SetToInactive(Guid userId);
        void SetPolicyDetails(Guid userId);
        DataSet GetFlattenedQuestionnaireResults(Guid id);
        Task<int> UpdateAsync(List<LiabilityInsuranceResult> liabilityResultModel, CancellationToken cancellationToken);
    }
}
