﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoverBuilder.Entities.Entities;

namespace CoverBuilder.BLL.Interfaces
{
    public interface IDropDownValueBusinessLogic
    {
        List<DropDownValue> GetDropDownValue();

        List<DropDownValue> GetDropdownValueByInsuranceID(int insuranceId);
    }
}
