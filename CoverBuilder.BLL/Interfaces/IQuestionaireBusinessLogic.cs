﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoverBuilder.Entities.Entities;

namespace CoverBuilder.BLL.Interfaces
{
    public interface IQuestionaireBusinessLogic
    {
        List<Questionaire> GetQuestionaire();
        Questionaire SelectQuestoinaire(int id);
        List<Questionaire> GetByInsuranceId(int liabilityInsuranceId);
    }
}
