﻿using System.Web;
using System.Web.Optimization;

namespace CoverBuilder
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/mask.js",
                        "~/Scripts/moment.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryajax").Include(
                        "~/Scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.val.date.js",
                        "~/Scripts/jquery.validate.unobstrusive.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            //Datepicker
            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                      "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new StyleBundle("~/Content/datepicker").Include(
                      "~/Content/datepicker.css"));

            //Numeral
            bundles.Add(new ScriptBundle("~/bundles/numeral").Include(
                "~/Scripts/numeral/numeral.js"));

            //General CSS Layouts
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/custom.css"));

            //Data Tables
            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                    "~/Scripts/jquery.dataTables.min.js",
                    "~/Scripts/dataTables.bootstrap.min.js",
                    "~/Scripts/jquery.jeditable.min.js",
                    "~/Scripts/dataTables.buttons.min.js",
                    "~/Scripts/dataTables.select.min.js"                   
                ));

            bundles.Add(new StyleBundle("~/Content/datatable-styles").Include(
                    "~/Content/bootstrap-dataTables.css"
                ));
        }
    }
}
