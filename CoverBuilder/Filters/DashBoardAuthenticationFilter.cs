﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hangfire.Dashboard;
using System.Web;
namespace CoverBuilder.Filters
{
    public class DashBoardAuthenticationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated) return false;

            return true;
        }
    }
}
