﻿using Microsoft.Office.Interop.Word;
namespace CoverBuilder.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Web;
    using System.Web.Mvc;
    using Microsoft.Reporting.WebForms;
    using CoverBuilder.Entities.Entities;
    using System.Data;
    using CoverBuilder.BLL.Interfaces;
    using CoverBuilder.BLL.BusinessLogics;
    using System.IO;
    using System.Runtime.InteropServices;

    public static class ReportHelper
    {

        private static DataSet dsRpt;

        static void PremiumSubreportProcessingEventHandler(object sender, SubreportProcessingEventArgs e)
        {
            //e.DataSources.Add(new ReportDataSource("dsPremiumData",  dsRpt));

            for (int i = 0; i < dsRpt.Tables.Count; i++)
            {
                var table = dsRpt.Tables[i];
                e.DataSources.Add(new ReportDataSource(table.TableName, table));
            }
        }
        public static byte[] ConvertToByteArray(string bytes)
        {
            string strBytes = bytes;
            string incoming = strBytes.Replace('_', '/').Replace('-', '+');
            switch (strBytes.Length % 4)
            {
                case 2: incoming += "=="; break;
                case 3: incoming += "="; break;
            }
            return Convert.FromBase64String(incoming);
        }
        public static ReportViewer LoadPremiumReport(ReportModel rptModel)
        {
            rptModel.ReportFileName = string.Format("rpt{0}.rdlc", rptModel.ReportName.Replace(" ", "").Replace("&", "And").Replace("'", "").Replace("(Individual)", "").Replace("/Pubs", ""));
            //rptModel.ReportPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("{0}{1}", ConfigurationManager.AppSettings["ReportFolderPath"].ToString(), rptModel.ReportFileName));
            rptModel.ReportPath = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("{0}{1}", ConfigurationManager.AppSettings["ReportFolderPath"].ToString(), rptModel.ReportFileName));

            rptModel.ReportName = "PremiumReport";
            ReportViewer rptViewer = new ReportViewer();
            rptViewer.LocalReport.DisplayName = rptModel.ReportName;
            rptViewer.ProcessingMode = ProcessingMode.Local;
            rptViewer.LocalReport.ReportPath = rptModel.ReportPath;
            rptViewer.LocalReport.ReportEmbeddedResource = rptModel.ReportFileName;
            dsRpt = rptModel.ReportDataSource;
            for (int i = 0; i < rptModel.ReportDataSource.Tables.Count; i++)
            {
                var table = rptModel.ReportDataSource.Tables[i];
                rptViewer.LocalReport.DataSources.Add(new ReportDataSource(table.TableName, table));
            }

            // Subreport
            rptViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(PremiumSubreportProcessingEventHandler);

            return rptViewer;
        }
        public static byte[] ExportPdf(ReportViewer rptViewer)
        {
            //rptViewer.LocalReport.DisplayName = model.ReportName;
            //rptViewer.LocalReport.ReportPath = model.ReportPath;
            //model.DataSource = new ReportDataSource("dsBudgetAccomodation", pModel);
            //model.DataSource1 = new ReportDataSource("dsBudgetAccomodation_UserInfo", uModel);
            //rptViewer.LocalReport.DataSources.Add(model.DataSource);
            //rptViewer.LocalReport.DataSources.Add(model.DataSource1);
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
            return bytes;
        }
        public static MemoryStream GetRawPdfFile(string reportBytes)
        {
            try
            {
                string strBytes = reportBytes;
                string incoming = strBytes.Replace('_', '/').Replace('-', '+');
                switch (strBytes.Length % 4)
                {
                    case 2: incoming += "=="; break;
                    case 3: incoming += "="; break;
                }
                byte[] bytes = Convert.FromBase64String(incoming);
                MemoryStream ms = new MemoryStream(bytes);
                return ms;
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }

        }
        public static string GeneratePremiumReport(Guid userInfoId, string reportName)
        {
            char[] padding = { '=' };

            ReportModel rptModel = new ReportModel();
            ILiabilityInsuranceResultBusinessLogic loadReportObj = new LiabilityInsuranceResultBusinessLogic();
            System.Data.DataSet ds = loadReportObj.GetReportDataSource(userInfoId);
            ds.Tables[0].TableName = "dsPremiumReport_HeaderInfo";
            ds.Tables[1].TableName = "dsPremiumReport_DetailInfo";
            ds.Tables[2].TableName = "dsPremiumReport_Referral";
            ds.Tables[3].TableName = "dsPremiumReport_Breakdown";
            // New Data Sets [User Response/Details]
            ds.Tables[5].TableName = "dsPremiumReport_UserDetails";
            ds.Tables[4].TableName = "dsPremiumReport_UserResponse";
            ds.Tables[6].TableName = "dsPremiumReport_PolicyEndorsements";
            ds.Tables[7].TableName = "dsPremiumReport_GenericDetails";
            rptModel.ReportDataSource = ds;
            rptModel.ReportName = reportName;

            var rawReport = ReportHelper.ExportPdf(ReportHelper.LoadPremiumReport(rptModel));

            string premiumReport = Convert.ToBase64String(rawReport).TrimEnd(padding).Replace('+', '-').Replace('/', '_');

            return (premiumReport);
        }


        #region PROTO: Word To PDF 

        public static byte[] GeneratePremiumReportV2(string referenceNo)
        {
            //string root = @"C:\Users\k.calvendra\Documents\[03] Projects\Guinea Fox\";
            string path = @"C:\Users\k.calvendra\Documents\[03] Projects\Guinea Fox\Word Automation.docx";
            byte[] bytes = null;

            if (!File.Exists(path))
            {
                Console.WriteLine("File does not exist");
                Console.ReadLine();
            }
            else
            {
                Application wordApp = new Application();
                Document wordDocument = wordApp.Documents.Open(path, ReadOnly: false);
                //Document doc = app.Documents.Open("file.doc", ref missing, true);

                Range wordRange = wordDocument.Bookmarks["tableLeadResponse"].Range;
                //Table wordTable;

                try
                {
                    ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();
                    Guid userInfoId = headerRepo.GetUserInfoByReferenceNumber(referenceNo).Id;

                    // Get Datasets/Tables
                    ILiabilityInsuranceResultBusinessLogic loadReportObj = new LiabilityInsuranceResultBusinessLogic();
                    System.Data.DataSet ds = loadReportObj.GetReportDataSource(userInfoId);
                    ds.Tables[0].TableName = "dsPremiumReport_HeaderInfo";
                    ds.Tables[1].TableName = "dsPremiumReport_DetailInfo";
                    ds.Tables[2].TableName = "dsPremiumReport_Referral";
                    ds.Tables[3].TableName = "dsPremiumReport_Breakdown";
                    // New Data Sets [User Response/Details]
                    ds.Tables[5].TableName = "dsPremiumReport_UserDetails";
                    ds.Tables[4].TableName = "dsPremiumReport_UserResponse";
                    ds.Tables[6].TableName = "dsPremiumReport_PolicyEndorsements";
                    ds.Tables[7].TableName = "dsPremiumReport_GenericDetails";


                    // Fill Data Table
                    DataTable dt = ds.Tables[4];
                    //wordTable = wordDocument.Tables.Add(wordRange, dt.Rows.Count, dt.Columns.Count);
                    WordTableGenerator(dt, wordDocument, wordRange, 2);

                    //wordDocument.Activate();
                    // --> Replace Text by Content Title
                    UpdateContentControlByTitle(wordDocument, "txtName", "Akstar Fox");
                    UpdateContentControlByTitle(wordDocument, "txtPhone", "0917 595 1090");

                    // Other Table Method
                    //Microsoft.Office.Interop.Word.Table oTable = wordDocument.Tables.Add(wordRange, 10, 2);
                    //oTable.Borders.Enable = 1; // set type of border (solid=1)
                    //oTable.Range.ParagraphFormat.SpaceAfter = 6;
                    //int r, c;
                    //string strText;
                    //for (r = 1; r <= 10; r++)
                    //{
                    //    for (c = 1; c <= 2; c++)
                    //    {
                    //        strText = "r" + r + "c" + c;
                    //        oTable.Cell(r, c).Range.Text = strText;
                    //        oTable.Cell(r, c).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
                    //        oTable.Cell(r, c).VerticalAlignment = Microsoft.Office.Interop.Word.WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                    //    }
                    //}
                    //oTable.Rows[1].Range.Font.Bold = 1;
                    //oTable.Rows[1].Range.Font.Italic = 1;
                    //oTable.Rows[1].SetHeight(50, Microsoft.Office.Interop.Word.WdRowHeightRule.wdRowHeightExactly);

                    // --> Save As using Temp Name
                    object missing = System.Reflection.Missing.Value;
                    object fileName = Path.GetTempFileName();

                    //wordDocument.SaveAs2(root + "testWordTemplate_" + DateTime.Now.ToString("yyyyMMddHHmmss"), WdSaveFormat.wdFormatPDF);
                    wordDocument.SaveAs2(fileName, WdSaveFormat.wdFormatPDF);

                    // --> QUIT AND RELEASE: Word Application and COM objects
                    object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
                    wordDocument.Application.Quit(ref saveChanges, ref missing, ref missing);
                    Marshal.ReleaseComObject(wordDocument);

                    // --> CONVERT: Bytes
                    //bytes = System.IO.File.ReadAllBytes(tempfile);
                    bytes = System.IO.File.ReadAllBytes((string)fileName);

                }
                catch (ArgumentNullException ex)
                {
                    // Close All Application Instances
                    //wordDocument.Close(false);
                    //wordDocument = null;
                    //wordApp = null;

                    throw ex;
                }
            }


            return bytes;
        }

        public static void UpdateContentControlByTitle(Document wordDocument, string ccTitle, string ccValue)
        {
            foreach (ContentControl cc in wordDocument.ContentControls)
            {
                if (cc.Title == ccTitle)
                    cc.Range.Text = ccValue;
            }
        }

        public static void WordTableGenerator(DataTable dt, Document wordDocument, Range wordRange, int fixedColCount)
        {
            int rowCount = dt.Rows.Count;
            int colCount = fixedColCount;

            Table wordTable = wordDocument.Tables.Add(wordRange, rowCount, colCount);
            wordTable.Borders.Enable = 1; // set type of border (solid=1)
            wordTable.AllowAutoFit = true;

            // User Response DataTable
            for (int col = 0; col < colCount; col++)
            {
                for (int row = 0; row < rowCount; row++)
                {
                    //wordTable.Rows[i].Cells[j].AddParagraph().AppendText(dt.Rows[i][j].ToString());
                    wordTable.Rows[row + 1].Cells[col + 1].Range.Text = dt.Rows[row][col].ToString();
                }

            }
            //return wordTable;
        }

        #endregion

        //public static string ConvertPdfBytesToString(ReportViewer rptViewer)
        //{
        //    string cs64b = Convert.ToBase64String(ExportPdf).TrimEnd(padding).Replace('+', '-').Replace('/', '_');
        //    return cs64b;
        //}
    }
}