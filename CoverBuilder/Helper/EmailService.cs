﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Newtonsoft.Json;

using CoverBuilder.BLL.BusinessLogics;
using CoverBuilder.BLL.Interfaces;
using CoverBuilder.Common.Email;
using CoverBuilder.Controllers;
using CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.Enums;
using CoverBuilder.Entities.ViewModels;
using CoverBuilder.Common.Logging.NLog;

using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Net.Mail;
using Hangfire;
using CoverBuilder.Common.Logging;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Net.Http;
using System.Globalization;
using System.Runtime.Caching;


namespace CoverBuilder.Helper
{
    public static class EmailService
    {
        private static ILogger logger = new NLogger();
        [SkipWhenPreviousJobIsRunning]
        public static void CheckAndQueueMail()
        {
            //logger.Info(string.Format("\n Check Pending Email and Retry status... \n"));

            IEmailTransactionBusinessLogic transactionRepo = new EmailTransactionBusinessLogic();

            // Pending Mails
            var pendingMails = transactionRepo.GetEmailTransaction().Where(w => w.Status == "Pending");
            foreach (var mail in pendingMails)
            {
                    //BackgroundJob.Enqueue(() => EmailService.SendEmail(mail.Id, mail.ReferenceNumber, mail.TemplateId));
                    EmailService.SendEmail(mail.Id, mail.ReferenceNumber, mail.TemplateId);
            }

            // Mail Transactions to Retry
            var retryMails = transactionRepo.GetEmailTransaction().Where(w => w.Status == "Retry");
            foreach (var mail in retryMails)
            {
                //BackgroundJob.Enqueue(() => EmailService.SendEmail(mail.Id, mail.ReferenceNumber, mail.TemplateId));
                EmailService.SendEmail(mail.Id, mail.ReferenceNumber, mail.TemplateId);
            }

        }
        public static byte[] GenerateEmailPdf(string html)
        {
            float dpi = 150;
            float width = (dpi * 4.25f);
            float height = (dpi * 5.5f);
            String htmlText = html.ToString();
            Document document = new Document(new Rectangle(width, height));
            using (MemoryStream memstream = new MemoryStream())
            {
                PdfWriter writer = PdfWriter.GetInstance(document, memstream);
                document.Open();
                StringReader sr = new StringReader(htmlText);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
                document.Close();

                return memstream.ToArray();
            }
        }
        public static void SendEmail(int emailId, string referenceNo, int? templateId)
        {

            int TemplateId = 0;

            // SET: Email Template
            if (templateId.HasValue)
                TemplateId = (int)templateId;

            IWorkerService workerService = new WorkerService();

            try
            {
                // Email Transaction Repo
                IEmailTransactionBusinessLogic transactionRepo = new EmailTransactionBusinessLogic();

                // Email Details
                IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
                var emailInfo = templateRepo.GetEmailTemplateByTemplateID(TemplateId).FirstOrDefault();
                var emailTemplate = emailInfo.Body;
                var emailSubject = emailInfo.Subject;
                var withFileAttachment = emailInfo.WithAttachment;


                // Client Details
                ILiabilityQuotationHeaderBusinessLogic userRepo = new LiabilityQuotationHeaderBusinessLogic();
                var userInfo = userRepo.GetUserInfoByReferenceNumber(referenceNo.ToString());

                // UPDATE: Mail Status and Exit Method if UserInfo == null
                if (userInfo == null)
                {
                    // UPDATE: Email Status - NO DATA RECORD
                    var currentEmailStatus = transactionRepo.GetEmailTransaction().Where(w => w.Id == emailId).Select(s => s.Status).FirstOrDefault();
                    transactionRepo.UpdateEmailStatus(emailId, "No Data Record");
                    return;
                }

                Guid userId = userInfo.Id; //Guid
                var insuranceId = userInfo.InsuranceId == 99 ? 13 : userInfo.InsuranceId;
                var referenceNumber = userInfo.ReferenceNumber;
                var fullInsuredName = userInfo.InsuredFullName;
                var clientName = userInfo.FullName;
                var clientEmail = userInfo.Email;
                var underwriterComments = userInfo.UnderwriterComments;

                // Underwriter Details
                ILiabilityInsuranceTypeBusinessLogic insuranceTypeRepo = new LiabilityInsuranceTypeBusinessLogic();
                IUnderwriterBusinessLogic underwriterRepo = new UnderwriterBusinessLogic();

                var initialUnderwriterInfo = underwriterRepo.GetByUserId(userInfo.UnderwriterId);

                var initialUnderwriterEmail = initialUnderwriterInfo.Email;
                var secondaryUnderwriterEmail = userInfo.UnderwriterEmail;

                // CALL: Email Handler
                IEmailHandler emailHandler = new EmailHandler();

                // SET: Recipient Mails
                emailHandler.From = emailInfo.From;
                emailHandler.CarbonCopy = emailInfo.Cc;
                emailHandler.BlindCarbonCopy = emailInfo.Bcc;

                if (TemplateId == 2 || TemplateId == 7) //Templates for Underwriter
                    emailHandler.To = initialUnderwriterEmail.ToString();
                else
                    emailHandler.To = clientEmail.ToString();

                // CREATE: Email Body
                var body = emailTemplate;

                // Replacements
                body = body.Replace("{{Your Name}}", clientName);
                body = body.Replace("{{Reference Number}}", referenceNumber);
                body = body.Replace("+DeclineReason+", underwriterComments);
                emailSubject = emailSubject.Replace("{{Full Insured Name}}", fullInsuredName);
                emailSubject = emailSubject.Replace("{{Reference Number}}", referenceNumber);

                //var projectLink = System.Configuration.ConfigurationManager.AppSettings["projectLink"];
                //body = body.Replace("{{Project Link}}", projectLink);

                // Declare File Attachment Variable
                var file = new List<System.Net.Mail.Attachment>();
                MemoryStream msQuote = new MemoryStream();
                MemoryStream msProposal = new MemoryStream();

                // CHECK: If email has a required file attachment
                if (withFileAttachment == true)
                {
                    // GET: Insurance Type
                    var liabilityType = insuranceTypeRepo.GetLiabilityInsuranceType().Where(w => w.InsuranceId == insuranceId).Select(s => s.LiabilityInsurance).SingleOrDefault();

                    // GENERATE: Quote Document
                    var reportName = TemplateId == 4 ? liabilityType.ToString() : "BusinessSchedule"; //BusinessSchedule can be replaced with a database value TO:DO
                    string reportBytes = ReportHelper.GeneratePremiumReport(userId, reportName);
                    msQuote = ReportHelper.GetRawPdfFile(reportBytes);

                    // -> ADD: Attachment List
                    var fileAttachmentName = TemplateId == 4 ? "PremiumQuote.pdf" : "BusinessSchedule.pdf"; //BusinessSchedule can be replaced with a database value TO:DO
                    file.Add(new System.Net.Mail.Attachment(msQuote, fileAttachmentName, "application/pdf"));

                    // GENERATE: Proposal Document
                    //reportBytes = ReportHelper.GeneratePremiumReport(userId, "ConfirmationProposal");
                    //msProposal = ReportHelper.GetRawPdfFile(reportBytes);

                    // -> ADD: Attachment List
                    //fileAttachmentName = "ConfirmationProposal.pdf";
                    //file.Add(new System.Net.Mail.Attachment(msProposal, fileAttachmentName, "application/pdf"));

                    if (insuranceId == 17)
                    {
                        var attachmentPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/Attachments/Sompo International Wording 2019.pdf");
                        file.Add(new System.Net.Mail.Attachment(attachmentPath));
                    }
                    else
                    {
                        var attachmentPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/Attachments/RYNO General and  Products Liability Policy Wording - RY.GPL.LLO.V260916.pdf");
                        file.Add(new System.Net.Mail.Attachment(attachmentPath));
                    }
                }

                // SEND: Mail
                logger.Info(string.Format("\n Start Sending Email \n\t : {0}\n", emailSubject.ToString()));
                var htmlPdfEmail = emailHandler.Send(emailSubject.ToString(), body.ToString(), false, file);

                // UPLOAD: Upload File Attachments to Zoho
                // Proposal Document
                if (TemplateId == 1 || TemplateId == 3 || TemplateId == 4 || TemplateId == 7)
                {
                    List<Entities.Entities.Attachment> attachmentList = new List<Entities.Entities.Attachment>();
                    var emailBytes = GenerateEmailPdf(htmlPdfEmail);
                    attachmentList.Add(new Entities.Entities.Attachment { File = emailBytes, LiabilityQuotationHeaderId = userInfo.Id, Remarks = "Email PDF", FileName = String.Format("{0}{1}.pdf", emailInfo.Description, DateTime.Now.ToString("yyyyMMddHHmmss")) });

                    ObjectCache cache = MemoryCache.Default;
                    var zohoAgent = new ZohoStoreV2.Service.ZohoAgentV2();
                    var access_token = cache["access_token"] as string;
                    if (access_token == null)
                    {
                        access_token = zohoAgent.GenerateTokenv2();
                        cache.Set("access_token", access_token, DateTime.Now.AddMinutes(55));
                    }

                    workerService.UploadFilesAsync(userInfo.ReferenceNumber, attachmentList, access_token);
                }

                // CLOSE: Memory Streams
                msQuote.Close();
                msProposal.Close();

                // UPDATE: Email Status - SENT
                transactionRepo.UpdateEmailStatus(emailId, "Sent");
            }
            catch (Exception ex)
            {
                logger.Log(LogTypeEnum.Error, referenceNo, "Caught something", ex);
                // UPDATE: Email Status
                IEmailTransactionBusinessLogic transactionRepo = new EmailTransactionBusinessLogic();
                var currentEmailStatus = transactionRepo.GetEmailTransaction().Where(w => w.Id == emailId).Select(s => s.Status).FirstOrDefault();

                if (currentEmailStatus.ToString() == "Retry")
                    transactionRepo.UpdateEmailStatus(emailId, "Failed");
                else
                    transactionRepo.UpdateEmailStatus(emailId, "Retry");

                throw ex;
            }
        }
    }
}
