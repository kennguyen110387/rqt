﻿namespace CoverBuilder.Helper
{
    using System.Configuration;
    using System.Data.Entity.Core.EntityClient;
    using CoverBuilder.Utilities;

    public static class ConnectionHelper
    {
        const string ConnectionStringName = "CoverBuilderEntities";
        const string ConnectionStringDb = "CoverBuilder";

        public static string GetConnectionString()
        {
            string currentConnectionString = new ConfigHelper().GetConnectionString(ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString, ConnectionStringDb);
            EntityConnectionStringBuilder cBuilder = new EntityConnectionStringBuilder(currentConnectionString);
            cBuilder.Metadata = string.Empty;

            return cBuilder.ProviderConnectionString;
        }
    }
}