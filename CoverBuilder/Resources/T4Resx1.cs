﻿using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.IO;
using System.Resources;


namespace CoverBuilder.MVC.Resources
{
    public static class ResourcesManager
    {

        public static string GetString(string key) 
        {
            var ctx = HttpContext.Current;
            if (ctx.Cache["__Resources"] == null)
            {
                var filePath = Path.Combine(ctx.Request.PhysicalApplicationPath + "bin\\Resources\\", "CoverBuilderResource.resx");

                Dictionary<string, string> data = new Dictionary<string, string>();
                using (ResXResourceSet resxSet = new ResXResourceSet(filePath))
                {
                    foreach (DictionaryEntry item in resxSet)
                    {
                        data.Add((string)item.Key, (string)item.Value);
                    }
                }
                ctx.Cache.Insert("__Resources", data, new CacheDependency(filePath));

            }
            var resources = (Dictionary<string, string>)ctx.Cache["__Resources"];
            if (resources.ContainsKey(key))
                return resources[key];
            else
                return key;
        }
    }
}
