﻿function ajaxindicatorstart(text) {
    if (!text) {
        text = "Processing Request...";
    }

    if (jQuery('body').find('#resultLoading').attr('id') != 'resultLoading') {
        jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src=' + loaderGIFpath + '><div>' + text + '</div></div><div class="bg"></div></div>');

    }

    jQuery('#resultLoading').css({
        'width': '100%',
        'height': '100%',
        'position': 'fixed',
        'z-index': '10000000',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto'
    });

    jQuery('#resultLoading .bg').css({
        'background': '#000000',
        'opacity': '0.7',
        'width': '100%',
        'height': '100%',
        'position': 'absolute',
        'top': '0'
    });

    jQuery('#resultLoading>div:first').css({
        'width': '250px',
        'height': '75px',
        'text-align': 'center',
        'position': 'fixed',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto',
        'font-size': '16px',
        'z-index': '10',
        'color': '#ffffff'

    });

    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop() {
    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}

function successNotify(_title, _text) {
    PNotify.prototype.options.styling = "bootstrap3";
    new PNotify({
        title: _title,
        text: _text,
        type: 'success',
        cornerclass: 'ui-pnotify-sharp'
    });
}

function errorNotify(_title, _text) {
    PNotify.prototype.options.styling = "bootstrap3";
    new PNotify({
        title: _title,
        text: _text,
        type: 'error',
        cornerclass: 'ui-pnotify-sharp'
    });
}

function infoNotify(_title, _text) {
    PNotify.prototype.options.styling = "bootstrap3";
    new PNotify({
        title: _title,
        text: _text,
        type: 'info',
        cornerclass: 'ui-pnotify-sharp'
    });
}

function confirmNotify(_title, _text, callback) {
    PNotify.prototype.options.styling = "bootstrap3";
    var confirmNotice = new PNotify({
        title: _title,
        text: _text,
        icon: 'glyphicon glyphicon-question-sign',
        hide: false,
        cornerclass: 'ui-pnotify-sharp',
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        }
    });

    confirmNotice.get().on('pnotify.confirm', function () {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    });
    confirmNotice.get().on('pnotify.cancel', function () {
        return false;
    });
}

function setDatepickers(classname) {
    $("." + classname).each(function () {
        //$(this).datepicker({
        //    showOn: "both",
        //    autoclose: true,
        //    buttonImage: "../Images/Calendar.png",
        //    buttonImageOnly: true,
        //    //format: 'dd/mm/yyyy',
        //dateFormat: "dd/MM/yyyy"
        //    changeMonth: true,
        //    changeYear: true,
        //onClose: function (selectedDate) {
        //}
        //});
        $(this).datepicker({ format: "dd/mm/yyyy" });
    });
}


(function ($) {
    $.fn.turnOffValidation = function (form) {
        var settings = form.validate().settings;

        for (var ruleIndex in settings.rules) {
            delete settings.rules[ruleIndex];
        }
    };
})(jQuery);


function updateValidators() {
    $("form").removeData("validator");
    $("form").removeData("unobtrusiveValidation");
    $.validator.unobtrusive.parse("form");
}