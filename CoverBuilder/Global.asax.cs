﻿using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CoverBuilder.Common.Logging;
using CoverBuilder.Common.Logging.NLog;
using CoverBuilder.Helper;
using NLog.Config;



namespace CoverBuilder
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("aspnet-request-ip", typeof(AspNetRequestIpLayoutRenderer));
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("utc_date", typeof(UtcDateRenderer));

            ILogger logger = new NLogger();
            logger.Info("Hangfire starting...");

            HangfireBootstrapper.Instance.Start();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            ILogger logger = new NLogger();
            Exception exception = Server.GetLastError();
            logger.Log(Entities.Enums.LogTypeEnum.Fatal, null, exception.InnerException != null ? exception.InnerException.ToString() : exception.ToString(), exception);
            Server.ClearError();
            Session["StackTrace"] = exception.StackTrace;
            Response.Redirect("~/Error/Error");
        }

        protected void Application_End()
        {
            HangfireBootstrapper.Instance.Stop();
        }
    }
}
