﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoverBuilder.Filters;
using Hangfire;
using Microsoft.Owin;
using Owin;
//using System.Runtime.Caching;
using CoverBuilder.Helper;


[assembly: OwinStartupAttribute(typeof(CoverBuilder.Startup))]
namespace CoverBuilder
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var requestContext = HttpContext.Current.Request.RequestContext;
            var dashboardOptions = new DashboardOptions
            {
                AppPath = new UrlHelper(requestContext).Action("Index", new { area="Admin", controller = "Manage" }),
                Authorization = new[] { new DashBoardAuthenticationFilter() }
            };
            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 3 });
            GlobalJobFilters.Filters.Add(new SkipWhenPreviousJobIsRunningAttribute());
            app.UseHangfireDashboard("/hangfire", dashboardOptions);

            //InitializeRecurringJobs
            var manager = new RecurringJobManager();
            manager.AddOrUpdate("email-recurring-job", () => EmailService.CheckAndQueueMail(), Cron.Minutely);
        }
    }
}