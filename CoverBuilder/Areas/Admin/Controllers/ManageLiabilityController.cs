﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoverBuilder.Filters;
using CoverBuilder.BLL.BusinessLogics;
using CoverBuilder.BLL.Interfaces;
using CoverBuilder.Entities.Enums;
using CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.ViewModels;
using CoverBuilder.Helper;

namespace CoverBuilder.Areas.Admin.Controllers
{
    public partial class ManageLiabilityController : Controller
    {
        //
        // GET: /Admin/ManageLiability/
        [HandleAuthorize]
        public virtual ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult Index(UnderwriterLiabilityViewModel model)
        {
            if (ModelState.IsValid)
            {
                ILiabilityInsuranceTypeBusinessLogic service = new LiabilityInsuranceTypeBusinessLogic();
                service.UpdateAssignedUnderwriter(model.LiabilityInsuranceType);
            }
            return View();
        }

        public virtual JsonResult GetLiability() 
        {
            
            IUnderwriterBusinessLogic uwService = new UnderwriterBusinessLogic();
            ILiabilityInsuranceTypeBusinessLogic service = new LiabilityInsuranceTypeBusinessLogic();
            var list = service.GetWithUnderwriter();

            var model = new UnderwriterLiabilityViewModel
            {
                LiabilityInsuranceList = list,
                UnderwriterList = uwService.GetUnderwriterList().Where(x =>  x.IsActive == true && x.Role == "Underwriter" && !string.IsNullOrEmpty(x.ExecutiveCode)).Select(p => new SelectListItem
                {
                    Text = p.Name,
                    Value = p.UserId
                }).ToList()
            };


            
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        
	}
}