﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoverBuilder.Entities.ViewModels;
using CoverBuilder.BLL.Interfaces;
using CoverBuilder.BLL.BusinessLogics;
using System.Web.Security;
using CoverBuilder.Entities.Enums;
using System.Configuration;


namespace CoverBuilder.Areas.Admin.Controllers
{
    public partial class SecurityController : Controller
    {
        public virtual ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Login(LoginViewModel model)
        {
            
            if (ModelState.IsValid)
            {
                IUserService userService = new UserService();
                IZohoService zohoService = new ZohoService();
               
                var isAuthorize = userService.Authenticate(model.UserId, model.Password);

                if (isAuthorize)
                {
                    FormsAuthentication.SetAuthCookie(model.UserId, model.RememberMe);
                    return RedirectToAction("EmailTransactions", "Manage");
                }
                else
                {
                    ModelState.AddModelError("", "Username/Password is not valid!");
                }        
            }

            return View(model);
        }

        public virtual ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        public virtual ActionResult AddUser()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult AddUser(RegisterViewModel model)
        {
            IUserService userService = new UserService();
            if (!ModelState.IsValid) return View(model);
            var result = userService.Register(model.Username, model.Password);
            switch (result)
            {
                case UserVerificationResult.Success:
                    return RedirectToAction("Login");
                case UserVerificationResult.Failed:
                    break;
                default:
                    ModelState.AddModelError("", "Registration error.");
                    return View(model);
            }
            return View(model);
        }
	}
}