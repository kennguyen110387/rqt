﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoverBuilder.Filters;
using CoverBuilder.BLL.BusinessLogics;
using CoverBuilder.BLL.Interfaces;
using CoverBuilder.Entities.Enums;
using CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.ViewModels;
using CoverBuilder.Helper;


namespace CoverBuilder.Areas.Admin.Controllers
{
    public partial class ManageController : Controller
    {
        [HandleAuthorize]
        public virtual ActionResult Index()
        {
            ViewBag.Title = "Admin Section";

            return View();
        }
        [HandleAuthorize]
        public virtual ActionResult EmailQuote()
        {
            ViewBag.Title = "Email Requested";
            return View();
        }
        [HandleAuthorize]
        public virtual ActionResult EmailTransactions()
        {
            IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
            IEmailTransactionBusinessLogic emailTransactionRepo = new EmailTransactionBusinessLogic();
            var transactionList = emailTransactionRepo.GetEmailTransaction();

            return View(transactionList);
        }
        public virtual ActionResult Underwriters()
        {
            IUnderwriterBusinessLogic underwriterRepo = new UnderwriterBusinessLogic();
            var underwriterList = underwriterRepo.GetUnderwriterList().Where(w => (bool)w.IsActive == true);

            return View(underwriterList);
        }
        public virtual ActionResult ResendMail(int transactionId)
        {
            IEmailTransactionBusinessLogic emailTransactionRepo = new EmailTransactionBusinessLogic();
            emailTransactionRepo.UpdateEmailStatus(transactionId, "Pending");

            return RedirectToAction("EmailQuote", "Manage");
        }
        public virtual ActionResult Test()
        {
            ViewBag.Title = "Development Platform";
            return View();
        }
        [HttpGet]
        public virtual ActionResult _EditFactors()
        {
           return PartialView();
        }
        [HandleAuthorize]
        public virtual ActionResult ConfigFormula()
        {
            ConfigFormulaViewModels model = new ConfigFormulaViewModels();
            IPremiumCalculationReferenceBusinessLogic referenceRepo = new PremiumCalculationReferenceBusinessLogic();
            var formulaFactorsList = referenceRepo.GetPremiumCalculationReference().Select(item => new PremiumCalculationReference
            {
                ID = item.ID,
                InsuranceTypeID = item.InsuranceTypeID,
                ConditionType = item.ConditionType,
                ConditionA = item.ConditionA,
                ConditionB = item.ConditionB,
                LiabilityLimit = item.LiabilityLimit,
                Rate = item.Rate,
            });

            model.FormulaFactors = formulaFactorsList;


            var insuranceTypes = new LiabilityInsuranceTypeBusinessLogic().GetLiabilityInsuranceType();
            model.InsuranceTypes = insuranceTypes.Select(ins => new SelectListItem
            {
                Text = ins.LiabilityInsurance,
                Value = ins.InsuranceId.ToString()
            }).ToList();


            ViewBag.Title = "Configure Formula Variables";
            return View(model);
        }

        #region MANAGE: Email Templates/Addresses

        public virtual ActionResult UpdateMailAddress()
        {
            IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
            var templateList = templateRepo.GetEmailTemplate().Where(w => w.IsActive == true);

            return View(templateList);
        }


        public virtual ActionResult DisplayModalCc(int templateId)
        {
            IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
            EmailTemplate model = templateRepo.GetEmailTemplateByTemplateID(templateId).FirstOrDefault();
            return PartialView("_UpdateCc", model);
        }

        public virtual JsonResult UpdateCc(int templateId, string CcAddress)
        {
            IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
            JsonResponseResult result = new JsonResponseResult();

            var updateResult = templateRepo.UpdateCc(templateId, CcAddress);
            if (updateResult > 0)
            {
                result.Status = "Mail/s updated successfully!";
            }
            else
            {
                result.Status = "Something went wrong while updating the mail/s.";
            }

            return Json(result.Status, JsonRequestBehavior.AllowGet);
            //return JsonResult("<script language='javascript' type='text/javascript'>alert ('Mails Updated Successfully ');</script>");
        }

        public virtual ActionResult DisplayModalBcc(int templateId)
        {
            IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
            EmailTemplate model = templateRepo.GetEmailTemplateByTemplateID(templateId).FirstOrDefault();
            return PartialView("_UpdateBcc", model);
        }

        public virtual JsonResult UpdateBcc(int templateId, string BccAddress)
        {
            IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
            JsonResponseResult result = new JsonResponseResult();

            var updateResult = templateRepo.UpdateBcc(templateId, BccAddress);
            if (updateResult > 0)
            {
                result.Status = "Mail/s updated successfully!";
            }
            else
            {
                result.Status = "Something went wrong while updating the mail/s.";
            }

            return Json(result.Status, JsonRequestBehavior.AllowGet);
            //return JsonResult("<script language='javascript' type='text/javascript'>alert ('Mails Updated Successfully ');</script>");
        }

        #endregion
    }
}