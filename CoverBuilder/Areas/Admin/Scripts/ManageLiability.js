﻿var MyModel = (function () {
    //Constructor
    function MyModel() {
    }
    var liabilityList = {};
    var liabilityId = '';
    var liabilityName = '';
    var liabilityCode = '';
    var underwriter = '';
    var liabilityTable = {};
    var underwriterList = {};
    
    function MapDataToFields() {
        $('#hdnInsuranceId').val(parseInt(liabilityId));
        $('#liability-type').val(liabilityName);
        $('#liability-code').val(liabilityCode);
        $('#ddlUnderwriters').val(underwriter);
    };
    function RefreshList() {
        $.ajax({
            url: root + "Admin/ManageLiability/GetLiability",
            dataType: 'json',
            async: false,
            success: function (data) {
                liabilityList = data;
                underwriterList = liabilityList.UnderwriterList;

            }
        });
    };
    function ClearFields() {
        $('[type="text"]').each(function () {
            $(this).val('');
        });
    };
    function drawLiabilityTable() {
        debugger
        liabilityTable = null;
        liabilityTable = $("#liability-table").DataTable({
            data: liabilityList.LiabilityInsuranceList,
            pageLength: 20,
            select: {
                style: 'single'
            },
            bDestroy: true,
            columns: [
              { title: "Liability Type", data: "DisplayName", },
              { title: "Liability Code", data: "Code" },
              { title: "Underwriter", data: "Underwriter.Name" },
              { title: "Executive Code", data: "Underwriter.ExecutiveCode" },
              { title: "Email", data: "Underwriter.Email" }
            ],
            dom: "<'row'<'#filters.col-sm-5'><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>",
            "drawCallback": function (settings) {
                $('#liability-table tr').on('click', function () {
                    if ($(this).hasClass('selected')) {
                        $(this).removeClass('selected');
                    }
                    else {
                        liabilityTable.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                    }

                    var selectedRow = $("#liability-table tr.selected");
                    var rowdata = liabilityTable.row(selectedRow).data();
                    debugger
                    if (rowdata != null) {
                        liabilityId = rowdata.InsuranceId;
                        liabilityName = rowdata.DisplayName;
                        liabilityCode = rowdata.Code;
                        underwriter = rowdata.Underwriter.Id;
                        MapDataToFields();
                    }
                    else {
                        ClearFields();
                    }

                    console.log(rowdata);
                })
            }
        });
    };

    MyModel.prototype.submit = function () {
        showLoader();
        var formData = new FormData();
        formData.append('model.LiabilityInsuranceType.InsuranceId', liabilityId);
        formData.append('model.LiabilityInsuranceType.UnderwriterId', $('#ddlUnderwriters').val()); 
        $.ajax({
            url: root + "Admin/ManageLiability/Index",
            type: 'POST',
            async: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            data: formData,
        });
        
        RefreshList();
        drawLiabilityTable();
        setTimeout(function () {
            killLoader();
        }, 1000);
    };
    MyModel.prototype.render = function () {
        debugger
        showLoader();
        RefreshList();
        drawLiabilityTable();
        
        var underwriterListDom = $("#ddlUnderwriters");
        $.each(underwriterList, function (index, v) {
            underwriterListDom.append($('<option></option>').attr('value', v.Value).text(v.Text));
        });
        killLoader();


    };
    return MyModel;
})();
$(document).ready(function () {
    var model = new MyModel();
    model.render();
    $('#save').on('click', function () {
        model.submit();
    })
});
