﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CoverBuilder.BLL.BusinessLogics;
using CoverBuilder.BLL.Interfaces;
using CoverBuilder.Entities;
using CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.Enums;
using CoverBuilder.Entities.ViewModels;
using CoverBuilder.Extensions;
using CoverBuilder.Helper;
using Hangfire;
using Hangfire.States;
using Newtonsoft.Json;
using System.IO;
using CoverBuilder.Common.Logging;
using CoverBuilder.Common.Logging.NLog;
using NLog.Config;
using System.Web.Caching;
using System.Runtime.Caching;


namespace CoverBuilder.Controllers
{
    public partial class QuotationController : Controller
    {

        //private static readonly ILog Log = LogManager.GetLogger(typeof(QuotationController));
        private static readonly ILogger Log = new NLogger();

        static readonly char[] padding = { '=' };
        internal const string STAMPTAXRATE = "STAMPTAXRATE";
        private const string MBBACCOMMODATIONTYPE = "MOTELSANDBB";

        public LiabilityQuotationHeader QuotationHeader
        {
            get
            {
                return (LiabilityQuotationHeader)Session["QuotationHeader"];
            }
        }

        #region **************************************** PUBLIC FUNCTIONS ****************************************

        public List<LiabilityInsuranceResult> ConvertPropertyOwnerViewModel(List<PropertyOwnerSituationDetailViewModel> propOwnerViewModel, Guid id)
        {
            List<LiabilityInsuranceResult> liabilityResult = new List<LiabilityInsuranceResult>();
            foreach (var item in propOwnerViewModel.Select((value, i) => new { i, value }))
            {
                foreach (var prop in item.value.GetType().GetProperties())
                {
                    var type = prop.Name;
                    var value = prop.GetValue(item.value, null);
                    var trigger = false;
                    var questionId = string.Empty;
                    switch (type)
                    {
                        case "StreetName":
                            questionId = "71";
                            break;
                        case "StreetNumber":
                            questionId = "70";
                            break;
                        case "Suburb":
                            questionId = "72";
                            break;
                        case "State":
                            questionId = "73";
                            break;
                        case "PostalCode":
                            questionId = "74";
                            break;
                        case "Activities":
                            questionId = "226";
                            break;
                        case "BuildingValue":
                            questionId = "75";

                            break;
                        case "TenantOccupation":
                            questionId = "117";
                            break;
                        case "RentalIncome":
                            questionId = "118";
                            break;
                        case "Unoccupied":
                            questionId = "119";
                            break;
                        default:
                            break;
                    }
                    if (type == "SituationNumber")
                    {
                    }
                    else
                    {

                        if (type == "Activities")
                        {
                            var checkboxes = value as List<SelectListItemCheckboxes>;
                            var SelectedCheckBoxValuesIncludedActivities = checkboxes.Where(x => x.Selected == true).ToList();
                            foreach (var activity in SelectedCheckBoxValuesIncludedActivities)
                            {
                                //Determine Trigger if Checkbox Value = None of the above
                                if (activity.Text.ToUpper().Trim() == "NONE OF THE ABOVE")
                                {
                                    liabilityResult.Add(new LiabilityInsuranceResult()
                                    {
                                        UserInfoId = id,
                                        QuestionId = 226,
                                        Response = activity.Text,
                                        GroupId = activity.GroupId
                                    });
                                }
                                else
                                {
                                    liabilityResult.Add(new LiabilityInsuranceResult()
                                    {
                                        UserInfoId = id,
                                        QuestionId = 226,
                                        Response = activity.Text,
                                        GroupId = Convert.ToInt32(activity.GroupId),
                                        IsTriggered = true
                                    });
                                }
                            }

                        }
                        else
                        {
                            liabilityResult.Add(new LiabilityInsuranceResult()
                            {
                                UserInfoId = id,
                                QuestionId = Convert.ToInt32(questionId),
                                Response = value.ToString(),
                                GroupId = (item.i + 1),
                                IsTriggered = trigger
                            });
                        }

                    }
                }



            }
            return liabilityResult;
        }
        public List<LiabilityInsuranceResult> ConvertVacantPropertyViewModel(List<VacantLandPropertySituationModel> vacantPropertyViewModel, Guid id, IDictionary<string, string> situationQuestions)
        {
            List<LiabilityInsuranceResult> liabilityResult = new List<LiabilityInsuranceResult>();

            for (int i = 0; i < vacantPropertyViewModel.Count; i++)
            {
                foreach (var prop in situationQuestions)
                {
                    var type = prop.Key;
                    var value = vacantPropertyViewModel[i].GetType().GetProperty(prop.Key).GetValue(vacantPropertyViewModel[i], null);
                    var trigger = false;
                    var questionId = prop.Value;

                    if (type == "SituationNumber")
                    {
                    }
                    else
                    {

                        if (type == "WallConstruction")
                        {
                            var checkboxes = value as List<SelectListItemCheckboxes>;
                            var SelectedCheckBoxValuesIncludedActivities = checkboxes.Where(x => x.Selected == true).ToList();
                            foreach (var activity in SelectedCheckBoxValuesIncludedActivities)
                            {
                                liabilityResult.Add(new LiabilityInsuranceResult()
                                    {
                                        UserInfoId = id,
                                        QuestionId = Convert.ToInt32(questionId),
                                        Response = activity.Text,
                                        GroupId = Convert.ToInt32(activity.GroupId),
                                        IsTriggered = true
                                    });
                            }

                        }
                        else if (type == "RoofConstrucion")
                        {
                            var checkboxes = value as List<SelectListItemCheckboxes>;
                            var SelectedCheckBoxValuesIncludedActivities = checkboxes.Where(x => x.Selected == true).ToList();
                            foreach (var activity in SelectedCheckBoxValuesIncludedActivities)
                            {
                                liabilityResult.Add(new LiabilityInsuranceResult()
                                {
                                    UserInfoId = id,
                                    QuestionId = Convert.ToInt32(questionId),
                                    Response = activity.Text,
                                    GroupId = Convert.ToInt32(activity.GroupId),
                                    IsTriggered = true
                                });
                            }

                        }
                        else if (type == "FloorConstruction")
                        {
                            var checkboxes = value as List<SelectListItemCheckboxes>;
                            var SelectedCheckBoxValuesIncludedActivities = checkboxes.Where(x => x.Selected == true).ToList();
                            foreach (var activity in SelectedCheckBoxValuesIncludedActivities)
                            {
                                liabilityResult.Add(new LiabilityInsuranceResult()
                                {
                                    UserInfoId = id,
                                    QuestionId = Convert.ToInt32(questionId),
                                    Response = activity.Text,
                                    GroupId = Convert.ToInt32(activity.GroupId),
                                    IsTriggered = true
                                });
                            }
                        }
                        else if (type == "FireProtection")
                        {
                            var checkboxes = value as List<SelectListItemCheckboxes>;
                            var SelectedCheckBoxValuesIncludedActivities = checkboxes.Where(x => x.Selected == true).ToList();
                            foreach (var activity in SelectedCheckBoxValuesIncludedActivities)
                            {
                                liabilityResult.Add(new LiabilityInsuranceResult()
                                {
                                    UserInfoId = id,
                                    QuestionId = Convert.ToInt32(questionId),
                                    Response = activity.Text,
                                    GroupId = Convert.ToInt32(activity.GroupId),
                                    IsTriggered = true
                                });
                            }
                        }
                        else if (type == "SecurityProtection")
                        {
                            var checkboxes = value as List<SelectListItemCheckboxes>;
                            var SelectedCheckBoxValuesIncludedActivities = checkboxes.Where(x => x.Selected == true).ToList();
                            foreach (var activity in SelectedCheckBoxValuesIncludedActivities)
                            {
                                liabilityResult.Add(new LiabilityInsuranceResult()
                                {
                                    UserInfoId = id,
                                    QuestionId = Convert.ToInt32(questionId),
                                    Response = activity.Text,
                                    GroupId = Convert.ToInt32(activity.GroupId),
                                    IsTriggered = true
                                });
                            }
                        }
                        else
                        {
                            if (value == null)
                            {
                                liabilityResult.Add(new LiabilityInsuranceResult()
                                {
                                    UserInfoId = id,
                                    QuestionId = Convert.ToInt32(questionId),
                                    Response = null,
                                    GroupId = (i + 1),
                                    IsTriggered = trigger
                                });
                            }
                            else
                            {
                                liabilityResult.Add(new LiabilityInsuranceResult()
                                {
                                    UserInfoId = id,
                                    QuestionId = Convert.ToInt32(questionId),
                                    Response = value.ToString(),
                                    GroupId = (i + 1),
                                    IsTriggered = trigger
                                });
                            }

                        }

                    }
                }

            }




            return liabilityResult;
        }
        public QuotationController()
        {

        }
        public virtual JsonResult GetQuestions(int insuranceId)
        {
            List<Questionaire> questions = new List<Questionaire>();

            QuestionaireBusinessLogic bl = new QuestionaireBusinessLogic();
            questions = bl.GetByInsuranceId(insuranceId);

            return Json(questions, JsonRequestBehavior.AllowGet);
        }
        public Decimal ApplyDiscount(decimal premiumBase, decimal discountPerc)
        {
            var discount = premiumBase * (discountPerc / 100);
            if (0 == discountPerc)
                return premiumBase;
            else
                return premiumBase - discount;
        }
        private LiabilityQuotationHeader ComputePremium(LiabilityQuotationHeader header, List<PremiumDetail> details)
        {
            // Factor Conditions
            //--> Hotels
            //decimal conditionalUWFee = header.InsuranceId == 17 ? details.Select(s => s.BasePremium).FirstOrDefault() * .1M : 100;
            decimal conditionalUWFee = header.InsuranceId == 17 ? 185 : 100;
            decimal conditionalBrokerCommRate = header.InsuranceId == 17 ? .1M : .15M;

            // DECLARE: Computation Variables
            header.UWFee = conditionalUWFee;
            header.UWFeeGSTRate = 0.1M;
            decimal totalGST = 0;
            decimal totalStampTax = 0;
            decimal totalBasePremium = 0;
            decimal totalUWFee = header.UWFee.Value * header.UWFeeGSTRate.Value;
            decimal totalESL = 0; // <-- NEW: Vacant Land Property
            decimal excess = 0;

            //decimal stampTax;
            //decimal GST;


            // COMPUTE: Premium Details
            foreach (var item in details)
            {
                // NOTE: ALL ESL Values will remain 0 if Insurance Type != 19
                if (header.InsuranceId == 19)
                {
                    item.ESL = item.BasePremium * (decimal)item.ESLRate;
                    item.BasePremium = item.BasePremium + (decimal)item.ESL;
                    totalESL += (decimal)item.ESL;
                }
                //GST = item.BasePremium * item.GSTRate;
                //stampTax = (item.BasePremium + (item.BasePremium * item.GSTRate));
                //item.GST = GST; //Populate GST Detail on Premium Detail Model

                // Reworked
                item.GST = item.BasePremium * item.GSTRate;
                item.StampTax = (item.BasePremium + item.GST) * item.StateTaxRate;

                totalGST += item.GST;
                totalStampTax += item.StampTax;
                totalBasePremium += item.BasePremium;
                excess += item.Excess;
            }

            header.TotalPremiumBase = totalBasePremium;
            header.BrokerComm = header.TotalPremiumBase.Value * conditionalBrokerCommRate;
            header.BrokerCommGST = header.BrokerComm * .1M;
            header.TotalESL = totalESL; // <-- NEW: Vacant Land Property
            header.TotalGST = totalGST;
            header.TotalStampTax = totalStampTax;
            header.TotalUwFee = totalUWFee;
            header.PremiumSubTotal = totalBasePremium + totalESL + totalGST + totalStampTax + header.UWFee + header.TotalUwFee;
            header.TotalFinalPremium = header.PremiumSubTotal - (header.BrokerComm + header.BrokerCommGST);
            header.PolicyExcess = excess.ToString();

            // ADD: Rating Rationale Total Premium [Multiple Situations]
            if (header.InsuranceId == 7 || header.InsuranceId == 10)
                header.RatingRationale = header.RatingRationale + " Total = $" + totalBasePremium.ToString("N2");

            return header;
        }
        private void TagAsForReferral(bool HasReferral)
        {
            var policyInfoReferral = (bool)Session["ShowReferralPage"];

            Session["WithReferral"] = policyInfoReferral ? policyInfoReferral : HasReferral;

            if (QuotationHeader.Status == UserResponseEnum.New.ToString())
            {
                QuotationHeader.HasTriggerReferral = (bool)Session["WithReferral"];
            }
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                if (QuotationHeader.HasTriggerReferral == false)
                {
                    QuotationHeader.HasTriggerReferral = (bool)Session["WithReferral"];
                }
            }

        }
        private string ConvertLiabilityLimit(int liabilityLimit)
        {
            string strLiabilityLimit = "";

            if (liabilityLimit == 5000000)
                strLiabilityLimit = "5m";
            else if (liabilityLimit == 10000000)
                strLiabilityLimit = "10m";
            else if (liabilityLimit == 20000000)
                strLiabilityLimit = "20m";

            return strLiabilityLimit;
        }
        private decimal ComputeSubContractorLoading(decimal annualTurnover, decimal subconPayment)
        {
            decimal subconLoading = subconPayment / annualTurnover;

            return subconLoading;
        }

        //public virtual ActionResult GeneratePremiumReport(Guid userInfoId, string liabilityInsurance)
        //{
        //    ReportModel rptModel = new ReportModel();
        //    ILiabilityInsuranceResultBusinessLogic loadReportObj = new LiabilityInsuranceResultBusinessLogic();
        //    System.Data.DataSet ds = loadReportObj.GetReportDataSource(userInfoId);
        //    ds.Tables[0].TableName = "dsPremiumReport_HeaderInfo";
        //    ds.Tables[1].TableName = "dsPremiumReport_DetailInfo";
        //    ds.Tables[2].TableName = "dsPremiumReport_Referral";
        //    ds.Tables[3].TableName = "dsPremiumReport_Breakdown";
        //    rptModel.ReportDataSource = ds;
        //    rptModel.ReportName = liabilityInsurance;

        //    var rawReport = ReportHelper.ExportPdf(ReportHelper.LoadPremiumReport(rptModel));

        //    TempData["reportAsString"] = Convert.ToBase64String(rawReport).TrimEnd(padding).Replace('+', '-').Replace('/', '_');

        //    return RedirectToAction("ThankYou", "Home");
        //}

        #endregion

        #region **************************************** ACCOMMODATION *******************************************
        public virtual ActionResult Accommodation()
        {
            var InsuranceID = 16;

            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            IDropDownValueBusinessLogic dropDownService = new DropDownValueBusinessLogic();
            ICheckBoxValueBusinessLogic checkBoxService = new CheckBoxValueBusinessLogic();
            var model = new LiabilityInsuranceResultViewModels();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);

            model.StateList = stateTaxRates.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Text
            }).ToList();
            model.DropdownReferenceLookup = dropDownService
                                            .GetDropdownValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 306)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();
            model.CheckboxReferenceLookup = checkBoxService
                                            .GetCheckBoxValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 308)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item.Trim(),
                                                Value = chb.OrderID.ToString()
                                            }).ToList();

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);
                var selectedCheckboxes = model.LiabilityInsuranceListResult
                                              .Where(x => x.QuestionId == 308)
                                              .Select(x => x.Response.Trim()).ToArray();

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == 304)
                                                          .Select(p => p.Response).SingleOrDefault())
                               .FirstOrDefault().Selected = true;
                model.CheckboxReferenceLookup.Where(p => selectedCheckboxes.Contains(p.Text)).ToList().ForEach(cc => cc.Selected = true);
                model.DropdownReferenceLookup.Where(p => p.Value == model.LiabilityInsuranceListResult
                                                                         .Where(x => x.QuestionId == 306)
                                                                         .Select(x => x.Response).FirstOrDefault())
                                             .FirstOrDefault().Selected = true;
            }

            return View(model);
        }
        [HttpPost]
        public virtual async Task<ActionResult> Accommodation(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;

            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();
            IDropDownValueBusinessLogic dropDownService = new DropDownValueBusinessLogic();
            IPremiumCalculationReferenceBusinessLogic referenceRepo = new PremiumCalculationReferenceBusinessLogic();
            StampTaxRateViewModels taxRate = new StampTaxRateViewModels();
            var liabilityQuotationHeaderModel = new LiabilityQuotationHeader();

            //***************************    GET: STATE TAX RATE   ***************************

            var StateQuestionID = 304;
            var taxRateList = taxRate.StampTaxRateList;
            var stateTaxRate = (from tax in taxRateList
                                where tax.Text == (model.LiabilityInsuranceListResult.Where(y => y.QuestionId == StateQuestionID).Select(z => z.Response).FirstOrDefault())
                                select tax.Value
                                    ).FirstOrDefault();

            List<PremiumDetail> premDtls = new List<PremiumDetail>();

            //***************************    APPEND: CHECKBOX VALUES   ***************************

            //Get Checkbox Record where IsChecked = true
            var selectedCheckBoxValuesAccommodationUsed = model.CheckboxReferenceLookup.Where(x => x.Selected == true).ToList();

            //Add Necessary Question ID and User ID on the record
            foreach (var SelectedCheckBoxItem in selectedCheckBoxValuesAccommodationUsed) //Included Activities
            {
                //Determine Trigger if Checkbox Value = None of the above
                if (SelectedCheckBoxItem.Text.Trim() == "NONE OF THE ABOVE")
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = 308,
                        Response = SelectedCheckBoxItem.Text,
                        GroupId = null
                    });
                }
                else
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = 308,
                        Response = SelectedCheckBoxItem.Text,
                        GroupId = null,
                        IsTriggered = true
                    });
                }
            }

            //***************************    CALCULATE: BASE PREMIUM   ***************************

            int insuranceId = 16;
            decimal premiumBase = 0;
            decimal excesses = 0;
            int liabilityLimit = (int)userInfo.LiabilityLimit;
            int NumberOfRooms = Convert.ToInt32(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 307).Select(s => s.Response).FirstOrDefault());
            int AnnualTurnover = Convert.ToInt32(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 310).Select(s => s.Response).FirstOrDefault());
            int yearsInBusiness = Convert.ToInt32(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 311).Select(s => s.Response).FirstOrDefault());

            bool HasReferral = model.NeedsUnderwritingReferral;
            var propertyOwnersLiability = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 309).Select(s => s.Response).FirstOrDefault() == "No" ? 0 : 1;
            decimal withRestaurant = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 313).Select(s => s.Response).FirstOrDefault() == "No" ? 0 : 1;
            string accommodationClass = dropDownService.GetDropdownValueByInsuranceID(insuranceId)
                                         .Where(x => x.QuestionID == 306 && x.Item == model.LiabilityInsuranceListResult.Where(z => z.QuestionId == 306).Select(s => s.Response).FirstOrDefault())
                                            .Select(z => z.Value).FirstOrDefault();
            decimal baseRate = 0;
            decimal minBaseRate = 0;

            string ratingRationale = "";
            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);
            string strMinPremium = "";
            string strDiscount = "";

            switch (accommodationClass)
            {
                //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   BUDGET   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                case "Budget":
                    baseRate = (decimal)referenceRepo.GetByConditionType("Budget").Where(w => w.InsuranceTypeID == insuranceId && w.LiabilityLimit == liabilityLimit).Select(s => s.Rate).FirstOrDefault();
                    minBaseRate = (decimal)referenceRepo.GetByConditionType("BudgetMinimum").Where(w => w.InsuranceTypeID == insuranceId && w.LiabilityLimit == liabilityLimit).Select(s => s.Rate).FirstOrDefault();

                    // Determine Excess Rates
                    var roomsExcessCondition = referenceRepo.GetByConditionType("BudgetExcess").Where(w => w.InsuranceTypeID == insuranceId).Select(s => s);
                    decimal roomExcess = (decimal)roomsExcessCondition.Where(x => NumberOfRooms >= x.ConditionA && NumberOfRooms <= x.ConditionB).Select(s => s.Rate).FirstOrDefault();

                    // Apply Excess Rates
                    excesses = roomExcess;

                    // Total Discounts Calculation
                    decimal totalDiscountPercentage = yearsInBusiness >= 5 ? Convert.ToDecimal(Session["SessionAccumulatedDiscount"]) : 0;
                    var polDiscount = referenceRepo.GetByConditionType("BudgetPolDiscount").Where(w => w.InsuranceTypeID == insuranceId).Select(s => s).FirstOrDefault();
                    totalDiscountPercentage += propertyOwnersLiability == 0 ? polDiscount.Rate.Value : 0;

                    var overRooms = referenceRepo.GetByConditionType("OverRoomDiscount").Where(w => w.InsuranceTypeID == insuranceId).Select(s => s).FirstOrDefault();
                    totalDiscountPercentage += NumberOfRooms >= overRooms.ConditionA ? overRooms.Rate.Value : 0;

                    // Determine Initial Premium
                    premiumBase = baseRate * NumberOfRooms;

                    // Apply Discounts
                    premiumBase = ApplyDiscount(premiumBase, totalDiscountPercentage);
                    if (totalDiscountPercentage != 0)
                        strDiscount = " less " + totalDiscountPercentage.ToString("N0") + "% PO/Clms/Yrs/Ov 20 rooms Discount";

                    // INITIATE: RATING RATIONALE
                    ratingRationale = "BA " + strLiabilityLimit + " $" + baseRate.ToString("N2") + " x " + NumberOfRooms + " room/s" + strDiscount +
                                      " = $" + premiumBase.ToString("N2");

                    // Apply Minimum Base Rate if Applicable
                    if (premiumBase < minBaseRate)
                    {
                        premiumBase = minBaseRate;
                        strMinPremium = " | Min Prem Applied - $" + premiumBase.ToString("N2");
                    }

                    //***************************    GENERATE: RATING RATIONALE   ***************************
                    //BA 5m $93.50 x 21 rm less 25% PO/Clms/Yrs/Ov 20 rms Disc = $1,472.63
                    ratingRationale = ratingRationale + strMinPremium;
                    userInfo.RatingRationale = ratingRationale;


                    //***************************    APPEND: PREMIUM DETAILS ***************************

                    premDtls.Add(new PremiumDetail
                    {
                        BasePremium = premiumBase,
                        GSTRate = 0.1M,
                        StateTaxRate = Convert.ToDecimal(stateTaxRate),
                        Excess = excesses,
                        HeaderId = userInfo.Id,
                        CreateDate = DateTime.Now
                    });

                    liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

                    //***************************    FINALIZE -> SAVE ***************************

                    // Check IsTriggered Column for any Referral Scenarios
                    if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
                    {
                        HasReferral = true;
                    }
                    TagAsForReferral(HasReferral);

                    if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
                        await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
                    else if (QuotationHeader.Status.Contains(UserResponseEnum.Requote.ToString()))
                    {
                        await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
                        SaveRequoteAsync();
                    }

                    break;

                //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   MOTELS   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                case "Motels":

                    // Rationale Factors
                    string strWithRestaurant = withRestaurant == 0 ? " w/o Restaurant " : " w/ Restaurant ";

                    if (withRestaurant == 0)
                    {
                        baseRate = (decimal)referenceRepo.GetByConditionType("MotelsW/OResto").Where(w => w.InsuranceTypeID == insuranceId && w.LiabilityLimit == liabilityLimit).Select(s => s.Rate).FirstOrDefault();
                        minBaseRate = (decimal)referenceRepo.GetByConditionType("MotelsW/ORestoMinimum").Where(w => w.InsuranceTypeID == insuranceId && w.LiabilityLimit == liabilityLimit).Select(s => s.Rate).FirstOrDefault();
                    }
                    else
                    {
                        baseRate = (decimal)referenceRepo.GetByConditionType("MotelsWResto").Where(w => w.InsuranceTypeID == insuranceId && w.LiabilityLimit == liabilityLimit).Select(s => s.Rate).FirstOrDefault();
                        minBaseRate = (decimal)referenceRepo.GetByConditionType("MotelsWRestoMinimum").Where(w => w.InsuranceTypeID == insuranceId && w.LiabilityLimit == liabilityLimit).Select(s => s.Rate).FirstOrDefault();
                    }

                    // Determine Excess Rates
                    var motelExcess = referenceRepo.GetByConditionType("MotelExcess").Where(w => w.InsuranceTypeID == insuranceId).Select(s => s.Rate).FirstOrDefault();
                    excesses = motelExcess.Value;

                    // Total Discounts Calculation
                    decimal totalDiscountPercentageMotel = yearsInBusiness >= 5 ? Convert.ToDecimal(Session["SessionAccumulatedDiscount"]) : 0;
                    var polDiscountMotel = referenceRepo.GetByConditionType("MotelPolDiscount").Where(w => w.InsuranceTypeID == insuranceId).Select(s => s).FirstOrDefault();
                    totalDiscountPercentageMotel += propertyOwnersLiability == 0 ? polDiscountMotel.Rate.Value : 0;

                    // Determine Initial Premium
                    premiumBase = baseRate * NumberOfRooms;

                    // Apply Discounts
                    premiumBase = ApplyDiscount(premiumBase, totalDiscountPercentageMotel);
                    if (totalDiscountPercentageMotel != 0)
                        strDiscount = " less " + totalDiscountPercentageMotel.ToString("N0") + "% PO/Clms Discount";

                    // INITIATE: RATING RATIONALE
                    ratingRationale = "MO " + strLiabilityLimit + strWithRestaurant + "$" + baseRate.ToString("N2") + " x " + NumberOfRooms + " room/s" + strDiscount +
                                      " = $" + premiumBase.ToString("N2");

                    // Apply Minimum Base Rate if Applicable
                    if (premiumBase < minBaseRate)
                    {
                        premiumBase = minBaseRate;
                        strMinPremium = " | Min Prem Applied - $" + premiumBase.ToString("N2");
                    }

                    //***************************    GENERATE: RATING RATIONALE   ***************************
                    //MO 10m w Restaurant $20.65 x 7 rm less 20% PO/Clms Disc = $725.00 Min Prem Applied
                    ratingRationale = ratingRationale + strMinPremium;

                    userInfo.RatingRationale = ratingRationale;


                    //***************************    APPEND: PREMIUM DETAILS ***************************

                    premDtls.Add(new PremiumDetail
                    {
                        BasePremium = premiumBase,
                        GSTRate = 0.1M,
                        StateTaxRate = Convert.ToDecimal(stateTaxRate),
                        Excess = excesses,
                        HeaderId = userInfo.Id,
                        CreateDate = DateTime.Now
                    });

                    liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

                    //***************************    FINALIZE -> SAVE ***************************

                    // Check IsTriggered Column for any Referral Scenarios
                    if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
                    {
                        HasReferral = true;
                    }
                    TagAsForReferral(HasReferral);

                    if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
                        await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
                    else if (QuotationHeader.Status.Contains(UserResponseEnum.Requote.ToString()))
                    {
                        await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                        SaveRequoteAsync();
                    }

                    break;

                //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   BED AND BREAKFAST   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                case "Bnb":
                    baseRate = (decimal)referenceRepo.GetByConditionType("Bnb").Where(w => w.InsuranceTypeID == insuranceId && w.LiabilityLimit == liabilityLimit && NumberOfRooms >= w.ConditionA && NumberOfRooms <= w.ConditionB).Select(s => s.Rate).FirstOrDefault();

                    // Determine Excess Rates
                    var bnbExcess = referenceRepo.GetByConditionType("BnbExcess").Where(w => w.InsuranceTypeID == insuranceId).Select(s => s.Rate).FirstOrDefault();
                    excesses = bnbExcess.Value;

                    // Base Rate
                    premiumBase = baseRate;

                    //***************************    GENERATE: RATING RATIONALE   ***************************
                    //$480.00 Premium for 11-15 Rooms
                    var roomA = referenceRepo.GetByConditionType("Bnb").Where(w => w.InsuranceTypeID == insuranceId && w.LiabilityLimit == liabilityLimit && NumberOfRooms >= w.ConditionA && NumberOfRooms <= w.ConditionB).Select(s => s.ConditionA).FirstOrDefault();
                    var roomB = referenceRepo.GetByConditionType("Bnb").Where(w => w.InsuranceTypeID == insuranceId && w.LiabilityLimit == liabilityLimit && NumberOfRooms >= w.ConditionA && NumberOfRooms <= w.ConditionB).Select(s => s.ConditionB == 999999999999999 ? s.ConditionA : s.ConditionB).FirstOrDefault();

                    string strRange = roomA + "-" + roomB;
                    if (roomB == roomA)
                        strRange = "Greater Than " + roomB;

                    ratingRationale = "BB " + strRange + " Rooms " + strLiabilityLimit + " = $" + premiumBase.ToString("N2");

                    userInfo.RatingRationale = ratingRationale;

                    //***************************    APPEND: PREMIUM DETAILS ***************************

                    premDtls.Add(new PremiumDetail
                    {
                        BasePremium = premiumBase,
                        GSTRate = 0.1M,
                        StateTaxRate = Convert.ToDecimal(stateTaxRate),
                        Excess = excesses,
                        HeaderId = userInfo.Id,
                        CreateDate = DateTime.Now
                    });

                    liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

                    //***************************    FINALIZE -> SAVE ***************************

                    //Check IsTriggered Column for any Referral Scenarios
                    if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
                    {
                        HasReferral = true;
                    }
                    TagAsForReferral(HasReferral);

                    if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
                        await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
                    else if (QuotationHeader.Status.Contains(UserResponseEnum.Requote.ToString()))
                    {
                        await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                        SaveRequoteAsync();
                    }
                    break;
                default:
                    break;
            }
            return RedirectToAction("Thankyou", "Home");

        }
        #endregion

        #region **************************************** CLEANERS ****************************************
        public virtual ActionResult Cleaners()
        {
            ViewBag.Status = "New";
            ViewBag.OtherHasValue = '0';

            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            ICheckBoxValueBusinessLogic checkBoxRepo = new CheckBoxValueBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            LiabilityInsuranceResultViewModels model = new LiabilityInsuranceResultViewModels();
            string[] checkedItems;
            List<LiabilityInsuranceResult> checkedIncludedActivities = new List<LiabilityInsuranceResult>();
            List<SelectListItem> cblCleanersActivitiesIncluded = new List<SelectListItem>();
            var cleaningActivities = checkBoxRepo.GetCheckBoxValueByInsuranceID(2);
            var primaryCleaningActivities = cleaningActivities.Where(w => w.QuestionID == 11);
            var secondaryCleaningActivities = cleaningActivities.Where(w => w.QuestionID == 18);
            var selectedLossOfKeys = string.Empty;
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);

            model.StateList = stateTaxRates.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Text
            }).ToList();

            IEnumerable<SelectListItem> cleanersLossKeys = Enum.GetValues(typeof(CleanersLossKeys)).Cast<CleanersLossKeys>().Select(v => new SelectListItem
            {
                Text = v.ToString().Replace("_", "").Replace("slash", "/").Replace("COMMA", ","),
                Value = ((double)v).ToString()
            });

            model.LossKeys = cleanersLossKeys.ToList();

            foreach (var activity in secondaryCleaningActivities)
            {
                cblCleanersActivitiesIncluded.Add(new SelectListItem
                {
                    Text = activity.Item,
                    Value = activity.OrderID.ToString()
                });
            }
            model.CleanersModelCheckBoxIncludedActivities = cblCleanersActivitiesIncluded;

            //for (int i = 1; i < 4 + 1; i++)
            //{
            //    var GroupedCheckBoxList = new List<SelectListItem>();
            //    foreach (var activity in primaryCleaningActivities.Where(grp => grp.GroupID == i))
            //    {
            //        GroupedCheckBoxList.Add(new SelectListItem
            //        {
            //            Text = activity.Item,
            //            Value = i.ToString(),
            //            Selected = checkedItems.Where(itm => itm.GroupId == i && itm.Response == activity.Item).Count() == 0 ? false : true
            //        });
            //    }
            //    model.GetType().GetProperty(string.Format("CleanersModelCheckBoxGroup{0}", i)).SetValue(model, GroupedCheckBoxList);
            //}

            model.CleanersModelCheckBoxGroup = primaryCleaningActivities.Select(p => new SelectListItemCheckboxes
            {
                Text = p.Item,
                Value = p.Value,
                GroupId = p.GroupID.Value
            }).ToList();

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                ViewBag.Status = "Requote";

                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult.Where(p => p.QuestionId == 239).Select(p => p.Response).SingleOrDefault()).FirstOrDefault().Selected = true;

                //checkedItems = model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 11).Select(p => p.Response.Trim()).ToArray();

                model.LossKeys
                    .Where(p => p.Value == model.LiabilityInsuranceListResult
                        .Where(x => x.QuestionId == 16)
                        .Select(s => s.Response).SingleOrDefault())
                    .FirstOrDefault().Selected = true;

                model.CleanersModelCheckBoxIncludedActivities
                    .Where(p => model.LiabilityInsuranceListResult
                        .Where(x => x.QuestionId == 18).Select(x => x.Response.Trim())
                        .ToArray().Contains(p.Text.Trim()))
                    .ToList()
                    .ForEach(sel => sel.Selected = true);

                //model.CleanersModelCheckBoxGroup.Where(x => checkedItems.Contains(x.Text.Trim())).ToList().ForEach(xc => xc.Selected = true);

                string[] selectedCheckboxes = model.LiabilityInsuranceListResult
                                              .Where(x => x.QuestionId == 11)
                                              .Select(x => x.Response.Trim()).ToArray();
                var other = selectedCheckboxes.Where(x => model.CleanersModelCheckBoxGroup.All(p2 => p2.Text != x)).FirstOrDefault();
                model.CleanersModelCheckBoxGroup.Where(p => selectedCheckboxes.Contains(p.Text.Trim())).ToList().ForEach(cc => cc.Selected = true);

                if (!string.IsNullOrWhiteSpace(other))
                {
                    model.CleanersModelCheckBoxGroup.Where(p => p.Text == "Other").ToList().ForEach(cc => { cc.Selected = true; cc.Value = other; });
                    ViewBag.OtherHasValue = '1';
                }
            }

            ModelState.Clear();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Cleaners(LiabilityInsuranceResultViewModels model)
        {

            var userInfo = QuotationHeader;

            //***************************    APPEND: CHECKBOX VALUES AND TRIGGERS  ***************************

            ICheckBoxValueBusinessLogic CheckBoxPerformedActivitiesRepo = new CheckBoxValueBusinessLogic();
            IPremiumCalculationReferenceBusinessLogic ComputationReferenceRepo = new PremiumCalculationReferenceBusinessLogic();
            ILiabilityInsuranceResultBusinessLogic resultRepo = new LiabilityInsuranceResultBusinessLogic();

            var selectedCheckBoxValues = model.CleanersModelCheckBoxGroup.Where(x => x.Selected == true);

            // Add Necessary Question ID and User ID on the record
            foreach (var selectedItem in selectedCheckBoxValues) //All Groups
            {
                var item = new LiabilityInsuranceResult()
                    {
                        UserInfoId = QuotationHeader.Id,
                        QuestionId = 11,
                        ActivityGroup = selectedItem.GroupId,
                        Response = selectedItem.Value
                    };

                if (selectedItem.Text != "Other")
                {
                    model.LiabilityInsuranceListResult.Add(item);
                }
                else if (selectedItem.Text == "Other")
                {
                    item.IsTriggered = true;
                    model.LiabilityInsuranceListResult.Add(item);
                }
            }

            // Get Checkbox Record where IsChecked = true
            var SelectedCheckBoxValuesIncludedActivities = model.CleanersModelCheckBoxIncludedActivities.Where(x => x.Selected == true).ToList();

            // Add Necessary Question ID and User ID on the record
            foreach (var SelectedCheckBoxItem in SelectedCheckBoxValuesIncludedActivities) //Included Activities
            {
                //Determine Trigger if Checkbox Value = None of the above
                if (SelectedCheckBoxItem.Text.Trim() == "NONE OF THE ABOVE")
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        //UserInfoId = QuotationHeader.Id,
                        QuestionId = 18,
                        Response = SelectedCheckBoxItem.Text
                    });
                }
                else
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = QuotationHeader.Id,
                        QuestionId = 18,
                        Response = SelectedCheckBoxItem.Text,
                        IsTriggered = true
                    });
                }

            }

            //***************************    CALCULATE: BASE PREMIUM   ***************************

            // INITIATE: VARIABLES
            int InsuranceID = 2;
            int liabilityLimit = (int)QuotationHeader.LiabilityLimit;

            decimal PremiumBase = 0;
            decimal BaseRate = 0;
            decimal MinBaseRate = 0;
            decimal InjuryExcess = 0;

            int AnnualTurnover = Convert.ToInt32(model.LiabilityInsuranceListResult[1].Response);
            bool HasReferral = model.NeedsUnderwritingReferral;
            int LossOfKeys = Convert.ToInt32(model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 16).Select(s => s.Response).FirstOrDefault());
            decimal LossOfKeysRate = (decimal)ComputationReferenceRepo.GetPremiumCalculationReference().Where(x => x.InsuranceTypeID == InsuranceID && x.ConditionType == "LossKeys" && x.ConditionB == LossOfKeys).Select(s => s.Rate).FirstOrDefault();
            int HighestGroup = model.CleanersModelCheckBoxGroup.Where(w => w.Selected == true).Max(s => s.GroupId); //Determine the Highest Group Picked


            // GET: BASE RATE
            BaseRate = (decimal)ComputationReferenceRepo.GetPremiumCalculationReference().Where(x => x.InsuranceTypeID == InsuranceID && x.ConditionType == "BaseRate" && x.ConditionB == HighestGroup).Select(s => s.Rate).FirstOrDefault();
            MinBaseRate = (decimal)ComputationReferenceRepo.GetPremiumCalculationReference().Where(x => x.InsuranceTypeID == InsuranceID && x.ConditionType == "MinBaseRate" && x.ConditionB == HighestGroup && x.LiabilityLimit == liabilityLimit).Select(s => s.Rate).FirstOrDefault();

            // INITIATE: RATING RATIONALE
            string ratingRationale = "CL Turnover $" + AnnualTurnover.ToString("N0") + " x " + BaseRate.ToString("P3") + " Class " + HighestGroup;
            string strMinPremium = "";
            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);

            // RATE ADJUSTMENT: 5M or 20M
            decimal rateAdjustment = (decimal)ComputationReferenceRepo.GetPremiumCalculationReference().Where(x => x.InsuranceTypeID == InsuranceID && x.ConditionType == "RateAdjustment" && x.LiabilityLimit == liabilityLimit).Select(s => s.Rate).FirstOrDefault();

            if (liabilityLimit == 5000000)
            {
                BaseRate = BaseRate - (BaseRate * rateAdjustment);
                ratingRationale = ratingRationale + " less " + rateAdjustment.ToString("P0") + " Limit Multiplier";
            }
            else if (liabilityLimit == 20000000)
            {
                BaseRate = BaseRate + (BaseRate * rateAdjustment);
                ratingRationale = ratingRationale + " plus " + rateAdjustment.ToString("P0") + " Limit Multiplier";
            }

            // CALCULATE: PREMIUM BASE
            PremiumBase = BaseRate * AnnualTurnover;

            // ADD: LOSS KEYS?
            PremiumBase = PremiumBase + LossOfKeysRate;

            // INITIATE: RATING RATIONALE
            ratingRationale = ratingRationale + " plus $" + LossOfKeysRate.ToString("N2") + " LK " + strLiabilityLimit + " = $" + PremiumBase.ToString("N2");

            // APPLY: MINIMUM BASE RATE?
            if (PremiumBase < MinBaseRate)
            {
                PremiumBase = MinBaseRate + LossOfKeysRate;
                strMinPremium = " | Min Prem Applied - $" + PremiumBase.ToString("N2");
            }

            // GET: EXCESS RATE
            decimal excess = (decimal)ComputationReferenceRepo.GetByConditionType("Excess").Where(w => w.InsuranceTypeID == InsuranceID && w.ConditionB == HighestGroup).Select(s => s.Rate).FirstOrDefault();
            InjuryExcess = (decimal)ComputationReferenceRepo.GetByConditionType("InjuryExcess").Where(w => w.InsuranceTypeID == InsuranceID).Select(s => s.Rate).FirstOrDefault();


            // GET AND CALCULATE: SUBCONTRACTOR LOADING
            string hasSubContractor = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconQuestionId(InsuranceID)).Select(s => s.Response).FirstOrDefault();
            string strSubContractor = "";

            if (hasSubContractor == "Yes")
            {
                decimal subConPayment = Convert.ToDecimal(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconPaymentQuestionId(InsuranceID)).Select(s => s.Response).FirstOrDefault());
                decimal subConPaymentLoading = ComputeSubContractorLoading((decimal)AnnualTurnover, subConPayment);

                PremiumBase = PremiumBase + (PremiumBase * subConPaymentLoading);
                strSubContractor = " + " + subConPaymentLoading.ToString("P2") + " Subcontractor Loading = $" + PremiumBase.ToString("N2");
            }


            //***************************    GENERATE: RATING RATIONALE   ***************************
            ratingRationale = ratingRationale + " " + strMinPremium + strSubContractor;
            userInfo.RatingRationale = ratingRationale;

            //***************************    APPEND: PREMIUM DETAILS ***************************

            int StateQuestionId = 239;
            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            var stateTaxRate = (from t in taxRateList.StampTaxRateList
                                where t.Text == model.LiabilityInsuranceListResult.Where(txr => txr.QuestionId == StateQuestionId).Select(slc => slc.Response).FirstOrDefault()
                                select t.Value).FirstOrDefault();

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = PremiumBase,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = excess,
                HeaderId = QuotationHeader.Id
            });

            var liabilityQuotationHeaderModel = ComputePremium(QuotationHeader, premDtls);

            //***************************    FINALIZE -> SAVE ***************************

            //Check IsTriggered Column for any Referral Scenarios
            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);

            if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                SaveRequoteAsync();
            }

            //GeneratePremiumReport(QuotationHeader.Id, "Cleaners");
            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** RESTAURANTS ****************************************

        public virtual ActionResult CafesAndRestaurants()
        {
            LiabilityInsuranceResultViewModels model = new LiabilityInsuranceResultViewModels();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            ILiabilityInsuranceResultBusinessLogic liabilityResultService = new LiabilityInsuranceResultBusinessLogic();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);

            model.StateList = stateTaxRates.Select(p => new SelectListItem
            {
                Text = p.Text,
                Value = p.Text
            }).ToList();

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResultService.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == 251)
                                                          .Select(p => p.Response).SingleOrDefault())
                               .FirstOrDefault().Selected = true;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> CafesAndRestaurants(LiabilityInsuranceResultViewModels model)
        {

            var userInfo = QuotationHeader;

            //***************************    CALCULATE: BASE PREMIUM   ***************************

            // Variable Declarations
            int InsuranceID = 4;
            int liabilityLimit = (int)userInfo.LiabilityLimit;

            decimal PremiumBase = 0;
            decimal BaseRate = 0;
            decimal MinBaseRate = 0;
            decimal? Excesses = 0;

            int FoodTurnover = Convert.ToInt32(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 41).Select(s => s.Response).FirstOrDefault()); //Food Turnover
            int BarTurnover = Convert.ToInt32(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 42).Select(s => s.Response).FirstOrDefault()); //Bar Turnover
            int TotalTurnover = FoodTurnover + BarTurnover;

            string ConditionType = "";

            // GET: Base Rate Data from Repo
            IPremiumCalculationReferenceBusinessLogic ComputationReferenceRepo = new PremiumCalculationReferenceBusinessLogic();

            //Determine Condition Type
            ConditionType = "BaseRate";

            // GET: Base Rate (from DB)
            BaseRate = (decimal)ComputationReferenceRepo.GetPremiumCalculationReference().Where(x => x.InsuranceTypeID == InsuranceID && x.ConditionType == ConditionType && x.LiabilityLimit == liabilityLimit && TotalTurnover >= x.ConditionA && TotalTurnover <= x.ConditionB).Select(s => s.Rate).FirstOrDefault();

            //Determine Initial Premium
            PremiumBase = BaseRate;

            //Apply Minimum Base Rate if Applicable
            if (PremiumBase < MinBaseRate)
                PremiumBase = MinBaseRate;

            //Determine Excess Rates
            Excesses = ComputationReferenceRepo.GetPremiumCalculationReference().Where(x => x.InsuranceTypeID == InsuranceID && x.ConditionType == "Excess" && TotalTurnover >= x.ConditionA && TotalTurnover <= x.ConditionB).Select(s => s.Rate).FirstOrDefault();

            //***************************    GENERATE: RATING RATIONALE   ***************************
            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);

            decimal conditionA = (decimal)ComputationReferenceRepo.GetPremiumCalculationReference()
                                    .Where(x => x.InsuranceTypeID == InsuranceID && x.ConditionType == ConditionType && x.LiabilityLimit == liabilityLimit && TotalTurnover >= x.ConditionA && TotalTurnover <= x.ConditionB)
                                    .Select(s => s.ConditionA).FirstOrDefault();
            decimal conditionB = (decimal)ComputationReferenceRepo.GetPremiumCalculationReference()
                                    .Where(x => x.InsuranceTypeID == InsuranceID && x.ConditionType == ConditionType && x.LiabilityLimit == liabilityLimit && TotalTurnover >= x.ConditionA && TotalTurnover <= x.ConditionB)
                                    .Select(s => s.ConditionB == 999999999999999 ? (decimal)s.ConditionA : Math.Floor((decimal)s.ConditionB)).FirstOrDefault();

            string ratingRationale = "RS Turnover Up to $" + conditionB.ToString("N0") + " " + strLiabilityLimit + " = $" + PremiumBase.ToString("N2");
            //RS Turnover Up to $5,000,000 20m = $1,012.50
            userInfo.RatingRationale = ratingRationale;


            //***************************    APPEND: PREMIUM DETAILS ***************************

            var StateQuestionID = 251;

            StampTaxRateViewModels taxRate = new StampTaxRateViewModels();
            var taxRateList = taxRate.StampTaxRateList;
            var stateTaxRate = (from tax in taxRateList
                                where tax.Text == (model.LiabilityInsuranceListResult.Where(y => y.QuestionId == StateQuestionID).Select(z => z.Response).FirstOrDefault())
                                select tax.Value
                                    ).FirstOrDefault();

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = PremiumBase,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = Excesses ?? 0,
                HeaderId = userInfo.Id
            });

            var liabilityQuotationHeaderModel = ComputePremium(QuotationHeader, premDtls);

            //***************************    FINALIZE -> SAVE ***************************

            // Check IsTriggered Column for any Referral Scenarios
            bool HasReferral = model.NeedsUnderwritingReferral;
            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);

            if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
            {
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
            }
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                SaveRequoteAsync();
            }

            //GeneratePremiumReport(userInfo.Id, "Restaurants");
            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** TRANSPORT AND LOGISTICS ****************************************

        public virtual ActionResult TransportAndLogistics()
        {
            LiabilityInsuranceResultViewModels model = new LiabilityInsuranceResultViewModels();
            ILiabilityInsuranceResultBusinessLogic liabilityResultService = new LiabilityInsuranceResultBusinessLogic();
            ICheckBoxValueBusinessLogic checkboxValueService = new CheckBoxValueBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);
            var checkboxData = checkboxValueService.GetCheckBoxValueByInsuranceID(5).Where(q => q.QuestionID == 49).ToList();

            model.StateList = stateTaxRates.Select(p => new SelectListItem
            {
                Text = p.Text,
                Value = p.Text
            }).ToList();

            model.CheckboxReferenceLookup = checkboxData.Select(p => new SelectListItem
            {
                Text = p.Item,
                Value = p.Item
            }).ToList();

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResultService.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                         .Where(p => p.QuestionId == 204)
                                                         .Select(p => p.Response).SingleOrDefault())
                              .FirstOrDefault().Selected = true;

                model.CheckboxReferenceLookup.Where(p => model.LiabilityInsuranceListResult
                                                              .Where(q => q.QuestionId == 49)
                                                              .Select(m => m.Response)
                                                              .ToArray()
                                                              .Contains(p.Text)).ToList().ForEach(cc => cc.Selected = true);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> TransportAndLogistics(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;
            int insuranceId = (int)QuotationHeader.InsuranceId;

            //***************************    APPEND: CHECKBOX VALUES   ***************************
            var SelectedCheckBoxValuesIncludedActivities = model.CheckboxReferenceLookup.Where(x => x.Selected == true).ToList();
            foreach (var SelectedCheckBoxItem in SelectedCheckBoxValuesIncludedActivities) //Included Activities
            {
                // Determine Trigger if Checkbox Value = None of the above
                if (SelectedCheckBoxItem.Text.ToUpper().Trim() == "NONE OF THE ABOVE")
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = 49,
                        Response = SelectedCheckBoxItem.Text,
                        GroupId = null
                    });
                }
                else
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = 49,
                        Response = SelectedCheckBoxItem.Text,
                        GroupId = null,
                        IsTriggered = true
                    });
                }
            }

            //***************************    CALCULATE: BASE PREMIUM   ***************************
            // INITIATE: FACTORS
            int liabilityLimit = (int)userInfo.LiabilityLimit;
            int annualTurnover = Convert.ToInt32(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 44).Select(s => s.Response).FirstOrDefault());

            decimal PremiumBase = 0;
            decimal BaseRate = 0;

            bool HasReferral = model.NeedsUnderwritingReferral;

            // GET: BASE RATE
            IPremiumCalculationReferenceBusinessLogic lookup = new PremiumCalculationReferenceBusinessLogic();
            BaseRate = (from item in lookup.GetByInsuranceTypeId(userInfo.InsuranceId.Value)
                        where annualTurnover >= item.ConditionA && annualTurnover <= item.ConditionB
                        select item).Where(x => x.LiabilityLimit == userInfo.LiabilityLimit && x.ConditionType == "BaseRate").Select(y => y.Rate.Value).FirstOrDefault();

            // GET: INITIAL PREMIUM
            PremiumBase = BaseRate;

            // GET: EXCESS RATE
            decimal excess = (from item in lookup.GetByInsuranceTypeId(userInfo.InsuranceId.Value)
                              where annualTurnover >= item.ConditionA && annualTurnover <= item.ConditionB
                              select item).Where(x => x.ConditionType == "Excess").Select(y => y.Rate.Value).FirstOrDefault();

            // GET AND CALCULATE: SUBCONTRACTOR LOADING
            string hasSubContractor = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault();
            string strSubContractor = "";

            if (hasSubContractor == "Yes")
            {
                decimal subConPayment = Convert.ToDecimal(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconPaymentQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault());
                decimal subConPaymentLoading = ComputeSubContractorLoading((decimal)annualTurnover, subConPayment);

                PremiumBase = PremiumBase + (PremiumBase * subConPaymentLoading);
                strSubContractor = " + " + subConPaymentLoading.ToString("P2") + " Subcontractor Loading = $" + PremiumBase.ToString("N2");
            }


            //***************************    GENERATE: RATING RATIONALE   ***************************
            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);

            double conditionA = (from item in lookup.GetByInsuranceTypeId(userInfo.InsuranceId.Value)
                                 where annualTurnover >= item.ConditionA && annualTurnover <= item.ConditionB
                                 select item).Where(x => x.LiabilityLimit == userInfo.LiabilityLimit).Select(y => y.ConditionA.Value).FirstOrDefault();

            double conditionB = (from item in lookup.GetByInsuranceTypeId(userInfo.InsuranceId.Value)
                                 where annualTurnover >= item.ConditionA && annualTurnover <= item.ConditionB
                                 select item).Where(x => x.LiabilityLimit == userInfo.LiabilityLimit).Select(y => y.ConditionB.Value == 999999999999999 ? y.ConditionA.Value : Math.Floor(y.ConditionB.Value)).FirstOrDefault();

            string strRange = "$" + conditionA.ToString("N0") + " - $" + conditionB.ToString("N0");

            if (conditionB == conditionA)
                strRange = "Greater Than $" + conditionB.ToString("N0");

            string ratingRationale = "TL " + strRange + " Turnover " + strLiabilityLimit + " = $" + PremiumBase.ToString("N2") + strSubContractor;

            userInfo.RatingRationale = ratingRationale;


            //***************************    APPEND: PREMIUM DETAILS ***************************

            StampTaxRateViewModels taxRate = new StampTaxRateViewModels();
            var taxRateList = taxRate.StampTaxRateList;
            var stateTaxRate = (from tax in taxRateList
                                where tax.Text == (model.LiabilityInsuranceListResult.Where(y => y.QuestionId == 204).Select(z => z.Response).FirstOrDefault())
                                select tax.Value
                                    ).FirstOrDefault();

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = PremiumBase,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = excess,
                HeaderId = userInfo.Id
            });

            var liabilityQuotationHeaderModel = ComputePremium(QuotationHeader, premDtls);

            //***************************    FINALIZE -> SAVE ***************************

            //Check IsTriggered Column for any Referral Scenarios
            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);

            if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
            {
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
            }
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                SaveRequoteAsync();
            }
            //GeneratePremiumReport(userInfo.Id, "Transport And Logistics");
            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** PROJECT MANAGERS ****************************************

        public virtual ActionResult ProjectManagers()
        {
            LiabilityInsuranceResultViewModels model = new LiabilityInsuranceResultViewModels();
            ILiabilityInsuranceResultBusinessLogic liabilityResultService = new LiabilityInsuranceResultBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);
            var checkboxService = new CheckBoxValueBusinessLogic().GetCheckBoxValueByInsuranceID(6).Where(p => p.QuestionID == 276);

            model.CheckboxReferenceLookup = checkboxService.Select(itm => new SelectListItem
            {
                Text = itm.Item,
                Value = itm.Item
            }).ToList();
            model.StateList = stateTaxRates.Select(p => new SelectListItem
            {
                Text = p.Text,
                Value = p.Text
            }).ToList();

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResultService.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                         .Where(p => p.QuestionId == 209)
                                                         .Select(p => p.Response).SingleOrDefault())
                              .FirstOrDefault().Selected = true;
                model.CheckboxReferenceLookup.Where(p => model.LiabilityInsuranceListResult
                                                              .Where(q => q.QuestionId == 276)
                                                              .Select(m => m.Response)
                                                              .ToArray()
                                                              .Contains(p.Text)).ToList().ForEach(cc => cc.Selected = true);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ProjectManagers(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;

            //***************************    APPEND: CHECKBOX VALUES   ***************************

            var SelectedCheckBoxValuesIncludedActivities = model.CheckboxReferenceLookup.Where(x => x.Selected == true).ToList();
            foreach (var SelectedCheckBoxItem in SelectedCheckBoxValuesIncludedActivities) //Included Activities
            {
                if (SelectedCheckBoxItem.Text.ToUpper().Trim() == "NONE OF THE ABOVE")
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = 276,
                        Response = SelectedCheckBoxItem.Text,
                        GroupId = null
                    });
                }
                else
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = 276,
                        Response = SelectedCheckBoxItem.Text,
                        GroupId = null,
                        IsTriggered = true
                    });
                }
            }

            //***************************    CALCULATE: BASE PREMIUM   ***************************

            IPremiumCalculationReferenceBusinessLogic lookup = new PremiumCalculationReferenceBusinessLogic();
            decimal annualTurnover = Convert.ToDecimal(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 56).Select(s => s.Response).FirstOrDefault());
            var numOfEmployees = Convert.ToInt32(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 57).Select(s => s.Response).FirstOrDefault());
            decimal liability = (decimal)userInfo.LiabilityLimit;

            StampTaxRateViewModels taxRate = new StampTaxRateViewModels();
            var taxRateList = taxRate.StampTaxRateList;
            var stateTaxRate = (from tax in taxRateList
                                where tax.Text == (model.LiabilityInsuranceListResult.Where(y => y.QuestionId == 209).Select(z => z.Response).FirstOrDefault())
                                select tax.Value
                                    ).FirstOrDefault();

            var rate = (from item in lookup.GetByInsuranceTypeId(userInfo.InsuranceId.Value)
                        where numOfEmployees >= item.ConditionA && numOfEmployees <= item.ConditionB
                        select item).Where(x => Convert.ToDecimal(x.LiabilityLimit) == liability).Select(y => y.Rate).FirstOrDefault();

            decimal premiumBase = (decimal)rate;

            // GET: EXCESS RATE
            decimal excess = 500;

            // GET AND CALCULATE: SUBCONTRACTOR LOADING
            string hasSubContractor = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault();
            string strSubContractor = "";

            if (hasSubContractor == "Yes")
            {
                decimal subConPayment = Convert.ToDecimal(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconPaymentQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault());
                decimal subConPaymentLoading = ComputeSubContractorLoading((decimal)annualTurnover, subConPayment);

                premiumBase = premiumBase + (premiumBase * subConPaymentLoading);
                strSubContractor = " + " + subConPaymentLoading.ToString("P2") + " Subcontractor Loading = $" + premiumBase.ToString("N2");
            }

            //***************************    GENERATE: RATING RATIONALE   ***************************
            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);
            string employeeNumberCondition = numOfEmployees > 1 ? "2-5 Employees " : "1 Employee ";

            string ratingRationale = "PM " + employeeNumberCondition + strLiabilityLimit + " = $" + premiumBase.ToString("N2") + strSubContractor;

            userInfo.RatingRationale = ratingRationale;

            //***************************    APPEND: PREMIUM DETAILS ***************************

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = (decimal)premiumBase,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = excess,
                HeaderId = userInfo.Id
            });

            var liabilityQuotationHeaderModel = ComputePremium(QuotationHeader, premDtls);

            //***************************    FINALIZE -> SAVE ***************************

            //Check IsTriggered Column for any Referral Scenarios
            var HasReferral = model.NeedsUnderwritingReferral;
            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);

            if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
            {
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
            }
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                SaveRequoteAsync();
            }

            //GeneratePremiumReport(userInfo.Id, "Project Managers");
            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** PROPERTY OWNERS ****************************************

        [HttpGet]
        public virtual ActionResult PropertyOwners()
        {
            IZohoService zoho = new ZohoService();
            ILiabilityInsuranceResultBusinessLogic resultRepo = new LiabilityInsuranceResultBusinessLogic();
            LiabilityInsuranceResultViewModels model = new LiabilityInsuranceResultViewModels();
            var checkboxService = new CheckBoxValueBusinessLogic().GetCheckBoxValueByInsuranceID(7).Where(wh => wh.QuestionID == 226);
            string requoteStatus = UserResponseEnum.Requote.ToString();
            if (QuotationHeader.Status == requoteStatus.ToString())
            {
                model.LiabilityInsuranceListResult = resultRepo.GetByUserId(QuotationHeader.Id);
                TempData["PropertyOwnersModel"] = model;
                model.SituationNumber = model.LiabilityInsuranceListResult.Max(x => x.GroupId).Value;
            }

            List<SelectListItem> allStates = new List<SelectListItem>();
            StampTaxRateViewModels taxRates = new StampTaxRateViewModels();

            model.StateList = taxRates.StampTaxRateList.Select(item => new SelectListItem
            {
                Text = item.Text,
                Value = item.Text
            }).ToList();

            model.CheckboxReferenceLookup = checkboxService.Select(item => new SelectListItem
            {
                Text = item.Item,
                Value = item.Item
            }).ToList();

            var checkboxData = checkboxService.Select((itm, indx) => new SelectListItemCheckboxes
            {
                GroupId = indx,
                Text = itm.Item,
                Value = itm.Item
            }).ToList();

            var result = new List<PropertyOwnerSituationDetailViewModel>();
            result.Add(new PropertyOwnerSituationDetailViewModel
            {
                SituationNumber = "1",
                StreetName = string.Empty,
                StreetNumber = string.Empty,
                Suburb = string.Empty,
                State = "Queensland",
                PostalCode = string.Empty,
                Activities = checkboxData,
                BuildingValue = string.Empty,
                RentalIncome = string.Empty,
                TenantOccupation = string.Empty,
                Unoccupied = string.Empty
            });
            model.PropertyOwnerSituation = result;

            Session["testCheckBoxes"] = checkboxData;
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public virtual JsonResult LoadSituationData()
        {
            var result = new List<PropertyOwnerSituationDetailViewModel>();

            if (TempData["PropertyOwnersModel"] == null)
            {
                result.Add(new PropertyOwnerSituationDetailViewModel
                {
                    SituationNumber = "1",
                    StreetName = string.Empty,
                    StreetNumber = string.Empty,
                    Suburb = string.Empty,
                    State = "Queensland",
                    PostalCode = string.Empty,
                    Activities = Session["testCheckBoxes"] as List<SelectListItemCheckboxes>,
                    BuildingValue = string.Empty,
                    RentalIncome = string.Empty,
                    TenantOccupation = string.Empty,
                    Unoccupied = string.Empty
                });
                string data2 = JsonConvert.SerializeObject(result);
                return Json(data2, JsonRequestBehavior.AllowGet);
            }
            var propertyOwnerModel = (LiabilityInsuranceResultViewModels)TempData["PropertyOwnersModel"];
            var liabilityResult = propertyOwnerModel.LiabilityInsuranceListResult;
            var checkboxService = new CheckBoxValueBusinessLogic().GetCheckBoxValueByInsuranceID(7).Where(wh => wh.QuestionID == 226);


            for (int i = 1; i < liabilityResult.Max(x => x.GroupId).Value + 1; i++)
            {
                var checkBoxData = checkboxService.Select((itm, indx) => new SelectListItemCheckboxes
                {
                    GroupId = indx,
                    Text = itm.Item,
                    Value = itm.Item
                }).ToList();
                var newCheckBox = new List<SelectListItemCheckboxes>();
                newCheckBox = checkBoxData;
                newCheckBox.Where(x => liabilityResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 226).Select(sel => sel.Response).ToArray().Contains(x.Text)).ToList().ForEach(s => s.Selected = true);



                result.Add(new PropertyOwnerSituationDetailViewModel
                {
                    SituationNumber = i.ToString(),
                    StreetName = liabilityResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 71).Select(sel => sel.Response).FirstOrDefault(),
                    StreetNumber = liabilityResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 70).Select(sel => sel.Response).FirstOrDefault(),
                    Suburb = liabilityResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 72).Select(sel => sel.Response).FirstOrDefault(),
                    State = liabilityResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 73).Select(sel => sel.Response).FirstOrDefault(),
                    PostalCode = liabilityResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 74).Select(sel => sel.Response).FirstOrDefault(),
                    Activities = newCheckBox,
                    BuildingValue = liabilityResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 75).Select(sel => sel.Response).FirstOrDefault(),
                    RentalIncome = liabilityResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 118).Select(sel => sel.Response).FirstOrDefault(),
                    TenantOccupation = liabilityResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 117).Select(sel => sel.Response).FirstOrDefault(),
                    Unoccupied = liabilityResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 119).Select(sel => sel.Response).FirstOrDefault()
                });
            }

            propertyOwnerModel.PropertyOwnerSituation = result;

            string data = JsonConvert.SerializeObject(propertyOwnerModel.PropertyOwnerSituation);

            return Json(data, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> PropertyOwners(LiabilityInsuranceResultViewModels model)
        {
            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();
            IPremiumDetailBusinessLogic detailRepo = new PremiumDetailBusinessLogic();
            IPremiumCalculationReferenceBusinessLogic lookup = new PremiumCalculationReferenceBusinessLogic();

            var userInfo = QuotationHeader;

            //***************************    CALCULATE: BASE PREMIUM   ***************************

            //Decimal PremiumBase = 0;
            Decimal PremiumGst = 0;
            Decimal StampDutyTax = 0;
            Decimal liability = (decimal)userInfo.LiabilityLimit;

            var loading = .25M;
            var premiumList = lookup.GetByInsuranceTypeId(userInfo.InsuranceId.Value);

            string ratingRationale = "PO " + System.Environment.NewLine;
            var ctr = 0;

            StampTaxRateViewModels taxRate = new StampTaxRateViewModels();
            var taxRateList = taxRate.StampTaxRateList;

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            var sumBuildingValue = 0.0M;
            var excessValue = 0;
            foreach (var item in model.PropertyOwnerSituation)
            {
                var hasLoading = item.Unoccupied;
                PremiumDetail detail = new PremiumDetail();

                decimal buildingValue = Convert.ToDecimal(item.BuildingValue);
                sumBuildingValue += buildingValue;

                // GET: Base Rate per Situation
                var premium = (decimal)(from prem in premiumList
                                        where buildingValue >= Convert.ToDecimal(prem.ConditionA) && buildingValue <= Convert.ToDecimal(prem.ConditionB)
                                        select prem).Where(x => Convert.ToDecimal(x.LiabilityLimit) == liability).Select(y => y.Rate).FirstOrDefault();

                // GENERATE: Rating Rationale per Situation
                ctr++;
                var buildingValueCeiling = (decimal)(from prem in premiumList
                                                     where buildingValue >= Convert.ToDecimal(prem.ConditionA) && buildingValue <= Convert.ToDecimal(prem.ConditionB)
                                                     select prem).Where(x => Convert.ToDecimal(x.LiabilityLimit) == liability).Select(y => y.ConditionB == 999999999999999 ? (decimal)y.ConditionA : Math.Floor((decimal)y.ConditionB)).FirstOrDefault();

                ratingRationale = ratingRationale + "Sit " + ctr + " $" + premium.ToString("N2") + " Premium for Building Value Up to $" + buildingValueCeiling.ToString("N0") + " ";

                // UPDATE: Base Rate per Situation
                if (hasLoading.ToUpper() == "YES")
                {
                    premium += (premium * loading);
                    ratingRationale = ratingRationale + "plus 25% Unoccupied Loading ";
                }

                //PremiumBase += (decimal)premium;
                var rate = (from tax in taxRateList
                            where tax.Text == item.State
                            select tax.Value
                                    ).FirstOrDefault();

                var GST = premium * .1M;
                PremiumGst += (decimal)GST;

                var stampDutyFee = (premium + GST) * Convert.ToDecimal(rate);
                StampDutyTax += (decimal)stampDutyFee;

                //***************************    GENERATE: RATING RATIONALE   ***************************
                string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);
                ratingRationale = ratingRationale + strLiabilityLimit + " = $" + premium.ToString("N2") + " | " + System.Environment.NewLine;


                //***************************    APPEND: PREMIUM DETAILS ***************************
                premDtls.Add(new PremiumDetail
                {
                    BasePremium = (decimal)premium,
                    GSTRate = 0.1M,
                    StateTaxRate = Convert.ToDecimal(rate),
                    HeaderId = userInfo.Id
                });
            }

            // GET : Excess
            if (sumBuildingValue > 10000000)
            {
                excessValue = 1000;
            }
            else
            {
                excessValue = 500;
            }
            foreach (var item in premDtls)
            {
                item.Excess = excessValue;
            }

            // ADD : Finalized Rationale on model
            userInfo.RatingRationale = ratingRationale;

            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);


            //***************************    FINALIZE -> SAVE ***************************
            //userInfo.RatingRationale = ratingRationale;

            // Check and Add Triggers on Selected PO Questions
            var resultList = ConvertPropertyOwnerViewModel(model.PropertyOwnerSituation, userInfo.Id);

            // Add Trigger - Sum Building Value > 20,000,000
            if (sumBuildingValue > 20000000)
            {
                foreach (var item in resultList.Where(x => x.QuestionId == 75))
                {
                    item.IsTriggered = true;
                }
            }

            // Add Trigger - 60 days occuppied
            foreach (var item in resultList.Where(x => x.QuestionId == 119))
            {
                if (item.Response == "Yes")
                    item.IsTriggered = true;
            }

            // CHECK: IsTriggered Column for any Referral Scenarios
            var HasReferral = model.NeedsUnderwritingReferral;
            if (resultList.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);

            model.LiabilityInsuranceListResult.AddRange(resultList);

            if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                SaveRequoteAsync();
            }

            //GeneratePremiumReport(userInfo.Id, "Welders And Boilermakers");
            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** SCAFFOLDERS ****************************************

        public virtual ActionResult Scaffolders()
        {
            LiabilityInsuranceResultViewModels model = new LiabilityInsuranceResultViewModels();
            ILiabilityInsuranceResultBusinessLogic liabilityResultService = new LiabilityInsuranceResultBusinessLogic();
            var lookupService = new LookupBusinessLogic().GetReferenceByCode(STAMPTAXRATE);
            var checkboxService = new CheckBoxValueBusinessLogic().GetCheckBoxValueByInsuranceID(8).Where(p => p.QuestionID == 82);
            StampTaxRateViewModels taxRate = new StampTaxRateViewModels();

            model.CheckboxReferenceLookup = checkboxService.Select(itm => new SelectListItem
            {
                Text = itm.Item,
                Value = itm.Item
            }).ToList();

            model.StateList = lookupService.Select(itm => new SelectListItem
            {
                Text = itm.Text,
                Value = itm.Text
            }).ToList();

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResultService.GetByUserId(QuotationHeader.Id);
                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                         .Where(p => p.QuestionId == 214)
                                                         .Select(p => p.Response).SingleOrDefault())
                              .FirstOrDefault().Selected = true;
                model.CheckboxReferenceLookup
                  .Where(p => model.LiabilityInsuranceListResult
                      .Where(x => x.QuestionId == 82).Select(x => x.Response)
                      .ToArray().Contains(p.Text))
                  .ToList()
                  .ForEach(sel => sel.Selected = true);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Scaffolders(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;

            //***************************    APPEND: CHECKBOX VALUES   ***************************

            var SelectedCheckBoxValuesIncludedActivities = model.CheckboxReferenceLookup.Where(x => x.Selected == true).ToList();
            foreach (var SelectedCheckBoxItem in SelectedCheckBoxValuesIncludedActivities) //Included Activities
            {
                if (SelectedCheckBoxItem.Text.ToUpper().Trim() == "NONE OF THE ABOVE")
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = 82,
                        Response = SelectedCheckBoxItem.Text,
                        GroupId = null
                    });
                }
                else
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = 82,
                        Response = SelectedCheckBoxItem.Text,
                        GroupId = null,
                        IsTriggered = true
                    });
                }
            }

            //***************************    CALCULATE: BASE PREMIUM   ***************************
            Decimal BaseRate = 0;
            Decimal Excesses = 0;

            StampTaxRateViewModels taxRate = new StampTaxRateViewModels();
            var taxRateList = taxRate.StampTaxRateList;
            var stateTaxRate = (from tax in taxRateList
                                where tax.Text == (model.LiabilityInsuranceListResult.Where(y => y.QuestionId == 214).Select(z => z.Response).FirstOrDefault())
                                select tax.Value
                                    ).FirstOrDefault();
            Decimal liability = (decimal)userInfo.LiabilityLimit;
            Decimal annualTurnover = Convert.ToDecimal(model.LiabilityInsuranceListResult[5].Response);
            bool hasReferral = model.NeedsUnderwritingReferral;
            var overTurnover = annualTurnover * (Convert.ToDecimal(model.LiabilityInsuranceListResult[10].Response) / 100);
            var underTurnover = annualTurnover * (Convert.ToDecimal(model.LiabilityInsuranceListResult[9].Response) / 100);
            IPremiumCalculationReferenceBusinessLogic lookup = new PremiumCalculationReferenceBusinessLogic();
            var listOver = lookup.GetByConditionType("Over 10 M");
            var listUnder = lookup.GetByConditionType("Under 10 M");

            var percentageOver = Convert.ToDecimal(model.LiabilityInsuranceListResult[10].Response) / 100;
            var percentageUnder = Convert.ToDecimal(model.LiabilityInsuranceListResult[9].Response) / 100;

            var minPremOver = lookup.GetByConditionType("Min Prem Over 10 M").Where(w => Convert.ToDecimal(w.LiabilityLimit) == liability).Select(s => s.Rate.Value).FirstOrDefault();
            var minPremUnder = lookup.GetByConditionType("Min Prem Under 10 M").Where(w => Convert.ToDecimal(w.LiabilityLimit) == liability).Select(s => s.Rate.Value).FirstOrDefault();

            Decimal overRate = 0;
            Decimal underRate = 0;

            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);
            string ratingRationale = "SF " + strLiabilityLimit;

            // Annual Turnover < 200000
            if (annualTurnover < 200000)
            {
                if (percentageUnder == 1) //Under 10M = 100%
                {
                    BaseRate = Convert.ToDecimal(minPremUnder);
                }
                else if (percentageOver == 1) //Over 10M = 100%
                {
                    BaseRate = Convert.ToDecimal(minPremOver);
                }
                else //Percentage split
                {
                    if ((double)percentageOver <= 0.25) //Over 10M <= 25%
                    {
                        BaseRate = Convert.ToDecimal(minPremUnder);
                    }
                    else //Over 10M > 25%
                    {
                        BaseRate = Convert.ToDecimal(minPremOver);
                    }
                }
                // GENERATE: RATING RATIONALE - Turnover < $200,000
                ratingRationale = ratingRationale + " $" + annualTurnover.ToString("N2") + " Annual Turnover with " +
                    percentageUnder.ToString("P2") + " Work Under 10mt and " +
                    percentageOver.ToString("P2") + " Work Over 10mt = $" +
                    BaseRate.ToString("N2") + " Min Prem Applied";
                //ratingRationale = "Minimum Base Rate of $" + BaseRate + " applied for Annual Turnovers less than $200,000";

            }

            // Annual Turnover >= 200000
            else
            {
                overRate = overTurnover * ((decimal)(from item in listOver
                                                     where overTurnover >= Convert.ToDecimal(item.ConditionA) && overTurnover <= Convert.ToDecimal(item.ConditionB)
                                                     select item).Where(m => Convert.ToDecimal(m.LiabilityLimit) == liability).Select(x => x.Rate.Value).FirstOrDefault());
                underRate = underTurnover * ((decimal)(from item in listUnder
                                                       where underTurnover >= Convert.ToDecimal(item.ConditionA) && underTurnover <= Convert.ToDecimal(item.ConditionB)
                                                       select item).Where(m => Convert.ToDecimal(m.LiabilityLimit) == liability).Select(x => x.Rate.Value).FirstOrDefault());
                BaseRate = (overRate + underRate);

                // GENERATE: RATING RATIONALE - Turnover >= $200,000
                var overPerc = ((decimal)(from item in listOver
                                          where overTurnover >= Convert.ToDecimal(item.ConditionA) && overTurnover <= Convert.ToDecimal(item.ConditionB)
                                          select item).Where(m => Convert.ToDecimal(m.LiabilityLimit) == liability).Select(x => x.Rate.Value).FirstOrDefault());

                var underPerc = ((decimal)(from item in listUnder
                                           where underTurnover >= Convert.ToDecimal(item.ConditionA) && underTurnover <= Convert.ToDecimal(item.ConditionB)
                                           select item).Where(m => Convert.ToDecimal(m.LiabilityLimit) == liability).Select(x => x.Rate.Value).FirstOrDefault());

                ratingRationale = ratingRationale + " $" + underTurnover.ToString("N2") + " x " + underPerc.ToString("#0.###%") + " for Under 10mt plus $" +
                                                           overTurnover.ToString("N2") + " x " + overPerc.ToString("#0.###%") + " for Over 10mt = $" +
                                                           BaseRate.ToString("N2");
            }

            // GET AND CALCULATE: SUBCONTRACTOR LOADING
            string hasSubContractor = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault();
            string strSubContractor = "";

            if (hasSubContractor == "Yes")
            {
                decimal subConPayment = Convert.ToDecimal(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconPaymentQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault());
                decimal subConPaymentLoading = ComputeSubContractorLoading((decimal)annualTurnover, subConPayment);

                BaseRate = BaseRate + (BaseRate * subConPaymentLoading);
                strSubContractor = " + " + subConPaymentLoading.ToString("P2") + " Subcontractor Loading = $" + BaseRate.ToString("N2");
            }

            //***************************    GENERATE: RATING RATIONALE   ***************************
            userInfo.RatingRationale = ratingRationale + strSubContractor;


            //***************************    APPEND: PREMIUM DETAILS ***************************

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = BaseRate,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = Excesses,
                HeaderId = userInfo.Id
            });

            var liabilityQuotationHeaderModel = ComputePremium(QuotationHeader, premDtls);

            //***************************    FINALIZE -> SAVE ***************************

            //Check IsTriggered Column for any Referral Scenarios
            var HasReferral = model.NeedsUnderwritingReferral;

            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);


            if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
            {
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
            }
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                SaveRequoteAsync();
            }

            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** VACANT LAND ****************************************
        public virtual ActionResult VacantLand()
        {
            LiabilityInsuranceResultViewModels model = new LiabilityInsuranceResultViewModels();
            ILiabilityInsuranceResultBusinessLogic liabilityResultService = new LiabilityInsuranceResultBusinessLogic();
            var lookupService = new LookupBusinessLogic().GetReferenceByCode(STAMPTAXRATE);
            StampTaxRateViewModels taxRate = new StampTaxRateViewModels();
            List<VacantLandSituationViewModel> result = new List<VacantLandSituationViewModel>();

            model.StateList = lookupService.Select(itm => new SelectListItem
            {
                Text = itm.Text,
                Value = itm.Text
            }).ToList();

            result.Add(new VacantLandSituationViewModel
            {
                SituationNumber = "1",
                StreetName = string.Empty,
                StreetNumber = string.Empty,
                Suburb = string.Empty,
                State = "Queensland",
                PostalCode = string.Empty,
                LandSize = string.Empty,
                IsHobbyFarm = string.Empty,
                HobbyFarmDetail = string.Empty,
                IsForAgistment = string.Empty,
                AgistmentDetail = string.Empty,
                IsForMarketOrParking = string.Empty,
                MarketOrParkingDetail = string.Empty,
                HasStructuresOrBuildings = string.Empty,
                StructureOrBuildingDetail = string.Empty,
                BuildOrSellLand = string.Empty
            });

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                result.Clear();

                model.LiabilityInsuranceListResult = liabilityResultService.GetByUserId(QuotationHeader.Id);

                for (int i = 1; i < model.LiabilityInsuranceListResult.Max(x => x.GroupId).Value + 1; i++)
                {
                    result.Add(new VacantLandSituationViewModel
                    {
                        SituationNumber = i.ToString(),
                        StreetName = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 124).Select(sel => sel.Response).FirstOrDefault(),
                        StreetNumber = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 123).Select(sel => sel.Response).FirstOrDefault(),
                        Suburb = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 125).Select(sel => sel.Response).FirstOrDefault(),
                        State = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 126).Select(sel => sel.Response).FirstOrDefault(),
                        PostalCode = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 127).Select(sel => sel.Response).FirstOrDefault(),
                        LandSize = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 107).Select(sel => sel.Response).FirstOrDefault(),
                        IsHobbyFarm = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 108).Select(sel => sel.Response).FirstOrDefault(),
                        HobbyFarmDetail = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 109).Select(sel => sel.Response).FirstOrDefault(),
                        IsForAgistment = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 110).Select(sel => sel.Response).FirstOrDefault(),
                        AgistmentDetail = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 111).Select(sel => sel.Response).FirstOrDefault(),
                        IsForMarketOrParking = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 112).Select(sel => sel.Response).FirstOrDefault(),
                        MarketOrParkingDetail = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 113).Select(sel => sel.Response).FirstOrDefault(),
                        HasStructuresOrBuildings = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 114).Select(sel => sel.Response).FirstOrDefault(),
                        StructureOrBuildingDetail = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 115).Select(sel => sel.Response).FirstOrDefault(),
                        BuildOrSellLand = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 633).Select(sel => sel.Response).FirstOrDefault()
                    });
                }

                model.SituationNumber = model.LiabilityInsuranceListResult.Max(x => x.GroupId).Value;
            }

            model.VacantLandSituation = result;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> VacantLand(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;
            string ratingRationale = "VL " + System.Environment.NewLine;
            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);

            //***************************    CALCULATE: BASE PREMIUM   ***************************

            ILiabilityInsuranceResultBusinessLogic resultRepo = new LiabilityInsuranceResultBusinessLogic();
            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            IPremiumCalculationReferenceBusinessLogic objPremiumBase = new PremiumCalculationReferenceBusinessLogic();
            int sizeOfLandByState = 0;
            int totalSizeOfLandByState = 0;
            int totalLandSize = 0;
            decimal limitOfLiability = (decimal)userInfo.LiabilityLimit;
            decimal premiumBase = 0;

            IDictionary<string, string> situationQuestions = new Dictionary<string, string>();
            string[] situationIds = new string[] { "123", "124", "125", "126", "127", "107", "108", "109", "110", "111", "112", "113", "114", "115", "633" };
            for (int y = 0; y < typeof(VacantLandSituationViewModel).GetProperties().Count() - 1; y++)
            {
                situationQuestions.Add(typeof(VacantLandSituationViewModel).GetProperties()[y + 1].Name, situationIds[y]);
            }

            // GROUP: situations by state
            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            var groups = model.VacantLandSituation.GroupBy(x => x.State).ToList();
            for (int i = 0; i <= groups.Count() - 1; i++)
            {
                // GET: situation by state
                var situations = model.VacantLandSituation.Where(x => x.State == groups[i].Key).ToList();

                foreach (var item in situations)
                {
                    int.TryParse(item.LandSize, out sizeOfLandByState);
                    totalSizeOfLandByState += sizeOfLandByState;
                }
                // GET: Base Rate per state
                premiumBase = objPremiumBase.GetPremiumBaseByLandSize(totalSizeOfLandByState, userInfo.InsuranceId.Value).Rate.Value;

                // GENERATE: Rationale per state
                ratingRationale = ratingRationale + "On " + situations.Select(s => s.State).FirstOrDefault() + ", $" + premiumBase.ToString("N2") + " Premium for " + totalSizeOfLandByState.ToString("N0") + " sqm Land Size ";

                // ADJUST: Base Rate by Liability Limit
                if (limitOfLiability == 5000000)
                {
                    premiumBase = ApplyDiscount(premiumBase, 15);
                    ratingRationale = ratingRationale + " less 15% Multiplier ";
                }
                else if (limitOfLiability == 20000000)
                {
                    var additionalFee = premiumBase * (30M / 100);
                    premiumBase += additionalFee;
                    ratingRationale = ratingRationale + " plus 30% Multiplier ";
                }

                // ADD NEW LINE: Rationale per state
                ratingRationale = ratingRationale + strLiabilityLimit + " | " + System.Environment.NewLine;


                //***************************    APPEND: PREMIUM DETAILS ***************************

                decimal rate = taxRateList.StampTaxRateList.Where(x => x.Text == groups[i].Key).Select(e => Convert.ToDecimal(e.Value)).FirstOrDefault();

                var gst = (premiumBase * .1M);
                var premiumGst = gst + premiumBase;
                var stampDutyFee = premiumGst * Convert.ToDecimal(rate);

                premDtls.Add(new PremiumDetail
                {
                    BasePremium = premiumBase,
                    GSTRate = 0.1M,
                    StateTaxRate = Convert.ToDecimal(rate),
                    HeaderId = userInfo.Id
                });

                totalLandSize += totalSizeOfLandByState;
                sizeOfLandByState = 0;
                totalSizeOfLandByState = 0;
            }

            for (int i = 0; i < model.VacantLandSituation.Count; i++)
            {
                foreach (var item in situationQuestions)
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = int.Parse(item.Value),
                        Response = model.VacantLandSituation[i].GetType().GetProperty(item.Key).GetValue(model.VacantLandSituation[i], null) as string,
                        GroupId = i + 1
                    });
                }
            }

            model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 107 && totalLandSize > 2600000).ToList().ForEach(e => e.IsTriggered = true);
            model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 108 && x.Response == "Yes").ToList().ForEach(e => e.IsTriggered = true);
            model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 110 && x.Response == "Yes").ToList().ForEach(e => e.IsTriggered = true);
            model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 112 && x.Response == "Yes").ToList().ForEach(e => e.IsTriggered = true);
            model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 114 && x.Response == "Yes").ToList().ForEach(e => e.IsTriggered = true);
            //model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 633 && x.Response == "Yes").ToList().ForEach(e => e.IsTriggered = true);

            premDtls.FirstOrDefault().Excess = totalLandSize > 20000 ? 500 : 250;

            //***************************    ADD: RATING RATIONALE   ***************************
            userInfo.RatingRationale = ratingRationale;


            //***************************    FINALIZE -> SAVE ***************************
            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

            // CHECK: IsTriggered Column for any Referral Scenarios
            var HasReferral = model.NeedsUnderwritingReferral;
            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);


            if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                SaveRequoteAsync();
            }

            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** WELDERS AND BOILERMARKERS ****************************************
        public virtual ActionResult WeldersAndBoilermakers()
        {
            ViewBag.GenericUserID = Session["GenericUserID"];
            ViewBag.Status = "New";
            ViewBag.OtherHasValue = '0';
            ILookupBusinessLogic lookupList = new LookupBusinessLogic();
            ILiabilityInsuranceResultBusinessLogic resultRepo = new LiabilityInsuranceResultBusinessLogic();
            LiabilityInsuranceResultViewModels model = new LiabilityInsuranceResultViewModels();

            var referenceLookup = lookupList.GetReferenceByCode("WELDERSANDBOILERMAKERS").OrderBy(x => x.Sequence);
            model.CheckboxReferenceLookup = referenceLookup.Select(x => new SelectListItem()
            {
                Text = x.Text,
                Value = x.Value
            }).ToList();

            List<SelectListItem> allStates = new List<SelectListItem>();
            StampTaxRateViewModels taxRates = new StampTaxRateViewModels();
            foreach (var item in taxRates.StampTaxRateList)
            {
                allStates.Add(
                    new SelectListItem
                    {
                        Text = item.Text,
                        Value = item.Text
                    });
            }
            model.StateList = allStates;

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                int stateQuestionId = 219;
                int welderActivityQuestionId = 132;
                ViewBag.Status = "Requote";
                model.LiabilityInsuranceListResult = resultRepo.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == stateQuestionId)
                                                          .Select(p => p.Response).SingleOrDefault())
                               .FirstOrDefault().Selected = true;

                string[] selectedCheckboxes = model.LiabilityInsuranceListResult
                                              .Where(x => x.QuestionId == welderActivityQuestionId)
                                              .Select(x => x.Response).ToArray();
                var other = selectedCheckboxes.Where(x => model.CheckboxReferenceLookup.All(p2 => p2.Text != x)).FirstOrDefault();

                model.CheckboxReferenceLookup.Where(p => selectedCheckboxes.Contains(p.Text)).ToList().ForEach(cc => cc.Selected = true);
                if (!string.IsNullOrWhiteSpace(other))
                {
                    model.CheckboxReferenceLookup.Where(p => p.Text == "Others").ToList().ForEach(cc => { cc.Selected = true; cc.Value = other; });
                    ViewBag.OtherHasValue = '1';
                }

            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> WeldersAndBoilermakers(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;
            int insuranceId = (int)QuotationHeader.InsuranceId;
            //***************************    APPEND: CHECKBOX VALUES   ***************************

            ILookupBusinessLogic lookupList = new LookupBusinessLogic();
            string[] checkBoxWithTrigger = new string[] { "wb12", "wb13", "wb14" };
            var selectedCheckboxValues = model.CheckboxReferenceLookup.Where(x => x.Selected == true).ToList();
            var selectedCheckboxValuesWithTrigger = new List<SelectListItem>();
            var groupCategory = lookupList.GetReferenceByCode("WELDERSANDBOILERMAKERS").Where(x => selectedCheckboxValues.Select(y => y.Text).Contains(x.Text))
                                                                                      .Select(x => x.Group).Max();
            selectedCheckboxValuesWithTrigger = selectedCheckboxValues.Where(x => checkBoxWithTrigger.Contains(x.Value)).ToList();
            selectedCheckboxValues.RemoveAll(x => checkBoxWithTrigger.Contains(x.Value));

            foreach (var selectedCheckboxItem in selectedCheckboxValues)
            {
                if (selectedCheckboxItem.Text != "Others")
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                      {
                          QuestionId = 132,
                          Response = selectedCheckboxItem.Text
                      });
                }
            }

            foreach (var item in selectedCheckboxValuesWithTrigger)
            {
                model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                {
                    QuestionId = 132,
                    IsTriggered = true,
                    Response = item.Text
                });
            }

            //***************************    CALCULATE: BASE PREMIUM   ***************************

            // INITIALIZE: Variables
            IPremiumCalculationReferenceBusinessLogic referenceRepo = new PremiumCalculationReferenceBusinessLogic();

            decimal annualTurnover = Convert.ToDecimal(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 128).Select(s => s.Response).FirstOrDefault());
            int liabilityLimit = (int)QuotationHeader.LiabilityLimit;
            string strGroupCategory = groupCategory == 5 ? "Diesel" : "Group" + groupCategory;

            // GET: Rates
            //decimal baseRate = (decimal)referenceRepo.GetByInsuranceTypeId(insuranceId).Where(w => w.ConditionType == strGroupCategory).Select(s => s.Rate).FirstOrDefault();
            //decimal baseRate = (from item in referenceRepo.GetByInsuranceTypeId(insuranceId)
            //            where annualTurnover >= (decimal)item.ConditionA && annualTurnover <= (decimal)item.ConditionB
            //            select item).Where(x => x.ConditionType == strGroupCategory).Select(y => y.Rate.Value).FirstOrDefault();

            decimal baseRate = (from item in referenceRepo.GetByInsuranceTypeId(insuranceId)
                                where item.ConditionType == strGroupCategory
                                select item).Where(x => annualTurnover >= (decimal)x.ConditionA && annualTurnover <= (decimal)x.ConditionB).Select(y => y.Rate.Value).FirstOrDefault();

            decimal minPremiumBase = (decimal)referenceRepo.GetByInsuranceTypeId(insuranceId).Where(w => w.ConditionType == "MinBaseRate" && w.ConditionA == groupCategory && w.LiabilityLimit == liabilityLimit).Select(s => s.Rate).FirstOrDefault();
            decimal premiumBase = baseRate * annualTurnover;


            // GET: Excess Rate
            decimal excess = (decimal)referenceRepo.GetByConditionType("Excess").Where(w => w.InsuranceTypeID == userInfo.InsuranceId && w.ConditionB == groupCategory).Select(s => s.Rate).FirstOrDefault();

            // INITIATE: RATING RATIONALE
            string ratingRationale = "WB Group " + groupCategory + " $" + annualTurnover.ToString("N0") + " x " + baseRate.ToString("P3");
            string strPremiumBase = "";
            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);

            // Adjustment to Base Rates if applicable(Only IF Limit of Liability = $5M or $20M)
            decimal rateAdjustment = (decimal)referenceRepo.GetPremiumCalculationReference().Where(x => x.InsuranceTypeID == insuranceId && x.ConditionType == "RateAdjustment" && x.LiabilityLimit == liabilityLimit).Select(s => s.Rate).FirstOrDefault();

            if (liabilityLimit == 5000000)
            {
                baseRate = baseRate - (baseRate * rateAdjustment);
                ratingRationale = ratingRationale + " less " + rateAdjustment.ToString("P0") + " Limit Multiplier";
            }
            else if (liabilityLimit == 20000000)
            {
                baseRate = baseRate + (baseRate * rateAdjustment);
                ratingRationale = ratingRationale + " plus " + rateAdjustment.ToString("P0") + " Limit Multiplier";
            }

            // Determine Initial Premium
            premiumBase = baseRate * annualTurnover;
            strPremiumBase = "= $" + premiumBase.ToString("N2");

            // Apply Minimum Base Rate if Applicable
            if (premiumBase < minPremiumBase)
            {
                premiumBase = minPremiumBase;
                strPremiumBase = "= $" + premiumBase.ToString("N2") + " Min Prem Applied";
            }


            // GET AND CALCULATE: SUBCONTRACTOR LOADING
            string hasSubContractor = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault();
            string strSubContractor = "";

            if (hasSubContractor == "Yes")
            {
                decimal subConPayment = Convert.ToDecimal(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconPaymentQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault());
                decimal subConPaymentLoading = ComputeSubContractorLoading((decimal)annualTurnover, subConPayment);

                premiumBase = premiumBase + (premiumBase * subConPaymentLoading);
                strSubContractor = " + " + subConPaymentLoading.ToString("P2") + " Subcontractor Loading = $" + premiumBase.ToString("N2");
            }

            //***************************    GENERATE: RATING RATIONALE   ***************************
            ratingRationale = ratingRationale + " " + strPremiumBase + strSubContractor;
            userInfo.RatingRationale = ratingRationale;


            //***************************    APPEND: PREMIUM DETAILS ***************************

            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            var stateTaxRate = (from t in taxRateList.StampTaxRateList
                                where t.Text == model.LiabilityInsuranceListResult.Where(txr => txr.QuestionId == 219).Select(slc => slc.Response).FirstOrDefault()
                                select t.Value).FirstOrDefault();

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = premiumBase,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = excess,
                HeaderId = userInfo.Id
            });

            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);


            //***************************    FINALIZE -> SAVE ***************************

            //Check IsTriggered Column for any Referral Scenarios
            var HasReferral = model.NeedsUnderwritingReferral;
            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);

            if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                SaveRequoteAsync();
            }

            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** SHOPPING CENTRE ****************************************

        public virtual ActionResult ShoppingCentre()
        {
            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            LiabilityInsuranceResultViewModels model = new LiabilityInsuranceResultViewModels();
            List<SelectListItem> allStates = new List<SelectListItem>();
            StampTaxRateViewModels taxRates = new StampTaxRateViewModels();

            foreach (var item in taxRates.StampTaxRateList)
            {
                allStates.Add(
                    new SelectListItem
                    {
                        Text = item.Text,
                        Value = item.Text
                    });
            }
            model.StateList = allStates;

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == 224)
                                                          .Select(p => p.Response).SingleOrDefault())
                               .FirstOrDefault().Selected = true;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> ShoppingCentre(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;

            //***************************    CALCULATE: BASE PREMIUM   ***************************

            decimal premiumBase = 0;
            double annualTurnover = Convert.ToDouble(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 136).Select(s => s.Response).FirstOrDefault());
            int anchorTenants = Convert.ToInt32(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 138).Select(s => s.Response).FirstOrDefault());
            int retailTenants = Convert.ToInt32(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 139).Select(s => s.Response).FirstOrDefault());
            int totalTenantCount = anchorTenants + retailTenants;
            double limitOfLiability = (double)userInfo.LiabilityLimit;
            //decimal anchorRate = 0;
            //decimal retailRate = 0;

            //GET: BASE RATE

            //if (limitOfLiability == 5000000)
            //{
            //    anchorRate = 1700;
            //    retailRate = 212.5M;
            //}
            //else if (limitOfLiability == 10000000)
            //{
            //    anchorRate = 2000;
            //    retailRate = 250;
            //}
            //else if (limitOfLiability == 20000000)
            //{
            //    anchorRate = 2600;
            //    retailRate = 325;
            //}

            IPremiumCalculationReferenceBusinessLogic referenceRepo = new PremiumCalculationReferenceBusinessLogic();

            decimal anchorRate = (decimal)referenceRepo.GetPremiumCalculationReference()
              .Where(w => w.InsuranceTypeID == userInfo.InsuranceId
                  && w.ConditionType == "RatePerAnchorTenant"
                  && w.LiabilityLimit == limitOfLiability)
              .Select(s => s.Rate).FirstOrDefault();

            decimal retailRate = (decimal)referenceRepo.GetPremiumCalculationReference()
             .Where(w => w.InsuranceTypeID == userInfo.InsuranceId
                 && w.ConditionType == "RatePerRetailTenant"
                 && w.LiabilityLimit == limitOfLiability)
             .Select(s => s.Rate).FirstOrDefault();


            premiumBase = anchorTenants * anchorRate;
            premiumBase += retailTenants * retailRate;

            // GET: EXCESS RATE
            decimal excess = 0;
            //if (totalTenantCount > 20) //20-40 tenants. what if more than 40 tenants??
            //    excess = 5000;
            //else
            //    excess = 2500;

            excess = (decimal)referenceRepo.GetPremiumCalculationReference()
           .Where(w => w.InsuranceTypeID == userInfo.InsuranceId
               && w.ConditionType == "TenantExcessRange"
               && totalTenantCount >= w.ConditionA && totalTenantCount <= w.ConditionB)
           .Select(s => s.Rate).FirstOrDefault();

            // GET AND CALCULATE: SUBCONTRACTOR LOADING
            string hasSubContractor = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault();
            string strSubContractor = "";

            if (hasSubContractor == "Yes")
            {
                decimal subConPayment = Convert.ToDecimal(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconPaymentQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault());
                decimal subConPaymentLoading = ComputeSubContractorLoading((decimal)annualTurnover, subConPayment);

                premiumBase = premiumBase + (premiumBase * subConPaymentLoading);
                strSubContractor = " + " + subConPaymentLoading.ToString("P2") + " Subcontractor Loading = $" + premiumBase.ToString("N2");
            }

            //***************************    GENERATE: RATING RATIONALE   ***************************
            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);

            string ratingRationale = "SC $" + anchorRate.ToString("N2") + " x " + anchorTenants + " Anchor Tenants plus $" + retailRate.ToString("N2") + " x " + retailTenants + " Retail Tenants " + strLiabilityLimit + " = $" + premiumBase.ToString("N2");

            userInfo.RatingRationale = ratingRationale + strSubContractor;

            //***************************    APPEND: PREMIUM DETAILS ***************************

            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            var stateTaxRate = (from t in taxRateList.StampTaxRateList
                                where t.Text == model.LiabilityInsuranceListResult.Where(txr => txr.QuestionId == 224).Select(slc => slc.Response).FirstOrDefault()
                                select t.Value).FirstOrDefault();

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = premiumBase,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = excess,
                HeaderId = userInfo.Id
            });

            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

            //***************************    FINALIZE -> SAVE ***************************

            //Check IsTriggered Column for any Referral Scenarios
            var HasReferral = model.NeedsUnderwritingReferral;
            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }

            TagAsForReferral(HasReferral);

            if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

                SaveRequoteAsync();
            }

            //GeneratePremiumReport(userInfo.Id, "Shopping Centre");
            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** EVENTS AND MARKETS ****************************************

        public virtual ActionResult EventsAndMarkets()
        {
            //EVENTSANDMARKETS
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            var model = new LiabilityInsuranceResultViewModels();
            var eventAndMarketList = lookupService.GetReferenceByCode("EVENTSANDMARKETS");

            model.EventType = eventAndMarketList.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Value
            }).ToList();

            //model.EventType.Add(new SelectListItem() { Text = " -- Please Select One -- ", Value = "0" });

            //Requote?
            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);

                var selectedEventType = model.LiabilityInsuranceListResult
                                        .Where(p => p.QuestionId == 499)
                                        .Select(p => p.Response).FirstOrDefault();

                model.EventType.Where(x => x.Text == selectedEventType).FirstOrDefault().Selected = true;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> EventsAndMarkets(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;

            if (model.SelectedEventType == "Market Organiser")
                userInfo.InsuranceId = 99;

            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();

            //*************************** Checkbox Values + Triggers -> Result Model ***************************

            //Get Checkbox Record where IsChecked = true
            var selectedCheckBoxValues = model.CheckboxReferenceLookup.Where(x => x.Selected == true).ToList();

            //Add Necessary Question ID and User ID on the record
            foreach (var SelectedCheckBoxItem in selectedCheckBoxValues) //Included Activities
            {
                model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                {
                    UserInfoId = userInfo.Id,
                    QuestionId = 518, //Just Events | 551 for Market | 620 for General
                    Response = SelectedCheckBoxItem.Text,
                    GroupId = null
                });
            }

            //Add Event Type
            model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
            {
                UserInfoId = userInfo.Id,
                QuestionId = 499, //Event Type
                Response = model.SelectedEventType,
                GroupId = null
            });

            //***************************    PREMIUM CALCULATIONS    ***************************
            //--> NAY!


            //--> Get State Tax Rate
            var StateQuestionID = 0;

            if (model.SelectedEventType == "Market Organiser")
                StateQuestionID = 537;
            else
                StateQuestionID = 503;

            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            var stateTaxRate = (from t in taxRateList.StampTaxRateList
                                where t.Text == model.LiabilityInsuranceListResult.Where(txr => txr.QuestionId == StateQuestionID).Select(slc => slc.Response).FirstOrDefault()
                                select t.Value).FirstOrDefault();

            //*************************** Premium Detail Appending ***************************
            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = 0,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = 1000,
                HeaderId = userInfo.Id,
                CreateDate = DateTime.Now
            });

            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

            //Always Redirect to Referral Page and Trigger Referral to Underwriter
            var HasReferral = true;
            TagAsForReferral(HasReferral);


            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
                SaveRequoteAsync();
            }
            else
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** EVENTS AND MARKETS - EVENT ORGANISER ****************************************
        public virtual ActionResult _EventOrganiser()
        {
            var InsuranceID = 13;

            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            IDropDownValueBusinessLogic dropDownService = new DropDownValueBusinessLogic();
            ICheckBoxValueBusinessLogic checkBoxService = new CheckBoxValueBusinessLogic();
            var model = new LiabilityInsuranceResultViewModels();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);

            model.CheckboxReferenceLookup = checkBoxService
                                            .GetCheckBoxValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 518)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.OrderID.ToString()
                                            }).ToList();

            model.AccommodationCoverRequired = dropDownService
                                            .GetDropdownValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 505)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();

            model.AccommodationIndoorOutdoor = dropDownService
                                            .GetDropdownValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 510)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();

            model.StateList = stateTaxRates.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Text
            }).ToList();


            //Requote?
            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == 503)
                                                          .Select(p => p.Response).SingleOrDefault())
                                                          .FirstOrDefault().Selected = true;

                var selectedCheckboxes = model.LiabilityInsuranceListResult
                                              .Where(x => x.QuestionId == 518)
                                              .Select(x => x.Response).ToArray();

                model.CheckboxReferenceLookup.Where(p => selectedCheckboxes.Contains(p.Text)).ToList().ForEach(cc => cc.Selected = true);

                model.AccommodationCoverRequired.Where(p => p.Value == model.LiabilityInsuranceListResult
                                                                         .Where(x => x.QuestionId == 505)
                                                                         .Select(x => x.Response).FirstOrDefault())
                                                                         .FirstOrDefault().Selected = true;

                model.AccommodationIndoorOutdoor.Where(p => p.Value == model.LiabilityInsuranceListResult
                                                                         .Where(x => x.QuestionId == 510)
                                                                         .Select(x => x.Response).FirstOrDefault())
                                                                         .FirstOrDefault().Selected = true;
            }

            return PartialView(model);
        }
        #endregion

        #region **************************************** EVENTS AND MARKETS - MARKET ORGANISER  ****************************************
        public virtual ActionResult _MarketOrganiser()
        {
            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            IDropDownValueBusinessLogic dropDownService = new DropDownValueBusinessLogic();
            ICheckBoxValueBusinessLogic checkBoxService = new CheckBoxValueBusinessLogic();
            var model = new LiabilityInsuranceResultViewModels();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);

            var dropDownSvcData = dropDownService.GetDropDownValue();

            model.CheckboxReferenceLookup = checkBoxService
                                            .GetCheckBoxValue()
                                            .Where(w => w.QuestionID == 551)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.OrderID.ToString()
                                            }).ToList();

            model.AccommodationCoverRequired = dropDownSvcData
                                            .Where(w => w.QuestionID == 539)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();

            model.AccommodationIndoorOutdoor = dropDownSvcData
                                            .Where(w => w.QuestionID == 543)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();

            model.StateList = stateTaxRates.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Text
            }).ToList();

            //Requote?
            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == 537)
                                                          .Select(p => p.Response).SingleOrDefault())
                                                          .FirstOrDefault().Selected = true;

                var selectedCheckboxes = model.LiabilityInsuranceListResult
                                              .Where(x => x.QuestionId == 551)
                                              .Select(x => x.Response).ToArray();

                model.CheckboxReferenceLookup.Where(p => selectedCheckboxes.Contains(p.Text)).ToList().ForEach(cc => cc.Selected = true);

                model.AccommodationCoverRequired.Where(p => p.Value == model.LiabilityInsuranceListResult
                                                                         .Where(x => x.QuestionId == 539)
                                                                         .Select(x => x.Response).FirstOrDefault())
                                                                         .FirstOrDefault().Selected = true;

                model.AccommodationIndoorOutdoor.Where(p => p.Value == model.LiabilityInsuranceListResult
                                                                         .Where(x => x.QuestionId == 543)
                                                                         .Select(x => x.Response).FirstOrDefault())
                                                                         .FirstOrDefault().Selected = true;
            }

            return PartialView(model);
        }
        #endregion

        #region **************************************** MARKET STALL HOLDERS ****************************************
        public virtual ActionResult MarketStallHolders()
        {
            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            IDropDownValueBusinessLogic dropDownService = new DropDownValueBusinessLogic();
            ICheckBoxValueBusinessLogic checkBoxService = new CheckBoxValueBusinessLogic();
            var model = new LiabilityInsuranceResultViewModels();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);

            model.StateList = stateTaxRates.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Text
            }).ToList();

            //Requote?
            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == 570)
                                                          .Select(p => p.Response).SingleOrDefault())
                                                          .FirstOrDefault().Selected = true;
            }
            return PartialView(model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> MarketStallHolders(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;
            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();

            //***************************    PREMIUM CALCULATIONS    ***************************
            //--> NAY!


            //--> Get State Tax Rate
            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            var stateTaxRate = (from t in taxRateList.StampTaxRateList
                                where t.Text == model.LiabilityInsuranceListResult.Where(txr => txr.QuestionId == 570).Select(slc => slc.Response).FirstOrDefault()
                                select t.Value).FirstOrDefault();

            //*************************** Premium Detail Appending ***************************
            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = 0,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = 250,
                HeaderId = userInfo.Id,
                CreateDate = DateTime.Now
            });

            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

            //Always Redirect to Referral Page and Trigger Referral to Underwriter
            var HasReferral = true;
            TagAsForReferral(HasReferral);


            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
                SaveRequoteAsync();
            }
            else
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

            return RedirectToAction("Thankyou", "Home");
        }
        #endregion

        #region **************************************** GENERAL ****************************************
        public virtual ActionResult General()
        {
            var InsuranceID = 15;

            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            IDropDownValueBusinessLogic dropDownService = new DropDownValueBusinessLogic();
            ICheckBoxValueBusinessLogic checkBoxService = new CheckBoxValueBusinessLogic();
            var model = new LiabilityInsuranceResultViewModels();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);

            model.CheckboxReferenceLookup = checkBoxService
                                            .GetCheckBoxValueByInsuranceID(InsuranceID)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.OrderID.ToString()
                                            }).ToList();

            model.StateList = stateTaxRates.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Text
            }).ToList();


            //Requote?
            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == 603)
                                                          .Select(p => p.Response).SingleOrDefault())
                                                          .FirstOrDefault().Selected = true;

                model.CheckboxReferenceLookup.Where(p => model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 620).Select(y => y.Response).ToArray().Contains(p.Text))
                    .ToList()
                    .ForEach(sel => sel.Selected = true);

            }

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> General(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;
            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();

            //***************************    PREMIUM CALCULATIONS    ***************************
            //--> NAY!


            //--> Get State Tax Rate
            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            var stateTaxRate = (from t in taxRateList.StampTaxRateList
                                where t.Text == model.LiabilityInsuranceListResult.Where(txr => txr.QuestionId == 603).Select(slc => slc.Response).FirstOrDefault()
                                select t.Value).FirstOrDefault();

            //*************************** Premium Detail Appending ***************************

            var workActivities = model.CheckboxReferenceLookup.Where(x => x.Selected == true).Select(y => new LiabilityInsuranceResult
            {
                UserInfoId = QuotationHeader.Id,
                QuestionId = 620,
                Response = y.Text
            });
            model.LiabilityInsuranceListResult.AddRange(workActivities);

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = 0,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = 1000,
                HeaderId = userInfo.Id,
                CreateDate = DateTime.Now
            });

            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

            //Always Redirect to Referral Page and Trigger Referral to Underwriter
            var HasReferral = true;
            TagAsForReferral(HasReferral);


            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
                SaveRequoteAsync();
            }
            else
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** HOTELS ****************************************
        public virtual ActionResult Hotels()
        {
            var InsuranceID = 17;
            ViewBag.Status = "New";
            ViewBag.OtherHasValue = '0';
            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            IDropDownValueBusinessLogic dropDownService = new DropDownValueBusinessLogic();
            ICheckBoxValueBusinessLogic checkBoxService = new CheckBoxValueBusinessLogic();
            var model = new LiabilityInsuranceResultViewModels();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);

            model.CheckboxReferenceLookup = checkBoxService
                                .GetCheckBoxValueByInsuranceID(InsuranceID)
                                .Select(chb => new SelectListItem
                                {
                                    Text = chb.Item,
                                    Value = chb.OrderID.ToString()
                                }).ToList();

            model.HLScope = dropDownService
                                            .GetDropdownValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 705)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();

            model.HLDanceFrequency = dropDownService
                                            .GetDropdownValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 719)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();

            model.StateList = stateTaxRates.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Text
            }).ToList();


            //Requote?
            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);
                ViewBag.Status = "Requote";
                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == 703)
                                                          .Select(p => p.Response).SingleOrDefault())
                                                          .FirstOrDefault().Selected = true;

                model.CheckboxReferenceLookup.Where(p => model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 716).Select(y => y.Response).ToArray().Contains(p.Text))
                    .ToList()
                    .ForEach(sel => sel.Selected = true);

                model.HLScope.Where(p => p.Value == model.LiabilityInsuranceListResult
                                                                         .Where(x => x.QuestionId == 705)
                                                                         .Select(x => x.Response).FirstOrDefault())
                                             .FirstOrDefault().Selected = true;

                model.HLDanceFrequency.Where(p => p.Value == model.LiabilityInsuranceListResult
                                                                         .Where(x => x.QuestionId == 719)
                                                                         .Select(x => x.Response).FirstOrDefault())
                                             .FirstOrDefault().Selected = true;

            }

            return PartialView(model);
            //return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Hotels(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;
            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();

            //***************************    PREMIUM CALCULATIONS    ***************************
            //--> NAY!


            //--> Get State Tax Rate
            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            var stateTaxRate = (from t in taxRateList.StampTaxRateList
                                where t.Text == model.LiabilityInsuranceListResult.Where(txr => txr.QuestionId == 703).Select(slc => slc.Response).FirstOrDefault()
                                select t.Value).FirstOrDefault();

            //*************************** Premium Detail Appending ***************************

            var liveEntertainment = model.CheckboxReferenceLookup.Where(x => x.Selected == true).Select(y => new LiabilityInsuranceResult
            {
                UserInfoId = QuotationHeader.Id,
                QuestionId = 716,
                Response = y.Text
            });
            model.LiabilityInsuranceListResult.AddRange(liveEntertainment);

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = 0,
                GSTRate = 0,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = 2500,
                HeaderId = userInfo.Id,
                CreateDate = DateTime.Now
            });

            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

            //Always Redirect to Referral Page and Trigger Referral to Underwriter
            var HasReferral = true;
            TagAsForReferral(HasReferral);


            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
                SaveRequoteAsync();
            }
            else
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** TRADES ****************************************
        public virtual ActionResult Trades()
        {
            var InsuranceID = 18;

            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            IDropDownValueBusinessLogic dropDownService = new DropDownValueBusinessLogic();
            ICheckBoxValueBusinessLogic checkBoxService = new CheckBoxValueBusinessLogic();
            var model = new LiabilityInsuranceResultViewModels();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);

            model.CheckboxReferenceLookup = checkBoxService
                                            .GetCheckBoxValueByInsuranceID(InsuranceID)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.OrderID.ToString()
                                            }).ToList();

            model.DropdownReferenceLookup = dropDownService
                                            .GetDropdownValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 755 && w.IsActive == true)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();

            model.StateList = stateTaxRates.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Text
            }).ToList();


            //Requote?
            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);

                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == 753)
                                                          .Select(p => p.Response).SingleOrDefault())
                                                          .FirstOrDefault().Selected = true;

                model.CheckboxReferenceLookup.Where(p => model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 770).Select(y => y.Response).ToArray().Contains(p.Text))
                    .ToList()
                    .ForEach(sel => sel.Selected = true);

                model.DropdownReferenceLookup.Where(p => p.Value == model.LiabilityInsuranceListResult
                                                                         .Where(x => x.QuestionId == 755 && x.IsActive == true)
                                                                         .Select(x => x.Response).FirstOrDefault())
                                             .FirstOrDefault().Selected = true;

            }

            return PartialView(model);
            //return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Trades(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;
            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();
            IDropDownValueBusinessLogic dropdownRepo = new DropDownValueBusinessLogic();
            IPremiumCalculationReferenceBusinessLogic referenceRepo = new PremiumCalculationReferenceBusinessLogic();

            //***************************    CALCULATE: BASE PREMIUM   ***************************
            // GET: FACTORS
            double annualTurnover = Convert.ToDouble(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 756).Select(s => s.Response).FirstOrDefault());
            int liabilityLimit = (int)userInfo.LiabilityLimit;

            string niche = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 755).Select(s => s.Response).FirstOrDefault();
            string nicheValue = dropdownRepo.GetDropDownValue().Where(w => w.QuestionID == 755 && w.Item == niche).Select(s => s.Value).FirstOrDefault();

            decimal baseRate = (decimal)referenceRepo.GetByConditionType("Niche" + nicheValue)
                .Where(w => annualTurnover >= w.ConditionA && annualTurnover <= w.ConditionB && w.LiabilityLimit == liabilityLimit)
                .Select(s => s.Rate).FirstOrDefault();

            // GET: STATE TAX RATE
            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            var stateTaxRate = (from t in taxRateList.StampTaxRateList
                                where t.Text == model.LiabilityInsuranceListResult.Where(txr => txr.QuestionId == 753).Select(slc => slc.Response).FirstOrDefault()
                                select t.Value).FirstOrDefault();

            decimal premiumBase = baseRate;

            // GET: EXCESS RATE
            decimal excess = (from item in referenceRepo.GetByInsuranceTypeId(userInfo.InsuranceId.Value)
                              where annualTurnover >= item.ConditionA && annualTurnover <= item.ConditionB
                              select item).Where(x => x.ConditionType == "Excess").Select(y => y.Rate.Value).FirstOrDefault();

            // GET AND CALCULATE: SUBCONTRACTOR LOADING
            string hasSubContractor = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault();
            string strSubContractor = "";

            if (hasSubContractor == "Yes")
            {
                decimal subConPayment = Convert.ToDecimal(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconPaymentQuestionId((int)userInfo.InsuranceId)).Select(s => s.Response).FirstOrDefault());
                decimal subConPaymentLoading = ComputeSubContractorLoading((decimal)annualTurnover, subConPayment);

                premiumBase = premiumBase + (premiumBase * subConPaymentLoading);
                strSubContractor = " + " + subConPaymentLoading.ToString("P2") + " Subcontractor Loading = $" + premiumBase.ToString("N2");
            }

            //***************************    GENERATE: RATING RATIONALE   ***************************

            decimal conditionA = (decimal)referenceRepo.GetByConditionType("Niche" + nicheValue)
                .Where(w => annualTurnover >= w.ConditionA && annualTurnover <= w.ConditionB && w.LiabilityLimit == liabilityLimit)
                .Select(s => s.ConditionA).FirstOrDefault();

            decimal conditionB = (decimal)referenceRepo.GetByConditionType("Niche" + nicheValue)
                .Where(w => annualTurnover >= w.ConditionA && annualTurnover <= w.ConditionB && w.LiabilityLimit == liabilityLimit)
                .Select(s => s.ConditionB == 999999999999999 ? (decimal)s.ConditionA : Math.Floor((decimal)s.ConditionB)).FirstOrDefault();

            string strRange = " $" + conditionA.ToString("N0") + " - $" + conditionB.ToString("N0");
            if (conditionB == conditionA)
                strRange = " Greater Than $" + conditionB.ToString("N0");

            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);
            string ratingRationale = "TR Niche " + nicheValue + strRange + " Turnover " + strLiabilityLimit + " = $" + premiumBase.ToString("N2") + strSubContractor;

            userInfo.RatingRationale = ratingRationale;

            //***************************    APPEND: CHECKBOX AND DROPDOWN VALUES AND TRIGGER   ***************************

            // APPEND: Checkbox Triggers and Question IDs
            var workActivities = model.CheckboxReferenceLookup.Where(x => x.Selected == true).ToList();
            foreach (var SelectedCheckBoxItem in workActivities) //Included Activities
            {
                // Determine Trigger if Checkbox Value = None of the above
                if (SelectedCheckBoxItem.Text.Trim() == "NONE OF THE ABOVE")
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = 770,
                        Response = SelectedCheckBoxItem.Text,
                        GroupId = null
                    });
                }
                else
                {
                    model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                    {
                        UserInfoId = userInfo.Id,
                        QuestionId = 770,
                        Response = SelectedCheckBoxItem.Text,
                        GroupId = null,
                        IsTriggered = true
                    });
                }
            }

            // APPEND: Dropdown Triggers [Selected Trade Niche]
            ILookupBusinessLogic lookupRepo = new LookupBusinessLogic();
            var tradeNicheTrigger = lookupRepo.GetByCodeAndText("TRADENICHE_TRIGGERS", niche);
            if (tradeNicheTrigger != null)
                foreach (var item in model.LiabilityInsuranceListResult)
                {
                    if (item.QuestionId == 755)
                        item.IsTriggered = true;
                }

            //***************************    APPEND: PREMIUM DETAILS ***************************

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = premiumBase,
                GSTRate = 0.1M,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                Excess = excess,
                HeaderId = userInfo.Id,
                CreateDate = DateTime.Now
            });

            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

            //***************************    FINALIZE -> SAVE ***************************

            // Check IsTriggered Column for any Referral Scenarios
            bool HasReferral = model.NeedsUnderwritingReferral;
            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);


            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
                SaveRequoteAsync();
            }
            else
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** VACANT LAND PROPERTIES ****************************************
        public virtual ActionResult VacantLandProperties()
        {
            var InsuranceID = 19;
            IZohoService zoho = new ZohoService();
            ILiabilityInsuranceResultBusinessLogic resultRepo = new LiabilityInsuranceResultBusinessLogic();
            IDropDownValueBusinessLogic dropDownService = new DropDownValueBusinessLogic();
            LiabilityInsuranceResultViewModels model = new LiabilityInsuranceResultViewModels();
            string requoteStatus = UserResponseEnum.Requote.ToString();
            List<SelectListItem> allStates = new List<SelectListItem>();
            StampTaxRateViewModels taxRates = new StampTaxRateViewModels();
            List<VacantLandPropertySituationModel> result = new List<VacantLandPropertySituationModel>();
            ILiabilityInsuranceResultBusinessLogic liabilityResultService = new LiabilityInsuranceResultBusinessLogic();

            model.StateList = taxRates.StampTaxRateList.Select(item => new SelectListItem
            {
                Text = item.Text,
                Value = item.Text
            }).ToList();

            if (QuotationHeader.Status == requoteStatus.ToString())
            {
                model.LiabilityInsuranceListResult = resultRepo.GetByUserId(QuotationHeader.Id);
                TempData["PropertyOwnersModel"] = model;
                model.SituationNumber = model.LiabilityInsuranceListResult.Max(x => x.GroupId).Value;
            }




            //CheckBox
            var WallConstructCheckBox = new CheckBoxValueBusinessLogic().GetCheckBoxValueByInsuranceID(19).Where(wh => wh.QuestionID == 357);
            var RoofConstructCheckBox = new CheckBoxValueBusinessLogic().GetCheckBoxValueByInsuranceID(19).Where(wh => wh.QuestionID == 358);
            var FloorConstructCheckBox = new CheckBoxValueBusinessLogic().GetCheckBoxValueByInsuranceID(19).Where(wh => wh.QuestionID == 359);
            var FireProtectionCheckBox = new CheckBoxValueBusinessLogic().GetCheckBoxValueByInsuranceID(19).Where(wh => wh.QuestionID == 363);
            var SecurityProtectionCheckBox = new CheckBoxValueBusinessLogic().GetCheckBoxValueByInsuranceID(19).Where(wh => wh.QuestionID == 366);

            //DropDown
            model.VLPUsePremises = dropDownService
                                            .GetDropdownValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 356)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();
            model.VLPIsPremiseConnected = dropDownService
                                            .GetDropdownValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 364)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();
            model.VLPWhatIsLocalFireBrigade = dropDownService
                                            .GetDropdownValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 365)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();
            model.VLPGlassSumInsured = dropDownService
                                            .GetDropdownValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 370)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item,
                                                Value = chb.Item
                                            }).ToList();


            result.Add(new VacantLandPropertySituationModel
            {
                SituationNumber = "1",
                StreetName = string.Empty,
                StreetNumber = string.Empty,
                Suburb = string.Empty,
                State = "Queensland",
                PostalCode = string.Empty,
                LandSize = string.Empty,
                IsHobbyFarm = string.Empty,
                HobbyFarmDetail = string.Empty,
                IsForAgistment = string.Empty,
                AgistmentDetail = string.Empty,
                IsForMarketOrParking = string.Empty,
                MarketOrParkingDetail = string.Empty,
                HasStructuresOrBuildings = string.Empty,
                StructureOrBuildingDetail = string.Empty,
                BuildOrSellLand = string.Empty,
                PremiseUse = string.Empty,
                WallConstruction = WallConstructCheckBox.Select((itm, indx) => new SelectListItemCheckboxes { GroupId = indx, Text = itm.Item, Value = itm.Item }).ToList(),
                RoofConstrucion = RoofConstructCheckBox.Select((itm, indx) => new SelectListItemCheckboxes { GroupId = indx, Text = itm.Item, Value = itm.Item }).ToList(),
                FloorConstruction = FloorConstructCheckBox.Select((itm, indx) => new SelectListItemCheckboxes { GroupId = indx, Text = itm.Item, Value = itm.Item }).ToList(),
                PercEPSorPIR = string.Empty,
                YearPremiseBuilt = string.Empty,
                YearPremiseRewired = string.Empty,
                FireProtection = FireProtectionCheckBox.Select((itm, indx) => new SelectListItemCheckboxes { GroupId = indx, Text = itm.Item, Value = itm.Item }).ToList(),
                PremiseTownWaterConnection = string.Empty,
                LocalFireBrigade = string.Empty,
                SecurityProtection = SecurityProtectionCheckBox.Select((itm, indx) => new SelectListItemCheckboxes { GroupId = indx, Text = itm.Item, Value = itm.Item }).ToList(),
                BuildingSumInsured = string.Empty,
                ContentsSumInsured = string.Empty,
                TheftSumInsured = string.Empty,
                GlassSumInsured = string.Empty
            });
            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                result.Clear();
                model.LiabilityInsuranceListResult = liabilityResultService.GetByUserId(QuotationHeader.Id);
                for (int i = 1; i < model.LiabilityInsuranceListResult.Max(x => x.GroupId).Value + 1; i++)
                {
                    var NewWallConstructCheckBox = WallConstructCheckBox.Select((itm, indx) => new SelectListItemCheckboxes { GroupId = indx, Text = itm.Item, Value = itm.Item }).ToList();
                    NewWallConstructCheckBox.Where(x => model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 357).Select(sel => sel.Response).ToArray().Contains(x.Text)).ToList().ForEach(s => s.Selected = true);

                    var NewRoofConstructCheckBox = RoofConstructCheckBox.Select((itm, indx) => new SelectListItemCheckboxes { GroupId = indx, Text = itm.Item, Value = itm.Item }).ToList();
                    NewRoofConstructCheckBox.Where(x => model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 358).Select(sel => sel.Response).ToArray().Contains(x.Text)).ToList().ForEach(s => s.Selected = true);

                    var NewFloorConstructCheckBox = FloorConstructCheckBox.Select((itm, indx) => new SelectListItemCheckboxes { GroupId = indx, Text = itm.Item, Value = itm.Item }).ToList();
                    NewFloorConstructCheckBox.Where(x => model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 359).Select(sel => sel.Response).ToArray().Contains(x.Text)).ToList().ForEach(s => s.Selected = true);

                    var NewFireProtectionCheckBox = FireProtectionCheckBox.Select((itm, indx) => new SelectListItemCheckboxes { GroupId = indx, Text = itm.Item, Value = itm.Item }).ToList();
                    NewFireProtectionCheckBox.Where(x => model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 363).Select(sel => sel.Response).ToArray().Contains(x.Text)).ToList().ForEach(s => s.Selected = true);

                    var NewSecurityProtectionCheckBox = SecurityProtectionCheckBox.Select((itm, indx) => new SelectListItemCheckboxes { GroupId = indx, Text = itm.Item, Value = itm.Item }).ToList();
                    NewSecurityProtectionCheckBox.Where(x => model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 366).Select(sel => sel.Response).ToArray().Contains(x.Text)).ToList().ForEach(s => s.Selected = true);

                    result.Add(new VacantLandPropertySituationModel
                    {
                        SituationNumber = i.ToString(),
                        StreetName = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 351).Select(sel => sel.Response).FirstOrDefault(),
                        StreetNumber = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 350).Select(sel => sel.Response).FirstOrDefault(),
                        Suburb = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 352).Select(sel => sel.Response).FirstOrDefault(),
                        State = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 353).Select(sel => sel.Response).FirstOrDefault(),
                        PostalCode = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 354).Select(sel => sel.Response).FirstOrDefault(),
                        LandSize = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 355).Select(sel => sel.Response).FirstOrDefault(),
                        PremiseUse = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 356).Select(sel => sel.Response).FirstOrDefault(),
                        WallConstruction = NewWallConstructCheckBox,
                        RoofConstrucion = NewRoofConstructCheckBox,
                        FloorConstruction = NewFloorConstructCheckBox,
                        PercEPSorPIR = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 360).Select(sel => sel.Response).FirstOrDefault(),
                        YearPremiseBuilt = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 361).Select(sel => sel.Response).FirstOrDefault(),
                        YearPremiseRewired = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 362).Select(sel => sel.Response).FirstOrDefault(),
                        FireProtection = NewFireProtectionCheckBox,
                        PremiseTownWaterConnection = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 364).Select(sel => sel.Response).FirstOrDefault(),
                        LocalFireBrigade = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 365).Select(sel => sel.Response).FirstOrDefault(),
                        SecurityProtection = NewSecurityProtectionCheckBox,
                        BuildingSumInsured = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 367).Select(sel => sel.Response).FirstOrDefault(),
                        ContentsSumInsured = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 368).Select(sel => sel.Response).FirstOrDefault(),
                        TheftSumInsured = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 369).Select(sel => sel.Response).FirstOrDefault(),
                        GlassSumInsured = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 370).Select(sel => sel.Response).FirstOrDefault(),
                        IsHobbyFarm = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 371).Select(sel => sel.Response).FirstOrDefault(),
                        HobbyFarmDetail = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 372).Select(sel => sel.Response).FirstOrDefault(),
                        IsForAgistment = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 373).Select(sel => sel.Response).FirstOrDefault(),
                        AgistmentDetail = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 374).Select(sel => sel.Response).FirstOrDefault(),
                        IsForMarketOrParking = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 375).Select(sel => sel.Response).FirstOrDefault(),
                        MarketOrParkingDetail = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 376).Select(sel => sel.Response).FirstOrDefault(),
                        HasStructuresOrBuildings = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 377).Select(sel => sel.Response).FirstOrDefault(),
                        StructureOrBuildingDetail = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 378).Select(sel => sel.Response).FirstOrDefault(),
                        BuildOrSellLand = model.LiabilityInsuranceListResult.Where(fltr => fltr.GroupId == i && fltr.QuestionId == 379).Select(sel => sel.Response).FirstOrDefault()
                    });
                }

                model.SituationNumber = model.LiabilityInsuranceListResult.Max(x => x.GroupId).Value;
            }

            model.VacantLandPropertySituation = result;
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> VacantLandProperties(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;
            string ratingRationale = "VP " + System.Environment.NewLine;
            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);

            //***************************    CALCULATE: BASE PREMIUM   ***************************

            ILiabilityInsuranceResultBusinessLogic resultRepo = new LiabilityInsuranceResultBusinessLogic();
            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            EmergencyServiceLevyRateViewModel ESLRateList = new EmergencyServiceLevyRateViewModel();
            IPremiumCalculationReferenceBusinessLogic objPremiumBase = new PremiumCalculationReferenceBusinessLogic();

            // VARIABLE: Old Vacant Land Questions
            int sizeOfLandByState = 0;
            int totalSizeOfLandByState = 0;
            int totalLandSize = 0;
            decimal limitOfLiability = (decimal)userInfo.LiabilityLimit;
            decimal premInitial = 0;
            decimal premiumBase = 0;

            // VARIABLE: New Vacant Land Property Questions
            decimal sumInsuredBuilding = 0;
            decimal sumInsuredContent = 0;
            decimal sumInsuredTheft = 0;
            bool hasInsuredGlass;

            decimal premPropDmg = 0;
            decimal premTheft = 0;
            decimal premGlass = 0; // Question Dependent

            decimal premPropDmgRate = 0.00395M;
            decimal premTheftRate = 0.01935M;

            // 
            IDictionary<string, string> situationQuestions = new Dictionary<string, string>();
            string[] situationIds = new string[] { "350", "351", "352", "353", "354", "355", "356", "357", "358", "359", "360", "361", "362", "363", "364", "365", "366", "367", "368", "369", "370", "371", "372", "373", "374", "375", "376", "377", "378", "379" };
            for (int y = 0; y < typeof(VacantLandPropertySituationModel).GetProperties().Count() - 1; y++)
            {
                situationQuestions.Add(typeof(VacantLandPropertySituationModel).GetProperties()[y + 1].Name, situationIds[y]);
            }

            // GROUP: situations by state
            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            var groups = model.VacantLandPropertySituation.GroupBy(x => x.State).ToList();
            for (int i = 0; i <= groups.Count() - 1; i++)
            {
                // GET: situation by state
                var situations = model.VacantLandPropertySituation.Where(x => x.State == groups[i].Key).ToList();

                foreach (var item in situations)
                {
                    int.TryParse(item.LandSize, out sizeOfLandByState);
                    totalSizeOfLandByState += sizeOfLandByState;
                }
                // GET: Base Rate per state
                premInitial = objPremiumBase.GetPremiumBaseByLandSize(totalSizeOfLandByState, userInfo.InsuranceId.Value).Rate.Value;

                // GENERATE: Rationale per state
                ratingRationale = ratingRationale + "On " + situations.Select(s => s.State).FirstOrDefault() + ", $" + premInitial.ToString("N2") + " Premium for " + totalSizeOfLandByState.ToString("N0") + " sqm Land Size ";

                // ADJUST: Base Rate by Liability Limit
                if (limitOfLiability == 5000000)
                {
                    premInitial = ApplyDiscount(premInitial, 15);
                    ratingRationale = ratingRationale + " less 15% Multiplier ";
                }
                else if (limitOfLiability == 20000000)
                {
                    var additionalFee = premInitial * (30M / 100);
                    premInitial += additionalFee;
                    ratingRationale = ratingRationale + " plus 30% Multiplier ";
                }

                // ADD NEW LINE: Rationale per state
                ratingRationale = ratingRationale + strLiabilityLimit + " | " + System.Environment.NewLine;


                // COMPUTE: Initial Base Premium + VLP Premiums
                sumInsuredBuilding = situations.Sum(s => Convert.ToDecimal(s.BuildingSumInsured));
                sumInsuredContent = situations.Sum(s => Convert.ToDecimal(s.ContentsSumInsured));
                sumInsuredTheft = situations.Sum(s => Convert.ToDecimal(s.TheftSumInsured));
                hasInsuredGlass = situations.Any(a => a.GlassSumInsured == "Replacement Value");

                premPropDmg = (sumInsuredBuilding + sumInsuredContent) * premPropDmgRate;
                premPropDmg = premPropDmg < 150 ? (premPropDmg == 0 ? 0 : 150) : premPropDmg; // if zero then do not apply minimum

                premTheft = sumInsuredTheft * premTheftRate;
                premTheft = premTheft < 100 ? (premTheft == 0 ? 0 : 100) : premTheft; // if zero then do not apply minimum

                premGlass = hasInsuredGlass == true ? 225 : 0;

                // COMPUTED PER-STATE PREMIUM BASE
                premiumBase = premInitial + premPropDmg + premTheft + premGlass;

                //***************************    APPEND: PREMIUM DETAILS ***************************

                decimal SDRate = taxRateList.StampTaxRateList.Where(x => x.Text == groups[i].Key).Select(e => Convert.ToDecimal(e.Value)).FirstOrDefault();
                decimal ESLRate = ESLRateList.ESLRateList.Where(w => w.Text == groups[i].Key).Select(s => Convert.ToDecimal(s.Value)).FirstOrDefault();

                var GST = (premiumBase * .1M);
                var premiumGST = GST + premiumBase;
                var stampDutyFee = premiumGST * Convert.ToDecimal(SDRate);

                premDtls.Add(new PremiumDetail
                {
                    BasePremium = premiumBase,
                    GSTRate = 0.1M,
                    StateTaxRate = Convert.ToDecimal(SDRate),
                    ESLRate = Convert.ToDecimal(ESLRate),
                    HeaderId = userInfo.Id
                });

                totalLandSize += totalSizeOfLandByState;
                sizeOfLandByState = 0;
                totalSizeOfLandByState = 0;
            }


            var liabilityResult = ConvertVacantPropertyViewModel(model.VacantLandPropertySituation, userInfo.Id, situationQuestions);
            //model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 107 && totalLandSize > 2600000).ToList().ForEach(e => e.IsTriggered = true);
            //model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 108 && x.Response == "Yes").ToList().ForEach(e => e.IsTriggered = true);
            //model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 110 && x.Response == "Yes").ToList().ForEach(e => e.IsTriggered = true);
            //model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 112 && x.Response == "Yes").ToList().ForEach(e => e.IsTriggered = true);
            //model.LiabilityInsuranceListResult.Where(x => x.QuestionId == 114 && x.Response == "Yes").ToList().ForEach(e => e.IsTriggered = true);

            premDtls.FirstOrDefault().Excess = totalLandSize > 20000 ? 500 : 250;

            //***************************    ADD: RATING RATIONALE   ***************************
            userInfo.RatingRationale = ratingRationale;


            //***************************    FINALIZE -> SAVE ***************************
            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

            // CHECK: IsTriggered Column for any Referral Scenarios
            var HasReferral = model.NeedsUnderwritingReferral;
            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);


            if (QuotationHeader.Status == (UserResponseEnum.New.ToString()))
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, liabilityResult);
            else if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, liabilityResult);

                SaveRequoteAsync();
            }

            return RedirectToAction("Thankyou", "Home");
        }

        #endregion

        #region **************************************** HANDMADE *******************************************
        public virtual ActionResult Handmade()
        {
            var InsuranceID = 20;

            ILiabilityInsuranceResultBusinessLogic liabilityResults = new LiabilityInsuranceResultBusinessLogic();
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            IDropDownValueBusinessLogic dropDownService = new DropDownValueBusinessLogic();
            ICheckBoxValueBusinessLogic checkBoxService = new CheckBoxValueBusinessLogic();
            var model = new LiabilityInsuranceResultViewModels();
            var stateTaxRates = lookupService.GetReferenceByCode(STAMPTAXRATE);

            model.StateList = stateTaxRates.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Text
            }).OrderBy(x => x.Text).ToList();

            model.CheckboxReferenceLookup = checkBoxService
                                            .GetCheckBoxValueByInsuranceID(InsuranceID)
                                            .Where(w => w.QuestionID == 789)
                                            .Select(chb => new SelectListItem
                                            {
                                                Text = chb.Item.Trim(),
                                                Value = chb.Value
                                            }).ToList();

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                model.LiabilityInsuranceListResult = liabilityResults.GetByUserId(QuotationHeader.Id);


                model.StateList.Where(x => x.Text == model.LiabilityInsuranceListResult
                                                          .Where(p => p.QuestionId == 787)
                                                          .Select(p => p.Response).SingleOrDefault()).FirstOrDefault().Selected = true;

                var selectedCheckboxes = model.LiabilityInsuranceListResult
                                             .Where(x => x.QuestionId == 789)
                                             .Select(x => x.Response.Trim()).ToArray();

                var other = selectedCheckboxes.Where(x => model.CheckboxReferenceLookup.All(p2 => p2.Text != x)).FirstOrDefault();

                model.CheckboxReferenceLookup.Where(p => selectedCheckboxes.Contains(p.Text)).ToList().ForEach(cc => cc.Selected = true);

                if (!string.IsNullOrWhiteSpace(other))
                {
                    model.CheckboxReferenceLookup.Where(p => p.Text == "Other:").ToList().ForEach(cc => { cc.Selected = true; cc.Value = other; });
                }

            }

            return View(model);
        }
        [HttpPost]
        public virtual async Task<ActionResult> Handmade(LiabilityInsuranceResultViewModels model)
        {
            var userInfo = QuotationHeader;
            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();
            IDropDownValueBusinessLogic dropdownRepo = new DropDownValueBusinessLogic();
            IPremiumCalculationReferenceBusinessLogic referenceRepo = new PremiumCalculationReferenceBusinessLogic();

            //***************************    CALCULATE: BASE PREMIUM   ***************************
            // GET: Factors
            double annualTurnover = Convert.ToDouble(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == 790).Select(s => s.Response).FirstOrDefault());
            int liabilityLimit = (int)userInfo.LiabilityLimit;
            int insuranceTypeId = 20; //Handmade insurance type

            decimal basePremium = (decimal)referenceRepo.GetPremiumCalculationReference()
                .Where(w => w.InsuranceTypeID == insuranceTypeId
                    && annualTurnover >= w.ConditionA
                    && annualTurnover <= w.ConditionB
                    && w.LiabilityLimit == liabilityLimit)
                .Select(s => s.Rate).FirstOrDefault();


            decimal underWritingFee = (decimal)referenceRepo.GetByConditionType("Underwriting Fee").Where(w => w.InsuranceTypeID == insuranceTypeId).Select(s => s.Rate).FirstOrDefault();
            decimal gstRate = (decimal)referenceRepo.GetByConditionType("GST % Rate").Where(w => w.InsuranceTypeID == insuranceTypeId).Select(s => s.Rate).FirstOrDefault();

            if (gstRate == 0.0m)
                gstRate = 0.1m;
            else
                gstRate = gstRate / 100;

            // Calculate GST
            decimal gst = basePremium * gstRate;

            // GET: State Tax Rate
            StampTaxRateViewModels taxRateList = new StampTaxRateViewModels();
            var stateTaxRate = (from t in taxRateList.StampTaxRateList
                                where t.Text == model.LiabilityInsuranceListResult.Where(txr => txr.QuestionId == 787).Select(slc => slc.Response).FirstOrDefault()
                                select t.Value).FirstOrDefault();

            decimal stampDuty = (basePremium + gst) * Convert.ToDecimal(stateTaxRate);
            decimal gstOnunderWritingFee = underWritingFee * gstRate;
            decimal totalBasePremium = basePremium + gst + stampDuty + underWritingFee + gstOnunderWritingFee;

            // GET AND CALCULATE: SUBCONTRACTOR LOADING
            string hasSubContractor = model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconQuestionId(insuranceTypeId)).Select(s => s.Response).FirstOrDefault();
            string strSubContractor = "";

            if (hasSubContractor == "Yes")
            {
                decimal subConPayment = Convert.ToDecimal(model.LiabilityInsuranceListResult.Where(w => w.QuestionId == StaticReference.GetSubconPaymentQuestionId(insuranceTypeId)).Select(s => s.Response).FirstOrDefault());
                decimal subConPaymentLoading = ComputeSubContractorLoading((decimal)annualTurnover, subConPayment);

                basePremium = basePremium + (basePremium * subConPaymentLoading);
                strSubContractor = " + " + subConPaymentLoading.ToString("P2") + " Subcontractor Loading = $" + basePremium.ToString("N2");
            }


            // GET: EXCESS RATE
            decimal excess = (from item in referenceRepo.GetByInsuranceTypeId(userInfo.InsuranceId.Value)
                              where annualTurnover >= item.ConditionA && annualTurnover <= item.ConditionB
                              select item).Where(x => x.ConditionType == "Excess").Select(y => y.Rate.Value).FirstOrDefault();

            ////***************************    GENERATE: RATING RATIONALE   ***************************

            decimal conditionA = (decimal)referenceRepo.GetPremiumCalculationReference()
                .Where(w => w.InsuranceTypeID == insuranceTypeId
                    && annualTurnover >= w.ConditionA
                    && annualTurnover <= w.ConditionB
                    && w.LiabilityLimit == liabilityLimit)
                .Select(s => s.ConditionA).FirstOrDefault();

            decimal conditionB = (decimal)referenceRepo.GetPremiumCalculationReference()
                 .Where(w => w.InsuranceTypeID == insuranceTypeId
                    && annualTurnover >= w.ConditionA
                    && annualTurnover <= w.ConditionB
                    && w.LiabilityLimit == liabilityLimit)
                .Select(s => s.ConditionB == 999999999999999 ? (decimal)s.ConditionA : Math.Floor((decimal)s.ConditionB)).FirstOrDefault();

            string strRange = " $" + conditionA.ToString("N0") + " - $" + conditionB.ToString("N0");
            if (conditionB == conditionA)
                strRange = " Greater Than $" + conditionB.ToString("N0");

            string strLiabilityLimit = ConvertLiabilityLimit((int)userInfo.LiabilityLimit);
            string ratingRationale = "HM " + strRange + " Turnover " + strLiabilityLimit + " = $" + basePremium.ToString("N2") + strSubContractor;

            userInfo.RatingRationale = ratingRationale;

            //***************************    APPEND: CHECKBOX TRIGGER   ***************************

            string[] checkBoxProductsWithTrigger = new string[] { "Organic/Natural sensory items; i.e. natural playdough,", 
                "Natural / organic oils, rubs, moisturizers, creams, lotions and the like", 
                "Children’s play equipment - climbing and riding equipment, playgrounds",
                "Seating & highchairs, including baby car seats & bean bags",
                "BPA Products",
                "Teething Products made from plastic or BPA",
                "Children's carrying products like baby cradles, chest carrying harnesses etc.",
                "Children's bouncers, walkers, jolly jumpers, infant swings",
                "Children's furniture - cots, beds, tables, chairs, change tables etc",
                "Other:",
            };
            var selectedProducts = model.CheckboxReferenceLookup.Where(x => x.Selected == true).ToList();
            var selectedCheckboxValuesWithTrigger = new List<SelectListItem>();
            selectedCheckboxValuesWithTrigger = selectedProducts.Where(x => checkBoxProductsWithTrigger.Contains(x.Text)).ToList();

            // APPEND: Checkbox Triggers and Question IDs
            foreach (var SelectedCheckBoxItem in selectedProducts) //Included Activities
            {
                bool? isTriggered = null;

                if (checkBoxProductsWithTrigger.Contains(SelectedCheckBoxItem.Text))
                    isTriggered = true;

                model.LiabilityInsuranceListResult.Add(new LiabilityInsuranceResult()
                {
                    UserInfoId = userInfo.Id,
                    QuestionId = 789,
                    Response = SelectedCheckBoxItem.Text,
                    GroupId = null,
                    IsTriggered = isTriggered
                });
            }

            //***************************    APPEND: PREMIUM DETAILS ***************************

            List<PremiumDetail> premDtls = new List<PremiumDetail>();
            premDtls.Add(new PremiumDetail
            {
                BasePremium = basePremium,
                GSTRate = gstRate,
                StateTaxRate = Convert.ToDecimal(stateTaxRate),
                HeaderId = userInfo.Id,
                CreateDate = DateTime.Now,
                Excess = excess
            });

            var liabilityQuotationHeaderModel = ComputePremium(userInfo, premDtls);

            //***************************    FINALIZE -> SAVE ***************************

            // Check IsTriggered Column for any Referral Scenarios
            bool HasReferral = model.NeedsUnderwritingReferral;
            if (model.LiabilityInsuranceListResult.Where(w => w.IsTriggered == true).Select(s => s.IsTriggered).Count() > 0)
            {
                HasReferral = true;
            }
            TagAsForReferral(HasReferral);

            if (QuotationHeader.Status == UserResponseEnum.Requote.ToString())
            {
                await SaveRequoteToDb(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);
                SaveRequoteAsync();
            }
            else
                await SaveNewQuoteAsync(liabilityQuotationHeaderModel, premDtls, model.LiabilityInsuranceListResult);

            return RedirectToAction("Thankyou", "Home");

        }
        #endregion

        public async Task SaveNewQuoteAsync(LiabilityQuotationHeader liabilityQuotationHeader, List<PremiumDetail> premiumDetail, List<LiabilityInsuranceResult> liabilityResult)
        {
            try
            {
                ObjectCache cache = MemoryCache.Default;
                var zohoAgent = new ZohoStoreV2.Service.ZohoAgentV2();
                var access_token = cache["access_token"] as string;
                if (access_token == null)
                {
                    access_token = zohoAgent.GenerateTokenv2();
                    cache.Set("access_token", access_token, DateTime.Now.AddMinutes(55));
                }

                var cancellationTokenSource = new CancellationTokenSource();
                var token = cancellationTokenSource.Token;

                ILiabilityInsuranceTypeBusinessLogic insuranceTypeRepo = new LiabilityInsuranceTypeBusinessLogic();
                ILiabilityQuotationHeaderBusinessLogic liabilityQuotationService = new LiabilityQuotationHeaderBusinessLogic();
                IPremiumDetailBusinessLogic premiumDetailService = new PremiumDetailBusinessLogic();
                ILiabilityInsuranceResultBusinessLogic liabilityResultService = new LiabilityInsuranceResultBusinessLogic();
                IWorkerService backgroundWorker = new WorkerService();
                var underwriterId = new LiabilityInsuranceTypeBusinessLogic().GetWithUnderwriterById(liabilityQuotationHeader.InsuranceId.Value).Underwriter.UserId;
                liabilityQuotationHeader.UnderwriterId = underwriterId;
                liabilityQuotationHeader.GeneralLiability = liabilityQuotationHeader.LiabilityLimit;
                liabilityQuotationHeader.ProductLiability = liabilityQuotationHeader.LiabilityLimit;
                List<Task> task = new List<Task> { liabilityQuotationService.CreateLiabilityQuotationHeaderAsync(liabilityQuotationHeader, token), premiumDetailService.CreatePremiumDetailAsync(premiumDetail, token) };
                await Task.WhenAll(task);
                var liabilityResultTask = liabilityResultService.CreateLiabilityInsuranceResult(QuotationHeader.Id, QuotationHeader.InsuranceId.Value, liabilityResult);

                liabilityResultService.SetPolicyDetails(QuotationHeader.Id);

                var jobId = BackgroundJob.Schedule(() => backgroundWorker.SaveAsync(QuotationHeader.ReferenceNumber, QuotationHeader.Status, null, access_token), TimeSpan.FromMinutes(1));
                if (!liabilityQuotationHeader.HasTriggerReferral.Value && liabilityQuotationHeader.Status == "New")
                {
                    List<Entities.Entities.Attachment> attachmentList = new List<Entities.Entities.Attachment>();
                    var liabilityType = insuranceTypeRepo.GetLiabilityInsuranceType().Where(w => w.InsuranceId == liabilityQuotationHeader.InsuranceId).Select(s => s.LiabilityInsurance).SingleOrDefault();
                    // Check if quote status = Closed Lost/Won/Declined
                    // Quote Document
                    string quoteBytes = ReportHelper.GeneratePremiumReport(liabilityQuotationHeader.Id, liabilityType);
                    attachmentList.Add(new Entities.Entities.Attachment { File = ReportHelper.ConvertToByteArray(quoteBytes), LiabilityQuotationHeaderId = liabilityQuotationHeader.Id, Remarks = "Quote PDF", FileName = String.Format("{0}{1}.pdf", "Quote", DateTime.Now.ToString("yyyyMMddHHmmss")) });

                    // Proposal Document
                    //string proposalBytes = ReportHelper.GeneratePremiumReport(liabilityQuotationHeader.Id, "ConfirmationProposal");
                    //attachmentList.Add(new Entities.Entities.Attachment { File = ReportHelper.ConvertToByteArray(proposalBytes), LiabilityQuotationHeaderId = liabilityQuotationHeader.Id, Remarks = "Proposal PDF", FileName = String.Format("{0}{1}.pdf", "Proposal", DateTime.Now.ToString("yyyyMMddHHmmss")) });

                    // ZOHO UPLOAD: Upload Attachments related to the Lead Record
                    BackgroundJob.ContinueJobWith(jobId, () => backgroundWorker.UploadFilesAsync(liabilityQuotationHeader.ReferenceNumber, attachmentList, access_token));
                };

                // ZOHO INSERT: Insert Rating Rationale Notes related to the Lead Record
                BackgroundJob.ContinueJobWith(jobId, () => backgroundWorker.SaveRationaleNoteAsync(liabilityQuotationHeader.ReferenceNumber, access_token));
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n SaveNewQuoteAsync error \n\t : {0}\n", ex));
            }
        }

        public async Task SaveRequoteToDb(LiabilityQuotationHeader liabilityQuotationHeader, List<PremiumDetail> premiumDetail, List<LiabilityInsuranceResult> liabilityResult)
        {
            try
            {
                ObjectCache cache = MemoryCache.Default;
                var zohoAgent = new ZohoStoreV2.Service.ZohoAgentV2();
                var access_token = cache["access_token"] as string;
                if (access_token == null)
                {
                    access_token = zohoAgent.GenerateTokenv2();
                    cache.Set("access_token", access_token, DateTime.Now.AddMinutes(55));
                }

                var cancellationTokenSource = new CancellationTokenSource();
                var token = cancellationTokenSource.Token;
                var userInfo = QuotationHeader;

                ILiabilityInsuranceTypeBusinessLogic insuranceTypeRepo = new LiabilityInsuranceTypeBusinessLogic();
                ILiabilityQuotationHeaderBusinessLogic liabilityQuotationService = new LiabilityQuotationHeaderBusinessLogic();
                IPremiumDetailBusinessLogic premiumDetailService = new PremiumDetailBusinessLogic();
                ILiabilityInsuranceResultBusinessLogic liabilityResultService = new LiabilityInsuranceResultBusinessLogic();
                IWorkerService backgroundWorker = new WorkerService();
                var underwriterId = userInfo.UnderwriterId;
                liabilityQuotationHeader.UnderwriterId = underwriterId;
                premiumDetailService.SetToInactive(QuotationHeader.Id);
                liabilityResultService.SetToInactive(QuotationHeader.Id);
                var quoteHeader = liabilityQuotationService.GetUserInfoByReferenceNumber(liabilityQuotationHeader.ReferenceNumber);
                if (quoteHeader.GeneralLiability != 0)
                    liabilityQuotationHeader.GeneralLiability = liabilityQuotationHeader.LiabilityLimit;
                if (quoteHeader.ProductLiability != 0)
                    liabilityQuotationHeader.ProductLiability = liabilityQuotationHeader.LiabilityLimit;

                List<Task> task = new List<Task> { liabilityQuotationService.UpdateQuotationHeaderAsync(liabilityQuotationHeader, token), premiumDetailService.CreatePremiumDetailAsync(premiumDetail, token) };
                await Task.WhenAll(task);

                liabilityResultService.CreateLiabilityInsuranceResult(QuotationHeader.Id, QuotationHeader.InsuranceId.Value, liabilityResult);

                liabilityResultService.SetPolicyDetails(QuotationHeader.Id);

                if (!liabilityQuotationHeader.HasTriggerReferral.Value)
                {
                    List<Entities.Entities.Attachment> attachmentList = new List<Entities.Entities.Attachment>();
                    var liabilityType = insuranceTypeRepo.GetLiabilityInsuranceType().Where(w => w.InsuranceId == liabilityQuotationHeader.InsuranceId).Select(s => s.LiabilityInsurance).SingleOrDefault();
                    // Check if quote status = Closed Lost/Won/Declined
                    // Quote Document
                    string quoteBytes = ReportHelper.GeneratePremiumReport(liabilityQuotationHeader.Id, liabilityType);
                    attachmentList.Add(new Entities.Entities.Attachment { File = ReportHelper.ConvertToByteArray(quoteBytes), LiabilityQuotationHeaderId = liabilityQuotationHeader.Id, Remarks = "Quote PDF", FileName = String.Format("{0}{1}.pdf", "Quote", DateTime.Now.ToString("yyyyMMddHHmmss")) });

                    // Proposal Document
                    //string proposalBytes = ReportHelper.GeneratePremiumReport(liabilityQuotationHeader.Id, "ConfirmationProposal");
                    //attachmentList.Add(new Entities.Entities.Attachment { File = ReportHelper.ConvertToByteArray(proposalBytes), LiabilityQuotationHeaderId = liabilityQuotationHeader.Id, Remarks = "Proposal PDF", FileName = String.Format("{0}{1}.pdf", "Proposal", DateTime.Now.ToString("yyyyMMddHHmmss")) });

                    // ZOHO UPLOAD: Upload Attachments related to the Lead Record
                    await backgroundWorker.UploadFilesAsync(liabilityQuotationHeader.ReferenceNumber, attachmentList, access_token);

                };
                // ZOHO INSERT: Insert Rating Rationale Notes related to the Lead Record
                backgroundWorker.SaveRationaleNoteAsync(liabilityQuotationHeader.ReferenceNumber, access_token);
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n SaveRequoteToDb error \n\t : {0}\n", ex));
            }
        }

        public void SaveRequoteAsync()
        {
            try
            {
                ObjectCache cache = MemoryCache.Default;
                var zohoAgent = new ZohoStoreV2.Service.ZohoAgentV2();
                var access_token = cache["access_token"] as string;
                if (access_token == null)
                {
                    access_token = zohoAgent.GenerateTokenv2();
                    cache.Set("access_token", access_token, DateTime.Now.AddMinutes(55));
                }

                if (QuotationHeader.LeadId != null)
                {
                    IWorkerService backgroundWorker = new WorkerService();
                    BackgroundJob.Enqueue(() => backgroundWorker.SaveAsync(QuotationHeader.ReferenceNumber, QuotationHeader.Status, null, access_token));
                }
            }
            catch (Exception ex)
            {
                Log.Warn(string.Format("\n SaveRequoteAsync error \n\t : {0}\n", ex));
            }
        }

        #region %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% UNUSED FUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //public void FetchPostedAttachments()
        //{
        //    IAttachmentBusinessLogic attachmentRepo = new AttachmentBusinessLogic();
        //    List<Attachment> attachments = new List<Attachment>();
        //    byte[] file;

        //    for (int i = 0; i < System.Web.HttpContext.Current.Request.Files.Count; i++)
        //    {
        //        HttpPostedFileBase fileUpload = Request.Files[i];

        //        using (Stream fileDataStream = fileUpload.InputStream)
        //        {
        //            using (var reader = new BinaryReader(fileDataStream))
        //            {
        //                file = reader.ReadBytes((int)fileDataStream.Length);
        //            }
        //        }

        //        attachments.Add(new Attachment
        //        {
        //            LiabilityQuotationHeaderId = QuotationHeader.Id,
        //            FileName = fileUpload.FileName,
        //            File = file
        //        });
        //    }

        //    attachmentRepo.CreateAttachment(attachments);
        //}
        #endregion
    }
}