﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CoverBuilder.BLL.BusinessLogics;
using CoverBuilder.BLL.Interfaces;
using CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.ViewModels;
using CoverBuilder.Entities.Enums;
using Hangfire;
using System.Threading;
using System.Collections.Generic;
using CoverBuilder.Helper;
using System.IO;
using CoverBuilder.Common.Logging;
using CoverBuilder.Common.Logging.NLog;
using NLog.Config;
using System.Runtime.Caching;

namespace CoverBuilder.Controllers
{
    public partial class PremiumController : Controller
    {
        //private static readonly ILog Log = LogManager.GetLogger(typeof(PremiumController));
        private static readonly ILogger Log = new NLogger();

        public virtual JsonResult TaggedAsSynced(string referenceNumber)
        {
            ILiabilityQuotationHeaderBusinessLogic liabilityHdr = new LiabilityQuotationHeaderBusinessLogic();
            JsonResponseResult result = new JsonResponseResult();
            var updateResult = liabilityHdr.UpdateIsSynced(referenceNumber);
            if (updateResult > 0)
            {
                result.Status = "Success";
            }
            else
            {
                result.Status = "Failed";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult ModifyStatusDecline(string userId, string status, string comments)
        {
            ILiabilityQuotationHeaderBusinessLogic liabilityHdr = new LiabilityQuotationHeaderBusinessLogic();
            JsonResponseResult result = new JsonResponseResult();
            var userInfo = liabilityHdr.GetUserInfoByReferenceNumber(userId);
            if (userInfo.Status == "Closed - Declined" || userInfo.Status == "Closed - Won" || userInfo.Status == "Closed - Lost")
            {
                result.Status = "Failed";
                result.Result = "Already Declined";
                result.Message = "Can't process request. Quote's already agreed and finalized .";
            }
            else
            {
                var updateResult = liabilityHdr.UpdateStatus(userId, status, comments);
                if (updateResult > 0)
                {
                    result.Status = "Success";
                }
                else
                {
                    result.Status = "Failed";
                }
            }


            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult ModifyStatus(string userId, string status)
        {
            ILiabilityQuotationHeaderBusinessLogic liabilityHdr = new LiabilityQuotationHeaderBusinessLogic();
            var result = liabilityHdr.UpdateStatus(userId, status);
            var response = new JsonResponseResult();
            if (result > 0)
            {
                response.Result = "Success";
            }
            else
            {
                response.Result = "Failed";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult ModifyStatusAccept(string userId, string status, string periodFrom, string periodTo)
        {
            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();

            var result = headerRepo.UpdatePeriod(userId, status, periodFrom, periodTo);
            var response = new JsonResponseResult();
            if (result > 0)
            {
                response.Result = "Success";

                // INITIATE: Generation of Proposal Document
                ILiabilityInsuranceTypeBusinessLogic insuranceTypeRepo = new LiabilityInsuranceTypeBusinessLogic();
                LiabilityQuotationHeader liabilityQuotationHeader = headerRepo.GetUserInfoByReferenceNumber(userId);
                IWorkerService workerService = new WorkerService();
                List<Entities.Entities.Attachment> attachmentList = new List<Entities.Entities.Attachment>();

                // GENERATE: Proposal Document
                string proposalBytes = ReportHelper.GeneratePremiumReport(liabilityQuotationHeader.Id, "ConfirmationProposal");
                attachmentList.Add(new Entities.Entities.Attachment { File = ReportHelper.ConvertToByteArray(proposalBytes), LiabilityQuotationHeaderId = liabilityQuotationHeader.Id, Remarks = "Proposal PDF", FileName = String.Format("{0}{1}.pdf", "Proposal", DateTime.Now.ToString("yyyyMMddHHmmss")) });

                // ZOHO UPLOAD: Upload Attachments related to the Lead Record
                //workerService.UploadFilesAsync(liabilityQuotationHeader.ReferenceNumber, attachmentList);

                ObjectCache cache = MemoryCache.Default;
                var zohoAgent = new ZohoStoreV2.Service.ZohoAgentV2();
                var access_token = cache["access_token"] as string;
                if (access_token == null)
                {
                    access_token = zohoAgent.GenerateTokenv2();
                    cache.Set("access_token", access_token, DateTime.Now.AddMinutes(55));
                }

                BackgroundJob.Schedule(() => workerService.UploadFilesAsync(liabilityQuotationHeader.ReferenceNumber, attachmentList, access_token), TimeSpan.FromMinutes(1));
            }
            else
            {
                response.Result = "Failed";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult UpdateOwner(string ownerId, string referenceNum)
        {
            ILiabilityQuotationHeaderBusinessLogic headerService = new LiabilityQuotationHeaderBusinessLogic();
            var result = headerService.UpdateOwnerId(ownerId, referenceNum);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult SyncZohoUsers()
        {
            IZohoService zohoService = new ZohoService();
            IUnderwriterBusinessLogic underwriterService = new UnderwriterBusinessLogic();

            var zohoUserList = zohoService.GetZohoUsers();
            var updateStatusResult = underwriterService.SetUnderwriterInactive();
            var createResult = underwriterService.CreateUnderwriter(zohoUserList);
            return Json(updateStatusResult, JsonRequestBehavior.AllowGet);

        }
        public virtual ActionResult UserResponse(string id, int? flag)
        {
            var scheduleJobMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["ScheduleJobStatusMinutes"]);
            if (Request != null && Request.QueryString.Keys.Count > 0)
            {
                ILiabilityQuotationHeaderBusinessLogic liabilityHdr = new LiabilityQuotationHeaderBusinessLogic();
                IEmailTransactionBusinessLogic transactionRepo = new EmailTransactionBusinessLogic();
                IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
                EmailTransaction vm = new EmailTransaction();

                // Initial Email Info
                vm.ReferenceNumber = id;
                vm.IsActive = true;
                vm.Status = "Pending";

                var userInfo = liabilityHdr.GetUserInfoByReferenceNumber(id);
                if (userInfo.Status.Contains("Closed") || userInfo.Status.Contains("Bind Cover"))
                {
                    return Redirect("~/Home/Closed");
                }
                switch (flag)
                {
                    case (int)UserResponseEnum.Deny:
                        //ModifyStatus(id, "Closed - Lost");
                        ViewBag.ReferenceNumber = id;
                        //BackgroundJob.Enqueue(() => SyncStatus(id, DateTime.Now));

                        // Checkbox logic
                        ICheckBoxValueBusinessLogic checkboxRepo = new CheckBoxValueBusinessLogic();
                        DeclinePageRemarksViewModels checkboxModel = new DeclinePageRemarksViewModels();

                        var checkboxLookup = checkboxRepo.GetCheckBoxValue().Where(w => w.QuestionID == 1001).OrderBy(o => o.OrderID);
                        checkboxModel.CheckboxReferenceLookup = checkboxLookup.Select(x => new SelectListItem()
                        {
                            Text = x.Item,
                            Value = x.Item
                        }).ToList();

                        return View("DenyQuote", checkboxModel);

                    case (int)UserResponseEnum.Accept:
                        var model = new CoverBuilder.Entities.ViewModels.GenericInfoViewModels();
                        model.GenericInfo = userInfo;
                        model.GenericInfo.InsurancePeriodFrom = DateTime.Now;
                        model.GenericInfo.InsurancePeriodTo = DateTime.Now.AddYears(1);

                        //// Record Email Transaction
                        //vm.SentDate = DateTime.Now;
                        //vm.QuoteStatus = "Bind Cover Request";
                        //vm.TemplateId = 7; //Bind Cover Notification Email (Underwriter)
                        //vm.TemplateName = templateRepo.GetEmailTemplateByTemplateID(7).Select(s => s.Description).FirstOrDefault();

                        //transactionRepo.CreateEmailTransaction(vm);

                        //ModifyStatus(id, "Bind Cover Request");

                        //var dateTimeModel = new LiabilityQuotationHeader
                        //{
                        //    Id = model.GenericInfo.Id,
                        //    InsurancePeriodFrom = DateTime.Now,
                        //    InsurancePeriodTo = DateTime.Now.AddYears(1)
                        //};

                        //var liabilityQuoteService = new LiabilityQuotationHeaderBusinessLogic();
                        //liabilityQuoteService.UpdateInsuranceDates(dateTimeModel);

                        //var payloadModel = new Entities.Entities.PremiumStatusInfo
                        //{
                        //    Status = "Bind Cover Request",
                        //    InsuranceDateFrom = dateTimeModel.InsurancePeriodFrom.Value.ToString("MM/dd/yyyy"),
                        //    InsuranceDateTo = dateTimeModel.InsurancePeriodTo.Value.ToString("MM/dd/yyyy")
                        //};

                        //BackgroundJob.Enqueue(() => SyncInsuranceDates(payloadModel, model.GenericInfo.LeadId));

                        return View("AcceptQuote", model);

                    case (int)UserResponseEnum.Requote:
                        liabilityHdr.UpdateStatus(userInfo.Id.ToString(), "Requote");
                        userInfo.Status = UserResponseEnum.Requote.ToString();
                        TempData["UserInformation"] = userInfo;
                        return RedirectToAction("Index", "Home");
                }
            }
            //Throw error
            return Redirect("Error/Error");
        }
        [HttpPost]
        public virtual ActionResult AcceptQuote(CoverBuilder.Entities.ViewModels.GenericInfoViewModels model)
        {

            var referenceNo = model.GenericInfo.ReferenceNumber;

            // Record Email Transaction
            IEmailTransactionBusinessLogic transactionRepo = new EmailTransactionBusinessLogic();
            IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
            IWorkerService backgroundWorker = new WorkerService();
            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();

            var userInfo = headerRepo.GetUserInfoByReferenceNumber(referenceNo);
            EmailTransaction vm = new EmailTransaction();

            // Email Transaction Info
            vm.ReferenceNumber = referenceNo;
            vm.IsActive = true;
            vm.Status = "Pending";

            vm.SentDate = DateTime.Now;
            vm.QuoteStatus = "Bind Cover Request";
            vm.TemplateId = 7; //Bind Cover Notification Email (Underwriter)
            vm.TemplateName = templateRepo.GetEmailTemplateByTemplateID(7).Select(s => s.Description).FirstOrDefault();

            transactionRepo.CreateEmailTransaction(vm);

            // Template 8 (Optional)
            //var BindCoverTemplateActive = templateRepo.GetEmailTemplateByTemplateID(8).Select(s => s.IsActive).FirstOrDefault();
            //if ((bool)BindCoverTemplateActive)
            //{
            //    vm.TemplateId = 8; //Bind Cover Confirmation Mail
            //    vm.TemplateName = templateRepo.GetEmailTemplateByTemplateID(8).Select(s => s.Description).FirstOrDefault();
            //    transactionRepo.CreateEmailTransaction(vm);
            //}

            // Update Quote Status - DB
            ModifyStatus(referenceNo, "Bind Cover Request");

            ILiabilityQuotationHeaderBusinessLogic liabilityQuoteService = new LiabilityQuotationHeaderBusinessLogic();
            var persistedModel = new LiabilityQuotationHeader   
            {
                Id = model.GenericInfo.Id,
                InsurancePeriodFrom = model.GenericInfo.InsurancePeriodFrom,
                InsurancePeriodTo = model.GenericInfo.InsurancePeriodTo
            };

            liabilityQuoteService.UpdateInsuranceDates(persistedModel);

            // Update Quote Status - Zoho
            var payloadModel = new Entities.Entities.PremiumStatusInfo
            {
                LeadId = model.GenericInfo.LeadId,
                Status = "Bind Cover Request",
                InsuranceDateFrom = model.GenericInfo.InsurancePeriodFrom.Value.ToString("yyyy-MM-dd"),
                InsuranceDateTo = model.GenericInfo.InsurancePeriodTo.Value.ToString("yyyy-MM-dd")
            };

            BackgroundJob.Enqueue(() => SyncInsuranceDates(payloadModel, model.GenericInfo.LeadId));
            //List<Entities.Entities.Attachment> attachmentList = new List<Entities.Entities.Attachment>();
            //string proposalBytes = ReportHelper.GeneratePremiumReport(userInfo.Id, "ConfirmationProposal");
            //attachmentList.Add(new Entities.Entities.Attachment { File = ReportHelper.ConvertToByteArray(proposalBytes), LiabilityQuotationHeaderId = userInfo.Id, Remarks = "Proposal PDF", FileName = String.Format("{0}{1}.pdf", "Proposal", DateTime.Now.ToString("yyyyMMddHHmmss")) });
            //backgroundWorker.UploadFilesAsync(referenceNo, attachmentList);
            //return RedirectToAction("Index", "Home");
            return new RedirectResult("https://rynoinsurancebroker.com.au");
        }
        public async Task<JsonResult> SyncPremiumInfo(string referenceNo)
        {
            ObjectCache cache = MemoryCache.Default;
            var zohoAgent = new ZohoStoreV2.Service.ZohoAgentV2();
            var access_token = cache["access_token"] as string;
            if (access_token == null)
            {
                access_token = zohoAgent.GenerateTokenv2();
                cache.Set("access_token", access_token, DateTime.Now.AddMinutes(55));
            }

            JsonResponseResult result = new JsonResponseResult();
            IZohoService zohoService = new ZohoService();
            var dataModel = await zohoService.SyncZohoData(referenceNo, access_token);
            var cancellationTokenSource = new CancellationTokenSource();
            var token = cancellationTokenSource.Token;

            ILiabilityQuotationHeaderBusinessLogic liabilityQuotationService = new LiabilityQuotationHeaderBusinessLogic();
            ILiabilityInsuranceResultBusinessLogic liabilityResultService = new LiabilityInsuranceResultBusinessLogic();
            ILiabilityInsuranceTypeBusinessLogic insuranceTypeRepo = new LiabilityInsuranceTypeBusinessLogic();
            IWorkerService backgroundWorker = new WorkerService();
            var liabilityQuotationHeader = liabilityQuotationService.GetUserInfoByReferenceNumber(referenceNo);

            if (dataModel.LiabilityQuotationHeader.QuoteSent == null)
            {
                dataModel.LiabilityQuotationHeader.QuoteSent = DateTime.Now;
            }
            else
            {
                result.DateExecuted = liabilityQuotationHeader.QuoteSent.Value.ToString("yyyy-MM-dd");
            }

            var updateResult = await liabilityQuotationService.UpdateQuotationHeaderAsync(dataModel.LiabilityQuotationHeader, token);
            await liabilityResultService.UpdateAsync(dataModel.LiabilityResultList, token);
            if (dataModel.DeserialisedLiabilityResultList != null)
            {
                liabilityResultService.CreateLiabilityInsuranceResult(dataModel.LiabilityQuotationHeader.Id, dataModel.LiabilityQuotationHeader.InsuranceId.Value, dataModel.DeserialisedLiabilityResultList);
            }

            if (updateResult > 0)
            {
                List<Entities.Entities.Attachment> attachmentList = new List<Entities.Entities.Attachment>();
                var liabilityType = insuranceTypeRepo.GetLiabilityInsuranceType().Where(w => w.InsuranceId == liabilityQuotationHeader.InsuranceId).Select(s => s.LiabilityInsurance).SingleOrDefault();
                // Check if quote status = Closed Lost/Won/Declined
                // Quote Document
                string quoteBytes = ReportHelper.GeneratePremiumReport(liabilityQuotationHeader.Id, liabilityType);
                attachmentList.Add(new Entities.Entities.Attachment { File = ReportHelper.ConvertToByteArray(quoteBytes), LiabilityQuotationHeaderId = liabilityQuotationHeader.Id, Remarks = "Quote PDF", FileName = String.Format("{0}{1}.pdf", "Quote", DateTime.Now.ToString("yyyyMMddHHmmss")) });

                // Proposal Document
                //string proposalBytes = ReportHelper.GeneratePremiumReport(liabilityQuotationHeader.Id, "ConfirmationProposal");
                //attachmentList.Add(new Entities.Entities.Attachment { File = ReportHelper.ConvertToByteArray(proposalBytes), LiabilityQuotationHeaderId = liabilityQuotationHeader.Id, Remarks = "Proposal PDF", FileName = String.Format("{0}{1}.pdf", "Proposal", DateTime.Now.ToString("yyyyMMddHHmmss")) });
                //await backgroundWorker.UploadFilesAsync(liabilityQuotationHeader.ReferenceNumber, attachmentList);
                await zohoService.UploadFilesAsync(liabilityQuotationHeader.LeadId, attachmentList, access_token);

                result.Result = "Success";
            }
            else
            {
                result.Result = "Failed";
            }


            return Json(result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public virtual ActionResult SaveRemarks(string referenceId, string remarks)
        {
            ILiabilityQuotationHeaderBusinessLogic quoteHeader = new LiabilityQuotationHeaderBusinessLogic();
            var result = string.Empty;

            var updateResult = quoteHeader.UpdateRemarks(referenceId, remarks);
            if (updateResult > 0)
            {
                result = "Success";
                ModifyStatus(referenceId, "Closed - Lost"); // UPDATE: MAY 31, 2018
                BackgroundJob.Enqueue(() => SyncStatus(referenceId, DateTime.Now));
            }
            else
            {
                result = "Failed";
            }

            //var redirectUrl = new UrlHelper(Request.RequestContext).Action("Index", "Home");
            var redirectUrl = "https://rynoinsurancebroker.com.au";

            // Ajax requirement
            return Json(new { Url = redirectUrl });
        }
        public virtual ActionResult SendQuoteEmail(string referenceNo, int templateId)
        {
            // Check if quote status = Closed Lost/Won/Declined
            ILiabilityQuotationHeaderBusinessLogic userInfo = new LiabilityQuotationHeaderBusinessLogic();
            var quoteStatus = userInfo.GetUserInfoByReferenceNumber(referenceNo).Status;

            if (quoteStatus.ToString().Contains("Closed"))
            {
                // Redirect to "Closed" info page
                return RedirectToAction("Closed", "Home");
            }
            else
            {
                IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
                IEmailTransactionBusinessLogic transactionRepo = new EmailTransactionBusinessLogic();

                // Record Email Transaction
                EmailTransaction vm = new EmailTransaction();
                vm.ReferenceNumber = referenceNo;
                vm.TemplateId = templateId;
                vm.IsActive = true;
                vm.Status = "Pending";

                vm.SentDate = DateTime.Now;
                vm.QuoteStatus = "Pipeline";
                vm.TemplateName = templateRepo.GetEmailTemplateByTemplateID(templateId).Select(s => s.Description).FirstOrDefault();

                transactionRepo.CreateEmailTransaction(vm);

                return RedirectToAction("EmailQuote", "Premium");
            }
        }
        public virtual ActionResult EmailQuote()
        {
            return View();
        }
        public virtual ActionResult DenyQuote()
        {
            // Checkbox logic
            ICheckBoxValueBusinessLogic checkboxRepo = new CheckBoxValueBusinessLogic();
            DeclinePageRemarksViewModels model = new DeclinePageRemarksViewModels();

            var checkboxLookup = checkboxRepo.GetCheckBoxValue().Where(w => w.QuestionID == 1001).OrderBy(o => o.OrderID);
            model.CheckboxReferenceLookup = checkboxLookup.Select(x => new SelectListItem()
            {
                Text = x.Item,
                Value = x.Item
            }).ToList();

            return View(model);
        }
        public async Task SyncStatus(string id, DateTime? quoteLostDate = null)
        {
            ObjectCache cache = MemoryCache.Default;
            var zohoAgent = new ZohoStoreV2.Service.ZohoAgentV2();
            var access_token = cache["access_token"] as string;
            if (access_token == null)
            {
                access_token = zohoAgent.GenerateTokenv2();
                cache.Set("access_token", access_token, DateTime.Now.AddMinutes(55));
            }

            IZohoService zohoService = new ZohoService();
            ILiabilityQuotationHeaderBusinessLogic quoteHeaderService = new LiabilityQuotationHeaderBusinessLogic();
            var quoteHeader = quoteHeaderService.GetUserInfoByReferenceNumber(id);

            Entities.Entities.PremiumStatusInfo statusInfo = new PremiumStatusInfo
            {
                LeadId = quoteHeader.LeadId,
                Remarks = quoteHeader.Remarks,
                Status = quoteHeader.Status,
                InsuranceDateFrom = quoteHeader.InsurancePeriodFrom.Value.ToString("yyyy-MM-dd"),
                InsuranceDateTo = quoteHeader.InsurancePeriodTo.Value.ToString("yyyy-MM-dd"),
                QuoteLostDate = quoteLostDate.HasValue ? quoteLostDate.Value.ToString("yyyy-MM-dd") : string.Empty
            };

            await zohoService.UpdateLeadStatusAsync(statusInfo, quoteHeader.LeadId, access_token);
        }
        public async Task SyncInsuranceDates(Entities.Entities.PremiumStatusInfo model, string zohoLeadId)
        {
            ObjectCache cache = MemoryCache.Default;
            var zohoAgent = new ZohoStoreV2.Service.ZohoAgentV2();
            var access_token = cache["access_token"] as string;
            if (access_token == null)
            {
                access_token = zohoAgent.GenerateTokenv2();
                cache.Set("access_token", access_token, DateTime.Now.AddMinutes(55));
            }

            IZohoService zohoService = new ZohoService();
            await zohoService.UpdateLeadStatusAsync(model, zohoLeadId, access_token);
        }
        public async Task RecalculateFinancials(string referenceNo, decimal zohoPremiumBase, decimal zohoUWFee)
        {
            IZohoService zohoService = new ZohoService();
            ILiabilityQuotationHeaderBusinessLogic headerRepo = new LiabilityQuotationHeaderBusinessLogic();
            IPremiumDetailBusinessLogic detailRepo = new PremiumDetailBusinessLogic();

            var quoteHeader = headerRepo.GetUserInfoByReferenceNumber(referenceNo);
            var insuranceId = quoteHeader.InsuranceId.Value;
            var userId = quoteHeader.Id;

            var premiumDetails = detailRepo.GetPremiumDetailByReferenceId(userId);
            var oldPremiumBase = headerRepo.GetUserInfoByReferenceNumber(referenceNo).TotalPremiumBase.Value;

            decimal[] situationPercentage = new decimal[premiumDetails.Count()];
            decimal[] newSituationPremiumBase = new decimal[premiumDetails.Count()];
            decimal newTotalPremiumBase = 0;
            decimal newTotalGST = 0;
            decimal newTotalStampTax = 0;
            if (insuranceId == 7 || insuranceId == 10) // PO or VL [Multiple Situation Insurance Types]
            {
                oldPremiumBase = premiumDetails.Sum(x => x.BasePremium);
                for (int i = 0; i <= premiumDetails.Count() - 1; i++)
                {
                    // Getting the percentage per situation
                    situationPercentage[i] = premiumDetails[i].BasePremium / oldPremiumBase;
                    // Optional: Store on a separate array
                    newSituationPremiumBase[i] = zohoPremiumBase * situationPercentage[i];
                    // Update Premium Detail Model
                    premiumDetails[i].BasePremium = zohoPremiumBase * situationPercentage[i];
                    premiumDetails[i].GST = premiumDetails[i].BasePremium * premiumDetails[i].GSTRate;
                    premiumDetails[i].StampTax = (premiumDetails[i].BasePremium + premiumDetails[i].GST) * premiumDetails[i].StateTaxRate;
                    // Get Total Base Premium, GST and Stamp Tax
                    newTotalPremiumBase = newTotalPremiumBase + premiumDetails[i].BasePremium;
                    newTotalGST = newTotalGST + premiumDetails[i].GST;
                    newTotalStampTax = newTotalStampTax + premiumDetails[i].StampTax;
                }

            }

            else // General [Single Situation Insurance Types]
            {
                foreach (var item in premiumDetails)
                {
                    // Update Premium Detail Model
                    item.BasePremium = zohoPremiumBase;
                    item.GST = item.BasePremium * item.GSTRate;
                    item.StampTax = (item.BasePremium + item.GST) * item.StateTaxRate;

                    // Get Total Base Premium, GST and Stamp Tax
                    newTotalPremiumBase = newTotalPremiumBase + item.BasePremium;
                    newTotalGST = newTotalGST + item.GST;
                    newTotalStampTax = newTotalStampTax + item.StampTax;
                }
            }

            // Factor Conditions
            //--> Hotels
            decimal conditionalBrokerCommRate = insuranceId == 17 ? .1M : .15M;
            //decimal conditionalUWFee = insuranceId == 17 ? 

            // Generic Computation
            quoteHeader.TotalPremiumBase = zohoPremiumBase; //newTotalPremiumBase
            quoteHeader.TotalGST = newTotalGST;
            quoteHeader.TotalStampTax = newTotalStampTax;

            quoteHeader.UWFee = zohoUWFee;
            quoteHeader.TotalUwFee = quoteHeader.UWFee * quoteHeader.UWFeeGSTRate;
            quoteHeader.PremiumSubTotal = newTotalPremiumBase + newTotalGST + newTotalStampTax + quoteHeader.UWFee + quoteHeader.TotalUwFee;

            quoteHeader.BrokerComm = newTotalPremiumBase * conditionalBrokerCommRate;
            quoteHeader.BrokerCommGST = quoteHeader.BrokerComm * .1M;
            quoteHeader.TotalFinalPremium = quoteHeader.PremiumSubTotal - (quoteHeader.BrokerComm + quoteHeader.BrokerCommGST);

            // Update Quotation Header Model
            // Update UW Fee and UW Tax

            // Update Zoho Record
            Entities.Entities.PremiumFinancialInfo financialInfo = new PremiumFinancialInfo
            {
                LeadId = quoteHeader.LeadId,
                PremiumBase = (decimal)quoteHeader.TotalPremiumBase,
                StampTax = (decimal)quoteHeader.TotalStampTax,
                GST = (decimal)quoteHeader.TotalGST,
                UnderwriterFee = (decimal)quoteHeader.UWFee,
                UnderwriterFeeGST = (decimal)quoteHeader.TotalUwFee,
                PremiumSubTotal = (decimal)quoteHeader.PremiumSubTotal,
                BrokerComm = (decimal)quoteHeader.BrokerComm,
                BrokerCommGST = (decimal)quoteHeader.BrokerCommGST,
                FinalPremium = (decimal)quoteHeader.TotalFinalPremium
            };

            ObjectCache cache = MemoryCache.Default;
            var zohoAgent = new ZohoStoreV2.Service.ZohoAgentV2();
            var access_token = cache["access_token"] as string;
            if (access_token == null)
            {
                access_token = zohoAgent.GenerateTokenv2();
                cache.Set("access_token", access_token, DateTime.Now.AddMinutes(55));
            }

            await zohoService.UpdateLeadFinancesAsync(financialInfo, quoteHeader.LeadId, access_token);
        }
        public virtual ActionResult WordTemplateToPDF(string referenceNo)
        {
            byte[] pdfBytes = ReportHelper.GeneratePremiumReportV2(referenceNo);
            MemoryStream ms = new MemoryStream(pdfBytes);

            Response.AddHeader("Content-Disposition", "attachment;");
            return new FileStreamResult(ms, "application/pdf"); ;
        }
    }

}