﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CoverBuilder.Controllers
{
    public partial class ErrorController : Controller
    {
        public virtual ActionResult Error()
        {
            return View();
        }

        public virtual ActionResult Unauthorized()
        {
            return View();
        }
	}
}