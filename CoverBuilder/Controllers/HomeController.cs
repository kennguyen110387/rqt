﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CoverBuilder.BLL.BusinessLogics;
using CoverBuilder.BLL.Interfaces;
using CoverBuilder.Common.Logging;
using CoverBuilder.Common.Logging.NLog;
using CoverBuilder.Entities.Entities;
using CoverBuilder.Entities.Enums;
using CoverBuilder.Entities.ViewModels;
using CoverBuilder.Helper;
using System.IO;
using System.Xml.Serialization;
using System.Configuration;
using System.Collections.Generic;
using CoverBuilder.Filters;
using Hangfire;
using System.Threading;

namespace CoverBuilder.Controllers
{

    public partial class HomeController : Controller
    {
        private static ILogger logger = new NLogger();

        [ValidateInput(false)]
        public virtual ActionResult BrokerPrefill()
        {
            var rawQueryString = Request.Url.Query.TrimStart('?');
            if (string.IsNullOrWhiteSpace(rawQueryString))
            {
                return RedirectToAction("Unauthorized", "Error");
            }
            var decryptedParam = CoverBuilder.Common.Encryption.CryptoUtil.DecryptUrlParams(rawQueryString.Replace("(10)", "").Replace(" ", ""));
            var starr = decryptedParam.Split('&').Where(x => x.Contains("=")).ToArray();
            var objModel = new BrokerInfoViewModel();
            var properties = typeof(BrokerInfoViewModel).GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                var strVal = starr[i].Split('=');
                if (strVal.Length > 2)
                {
                    properties[i].SetValue(objModel, System.Web.HttpUtility.UrlDecode(strVal[2]));
                }
                else
                {
                    properties[i].SetValue(objModel, System.Web.HttpUtility.UrlDecode(strVal[1]));
                }
            }

            Session["BrokerInformation"] = objModel;
            return RedirectToAction("Index");
        }

        public virtual ActionResult Index()
        {

            ViewBag.Title = "Welcome to Ryno Insurance Services";
            ViewBag.IsBais = ConfigurationManager.AppSettings["BAISIntegration"];
            var brokerData = Session["BrokerInformation"] as BrokerInfoViewModel;
            if (ConfigurationManager.AppSettings["BAISIntegration"] == "1" && brokerData == null && TempData["UserInformation"] as LiabilityQuotationHeader == null)
            {
                return RedirectToAction("Unauthorized", "Error");
            }
            GenericInfoViewModels model = new GenericInfoViewModels();
            var insuranceTypes = new LiabilityInsuranceTypeBusinessLogic().GetLiabilityInsuranceType().OrderBy(x => x.OrderId);
            ILookupBusinessLogic lookupService = new LookupBusinessLogic();
            var stateTaxRates = lookupService.GetReferenceByCode("STAMPTAXRATE");
            var liabilityLimit = lookupService.GetReferenceByCode("LIABILITYLIMIT");
            var state = brokerData == null ? "QLD" : brokerData.State;
            switch (state)
            {
                case "NSW":
                    state = "New South Wales";
                    break;
                case "NT":
                    state = "Northern Territory";
                    break;
                case "QLD":
                    state = "Queensland";
                    break;
                case "SA":
                    state = "South Australia";
                    break;
                case "TAS":
                    state = "Tasmania";
                    break;
                case "VIC":
                    state = "Victoria";
                    break;
                case "WA":
                    state = "Western Australia";
                    break;
                case "ACT":
                    state = "Australian Capital Territory";
                    break;
                default:
                    state = "Queensland";
                    break;
            }
            LiabilityQuotationHeader liabilityQuotationHeader;
            if (brokerData == null)
            {
                liabilityQuotationHeader = new LiabilityQuotationHeader
                {
                    Id = new Guid(),
                    Status = UserResponseEnum.New.ToString()
                };
            }
            else
            {
                liabilityQuotationHeader = new LiabilityQuotationHeader
                {
                    Id = new Guid(),
                    Status = UserResponseEnum.New.ToString(),
                    BASubAgent = brokerData.BASubAgent,
                    BrokerageName = brokerData.BrokerCompanyname,
                    FullName = brokerData.Name,
                    Email = brokerData.Email,
                    PhoneNo = brokerData.Phone,
                    SituationStreetNo = brokerData.Street,
                    SituationSuburb = brokerData.Suburb,
                    SituationPostCode = brokerData.PostCode,
                    SituationState = state
                    //BrokerFullAddress = brokerData.Street + ", " + brokerData.Suburb + ", " + brokerData.State + ", " + brokerData.PostCode
                };
            }


            model.InsuranceTypes = insuranceTypes.Select(ins => new SelectListItem
            {
                Text = ins.LiabilityInsurance,
                Value = ins.InsuranceId.ToString()
            }).ToList();

            model.States = stateTaxRates.Select(stl => new SelectListItem
            {
                Text = stl.Text,
                Value = stl.Text
            }).ToList();

            model.LiabilityLimit = liabilityLimit.Select(lbl => new SelectListItem
            {
                Text = lbl.Text,
                Value = lbl.Value
            }).ToList();

            model.GenericInfo = liabilityQuotationHeader;

            if (TempData["UserInformation"] as LiabilityQuotationHeader != null)
            {
                ViewBag.Title = "Ryno Insurance Requote Service";
                model.GenericInfo = TempData["UserInformation"] as LiabilityQuotationHeader;
                model.GenericInfo.Status = UserResponseEnum.Requote.ToString();
                Session["UnderwriterId"] = model.GenericInfo.UnderwriterId;
                var selectedInsuranceType = model.InsuranceTypes.Where(itm => itm.Value == model.GenericInfo.InsuranceId.Value.ToString()).First();
                var selectedState = model.States.Where(itm => itm.Text == model.GenericInfo.SituationState).First();
                var selectedLiabilityLimit = model.LiabilityLimit.Where(itm => itm.Value == model.GenericInfo.LiabilityLimit.ToString()).First();
                selectedState.Selected = true;
                selectedLiabilityLimit.Selected = true;
                selectedInsuranceType.Selected = true;
                model.InsuranceType = selectedInsuranceType.Text;

                if (model.GenericInfo.InsuranceId == 99)
                {
                    model.GenericInfo.InsuranceId = 13;
                }
            }

            model.InsuranceTypes.Remove(model.InsuranceTypes.Where(x => x.Value == "99").FirstOrDefault());

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[CaptchaValidator(RequiredMessage="The captcha field is required.")]
        public async virtual Task<ActionResult> GenericInfoToDB(GenericInfoViewModels UserInfo)
        {
            ILiabilityInsuranceTypeBusinessLogic blInsuranceType = new LiabilityInsuranceTypeBusinessLogic();
            var insuranceType = await blInsuranceType.GetLiabilityInsuranceTypeByIdAsync(UserInfo.GenericInfo.InsuranceId.Value);

            // GET: Specific Insurance Quotation Page
            var strQuotationType = UserInfo.InsuranceType.Trim().Replace(" ", "").Replace("&", "And");
            if (UserInfo.GenericInfo.InsuranceId == 3)
                strQuotationType = "MotelsAndBB";
            if (UserInfo.GenericInfo.InsuranceId == 13)
                strQuotationType = "EventsAndMarkets";
            if (UserInfo.GenericInfo.InsuranceId == 14)
                strQuotationType = "MarketStallHolders";
            if (UserInfo.GenericInfo.InsuranceId == 12)
                strQuotationType = "ShoppingCentre";
            if (UserInfo.GenericInfo.InsuranceId == 17)
                strQuotationType = "Hotels";
            if (UserInfo.GenericInfo.InsuranceId == 19)
                strQuotationType = "VacantLandProperties";



            // INSERT: Additional details on the input model data
            if (UserInfo.GenericInfo.CreateDate == null)
                UserInfo.GenericInfo.CreateDate = DateTime.Now;

            if (UserInfo.GenericInfo.Status == UserResponseEnum.New.ToString())
            {
                UserInfo.GenericInfo.Id = Guid.NewGuid();
                UserInfo.GenericInfo.ReferenceNumber = String.Format("{0}-{1}", insuranceType.Code, UserInfo.GenericInfo.Id.ToString().Replace("-", "").ToUpper().Substring(0, 10));
                logger.Log(LogTypeEnum.Info, UserInfo.GenericInfo.Id.ToString(), "Begin Quotation Transaction");
            }
            else
            {
                logger.Log(LogTypeEnum.Info, UserInfo.GenericInfo.Id.ToString(), "Begin Requote Transaction");
                //Declare Values Here
            }

            // -> Full Broker Address - Concatenated
            //UserInfo.GenericInfo.BrokerFullAddress = UserInfo.GenericInfo.SituationStreetNo + ", " + UserInfo.GenericInfo.SituationSuburb + ", " + UserInfo.GenericInfo.SituationState + " " + UserInfo.GenericInfo.SituationPostCode;

            //Record Input Data
            Session["QuotationHeader"] = UserInfo.GenericInfo;

            //Determine Liability Claims Discount.
            if (UserInfo.GenericInfo.DiscInfoClaim != true)
                Session["SessionAccumulatedDiscount"] = 10;

            //Check Date Trigger
            var aYearFromNow = UserInfo.GenericInfo.InsurancePeriodFrom.Value.AddYears(1);
            if (aYearFromNow != UserInfo.GenericInfo.InsurancePeriodTo)
            {
                UserInfo.NeedsUnderwritingReferral = true;
            }

            //Session Flag to redirect to Underwriter Referral Page
            Session["ShowReferralPage"] = UserInfo.NeedsUnderwritingReferral;

            return RedirectToAction(strQuotationType, "Quotation");
        }




        public virtual ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public virtual ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public virtual JsonResult ExecuteEmails()
        {
            EmailService.CheckAndQueueMail();
            ViewBag.Message = "Your contact page.";

            return Json("Sending email...", JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult UnderConstruction()
        {
            return View();

        }

        public virtual ActionResult Thankyou()
        {
            //string status = TempData["Status"] as string;
            var quotationHeader = (LiabilityQuotationHeader)Session["QuotationHeader"];
            string status = quotationHeader.Status;

            if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["DownloadPdfMode"]))
                return RedirectToAction("QuotationDownload");

            if (status.Contains(UserResponseEnum.New.ToString()))
            {
                ViewBag.HeaderContent = "Thank you for requesting a quotation from Ryno.";
                ViewBag.DetailContent = "An Underwriter will be in contact shortly via email with your quotation.";
            }
            else if (status.Equals(UserResponseEnum.Requote.ToString()))
            {
                ViewBag.HeaderContent = "Thank you for modifying your quotation.";
                ViewBag.DetailContent = "An Underwriter will be in contact shortly via email with your revised quotation.";
            }

            // Send email
            SendQuoteEmail(quotationHeader);


            //SendQuotationAsEmail(null);
            Session.Abandon();
            return View();
        }

        private void SendQuoteEmail(LiabilityQuotationHeader quotationHeader)
        {
            // Mail Sending
            // Initiate Client Info
            var cancellationTokenSource = new CancellationTokenSource();
            var token = cancellationTokenSource.Token;
            IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
            IEmailTransactionBusinessLogic transactionRepo = new EmailTransactionBusinessLogic();
            List<EmailTransaction> emailTransactions = new List<EmailTransaction>();

            // If "With Trigger" Send Templates 1 and 2
            if (quotationHeader.HasTriggerReferral == true)
            {
                // Record Email 1 Transaction
                emailTransactions.Add(new EmailTransaction(quotationHeader.ReferenceNumber, 1)
                {
                    TemplateName = templateRepo.GetEmailTemplateByTemplateID(1).Select(s => s.Description).FirstOrDefault()
                });

                // Record Email 2 Transaction
                emailTransactions.Add(new EmailTransaction(quotationHeader.ReferenceNumber, 2)
                {
                    TemplateName = templateRepo.GetEmailTemplateByTemplateID(2).Select(s => s.Description).FirstOrDefault()
                });
            }
            else // "No Trigger" Send Templates 4 and 5
            {
                // Record Email Transaction
                emailTransactions.Add(new EmailTransaction(quotationHeader.ReferenceNumber, 4)
                {
                    TemplateName = templateRepo.GetEmailTemplateByTemplateID(4).Select(s => s.Description).FirstOrDefault()
                });
            }
            BackgroundJob.Schedule(() => transactionRepo.CreateEmailTransactionsAsync(emailTransactions, token), TimeSpan.FromMinutes(1));
            //transactionRepo.CreateEmailTransactionsAsync(emailTransactions, token);
            //EmailService.CheckAndQueueMail();

        }

        public virtual ActionResult QuotationDownload()
        {
            return View();
        }

        public virtual ActionResult LiabilityPremiumReport()
        {
            // Get Current Quote Details
            var quotationHeader = (LiabilityQuotationHeader)Session["QuotationHeader"];

            // Get Insurance Type for PDF Report
            ILiabilityInsuranceTypeBusinessLogic insuranceTypeRepo = new LiabilityInsuranceTypeBusinessLogic();
            var liabilityType = insuranceTypeRepo.GetLiabilityInsuranceType().Where(w => w.InsuranceId == quotationHeader.InsuranceId).Select(s => s.LiabilityInsurance).SingleOrDefault();

            // Get Report Memory Stream
            var ms = ReportHelper.GetRawPdfFile(ReportHelper.GeneratePremiumReport(quotationHeader.Id, liabilityType.ToString()));
            Response.AddHeader("Content-Disposition", "attachment;");
            return new FileStreamResult(ms, "application/pdf");
        }

        public virtual ActionResult Closed()
        {
            return View();
        }

        public virtual ActionResult AdminLoginRedirect()
        {
            return RedirectToAction("Login", new { area = "Admin", controller = "Security" });
        }

        //private void SendQuotationAsEmail()
        //{
        //    // Client Details
        //    var QuotationHeader = (LiabilityQuotationHeader)Session["QuotationHeader"];
        //    var userId = QuotationHeader.Id;
        //    var fullInsuredName = QuotationHeader.InsuredFullName;
        //    var referenceNumber = QuotationHeader.ReferenceNumber;
        //    var clientName = QuotationHeader.FullName;
        //    var clientEmail = QuotationHeader.Email;


        //    // Initiate File Attachments
        //    var ms = GetRawPdfFile();

        //    // Get Data Tables from Stored Proc
        //    ILiabilityInsuranceResultBusinessLogic loadReportObj = new LiabilityInsuranceResultBusinessLogic();
        //    System.Data.DataSet ds = loadReportObj.GetReportDataSource(userId);
        //    //ds.Tables[0].TableName = "dsPremiumReport_HeaderInfo";

        //    //var emailHtmlBody = templateService.Parse()
        //    IEmailHandler emailHandler = new EmailHandler();
        //    emailHandler.From = System.Configuration.ConfigurationManager.AppSettings["From"].ToString();
        //    emailHandler.To = clientEmail.ToString();


        //    // Create Email Body
        //    var body = new System.Text.StringBuilder();

        //    //Pseudo CSS Line
        //    var emailStyle = "font-family:Century Gothic; font-size:13px; line-height:4px;";
        //    var emailTableStyle = "font-family:Century Gothic; font-size:13px; ";
        //    var emailHeaderStyle = "font-family:Century Gothic;";
        //    var emailCellStyle = "";

        //    // Email Body
        //    body.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        //    body.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
        //    body.Append("<p style='" + emailStyle + "'> Greeting from Ryno Insurances!</p>");
        //    body.Append("<br/>");
        //    body.Append("<p style='" + emailStyle + "'>Attached is your premium quotation for your reference and perusal.</p>");
        //    body.Append("<p style='" + emailStyle + "'>Let us know if you have inquiries or any additional concerns at happy@rynoinsurances.com.</p>");
        //    body.Append("<br/>");
        //    body.Append("<p style='" + emailStyle + "'>Thank you for your patronage!</p>");
        //    body.Append("<p style='" + emailStyle + "'>Ryno Insurances</p>");

        //    body.Append("<br/>");

        //    // Append Data Table
        //    //  User Info
        //    body.Append("<br/><h4 style='" + emailHeaderStyle + "'>User Info</h4>");
        //    body.Append("<table style ='" + emailTableStyle + "'>");
        //    body.Append("<thead style ='font-weight:600;'> <td>Question</td> <td>Response</td> </thead>");
        //    foreach (System.Data.DataRow row in ds.Tables[5].Rows)
        //    {
        //        foreach (System.Data.DataColumn column in ds.Tables[5].Columns)
        //        {
        //            if (column.ColumnName.ToString() == "SitutationStreetName")
        //            {
        //                //Do Nothing
        //            }
        //            else if (column.ColumnName.ToString() == "SituationStreetNo")
        //            {
        //                body.Append("<tr>");
        //                body.Append("<td>" + "SituationAddress" + "</td>");
        //                body.Append("<td>" + row[column.ColumnName].ToString() + "</td>");
        //                body.Append("</tr>");
        //            }
        //            else
        //            {
        //                body.Append("<tr>");
        //                body.Append("<td>" + column.ColumnName.ToString() + "</td>");
        //                body.Append("<td>" + row[column.ColumnName].ToString() + "</td>");
        //                body.Append("</tr>");
        //            }
        //        }
        //    }
        //    body.Append("</table>");

        //    //  User Response
        //    body.Append("<br/><h4 style='" + emailHeaderStyle + "'>User Response</h4>");
        //    body.Append("<table style ='" + emailTableStyle + "'>");
        //    body.Append("<thead style ='font-weight:600;'> <td style= 'width:40%;'>Question</td> <td style= 'width:60%;'>Response</td> </thead>");
        //    foreach (System.Data.DataRow row in ds.Tables[4].Rows)
        //    {
        //        body.Append("<tr>");
        //        foreach (System.Data.DataColumn column in ds.Tables[4].Columns)
        //        {
        //            body.Append("<td style='" + emailCellStyle + "'>" + row[column.ColumnName].ToString() + "</td>");
        //        }
        //        body.Append("</tr>");
        //    }
        //    body.Append("</table>");


        //    // Get File Attachments
        //    var file = new List<System.Net.Mail.Attachment>();
        //    file.Add(new System.Net.Mail.Attachment(ms, "PremiumQuotation.pdf", "application/pdf"));

        //    emailHandler.Send("Greetings from Ryno Insurances!", body.ToString(), false, file);
        //    ms.Close();
        //}


        //===================================================================================
        //Old Email Module
        //private void SendQuotationAsEmail(int? templateId, int? referenceNo)
        //{
        //    int TemplateId = 4;
        //    // Template Details
        //    if (templateId.HasValue)
        //    {
        //        TemplateId = (int)templateId;
        //    }

        //    IEmailTemplateBusinessLogic templateRepo = new EmailTemplateBusinessLogic();
        //    var emailTemplate = templateRepo.GetEmailTemplateByTemplateID(TemplateId).Select(s => s.Body).FirstOrDefault();

        //    // Client Details
        //    var QuotationHeader = (LiabilityQuotationHeader)Session["QuotationHeader"];
        //    var userId = QuotationHeader.Id;
        //    var fullInsuredName = QuotationHeader.InsuredFullName;
        //    var referenceNumber = QuotationHeader.ReferenceNumber;
        //    var clientName = QuotationHeader.FullName;
        //    var clientEmail = QuotationHeader.Email;

        //    // Get Data Tables from Stored Proc
        //    ILiabilityInsuranceResultBusinessLogic loadReportObj = new LiabilityInsuranceResultBusinessLogic();
        //    System.Data.DataSet ds = loadReportObj.GetReportDataSource(userId);
        //    //ds.Tables[0].TableName = "dsPremiumReport_HeaderInfo";

        //    // Call Email Helper
        //    IEmailHandler emailHandler = new EmailHandler();
        //    emailHandler.From = System.Configuration.ConfigurationManager.AppSettings["From"].ToString();
        //    emailHandler.To = clientEmail.ToString();

        //    // Create Email Body
        //    var body = emailTemplate;

        //    // Replacements
        //    body = body.Replace("{{Your Name}}", clientName);
        //    body = body.Replace("{{Reference Number}}", referenceNumber);
        //    body = body.Replace("{{Full Insured Name}}", fullInsuredName);


        //    // Initiate File Attachments
        //    var ms = GetRawPdfFile();

        //    // Get File Attachments
        //    var file = new List<System.Net.Mail.Attachment>();
        //    file.Add(new System.Net.Mail.Attachment(ms, "PremiumQuotation.pdf", "application/pdf"));

        //    emailHandler.Send("Liability Insurance Quotation - " + fullInsuredName, body.ToString(), false, file);
        //    ms.Close();

        //    ////Mail Definition Strat
        //    //MailDefinition md = new MailDefinition();
        //    //md.From = "test@domain.com";
        //    //md.IsBodyHtml = true;
        //    //md.Subject = "Test of MailDefinition";

        //    //ListDictionary replacements = new ListDictionary();
        //    //replacements.Add("{name}", "Martin");
        //    //replacements.Add("{country}", "Denmark");

        //    //string body2 = "<div>Hello {name} You're from {country}.</div>";

        //    //MailMessage msg = md.CreateMailMessage("you@anywhere.com", replacements, body2, new System.Web.UI.Control());

        //}


        //===================================================================================
        //private MemoryStream GetRawPdfFile()
        //{
        //    Session.Abandon();
        //    try
        //    {
        //        string strBytes = (string)TempData["reportAsString"];
        //        string incoming = strBytes.Replace('_', '/').Replace('-', '+');
        //        switch (strBytes.Length % 4)
        //        {
        //            case 2: incoming += "=="; break;
        //            case 3: incoming += "="; break;
        //        }
        //        byte[] bytes = Convert.FromBase64String(incoming);
        //        MemoryStream ms = new MemoryStream(bytes);
        //        return ms;
        //    }
        //    catch (ArgumentNullException ex)
        //    {
        //        throw ex;
        //    }

        //}

    }
}