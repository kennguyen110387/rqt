﻿namespace CoverBuilder.Entities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Reporting.WebForms;
    public class ReportModel
    {
        public string ReportPath { get; set; }
        public string ReportFileName { get; set; }
        public string ReportName { get; set; }
        public ProcessingMode ProcessingMode { get; set; }
        public System.Data.DataSet ReportDataSource { get; set; }

    }
}
