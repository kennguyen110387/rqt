﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace CoverBuilder.DAL.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        void Add(T newEntity);
        void Remove(T newEntity);
        IQueryable<T> Find(Expression<Func<T, bool>> predicate);
        Task<IQueryable<T>> FindAsync(Expression<Func<T, bool>> predicate);       
        IQueryable<T> FindAll();
        void Update(T newEntity);
        void UpdateLiabilityZohoLeadId(T entity);
        void UpdateLiabilityInsuranceDates(T entity);
        void UpdateLiabilityTypeUw(T entity);
        void UpdateUnderwriterIsActive(T entity);
        void UpdateUnderwriterId(T entity);
        void UpdateIsSynced(T entity);
        void UpdateCc(T entity);
        void UpdateBcc(T entity);
        IQueryable<T> AdvanceSearch(Expression<Func<T, bool>> predicate);
    }
}
