﻿using System;
using System.Configuration;
using CoverBuilder.Entities.Entities;
using CoverBuilder.Utilities;
using System.Diagnostics;
using CoverBuilder.Common.Logging;
using System.Data.Entity.Core.Objects;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Threading.Tasks;
using System.Threading;

namespace CoverBuilder.DAL.Infrastructure
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private static ILogger logger = new NLogger();
        readonly ObjectContext _context;
        const string ConnectionStringName = "CoverBuilderEntities";

        //SQL Tables
        DataRepository<LiabilityInsuranceType> _LiabilityInsuranceType = null;
        DataRepository<LiabilityQuotationHeader> _LiabilityQuotationHeader = null;
        DataRepository<DropDownValue> _DropDownValue = null;
        DataRepository<CheckBoxValue> _CheckBoxValue = null;
        DataRepository<Questionaire> _Questionaire = null;
        DataRepository<LiabilityInsuranceResult> _LiabilityInsuranceResult = null;
        DataRepository<Lookup> _Lookup = null;
        DataRepository<PremiumDetail> _PremiumDetail = null;
        DataRepository<PremiumCalculationReference> _PremiumCalculationReference = null;
        DataRepository<EmailTemplate> _EmailTemplate = null;
        DataRepository<EmailTransaction> _EmailTransaction = null;
        DataRepository<User> _User = null;
        DataRepository<Attachment> _Attachment = null;
        DataRepository<Underwriter> _Underwriter = null;

        public UnitOfWork()
        {
            var connectionString = new ConfigHelper().GetConnectionString(ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString, "CoverBuilder");
            //var connectionString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;

            _context = new ObjectContext(connectionString);
            _context.ContextOptions.LazyLoadingEnabled = false;
            _context.ContextOptions.ProxyCreationEnabled = false;
            _context.CommandTimeout = 2 * 60;
        }

        public IRepository<LiabilityInsuranceType> LiabilityInsuranceType
        {
            get
            {
                if (_LiabilityInsuranceType == null)
                {
                    _LiabilityInsuranceType = new DataRepository<LiabilityInsuranceType>(_context);
                }

                return _LiabilityInsuranceType;
            }
        }
        public IRepository<LiabilityQuotationHeader> LiabilityQuotationHeader
        {
            get
            {
                if (_LiabilityQuotationHeader == null)
                {
                    _LiabilityQuotationHeader = new DataRepository<LiabilityQuotationHeader>(_context);
                }

                return _LiabilityQuotationHeader;
            }
        }
        public IRepository<DropDownValue> DropDownValue
        {
            get
            {
                if (_DropDownValue == null)
                {
                    _DropDownValue = new DataRepository<DropDownValue>(_context);
                }

                return _DropDownValue;
            }
        }
        public IRepository<CheckBoxValue> CheckBoxValue
        {
            get
            {
                if (_CheckBoxValue == null)
                {
                    _CheckBoxValue = new DataRepository<CheckBoxValue>(_context);
                }

                return _CheckBoxValue;
            }
        }
        public IRepository<Questionaire> Questionaire
        {
            get
            {
                if (_Questionaire == null)
                {
                    _Questionaire = new DataRepository<Questionaire>(_context);
                }

                return _Questionaire;
            }
        }
        public IRepository<LiabilityInsuranceResult> LiabilityInsuranceResult
        {
            get
            {
                if (_LiabilityInsuranceResult == null)
                {
                    _LiabilityInsuranceResult = new DataRepository<LiabilityInsuranceResult>(_context);
                }

                return _LiabilityInsuranceResult;
            }
        }
        public IRepository<Lookup> Lookup
        {
            get
            {
                if (_Lookup == null) _Lookup = new DataRepository<Lookup>(_context);
                return _Lookup;
            }
        }
        public IRepository<PremiumCalculationReference> PremiumCalculationReference
        {
            get
            {
                if (_PremiumCalculationReference == null)
                {
                    _PremiumCalculationReference = new DataRepository<PremiumCalculationReference>(_context);
                }
                return _PremiumCalculationReference;
            }
        }
        public IRepository<PremiumDetail> PremiumDetail
        {
            get
            {
                if (_PremiumDetail == null)
                {
                    _PremiumDetail = new DataRepository<PremiumDetail>(_context);
                }

                return _PremiumDetail;
            }
        }
        public IRepository<EmailTemplate> EmailTemplate
        {
            get
            {
                if (_EmailTemplate == null)
                {
                    _EmailTemplate = new DataRepository<EmailTemplate>(_context);
                }

                return _EmailTemplate;
            }
        }
        public IRepository<EmailTransaction> EmailTransaction
        {
            get
            {
                if (_EmailTransaction == null)
                {
                    _EmailTransaction = new DataRepository<EmailTransaction>(_context);
                }

                return _EmailTransaction;
            }
        }
        public IRepository<User> User
        {
            get
            {
                if (_User == null)
                {
                    _User = new DataRepository<User>(_context);
                }

                return _User;
            }
        }
        public IRepository<Attachment> Attachment
        {
            get
            {
                if (_Attachment == null)
                {
                    _Attachment = new DataRepository<Attachment>(_context);
                }

                return _Attachment;
            }
        }
        public IRepository<Underwriter> Underwriter
        {
            get
            {
                if (_Underwriter == null)
                {
                    _Underwriter = new DataRepository<Underwriter>(_context);
                }

                return _Underwriter;
            }
        }

        public int Commit()
        {

            string returnStatus = string.Empty;

            try
            {
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Debug.Assert(false, ex.InnerException.Message);
                logger.Error("Database Error" + ex.InnerException.Message, ex);
                return -1;
            }
            finally
            {
                Dispose();
            }
        }

        public async Task<int> CommitAsync(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                cancellationToken.ThrowIfCancellationRequested();
            }

            try
            {
                var saveTask = _context.SaveChangesAsync(cancellationToken);
                await saveTask;

                return saveTask.Result;
            }
            catch (System.Data.Entity.Core.OptimisticConcurrencyException ex)
            {
                //log
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
            
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public System.Data.DataSet ExecuteQuery(string query, List<SqlParameter> sqlParams, CommandType cmdType)
        {
            using(SqlCommand cmd = new SqlCommand())
	        {
	        	cmd.CommandText = query;
                cmd.CommandType = cmdType;
                
                if (sqlParams.Count > 0)
                {
                    foreach (var param in sqlParams)
                    {
                        cmd.Parameters.Add(param);
                    }
                }

                DAUtility adoDAL = new DAUtility(_context);
                return adoDAL.ExecuteQuery(cmd);
	        }
        }
        public int ExecuteNonQuery(string query, List<SqlParameter> sqlParams, CommandType cmdType)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = query;
                cmd.CommandType = cmdType;

                if (sqlParams.Count > 0)
                {
                    foreach (var param in sqlParams)
                    {
                        cmd.Parameters.Add(param);
                    }
                }

                DAUtility adoDAL = new DAUtility(_context);
                return adoDAL.ExecuteNonQuery(cmd);
            }
        }
    }
}
