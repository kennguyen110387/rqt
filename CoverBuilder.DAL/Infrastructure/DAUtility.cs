﻿namespace CoverBuilder.DAL.Infrastructure
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Core.EntityClient;

    public class DAUtility : IDAUtility
    {
        private readonly ObjectContext _efContext;
        public DAUtility(ObjectContext Context)
        {
            _efContext = Context;
        }

        public DataSet ExecuteQuery(SqlCommand sqlCommand)
        {
            var ds = new DataSet();
            var efContext = _efContext.Connection as EntityConnection;
            if (null != efContext)
            {
                sqlCommand.Connection = new SqlConnection(efContext.StoreConnection.ConnectionString);
            }
            using (var adapter = new SqlDataAdapter(sqlCommand))
            {
                adapter.Fill(ds);
                return ds;
            }
        }
        public int ExecuteNonQuery(SqlCommand sqlCommand)
        {
            var ds = new DataSet();
            var efContext = _efContext.Connection as EntityConnection;
            if (null != efContext)
            {
                sqlCommand.Connection = new SqlConnection(efContext.StoreConnection.ConnectionString);
                sqlCommand.Connection.Open();
            }
            return sqlCommand.ExecuteNonQuery();
        }
        public DataTable ExecuteToTable(SqlCommand sqlCommand)
        {
            throw new NotImplementedException();
        }
    }
}
