﻿namespace CoverBuilder.DAL.Infrastructure
{
    using System.Data;
    using System.Data.SqlClient;

    public interface IDAUtility
    {
        DataSet ExecuteQuery(SqlCommand sqlCommand);
        DataTable ExecuteToTable(SqlCommand sqlCommand);
        int ExecuteNonQuery(SqlCommand sqlCommand);
    }
}
