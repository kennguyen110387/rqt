﻿using System;
using CoverBuilder.Entities.Entities;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoverBuilder.DAL.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<LiabilityInsuranceType> LiabilityInsuranceType { get; }
        IRepository<LiabilityQuotationHeader> LiabilityQuotationHeader { get; }
        IRepository<Questionaire> Questionaire { get; }
        IRepository<DropDownValue> DropDownValue { get; }
        IRepository<CheckBoxValue> CheckBoxValue { get; }
        IRepository<LiabilityInsuranceResult> LiabilityInsuranceResult { get; }
        IRepository<Lookup> Lookup { get; }
        IRepository<PremiumDetail> PremiumDetail { get; }
        IRepository<PremiumCalculationReference> PremiumCalculationReference { get; }
        IRepository<EmailTemplate> EmailTemplate { get; }
        IRepository<EmailTransaction> EmailTransaction { get; }
        IRepository<User> User { get; }
        IRepository<Attachment> Attachment { get; }
        IRepository<Underwriter> Underwriter { get; }

        int Commit();
        Task<int> CommitAsync(System.Threading.CancellationToken cancellationToken);
        DataSet ExecuteQuery(string query, List<SqlParameter> sqlParams, CommandType cmdType);
        int ExecuteNonQuery(string query, List<SqlParameter> sqlParams, CommandType cmdType);
    }
}
