﻿
using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using CoverBuilder.Entities;
namespace CoverBuilder.DAL.Infrastructure
{
    public class DataRepository<T> : IRepository<T> where T : class
    {
        protected ObjectSet<T> _objectSet;

        public DataRepository(ObjectContext context)
        {
            _objectSet = context.CreateObjectSet<T>();
        }

        public void Add(T newEntity)
        {
            _objectSet.AddObject(newEntity);
        }

        public void Remove(T newEntity)
        {
            _objectSet.Attach(newEntity);
            _objectSet.DeleteObject(newEntity);
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return _objectSet.Where(predicate);
        }

        public async System.Threading.Tasks.Task<IQueryable<T>> FindAsync(Expression<Func<T, bool>> predicate)
        {
            var getObjectAsync = _objectSet.Where(predicate).ToListAsync();
            var result =  await getObjectAsync;

            return result.AsQueryable();
        }

        public IQueryable<T> FindAll()
        {
            return _objectSet;
        }

        public void Update(T newEntity)
        {
            _objectSet.Attach(newEntity);
            _objectSet.Context.ObjectStateManager.ChangeObjectState(newEntity, System.Data.Entity.EntityState.Modified);
        }

        public void UpdateLiabilityZohoLeadId(T entity)
        {
            _objectSet.Attach(entity);
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("LeadId");
        }

        public IQueryable<T> AdvanceSearch(Expression<Func<T, bool>> predicate)
        {
            return _objectSet.AsQueryable().Where(predicate);
        }

        public void UpdateLiabilityInsuranceDates(T entity)
        {
            _objectSet.Attach(entity);
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("InsurancePeriodFrom");
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("InsurancePeriodTo");
        }

        public void UpdateLiabilityTypeUw(T entity)
        {
            _objectSet.Attach(entity);
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("UnderwriterId");
        }
        public void UpdateUnderwriterIsActive(T entity)
        {
            _objectSet.Attach(entity);
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("IsActive");
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("ModifiedDate");
        }
        public void UpdateUnderwriterId(T entity)
        {
            _objectSet.Attach(entity);
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("UnderwriterId");
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("ModifiedDate");
        }
        public void UpdateIsSynced(T entity)
        {
            _objectSet.Attach(entity);
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("IsSynced");
        }

        // UPDATE: CC/BCC
        public void UpdateCc(T entity)
        {
            _objectSet.Attach(entity);
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("Cc");
        }

        public void UpdateBcc(T entity)
        {
            _objectSet.Attach(entity);
            _objectSet.Context.ObjectStateManager.GetObjectStateEntry(entity).SetModifiedProperty("Bcc");
        }
    }
}
