﻿CREATE TABLE [adm].[User] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Username]    VARCHAR (50)  NULL,
    [Password]    VARCHAR (200) NULL,
    [Salt]        VARCHAR (50)  NULL,
    [IsActive]    BIT           NULL,
    [CreatedBy]   VARCHAR (50)  NULL,
    [CreatedDate] DATETIME      NULL,
    CONSTRAINT [PK_User_1] PRIMARY KEY CLUSTERED ([Id] ASC)
);

