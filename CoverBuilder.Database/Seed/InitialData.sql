﻿--CHECKBOXVALUE
SET IDENTITY_INSERT [dbo].[CheckBoxValue] ON 

INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (1, 2, 11, N'Bathroom Cleaning Including Tile & Grout Cleaning & Mould Removal ', NULL, 1, 1, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (2, 2, 11, N'Curtain & Blinds Cleaning ', NULL, 2, 1, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (3, 2, 11, N'Domestic Bond / End of Lease Cleaning / Move Out Cleaning / Spring Cleaning / Renovation Cleans ', NULL, 3, 1, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (4, 2, 11, N'Domestic Washing & Ironing Services ', NULL, 4, 1, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (5, 2, 11, N'Domestic/Residential Cleaning ', NULL, 5, 1, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (6, 2, 11, N'Office Cleaning - Outside Business Hours including Move Out Cleans / Spring Cleans ', NULL, 6, 1, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (7, 2, 11, N'Retail Shop - Outside Business Hours ', NULL, 7, 1, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (8, 2, 11, N'Body Corporate Cleaning ', NULL, 1, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (9, 2, 11, N'Builders Clean (Internal) ', NULL, 2, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (10, 2, 11, N'Car Washing & Detailing ', NULL, 3, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (11, 2, 11, N'Carpet / Upholstery Protection - Domestic & Commercial ', NULL, 4, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (12, 2, 11, N'Domestic Swimming Pool ', NULL, 5, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (13, 2, 11, N'Factories (Office and toilet areas only) ', NULL, 6, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (14, 2, 11, N'Gutter & Roof Cleaning - If High Pressure Rate in Group 3 ', NULL, 7, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (15, 2, 11, N'Handyman Services - Painting, fences, gates/doors, inlcuding minor welding etc - Excluding Structural Construction ', NULL, 8, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (16, 2, 11, N'Landscaping Services - Including Minor Tree Lopping (Tree Lopping Endorsement to Apply) ', NULL, 9, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (17, 2, 11, N'Lawn Mowing & Garden Maintenance ', NULL, 10, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (18, 2, 11, N'Rubbish Removal - Excluding Asbestos ', NULL, 11, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (19, 2, 11, N'Steam Cleaning ', NULL, 12, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (20, 2, 11, N'Window cleaning - Ground Level only ', NULL, 13, 2, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (21, 2, 11, N'Builders Clean (External) ', NULL, 1, 3, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (22, 2, 11, N'Cinema Cleaning - Outside Business Hours ', NULL, 2, 3, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (23, 2, 11, N'Factory Cleaning ', NULL, 3, 3, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (24, 2, 11, N'High Pressure Washing - External Walls, House Washing, Roof, Driveways & Pathways, Patios, Shade Sails, Fences, Mould Removal ', NULL, 4, 3, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (25, 2, 11, N'Sports Leisure facilities/Gymnasiums ', NULL, 5, 3, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (26, 2, 11, N'Toilet Cleaning - Outside Business Hours ', NULL, 6, 3, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (27, 2, 11, N'Window Cleaning - Above Ground Level up to 10 metres ', NULL, 7, 3, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (28, 2, 11, N'Chimney Shafts (Domestic, but not Industrial) ', NULL, 1, 4, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (29, 2, 11, N'Floor Polishing ', NULL, 2, 4, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (30, 2, 11, N'Floor Stripping & Resealing ', NULL, 3, 4, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (31, 2, 11, N'Public Areas: Common Mall / Arcade / Shopping Centre cleaning after hours ', NULL, 4, 4, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (32, 2, 11, N'Schools (Outside Business Hours Only) ', NULL, 5, 4, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (33, 2, 11, N'Street Cleaning ', NULL, 6, 4, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (34, 2, 18, N'Cleaning of Schools During School Hours ', NULL, 1, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (35, 2, 18, N'Cleaning of Medical Facilities & Hospitals ', NULL, 2, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (36, 2, 18, N'Cleaning of Shopping Centres/Supermarkets/Retail Premises during Business Hours ', NULL, 3, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (37, 2, 18, N'Pest Control ', NULL, 4, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (38, 2, 18, N'Structural Construction ', NULL, 5, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (39, 2, 18, N'Asbestos Removal ', NULL, 6, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (40, 2, 18, N'NONE OF THE ABOVE ', NULL, 7, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (41, 5, 49, N'Livestock', NULL, 1, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (42, 5, 49, N'Hazardous or Dangerous Goods', NULL, 2, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (43, 5, 49, N'None of the above', NULL, 3, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (44, 8, 82, N'Underground Mines', NULL, 1, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (45, 8, 82, N'Drag Line Excavators', NULL, 2, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (46, 8, 82, N'Refinery of Gas producing or bulk fuel storage', NULL, 3, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (47, 8, 82, N'High voltage power supply', NULL, 4, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (48, 8, 82, N'Airports', NULL, 5, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (49, 8, 82, N'Railways', NULL, 6, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (50, 8, 82, N'Temporary Seating', NULL, 7, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (51, 8, 82, N'Stages', NULL, 8, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (52, 8, 82, N'LIghting Towers', NULL, 9, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (53, 8, 82, N'Camera Towers', NULL, 10, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (54, 8, 82, N'Concerts', NULL, 11, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (55, 8, 82, N'Sporting Events', NULL, 12, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (56, 8, 82, N'Work outside of Australia', NULL, 13, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (57, 8, 82, N'NONE OF THE ABOVE', NULL, 14, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (58, 7, 226, N'Purely Residential Complexes', NULL, 1, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (59, 7, 226, N'Accommodation risks', NULL, 2, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (60, 7, 226, N'Pubs/Hotels/Taverns/Licensed Clubs/Nightclubs', NULL, 3, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (61, 7, 226, N'Cinemas', NULL, 4, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (62, 7, 226, N'NONE OF THE ABOVE', NULL, 5, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (63, 1, 235, N'Hotel Accommodation', NULL, 1, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (64, 1, 235, N'Homestay Accommodation', NULL, 2, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (65, 1, 235, N'Colleges / Universities / On Campus Accommodation/ Halls of Residence', NULL, 3, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (66, 1, 235, N'NONE OF THE ABOVE', NULL, 99, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (67, 3, 247, N'Nightclubs', NULL, 1, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (68, 3, 247, N'Sports / Theme Bars', NULL, 2, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (69, 3, 247, N'Hotel/Pub/Tavern', NULL, 3, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (70, 3, 247, N'NONE OF THE ABOVE', NULL, 99, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (71, 6, 276, N'Manual Work', NULL, 1, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (72, 6, 276, N'Construction Activities', NULL, 2, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (73, 6, 276, N'Marine Work including work on Vessels/Boats', NULL, 3, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (74, 6, 276, N'Work in connectoin with Underground MIning Activities', NULL, 4, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (75, 6, 276, N'Work in connection with Airfield& Airport Activities', NULL, 5, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (76, 6, 276, N'Work Off-shore or Overseas', NULL, 6, NULL, 1)
INSERT [dbo].[CheckBoxValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (77, 6, 276, N'NONE OF THE ABOVE', NULL, 6, NULL, 1)
SET IDENTITY_INSERT [dbo].[CheckBoxValue] OFF

--DROPDOWNVALUE
SET IDENTITY_INSERT [dbo].[DropDownValue] ON 

INSERT [dbo].[DropDownValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (1, 2, 14, N'N/A', NULL, 1, NULL, 1)
INSERT [dbo].[DropDownValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (2, 2, 14, N'$10,000', NULL, 2, NULL, 1)
INSERT [dbo].[DropDownValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (3, 2, 14, N'$25,000', NULL, 3, NULL, 1)
INSERT [dbo].[DropDownValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (4, 2, 14, N'$50,000', NULL, 4, NULL, 1)
INSERT [dbo].[DropDownValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (5, 1, 234, N'Backpackers', NULL, 1, NULL, 1)
INSERT [dbo].[DropDownValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (6, 1, 234, N'Boarding Houses', NULL, 2, NULL, 1)
INSERT [dbo].[DropDownValue] ([ID], [InsuranceID], [QuestionID], [Item], [Value], [OrderID], [GroupID], [IsActive]) VALUES (7, 1, 234, N'Student Accommodation', NULL, 3, NULL, 1)
SET IDENTITY_INSERT [dbo].[DropDownValue] OFF

--ENDORSEMENTLOOKUP
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (1, N'Budget Accommodation', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (2, N'Budget Accommodation', N'ERROR', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (3, N'Budget Accommodation', N'INNKE', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (4, N'Motels and B&Bs (MB)', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (5, N'Motels and B&Bs (MB)', N'ERROR', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (6, N'Motels and B&Bs (MB)', N'INNKE', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (7, N'Cleaners (CA)', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (8, N'Cleaners (CA)', N'LOSSK', N'Loss Keys selected')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (9, N'Cleaners (CA)', N'SUBOW', N'Sub-contractors used with own insurance')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (10, N'Cleaners (CA)', N'SUBEN', N'Sub-contractors used without own insurance')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (11, N'Restaurants (RS)', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (12, N'Transport & Logistics (TL)', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (13, N'Transport & Logistics (TL)', N'SUBOW', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (14, N'Transport & Logistics (TL)', N'SUBEN', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (15, N'Project Managers (PM)', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (16, N'Project Managers (PM)', N'PROJE', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (17, N'Project Managers (PM)', N'PROFE', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (18, N'Project Managers (PM)', N'MANUA', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (19, N'Project Managers (PM)', N'OVERS', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (20, N'Project Managers (PM)', N'SUBOW', N'Sub-contractors used with own insurance')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (21, N'Project Managers (PM)', N'SUBEN', N'Sub-contractors used without own insurance')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (22, N'Property Owners (PO)', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (23, N'Property Owners (PO)', N'VACAN', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (24, N'Property Owners (PO)', N'CONST', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (25, N'Scaffolders (SF)', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (26, N'Scaffolders (SF)', N'SCAFF', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (27, N'Scaffolders (SF)', N'SUPPO', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (28, N'Scaffolders (SF)', N'WELDI', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (29, N'Scaffolders (SF)', N'ERRDE', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (30, N'Scaffolders (SF)', N'PROPD', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (31, N'Scaffolders (SF)', N'AUSGU', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (32, N'Scaffolders (SF)', N'SUBOW', N'Sub-contractors used with own insurance')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (33, N'Scaffolders (SF)', N'SUBEN', N'Sub-contractors used without own insurance')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (34, N'Vacant Land (VL)', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (35, N'Vacant Land (VL)', N'VACAE', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (36, N'Vacant Land (VL)', N'CONST', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (37, N'Vacant Land (VL)', N'ROADS', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (38, N'Welders & Boilermakers (WB)', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (39, N'Welders & Boilermakers (WB)', N'WELDE', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (40, N'Welders & Boilermakers (WB)', N'SPREA', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (41, N'Welders & Boilermakers (WB)', N'SUBOW', N'Sub-contractors used with own insurance')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (42, N'Welders & Boilermakers (WB)', N'SUBEN', N'Sub-contractors used without own insurance')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (43, N'Shopping Centre (SC)', N'TOXIC', N'Default')
INSERT [dbo].[EndorsementLookup] ([Id], [InsuranceType], [Endorsement], [Conditions]) VALUES (44, N'Shopping Centre (SC)', N'SHOPP', N'Default')

--ENDORSEMENTS
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (1, N'TOXIC', N'TOXIC  MOULD   EXCLUSION', N'This  insurance  does not  cover  any  liability  whatsoever  arising directly  or  indirectly  out of  or  resulting  from  or  in consequence of,  or  in  any  way  involving:
a)	The  actual,  potential,  alleged  or  threatened  formation, growth,  presence,  release  or  dispersal  of  any  fungi,  moulds,  spores, or  mycotoxins  of  any  kind;  or

b)	Any  action  taken  by any  party  in  response  to  the  actual, potential,  alleged  or  threatened formation,  growth,  presence,  release or  dispersal  of  fungi,  moulds,  spores  or  mycotoxins  of  any  kind, such action  to  include  investigation,  testing  for,  detection  of, monitoring  of,  treating,  remediating  or removing  such fungi,  mould, spores or  mycotoxins;  or

c)	Any  government  or  regulatory  order,  requirement,  directive,  mandate or  decree  that  any  party  take  action  in  response  to  the  actual, potential,  alleged  or  threatened  formation,  growth,  presence,  release or  for  dispersal  of  fungi,  moulds,  spores  or  mycotoxins  of  any  kind, such action  to  include  investigating,  testing  for,  detection  of, monitoring  of,  treat,  remediating  or  removing  such fungi, moulds, spores or  mycotoxins.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (2, N'ERROR', N'ERRORS & OMISSIONS', N'Notwithstanding the provisions of Exclusion 3.5 of this policy, the Insurer will indemnify the Insured against the legal liability of the Insured to pay compensation in respect of Financial Loss occurring during the Period of Insurance provided that such Financial Loss arises out of any negligent act, error or omission committed or alleged to have been committed by or on behalf of the Insured in the course of the Business.

1.	The total aggregate liability of the Insurer during any one Period of Insurance for all claims arising out of Financial Loss shall not exceed AUD 250,000 or the specified amount stated in the Schedule inclusive of costs and expenses as specified under 2.2 Defence Costs and Supplementary Payments.

2.	The indemnity provided under this endorsement is subject to the Excess specified in the Schedule

3.	Definition Applicable to this Endorsement
“Financial Loss” means any loss which is economic in nature and not consequent upon Personal 
Injury or Property Damage.

Exclusions Applicable to this Endorsement
The liability of the Insurer to indemnify the Insured pursuant to this endorsement shall not extend to liability:
1.	for the cost of recalling, withdrawing, replacing or repairing Products or of making any refund on the price paid for Products;
2.	assumed under contract or agreement unless such liability would have attached in the absence of such contract or agreement;
3.	arising from facts or circumstances which are inevitable having regard to:
3.1 the circumstances and nature of the work undertaken, or
3.2 the Products;
4.	arising directly or indirectly out of any delay in the performance of services or out of the sale or supply of Products;
5.	incurred by or caused by a director or executive officer of the Insured whilst acting within the scope of their duties in such capacity;
6.	incurred by or caused by a director or executive officer of the Insured’s staff superannuation fund or funds whilst acting within the scope of their duties in such capacity;
7.	arising out of conspiracy, conversion, deceit, inducement, breach of contract or injurious falsehood;
8.	arising out of or in connection with any dishonest, fraudulent, intentional, criminal or malicious act, error or omission by the Insured or by an employee of the Insured;
9.	arising in the United States of America and/or Canada or in respect of any claim which would be subject to the jurisdiction of any court of competent jurisdiction within the United States of America and/or Canada.

Subject otherwise to the terms, Conditions and Exclusions of the policy.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (3, N'INNKE', N'INNKEEPERS LIABILITY', N'Notwithstanding anything contained in this policy to the contrary, other than the provisions of Exclusion 3.16, it is hereby agreed and declared that this policy extends to indemnify the Insured in respect of claims for Personal Injury and/or Property Damage in relation to any liability under any Innkeepers Liability Act, Licensing Act or any other Act of Parliament or Regulation or Ordinance but only to limit specified in the Schedule.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (4, N'LOSSK', N'NEGLIGENT LOSS OF KEYS ENDORSEMENT ', N'This insurance is extended to include Property Damage and costs of re-keying arising from the negligent loss of keys. However the sub limit for any loss sustained is limited to AUD [10,000 / 25,000 / 50,000] of each and every loss and in the aggregate.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (5, N'SUBOW', N'SUB CONTRACTORS OWN INSURANCE CLAUSE', N'It is a condition of cover under this Policy that all sub contractors carry no less than $10,000,000 liability cover. It is the responsibility of the Insured to ensure that such cover is in place prior to work commencing.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (6, N'SUBEN', N'SUB CONTRACTORS ENDORSEMENT ', N'It is hereby noted and agreed that liability is extended to include sub contractors liability, but only whilst sub contractors are working for or on behalf of the Insured and only whilst sub contractors do not carry their own liability insurance.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (7, N'PROJE', N'PROJECT MANGERS EXCLUSION', N'Excluding work performed in connection with marine work including work including work on vessels/boats, underground mining, airfields and airports.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (8, N'PROFE', N'PROFESSIONAL INDEMNITY EXCLUSION', N'Excluding Personal Injury and/or Property Damage arising out of information, advice, design, formulation or specification given or undertaken by the Insured.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (9, N'MANUA', N'MANUAL LABOUR EXCLUSION', N'Excluding Personal Injury and/or Property Damage arising out of manual labour undertaken by the Insured or on behalf of the Insured including but not limited to construction, fabrication, repair, alteration or installation activities.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (10, N'OVERS', N'OVERSEAS WORK EXCLUSION', N'Excluding Personal Injury and/or Property Damage arising out of work performed by, for or on behalf of the Insured outside of Australia and Australian territories.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (11, N'SCAFF', N'SCAFFOLDING EXCLUSION', N'Excluding scaffolding used in connection with temporary seating, stages, lighting towers, camera towers, concerts &/or sporting events, underground mining, drag line excavators, refinery or gas producing or bulk fuel storage facilities, high voltage power supply, airports, railways, or where insured’s work is outside of Australia.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (12, N'SUPPO', N'REMOVAL / WEAKENING OF SUPPORTS', N'This policy does not cover liability in respect of damage to any land or fixed property arising directly or indirectly from the removal or weakening of or interference with support to land, building or any other property.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (13, N'WELDI', N'WELDING', N'This policy does not cover liability in respect of claims caused by or arising out of arc or flame cutting, flame heating, arc or gas welding or similar operation in which welding is used, unless such activity is conducted in strict compliance with the Australian Standard issued by the Standards Association of Australia.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (14, N'ERRDE', N'ERROR IN FORMULA OR DESIGN', N'This policy  does not cover liability in respect of Personal Injury or Property Damage caused by or arising out of the nature, condition or quality of Products manufactured by You which nature, condition or quality result from the use of any design, formula, specification plan or pattern.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (15, N'PROPD', N'PROPERTY DAMAGE TO FIXED PROPERTY', N'This policy does not cover liability in respect of damage to or as a result of damage to roads, sewers, water pipes, gas pipes, electric, fibre optic or telecommunications wires or cables or their supports or to any land or fixed property whatsoever and/or the contents thereof unless You have ascertained from the relevant authorities the actual position of any such pipes, mains, cables and wires before commencing any operation.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (16, N'AUSGU', N'AUSTRALIA / NEW ZEALAND GUIDELINES', N'It is a condition of cover that the Insured must comply with Australian / New Zealand guidelines 
AS/NZ 4576:1995')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (17, N'SHOPP', N'SHOPPING MALL CONDITION', N'It shall be a condition precedent to liability under this policy that all contractors and sub contractors (including mall managers, cleaners, security firms and tenants and the like) have and maintain general liability insurance for at least AUD10,000,000 throughout the Period of Insurance.  ')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (18, N'VACAN', N'VACANT PROPERTY ENDORSEMENT', N'It is a condition of cover under this policy that vacant properties must be adequately secured to prevent unwanted access. If the property is not adequately secured, the Insured must maintain documentation evidencing regular inspections of not less than twice a week.

For the purpose of this endorsement, a property is deemed vacant when left unoccupied for 60 consecutive days or more.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (19, N'CONST', N'CONSTRUCTION EXCLUSION', N'This policy does not provide cover for Property Damage or Personal Injury arising directly or indirectly out of or caused by, through, or in connection with the construction, renovation, alteration or demolition of the property or structure.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (20, N'VACAE', N'VACANT LAND EXCLUSION', N'Excluding sealed / constructed car parks ')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (21, N'ROADS', N'ROADS, FOOTPATHS AND POWER LINES EXCLUSION', N'This policy excludes any liability, claims, costs or expenses arising directly or indirectly, out of or caused by, through or in connection with public roads, public power lines and/or public footpaths not owned or operated by the insured.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (22, N'WELDE', N'WELDING EXCLUSION', N'Excluding work performed on or in connection with auto trades including manufacturing of trailers and/or vehicle components, marine work including work on or in connection with vessels/boats and underwater welding.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (23, N'SPREA', N'SPREAD OF FIRE EXCLUSION', N'This policy excludes any claims, costs, Personal Injury or Property Damage arising directly or indirectly out of spread of fire which is the result of a sudden, specific and/or identifiable event occurring where a total fire ban has been declared by the relevant authority, other than for work occurring in workshops.

This exclusion will not apply if the Insured has approval from the relevant authority to conduct their work on total fire ban days.')
INSERT [dbo].[Endorsements] ([Id], [Code], [Name], [Description]) VALUES (24, N'HEATA', N'HEAT APPLICATION WARRANTY ', N'In consideration of the amount of the premium charged the Insured warrants that during any welding operation, cutting operation or operation involving the application of heat including: 
(a)	The use of a welding or cutting torch, or 

(b)	The use of electric arc welding equipment, or 

(c)	Any process which is similar to or carried on in connection with welding (including electric arc Welding or cutting which involves the application of heat by any means), or 

(d)	Any application of heat away from the named Insureds premises, each of the following precautions will be taken: 

1.	Immediately before each welding, cutting operation or operation involving the application of heat, all objects upon which any such operation is to be performed and all objects, parts, substances or structures which may be exposed to heat will be thoroughly wetted by application of water to the extent such application of water would not cause damage or pose a danger to such objects, parts, substances or structures. 

2.	During and for one half hour immediately following each welding operation, cutting operation or operation involving the application of heat, a fire extinguisher which is: 

a.	in full and proper working order, and 

b.	of a type and capacity capable of extinguishing a fire in any object, part, substance or structure which may be exposed to heat will be in close proximity to where such welding operation, cutting operation, or operation involving the application of heat is or was carried on. 

3.	A competent guard or watchman will be present during and for one half hour immediately following the completion of each welding operation, cutting operation or operation involving the application of heat and will watch for and extinguish any sparks, combustion or fire and will give warning of any sparks, combustion or fire. 

4.	Immediately after each welding operation, cutting operation or operation involving the application of heat, all objects upon which any such operation has been performed and all objects, parts, substances or structures which may be exposed to any degree of heat will be thoroughly wetted by application of water to the extent such application of water would not cause damage or pose a danger to such object, parts, substances or structures. 
The Insured must comply strictly with each of the above warranties. Violation of any of the above warranties voids all coverages.')

--LOOKUP
SET IDENTITY_INSERT [dbo].[Lookup] ON 

INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (1, N'WELDERSANDBOILERMAKERS', N'Domestic / Residential Work', N'wb1', N'', 1, 1)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (2, N'WELDERSANDBOILERMAKERS', N'Fence / Gate Manufacturing', N'wb2', N'', 2, 1)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (3, N'WELDERSANDBOILERMAKERS', N'Non Structural Components', N'wb3', N'', 3, 2)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (4, N'WELDERSANDBOILERMAKERS', N'Structural Components', N'wb4', N'', 4, 3)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (5, N'WELDERSANDBOILERMAKERS', N'Staircases / Handrails / Ballustrades Manufacturing', N'wb5', N'', 5, 3)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (6, N'WELDERSANDBOILERMAKERS', N'Earthmoving Machinery (excluding underground mining equipment)', N'wb6', N'', 6, 3)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (7, N'WELDERSANDBOILERMAKERS', N'Pressure Vessels', N'wb7', N'', 7, 4)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (8, N'WELDERSANDBOILERMAKERS', N'Gas Pipelines', N'wb8', N'', 8, 4)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (9, N'WELDERSANDBOILERMAKERS', N'Plastics Weldings', N'wb9', N'', 9, 4)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (10, N'WELDERSANDBOILERMAKERS', N'Aboveground Mining Equipment & Plant', N'wb10', N'', 10, 4)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (11, N'WELDERSANDBOILERMAKERS', N'Others', N'wb11', N'', 15, 4)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (12, N'STAMPTAXRATE', N'Queensland', N'0.09', N'Stamp tax rate for Queensland', 1, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (13, N'STAMPTAXRATE', N'New South Wales', N'0.09', N'Stamp tax rate for New South Wales', 2, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (14, N'STAMPTAXRATE', N'Victoria', N'0.10', N'Stamp tax rate for Victoria', 3, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (15, N'STAMPTAXRATE', N'Western Australia', N'0.10', N'Stamp tax rate for Western Australia', 4, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (16, N'STAMPTAXRATE', N'South Australia', N'0.11', N'Stamp tax rate for South Australia', 5, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (17, N'STAMPTAXRATE', N'Tasmania', N'0.10', N'Stamp tax rate for Tasmania', 6, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (18, N'STAMPTAXRATE', N'Australian Capital Territory', N'0', N'Stamp tax rate for South Australia', 7, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (19, N'STAMPTAXRATE', N'Northern Territory', N'0.10', N'Stamp tax rate for South Australia', 8, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (20, N'LIABILITYLIMIT', N'5,000,000', N'5000000', NULL, 1, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (21, N'LIABILITYLIMIT', N'10,000,000', N'10000000', NULL, 2, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (22, N'LIABILITYLIMIT', N'20,000,000', N'20000000', NULL, 3, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (23, N'WELDERSANDBOILERMAKERS', N'Auto trades including Manufacturing of Trailers or Vehicle Components', N'wb12', N'', 12, 4)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (24, N'WELDERSANDBOILERMAKERS', N'Marine Work including work on Vessels/Boats', N'wb13', N'', 13, 4)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (25, N'WELDERSANDBOILERMAKERS', N'Underwater Welding Services', N'wb14', N'', 14, 4)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (26, N'GENERICINFOQUESTIONS', N'BrokerageName', N'Brokerage Name', N'', 1, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (27, N'GENERICINFOQUESTIONS', N'FullName', N'Your Name', NULL, 2, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (28, N'GENERICINFOQUESTIONS', N'Email', N'Your Email', NULL, 3, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (29, N'GENERICINFOQUESTIONS', N'PhoneNo', N'Your PhoneNo', NULL, 4, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (30, N'GENERICINFOQUESTIONS', N'InsuredFullName', N'Full Insured Name', NULL, 5, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (31, N'GENERICINFOQUESTIONS', N'SituationStreetNo', N'Address', NULL, 6, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (32, N'GENERICINFOQUESTIONS', N'SituationSuburb', N'Suburb', NULL, 7, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (33, N'GENERICINFOQUESTIONS', N'SituationState', N'State', NULL, 8, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (34, N'GENERICINFOQUESTIONS', N'SituationPostCode', N'Post Code', NULL, 9, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (35, N'GENERICINFOQUESTIONS', N'InsurancePeriodFrom', N'Period Insurance From', NULL, 10, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (36, N'GENERICINFOQUESTIONS', N'InsurancePeriodTo', N'Period Insurance To', NULL, 11, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (37, N'GENERICINFOQUESTIONS', N'LiabilityLimit', N'Limit of Liability', NULL, 12, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (38, N'GENERICINFOQUESTIONS', N'DiscInfoClaim', N'Has the insured had any Liability Claims in the last 5 years?', NULL, 13, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (39, N'GENERICINFOQUESTIONS', N'DiscInfoClaimDetail', N'If yes, please provide full details:', NULL, 14, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (40, N'GENERICINFOQUESTIONS', N'DiscInfoDecline', N'Has the Insured ever had insurance declined or special terms imposed in the last 5 years?', NULL, 15, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (41, N'GENERICINFOQUESTIONS', N'DiscInfoDeclineDetail', N'If yes, please provide full details:', NULL, 16, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (42, N'GENERICINFOQUESTIONS', N'DiscInfoRenewal', N'Had an insurer refuse or not invite renewal?', NULL, 17, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (43, N'GENERICINFOQUESTIONS', N'DiscInfoRenewalDetail', N'If yes, please provide full details:', NULL, 18, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (44, N'GENERICINFOQUESTIONS', N'DiscInfoExcess', N'Had a special excess imposed on a policy insurance?', NULL, 19, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (45, N'GENERICINFOQUESTIONS', N'DiscInfoExcessDetail', N'If yes, please provide full details:', NULL, 20, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (46, N'GENERICINFOQUESTIONS', N'DiscInfoClaimRejected', N'Had a claim rejected under a policy of insurance?', NULL, 21, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (47, N'GENERICINFOQUESTIONS', N'DiscInfoClaimRejectedDetail', N'If yes, please provide full details:', NULL, 22, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (48, N'GENERICINFOQUESTIONS', N'DiscInfoBankrupt', N'Been declared bankrupt or put into receivership or liquidation?', NULL, 23, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (49, N'GENERICINFOQUESTIONS', N'DiscInfoBankruptDetail', N'If yes, please provide full details:', NULL, 24, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (50, N'GENERICINFOQUESTIONS', N'DiscInfoConvicted', N'Been charged with or convicted of a criminal offence?', NULL, 25, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (51, N'GENERICINFOQUESTIONS', N'DiscInfoConvictedDetail', N'If yes, please provide full details:', NULL, 26, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (52, N'MOTELSANDBB', N'Motel', N'Motel', NULL, 1, NULL)
INSERT [dbo].[Lookup] ([Id], [Code], [Text], [Value], [Description], [Sequence], [Group]) VALUES (53, N'MOTELSANDBB', N'Bed & Breakfast', N'Bed & Breakfast', NULL, 2, NULL)
SET IDENTITY_INSERT [dbo].[Lookup] OFF

--QUESTIONAIRE
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (1, 1, N'Number of Rooms', N'Number of Rooms is greater than 50', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (2, 1, N'Annual Turnover', N'Annual Turnover is greater than $2,500,000', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (3, 1, N'Does the premises have a Licensed Bar?', N'Has a Licensed Bar', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (4, 1, N'Is Property Owners Liability required?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (5, 1, N'Does the premises have hardwired smoke detectors?', N'Does not have hardwired smoke detectors', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (6, 1, N'Does the premises have any sporting facilities?', N'Premises have sporting facilities', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (7, 1, N'Does the premises provide accommodation for occupants who have an intellectual or physical disability?', N'Premises provide accommodation to those mentally and physically disabled', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (8, 1, N'Comments', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (9, 1, N'Please provide full details', NULL, 6, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (10, 1, N'Please provide full details', NULL, 7, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (11, 2, N'Please provide details of all cleaning activities undertaken', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (12, 2, N'Annual Turnover', N'Annual Turnover is greater than $1,000,000', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (13, 2, N'Years in business', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (14, 2, N'Are Sub Contractors used?', N'Sub Contractors used', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (15, 2, N'Are Labour Hire Personal used?', N'Labour Hired Personel used', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (16, 2, N'Loss of Keys Extension', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (17, 2, N'Are there any Contractual or Hold Harmless Agreements?', N'Have Contractual or Hold Harmless Agreement', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (18, 2, N'Are any of the following Activities or Services to be included?', N'Provided Cleaning Activities/Services', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (19, 2, N'Comments', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (20, 2, N'Total Amount paid to Sub Contractors', NULL, 14, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (21, 2, N'Activities Performed by Sub Contractors', NULL, 14, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (22, 2, N'Total Amount paid to Labour Hire Personal', NULL, 15, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (23, 2, N'Activities Performed by Labour Hire', NULL, 15, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (24, 3, N'Number of Rooms', N'Number of Rooms is greater than 50', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (25, 3, N'Annual Turnover', N'Annual Turnover is greater than $2,500,000', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (26, 3, N'Years in business', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (27, 3, N'Does the premises have a Licensed Restaurant?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (28, 3, N'Is Property Owners Liability Required?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (29, 3, N'Does the premises have hardwired smoke detectors?', N'Does not have hardwired smoke detectors', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (30, 3, N'Does the premises have any sporting facilities?', N'Premises have sporting facilities', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (31, 3, N'Are any of the following Activities or Services to be included?', NULL, NULL, 0)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (32, 3, N'Does the premises provide accommodation for occupants who have an intellectual or physical disability?', N'Premises provide accommodation to those mentally and physically disabled', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (33, 3, N'Comments', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (34, 3, N'If yes, please provide full details', NULL, 30, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (35, 3, N'If yes, please provide full details', NULL, 32, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (36, 4, N'Years in business', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (37, 4, N'Turnover Split', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (38, 4, N'Trading Hours', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (39, 4, N'Seating Capacity', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (40, 4, N'Comments', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (41, 4, N'Food', N'Combined Turnover exceeds $5,000,000', 37, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (42, 4, N'Bar / Alcohol Sales', N'Bar Sales exceeds 50% of Total Turnover', 37, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (43, 5, N'Please provide a full description of business activities and all types of goods carried', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (44, 5, N'Annual Turnover', N'Annual Turnover exceeds $1,000,000', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (45, 5, N'Years in business', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (46, 5, N'Are Sub Contractors used?', N'Sub Contractors used.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (47, 5, N'Are Labour Hire Personal used?', N'Labour Hire Personal used.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (48, 5, N'Are there any Contractual or Hold Harmless Agreements?', N'With Contractual or Hold Harmless Agreements.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (49, 5, N'Are any of the following Activities or Services to be included?', N'Activities or Services included.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (50, 5, N'Comments', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (51, 5, N'Total Amount paid to Sub Contractors', NULL, 46, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (52, 5, N'Activities Performed by Sub Contractors', NULL, 46, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (53, 5, N'Total Amount paid to Labour Hire Personal', NULL, 47, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (54, 5, N'Activities Performed by Labour Hire', NULL, 47, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (55, 6, N'Please provide a full details of business activitie', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (56, 6, N'Annual Turnover', N'Annual Turnover exceeds $1,000,000', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (57, 6, N'Number of Employees', N'Number of employees was more than 5.	', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (58, 6, N'Annual Wages', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (59, 6, N'Years in business', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (60, 6, N'Does the Insured perform any manual work or construction activities', N'The Insured undertook manual works or construction activities.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (61, 6, N'Are Sub Contractors used?', N'Sub Contractors used.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (62, 6, N'Are Labour Hire Personal used?', N'Labour Hire Personnel used.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (63, 6, N'Are there any Contractual or Hold Harmless Agreements?', N'Had Contractual or Hold Harmless Agreements.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (64, 6, N'Comments', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (65, 6, N'Total Amount paid to Sub Contractors', NULL, 61, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (66, 6, N'Activities Performed by Sub Contractors', NULL, 61, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (67, 6, N'Total Amount paid to Labour Hire Personal', NULL, 62, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (68, 6, N'Activities Performed by Labour Hire', NULL, 62, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (69, 6, N'If yes, please provide full details', NULL, 60, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (70, 7, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (71, 7, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (72, 7, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (73, 7, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (74, 7, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (75, 7, N'Building Value?', N'Building Value is greater than $20,000,000.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (76, 8, N'Annual Turnover?', N'Annual Turnover is less than $200,00 or more than $2,000,000.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (77, 8, N'Years in Business?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (78, 8, N'Number of Employees?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (79, 8, N'Annual Wages?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (80, 8, N'% of Work UNDER 10 Mtrs?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (81, 8, N'% of Work OVER 10 Mtrs?', N'% of Work Over 10 Mtrs is more than 60%.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (82, 8, N'Does the Insured perform any work in connection with the following?', N'Performs any work in connection of the following', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (83, 8, N'If yes to any of the above, please provide full details of activities and turnover derived from these activities?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (84, 8, N'Does the Insured use Sub Contractors?', N'Sub Contractors used.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (85, 8, N'Payments to Sub Contractors?', NULL, 84, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (86, 8, N'Activities Performed by Sub Contractors?', NULL, 84, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (87, 8, N'Do Sub Contractors have their own Liability Insurance?”', NULL, 84, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (88, 8, N'Does the Insured use Labour Hire Personnel?', N'Labour Hire Personnel used.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (89, 8, N'Payments to Labour Hire Personnel?', NULL, 88, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (90, 8, N'Activities Performed by Labour Hire?', NULL, 88, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (91, 8, N'Do Labour Hire Personnel have their own Liability Insurance?', NULL, 88, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (92, 8, N'Comments?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (93, 9, N'Annual Turnover?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (94, 9, N'Years in Business?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (95, 9, N'Number of Employees?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (96, 9, N'Annual Wages?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (97, 9, N'Number of Checkouts?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (98, 9, N'Does the Insured use Sub Contractors?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (99, 9, N'Payments to Sub Contractors?', NULL, 98, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (100, 9, N'Activities Performed by Sub Contractors?', NULL, 98, 1)
GO
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (101, 9, N'Do Sub Contractors have their own Liability Insurance?', NULL, 98, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (102, 9, N'Does the Insured use Labour Hire Personnel?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (103, 9, N'Payments to Labour Hire Personnel?', NULL, 102, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (104, 9, N'Activities Performed by Labour Hire?', NULL, 102, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (105, 9, N'Do Labour Hire Personnel have their own Liability Insurance?', NULL, 102, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (106, 9, N'Comments?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (107, 10, N'Land Size (SQM)?', N'Land Size is more than 2,600,000 sqm.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (108, 10, N'Is any of the land used as a Hobby Farm?', N'Land is used as Hobby Farm.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (109, 10, N'If yes, please provide full details?', NULL, 108, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (110, 10, N'Is any of the land used for agistment purposes?', N'Land is used for agistment purposes.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (111, 10, N'If Yes, please type of animals and number of animals:?', NULL, 110, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (112, 10, N'Is any of the land used for Car Parking or Markets?', N'Land is used for Car Parking or Market area', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (113, 10, N'If yes, please provide full details?', NULL, 112, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (114, 10, N'Are there any Structures or Buildings in the Land?', N'Land has buildings/structures on it.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (115, 10, N'If yes, please provide full details?', NULL, 114, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (116, 10, N'Comments?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (117, 7, N'Tenants Occupation?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (118, 7, N'Rental Income?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (119, 7, N'Have any of the locations been unoccupied for more than 60 days?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (120, 7, N'If yes, please provide full details?', NULL, 119, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (121, 7, N'Comments?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (122, 6, N'Does Sub Contractors have their own Liability Insurance?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (123, 10, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (124, 10, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (125, 10, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (126, 10, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (127, 10, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (128, 11, N'Annual Turnover', N'Annual Turnover is greater than $500,000', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (129, 11, N'Years in Business', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (130, 11, N'Number of Employees', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (131, 11, N'Annual Wages', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (132, 11, N'Welding Activities', N'Has Other types of Welding Activities', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (133, 11, N'Does the Insured use Sub Contractors?', N'Insurer has Sub Contractors', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (134, 11, N'Does the Insured use Labour Hire Personnel?', N'Insurer has Labour Hire Personnel', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (135, 11, N'Comments', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (136, 12, N'Annual Turnover?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (137, 12, N'Years in Business?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (138, 12, N'Number of Anchor Tenants (Large Scale Tenants i.e. Myer / David Jones / Coles / Woolworths)', N'Total number of tenant exceeds 40 (Anchor and Retail Tenants).', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (139, 12, N'Number of Retail Tenants (All other Tenants)?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (140, 12, N'Does the Insured use Sub Contractors?', N'Has Sub Contractors.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (141, 12, N'Payments to Sub Contractors?', NULL, 140, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (142, 12, N'Activities Performed by Sub Contractors?', NULL, 140, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (143, 12, N'Do Sub Contractors have their own Liability Insurance?', NULL, 140, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (144, 12, N'Does the Insured use Labour Hire Personnel?', N'Has Labour Hire Personnel.', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (145, 12, N'Payments to labour Hire Personnel?', NULL, 144, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (146, 12, N'Activities Performed by Labour Hire?', NULL, 144, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (147, 12, N'Do labour Hire Personnel have their own Liability Insurance?', NULL, 144, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (148, 12, N'Comments?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (149, 11, N'Payment to Sub Contractors', NULL, 133, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (150, 11, N'Activities Performed by Sub Contractors', NULL, 133, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (151, 11, N'Payments to Labour Hire Personnel', NULL, 134, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (152, 11, N'Activities Performed by Labour Hire', NULL, 134, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (153, 11, N'Do Labour Hire Personnel have their own Liability Insurance?', NULL, 134, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (156, 3, N'Does the premises have a children’s playground?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (158, 3, N'If yes, please provide full details', NULL, 156, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (159, 13, N'Select Cover Required', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (160, 13, N'Please provide full details of all activities undertaken by the Insured', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (161, 13, N'Please advise number of days cover is required for (including Bump-In and Bump-Out Days)', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (162, 13, N'Estimated Turnover', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (163, 13, N'Comments', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (164, 13, N'Please provide full details of the event', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (165, 13, N'Estimated Attendees Per Day', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (166, 13, N'Number of Stalls', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (167, 13, N'Is cover required for Stall Holders Liability?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (168, 13, N'Do all third party service providers (i.e. Entertainers, Performers, Security etc) carry their own Liability Insurance?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (169, 13, N'Event Cancellation / Non-Appearance Cover', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (170, 13, N'Will the event have any of the following?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (171, 13, N'If No, please provide additional details', NULL, 168, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (172, 9, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (174, 9, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (175, 9, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (178, 9, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (179, 9, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (180, 9, N'Situation', NULL, NULL, 0)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (201, 5, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (202, 5, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (203, 5, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (204, 5, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (205, 5, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (206, 6, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (207, 6, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (208, 6, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (209, 6, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (210, 6, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (211, 8, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (212, 8, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (213, 8, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (214, 8, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (215, 8, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (216, 11, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (217, 11, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (218, 11, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (219, 11, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (220, 11, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (221, 12, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (222, 12, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (223, 12, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (224, 12, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (225, 12, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (226, 7, N'Are any of the Tenants Occupation the following?', N'Tenant Occupation was included in the selection.', NULL, 1)
GO
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (229, 1, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (230, 1, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (231, 1, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (232, 1, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (233, 1, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (234, 1, N'Type of Accommodation', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (235, 1, N'Is the Accommodation Used for the following?', N'Specific Accommodation Used', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (236, 2, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (237, 2, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (238, 2, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (239, 2, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (240, 2, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (241, 2, N'If yes, please provide full details', NULL, 17, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (242, 3, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (243, 3, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (244, 3, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (245, 3, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (246, 3, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (247, 3, N'Does the Accommodation fall under any of the following categories?', N'Accommodation fell under the specific categories', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (248, 4, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (249, 4, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (250, 4, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (251, 4, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (252, 4, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (253, 4, N'Do you offer Takeaway?', N'Offers Takeaway', NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (254, 4, N'If yes, please provide full details', NULL, 253, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (256, 8, N'Street Number', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (257, 8, N'Street Name', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (258, 8, N'Suburb', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (259, 8, N'State', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (260, 8, N'Post Code', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (267, 6, N'If yes, please provide full details', N'', 63, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (268, 5, N'If yes, please provide full details', NULL, 48, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (269, 1, N'Years In Business', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (270, 11, N'Do Sub Contractors hold their own Insurance?', NULL, 133, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (271, 5, N'Do Sub Contractors hold their own Insurance?', NULL, 46, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (272, 5, N'Do Labour Hire hold their own Insurance?', NULL, 47, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (273, 3, N'Type Of Accommodation', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (274, 2, N'Do Sub Contractors hold their own Insurance?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (275, 2, N'Do Labour Hire hold their own Insurance?', NULL, NULL, 1)
INSERT [dbo].[Questionaire] ([QuestionId], [InsuranceId], [Question], [TriggerDesc], [ParentId], [IsActive]) VALUES (276, 6, N'Does the Insured Conduct any of the following?', N'Insured Conducted specific work/activity.', NULL, 1)

--PREMIUMCALCULATIONREFERENCE
SET IDENTITY_INSERT [dbo].[PremiumCalculationReference] ON 

INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (1, 2, N'BaseRate', NULL, 1, 10000000, CAST(0.00235 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (2, 2, N'BaseRate', NULL, 2, 10000000, CAST(0.00270 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (3, 2, N'BaseRate', NULL, 3, 10000000, CAST(0.00317 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (4, 2, N'BaseRate', NULL, 4, 10000000, CAST(0.00353 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (5, 2, N'MinBaseRate', NULL, 1, 10000000, CAST(635.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (6, 2, N'MinBaseRate', NULL, 2, 10000000, CAST(730.25000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (7, 2, N'MinBaseRate', NULL, 3, 10000000, CAST(857.25000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (8, 2, N'MinBaseRate', NULL, 4, 10000000, CAST(952.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (9, 2, N'LossKeys', NULL, 0, NULL, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (10, 2, N'LossKeys', NULL, 10000, NULL, CAST(25.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (11, 2, N'LossKeys', NULL, 25000, NULL, CAST(50.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (12, 2, N'LossKeys', NULL, 50000, NULL, CAST(100.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (13, 3, N'NoRestaurant', NULL, NULL, 5000000, CAST(14.05000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (14, 3, N'WithRestaurant', NULL, NULL, 5000000, CAST(17.55000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (15, 3, N'NoRestaurantMinBase', NULL, NULL, 5000000, CAST(467.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (16, 3, N'WithRestaurantMinBase', NULL, NULL, 5000000, CAST(616.25000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (17, 3, N'NoRestaurant', NULL, NULL, 10000000, CAST(16.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (18, 3, N'WithRestaurant', NULL, NULL, 10000000, CAST(20.65000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (19, 3, N'NoRestaurantMinBase', NULL, NULL, 10000000, CAST(550.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (20, 3, N'WithRestaurantMinBase', NULL, NULL, 10000000, CAST(725.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (21, 3, N'NoRestaurant', NULL, NULL, 20000000, CAST(21.45000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (22, 3, N'WithRestaurant', NULL, NULL, 20000000, CAST(26.85000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (23, 3, N'NoRestaurantMinBase', NULL, NULL, 20000000, CAST(715.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (24, 3, N'WithRestaurantMinBase', NULL, NULL, 20000000, CAST(942.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (25, 4, N'BaseRate', 1, 750000, 5000000, CAST(300.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (26, 4, N'BaseRate', 750001, 1500000, 5000000, CAST(450.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (27, 4, N'BaseRate', 1500001, 3000000, 5000000, CAST(675.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (28, 4, N'BaseRate', 3000001, 5000000, 5000000, CAST(1012.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (29, 4, N'BaseRate', 5000001, 999999999999999, 5000000, CAST(1012.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (30, 4, N'BaseRate', 1, 750000, 10000000, CAST(375.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (31, 4, N'BaseRate', 750001, 1500000, 10000000, CAST(562.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (32, 4, N'BaseRate', 1500001, 3000000, 10000000, CAST(843.75000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (33, 4, N'BaseRate', 3000001, 5000000, 10000000, CAST(1265.65000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (34, 4, N'BaseRate', 5000001, 999999999999999, 10000000, CAST(1265.65000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (35, 4, N'BaseRate', 1, 750000, 20000000, CAST(450.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (36, 4, N'BaseRate', 750001, 1500000, 20000000, CAST(675.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (37, 4, N'BaseRate', 1500001, 3000000, 20000000, CAST(1012.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (38, 4, N'BaseRate', 3000001, 5000000, 20000000, CAST(1518.78000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (39, 4, N'BaseRate', 5000001, 999999999999999, 20000000, CAST(1518.78000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (40, 6, NULL, 1, 1, 5000000, CAST(375.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (41, 6, NULL, 1, 1, 10000000, CAST(525.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (42, 6, NULL, 1, 1, 20000000, CAST(682.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (43, 6, NULL, 2, 5, 5000000, CAST(525.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (44, 6, NULL, 2, 5, 10000000, CAST(735.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (45, 6, NULL, 2, 5, 20000000, CAST(955.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (46, 6, NULL, 6, 999999999999999, 5000000, CAST(525.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (47, 6, NULL, 6, 999999999999999, 10000000, CAST(735.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (48, 6, NULL, 6, 999999999999999, 20000000, CAST(955.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (49, 7, NULL, 1, 300000, 5000000, CAST(300.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (50, 7, NULL, 300001, 750000, 5000000, CAST(350.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (51, 7, NULL, 750001, 1500000, 5000000, CAST(400.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (52, 7, NULL, 1500001, 3000000, 5000000, CAST(500.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (53, 7, NULL, 3000001, 5000000, 5000000, CAST(600.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (54, 7, NULL, 5000001, 6500000, 5000000, CAST(750.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (55, 7, NULL, 6500001, 8000000, 5000000, CAST(850.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (56, 7, NULL, 8000001, 10000000, 5000000, CAST(1025.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (57, 7, NULL, 10000001, 12500000, 5000000, CAST(1250.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (58, 7, NULL, 12500001, 15000000, 5000000, CAST(1500.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (59, 7, NULL, 15000001, 17500000, 5000000, CAST(1800.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (60, 7, NULL, 17500001, 20000000, 5000000, CAST(2160.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (61, 7, NULL, 1, 300000, 10000000, CAST(375.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (62, 7, NULL, 300001, 750000, 10000000, CAST(435.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (63, 7, NULL, 750001, 1500000, 10000000, CAST(475.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (64, 7, NULL, 1500001, 3000000, 10000000, CAST(650.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (65, 7, NULL, 3000001, 5000000, 10000000, CAST(800.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (66, 7, NULL, 5000001, 6500000, 10000000, CAST(950.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (67, 7, NULL, 6500001, 8000000, 10000000, CAST(1050.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (68, 7, NULL, 8000001, 10000000, 10000000, CAST(1230.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (69, 7, NULL, 10000001, 12500000, 10000000, CAST(1500.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (70, 7, NULL, 12500001, 15000000, 10000000, CAST(1800.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (71, 7, NULL, 15000001, 17500000, 10000000, CAST(2160.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (72, 7, NULL, 17500001, 20000000, 10000000, CAST(2595.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (73, 7, NULL, 1, 300000, 20000000, CAST(465.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (74, 7, NULL, 300001, 750000, 20000000, CAST(540.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (75, 7, NULL, 750001, 1500000, 20000000, CAST(618.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (76, 7, NULL, 1500001, 3000000, 20000000, CAST(750.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (77, 7, NULL, 3000001, 5000000, 20000000, CAST(975.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (78, 7, NULL, 5000001, 6500000, 20000000, CAST(1200.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (79, 7, NULL, 6500001, 8000000, 20000000, CAST(1300.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (80, 7, NULL, 8000001, 10000000, 20000000, CAST(1600.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (81, 7, NULL, 10000001, 12500000, 20000000, CAST(1950.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (82, 7, NULL, 12500001, 15000000, 20000000, CAST(2340.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (83, 7, NULL, 15000001, 17500000, 20000000, CAST(2810.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (84, 7, NULL, 17500001, 20000000, 20000000, CAST(3375.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (85, 7, NULL, 20000001, 999999999999999, 5000000, CAST(2160.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (86, 7, NULL, 20000001, 999999999999999, 10000000, CAST(2595.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (87, 7, NULL, 20000001, 999999999999999, 20000000, CAST(3375.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (88, 8, N'Under 10 M', 1, 200000, 5000000, CAST(1550.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (89, 8, N'Under 10 M', 200001, 225000, 5000000, CAST(0.85300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (90, 8, N'Under 10 M', 225001, 250000, 5000000, CAST(0.83400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (91, 8, N'Under 10 M', 250001, 275000, 5000000, CAST(0.81800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (92, 8, N'Under 10 M', 275001, 300000, 5000000, CAST(0.79900 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (93, 8, N'Under 10 M', 300001, 325000, 5000000, CAST(0.78100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (94, 8, N'Under 10 M', 325001, 350000, 5000000, CAST(0.76700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (95, 8, N'Under 10 M', 350001, 375000, 5000000, CAST(0.75400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (96, 8, N'Under 10 M', 375001, 400000, 5000000, CAST(0.74300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (97, 8, N'Under 10 M', 400001, 425000, 5000000, CAST(0.73400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (98, 8, N'Under 10 M', 425001, 450000, 5000000, CAST(0.72500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (99, 8, N'Under 10 M', 450001, 475000, 5000000, CAST(0.71800 AS Decimal(18, 5)))
GO
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (100, 8, N'Under 10 M', 475001, 500000, 5000000, CAST(0.71100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (101, 8, N'Under 10 M', 500001, 600000, 5000000, CAST(0.70400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (102, 8, N'Under 10 M', 600001, 700000, 5000000, CAST(0.67800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (103, 8, N'Under 10 M', 700001, 800000, 5000000, CAST(0.65800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (104, 8, N'Under 10 M', 800001, 900000, 5000000, CAST(0.64400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (105, 8, N'Under 10 M', 900001, 1000000, 5000000, CAST(0.63100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (106, 8, N'Under 10 M', 1000001, 1100000, 5000000, CAST(0.62300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (107, 8, N'Under 10 M', 1100001, 1200000, 5000000, CAST(0.58600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (108, 8, N'Under 10 M', 1200001, 1300000, 5000000, CAST(0.55600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (109, 8, N'Under 10 M', 1300001, 1400000, 5000000, CAST(0.53100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (110, 8, N'Under 10 M', 1400001, 1500000, 5000000, CAST(0.50700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (111, 8, N'Under 10 M', 1500001, 1600000, 5000000, CAST(0.48800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (112, 8, N'Under 10 M', 1600001, 1700000, 5000000, CAST(0.47200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (113, 8, N'Under 10 M', 1700001, 1800000, 5000000, CAST(0.45800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (114, 8, N'Under 10 M', 1800001, 1900000, 5000000, CAST(0.44400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (115, 8, N'Under 10 M', 1900001, 2000000, 5000000, CAST(0.43200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (116, 8, N'Over 10 M', 1, 200000, 5000000, CAST(1975.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (117, 8, N'Over 10 M', 200001, 225000, 5000000, CAST(1.08700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (118, 8, N'Over 10 M', 225001, 250000, 5000000, CAST(1.06300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (119, 8, N'Over 10 M', 250001, 275000, 5000000, CAST(1.04300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (120, 8, N'Over 10 M', 275001, 300000, 5000000, CAST(1.01800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (121, 8, N'Over 10 M', 300001, 325000, 5000000, CAST(0.99600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (122, 8, N'Over 10 M', 325001, 350000, 5000000, CAST(0.97700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (123, 8, N'Over 10 M', 350001, 375000, 5000000, CAST(0.96000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (124, 8, N'Over 10 M', 375001, 400000, 5000000, CAST(0.94600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (125, 8, N'Over 10 M', 400001, 425000, 5000000, CAST(0.93500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (126, 8, N'Over 10 M', 425001, 450000, 5000000, CAST(0.92400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (127, 8, N'Over 10 M', 450001, 475000, 5000000, CAST(0.91400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (128, 8, N'Over 10 M', 475001, 500000, 5000000, CAST(0.90500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (129, 8, N'Over 10 M', 500001, 600000, 5000000, CAST(0.89800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (130, 8, N'Over 10 M', 600001, 700000, 5000000, CAST(0.86200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (131, 8, N'Over 10 M', 700001, 800000, 5000000, CAST(0.83800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (132, 8, N'Over 10 M', 800001, 900000, 5000000, CAST(0.82000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (133, 8, N'Over 10 M', 900001, 1000000, 5000000, CAST(0.80500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (134, 8, N'Over 10 M', 1000001, 1100000, 5000000, CAST(0.79300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (135, 8, N'Over 10 M', 1100001, 1200000, 5000000, CAST(0.74700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (136, 8, N'Over 10 M', 1200001, 1300000, 5000000, CAST(0.70800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (137, 8, N'Over 10 M', 1300001, 1400000, 5000000, CAST(0.67500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (138, 8, N'Over 10 M', 1400001, 1500000, 5000000, CAST(0.64700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (139, 8, N'Over 10 M', 1500001, 1600000, 5000000, CAST(0.62300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (140, 8, N'Over 10 M', 1600001, 1700000, 5000000, CAST(0.60200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (141, 8, N'Over 10 M', 1700001, 1800000, 5000000, CAST(0.58300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (142, 8, N'Over 10 M', 1800001, 1900000, 5000000, CAST(0.56500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (143, 8, N'Over 10 M', 1900001, 2000000, 5000000, CAST(0.55100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (144, 8, N'Over 10 M', 1, 200000, 10000000, CAST(2468.75000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (145, 8, N'Over 10 M', 200001, 225000, 10000000, CAST(1.35900 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (146, 8, N'Over 10 M', 225001, 250000, 10000000, CAST(1.32800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (147, 8, N'Over 10 M', 250001, 275000, 10000000, CAST(1.30400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (148, 8, N'Over 10 M', 275001, 300000, 10000000, CAST(1.27200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (149, 8, N'Over 10 M', 300001, 325000, 10000000, CAST(1.24400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (150, 8, N'Over 10 M', 325001, 350000, 10000000, CAST(1.22100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (151, 8, N'Over 10 M', 350001, 375000, 10000000, CAST(1.20000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (152, 8, N'Over 10 M', 375001, 400000, 10000000, CAST(1.18400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (153, 8, N'Over 10 M', 400001, 425000, 10000000, CAST(1.16800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (154, 8, N'Over 10 M', 425001, 450000, 10000000, CAST(1.15400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (155, 8, N'Over 10 M', 450001, 475000, 10000000, CAST(1.14300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (156, 8, N'Over 10 M', 475001, 500000, 10000000, CAST(1.13200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (157, 8, N'Over 10 M', 500001, 600000, 10000000, CAST(1.12200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (158, 8, N'Over 10 M', 600001, 700000, 10000000, CAST(1.07800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (159, 8, N'Over 10 M', 700001, 800000, 10000000, CAST(1.04700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (160, 8, N'Over 10 M', 800001, 900000, 10000000, CAST(1.02400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (161, 8, N'Over 10 M', 900001, 1000000, 10000000, CAST(1.00500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (162, 8, N'Over 10 M', 1000001, 1100000, 10000000, CAST(0.99100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (163, 8, N'Over 10 M', 1100001, 1200000, 10000000, CAST(0.93400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (164, 8, N'Over 10 M', 1200001, 1300000, 10000000, CAST(0.88600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (165, 8, N'Over 10 M', 1300001, 1400000, 10000000, CAST(0.84400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (166, 8, N'Over 10 M', 1400001, 1500000, 10000000, CAST(0.80900 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (167, 8, N'Over 10 M', 1500001, 1600000, 10000000, CAST(0.77800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (168, 8, N'Over 10 M', 1600001, 1700000, 10000000, CAST(0.75100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (169, 8, N'Over 10 M', 1700001, 1800000, 10000000, CAST(0.72800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (170, 8, N'Over 10 M', 1800001, 1900000, 10000000, CAST(0.70700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (171, 8, N'Over 10 M', 1900001, 2000000, 10000000, CAST(0.68900 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (172, 8, N'Over 10 M', 1, 200000, 20000000, CAST(2765.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (173, 8, N'Over 10 M', 200001, 225000, 20000000, CAST(1.52100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (174, 8, N'Over 10 M', 225001, 250000, 20000000, CAST(1.48700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (175, 8, N'Over 10 M', 250001, 275000, 20000000, CAST(1.46000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (176, 8, N'Over 10 M', 275001, 300000, 20000000, CAST(1.42500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (177, 8, N'Over 10 M', 300001, 325000, 20000000, CAST(1.39400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (178, 8, N'Over 10 M', 325001, 350000, 20000000, CAST(1.36700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (179, 8, N'Over 10 M', 350001, 375000, 20000000, CAST(1.34400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (180, 8, N'Over 10 M', 375001, 400000, 20000000, CAST(1.32600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (181, 8, N'Over 10 M', 400001, 425000, 20000000, CAST(1.30800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (182, 8, N'Over 10 M', 425001, 450000, 20000000, CAST(1.29300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (183, 8, N'Over 10 M', 450001, 475000, 20000000, CAST(1.28000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (184, 8, N'Over 10 M', 475001, 500000, 20000000, CAST(1.26700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (185, 8, N'Over 10 M', 500001, 600000, 20000000, CAST(1.25600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (186, 8, N'Over 10 M', 600001, 700000, 20000000, CAST(1.20800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (187, 8, N'Over 10 M', 700001, 800000, 20000000, CAST(1.17300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (188, 8, N'Over 10 M', 800001, 900000, 20000000, CAST(1.14700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (189, 8, N'Over 10 M', 900001, 1000000, 20000000, CAST(1.12600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (190, 8, N'Over 10 M', 1000001, 1100000, 20000000, CAST(1.11000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (191, 8, N'Over 10 M', 1100001, 1200000, 20000000, CAST(1.04500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (192, 8, N'Over 10 M', 1200001, 1300000, 20000000, CAST(0.99100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (193, 8, N'Over 10 M', 1300001, 1400000, 20000000, CAST(0.94500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (194, 8, N'Over 10 M', 1400001, 1500000, 20000000, CAST(0.90500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (195, 8, N'Over 10 M', 1500001, 1600000, 20000000, CAST(0.87100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (196, 8, N'Over 10 M', 1600001, 1700000, 20000000, CAST(0.84200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (197, 8, N'Over 10 M', 1700001, 1800000, 20000000, CAST(0.81500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (198, 8, N'Over 10 M', 1800001, 1900000, 20000000, CAST(0.79200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (199, 8, N'Over 10 M', 1900001, 2000000, 20000000, CAST(0.77100 AS Decimal(18, 5)))
GO
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (200, 8, N'Under 10 M', 1, 200000, 10000000, CAST(1937.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (201, 8, N'Under 10 M', 200001, 225000, 10000000, CAST(1.06600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (202, 8, N'Under 10 M', 225001, 250000, 10000000, CAST(1.04200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (203, 8, N'Under 10 M', 250001, 275000, 10000000, CAST(1.02300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (204, 8, N'Under 10 M', 275001, 300000, 10000000, CAST(0.99900 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (205, 8, N'Under 10 M', 300001, 325000, 10000000, CAST(0.97700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (206, 8, N'Under 10 M', 325001, 350000, 10000000, CAST(0.95800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (207, 8, N'Under 10 M', 350001, 375000, 10000000, CAST(0.94300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (208, 8, N'Under 10 M', 375001, 400000, 10000000, CAST(0.92800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (209, 8, N'Under 10 M', 400001, 425000, 10000000, CAST(0.91600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (210, 8, N'Under 10 M', 425001, 450000, 10000000, CAST(0.90600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (211, 8, N'Under 10 M', 450001, 475000, 10000000, CAST(0.89800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (212, 8, N'Under 10 M', 475001, 500000, 10000000, CAST(0.88900 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (213, 8, N'Under 10 M', 500001, 600000, 10000000, CAST(0.88100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (214, 8, N'Under 10 M', 600001, 700000, 10000000, CAST(0.84600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (215, 8, N'Under 10 M', 700001, 800000, 10000000, CAST(0.82200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (216, 8, N'Under 10 M', 800001, 900000, 10000000, CAST(0.80400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (217, 8, N'Under 10 M', 900001, 1000000, 10000000, CAST(0.79000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (218, 8, N'Under 10 M', 1000001, 1100000, 10000000, CAST(0.77800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (219, 8, N'Under 10 M', 1100001, 1200000, 10000000, CAST(0.73300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (220, 8, N'Under 10 M', 1200001, 1300000, 10000000, CAST(0.69500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (221, 8, N'Under 10 M', 1300001, 1400000, 10000000, CAST(0.66200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (222, 8, N'Under 10 M', 1400001, 1500000, 10000000, CAST(0.63500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (223, 8, N'Under 10 M', 1500001, 1600000, 10000000, CAST(0.61100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (224, 8, N'Under 10 M', 1600001, 1700000, 10000000, CAST(0.59000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (225, 8, N'Under 10 M', 1700001, 1800000, 10000000, CAST(0.57200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (226, 8, N'Under 10 M', 1800001, 1900000, 10000000, CAST(0.55600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (227, 8, N'Under 10 M', 1900001, 2000000, 10000000, CAST(0.54100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (228, 8, N'Under 10 M', 1, 200000, 20000000, CAST(2170.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (229, 8, N'Under 10 M', 200001, 225000, 20000000, CAST(1.19400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (230, 8, N'Under 10 M', 225001, 250000, 20000000, CAST(1.16700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (231, 8, N'Under 10 M', 250001, 275000, 20000000, CAST(1.14600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (232, 8, N'Under 10 M', 275001, 300000, 20000000, CAST(1.11900 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (233, 8, N'Under 10 M', 300001, 325000, 20000000, CAST(1.09300 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (234, 8, N'Under 10 M', 325001, 350000, 20000000, CAST(1.07400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (235, 8, N'Under 10 M', 350001, 375000, 20000000, CAST(1.05500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (236, 8, N'Under 10 M', 375001, 400000, 20000000, CAST(1.04000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (237, 8, N'Under 10 M', 400001, 425000, 20000000, CAST(1.02600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (238, 8, N'Under 10 M', 425001, 450000, 20000000, CAST(1.01500 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (239, 8, N'Under 10 M', 450001, 475000, 20000000, CAST(1.00400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (240, 8, N'Under 10 M', 475001, 500000, 20000000, CAST(0.99400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (241, 8, N'Under 10 M', 500001, 600000, 20000000, CAST(0.98700 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (242, 8, N'Under 10 M', 600001, 700000, 20000000, CAST(0.94800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (243, 8, N'Under 10 M', 700001, 800000, 20000000, CAST(0.92100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (244, 8, N'Under 10 M', 800001, 900000, 20000000, CAST(0.90000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (245, 8, N'Under 10 M', 900001, 1000000, 20000000, CAST(0.88400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (246, 8, N'Under 10 M', 1000001, 1100000, 20000000, CAST(0.87100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (247, 8, N'Under 10 M', 1100001, 1200000, 20000000, CAST(0.82100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (248, 8, N'Under 10 M', 1200001, 1300000, 20000000, CAST(0.77800 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (249, 8, N'Under 10 M', 1300001, 1400000, 20000000, CAST(0.74100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (250, 8, N'Under 10 M', 1400001, 1500000, 20000000, CAST(0.71100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (251, 8, N'Under 10 M', 1500001, 1600000, 20000000, CAST(0.68400 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (252, 8, N'Under 10 M', 1600001, 1700000, 20000000, CAST(0.66100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (253, 8, N'Under 10 M', 1700001, 1800000, 20000000, CAST(0.64000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (254, 8, N'Under 10 M', 1800001, 1900000, 20000000, CAST(0.62200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (255, 8, N'Under 10 M', 1900001, 2000000, 20000000, CAST(0.60600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (256, 8, N'Under 10 M', 2000001, 999999999999999, 20000000, CAST(0.60600 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (257, 8, N'Under 10 M', 2000001, 999999999999999, 10000000, CAST(0.54100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (258, 8, N'Over 10 M', 2000001, 999999999999999, 20000000, CAST(0.77100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (259, 8, N'Over 10 M', 2000001, 999999999999999, 10000000, CAST(0.68900 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (260, 8, N'Over 10 M', 2000001, 999999999999999, 5000000, CAST(0.55100 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (261, 8, N'Under 10 M', 2000001, 999999999999999, 5000000, CAST(0.43200 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (262, 10, NULL, 0, 1000, 10000000, CAST(165.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (263, 10, NULL, 1001, 2000, 10000000, CAST(177.38000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (264, 10, NULL, 2001, 3000, 10000000, CAST(190.68000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (265, 10, NULL, 3001, 4000, 10000000, CAST(204.98000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (266, 10, NULL, 4001, 5000, 10000000, CAST(220.35000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (267, 10, NULL, 5001, 10000, 10000000, CAST(236.88000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (268, 10, NULL, 10000, 15000, 10000000, CAST(254.64000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (269, 10, NULL, 15000, 20000, 10000000, CAST(273.74000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (270, 10, NULL, 20000, 50000, 10000000, CAST(294.27000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (271, 10, NULL, 50001, 100000, 10000000, CAST(316.34000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (272, 10, NULL, 100001, 200000, 10000000, CAST(340.07000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (273, 10, NULL, 200001, 300000, 10000000, CAST(365.58000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (274, 10, NULL, 300001, 400000, 10000000, CAST(392.99000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (275, 10, NULL, 400001, 500000, 10000000, CAST(422.47000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (276, 10, NULL, 500001, 600000, 10000000, CAST(454.15000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (277, 10, NULL, 600001, 700000, 10000000, CAST(488.21000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (278, 10, NULL, 700001, 800000, 10000000, CAST(524.83000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (279, 10, NULL, 800001, 900000, 10000000, CAST(564.19000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (280, 10, NULL, 900001, 1000000, 10000000, CAST(606.51000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (281, 10, NULL, 1000001, 1100000, 10000000, CAST(652.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (282, 10, NULL, 1100001, 1200000, 10000000, CAST(700.90000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (283, 10, NULL, 1200001, 1300000, 10000000, CAST(753.46000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (284, 10, NULL, 1300001, 1400000, 10000000, CAST(809.97000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (285, 10, NULL, 1400001, 1500000, 10000000, CAST(870.72000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (286, 10, NULL, 1500001, 1600000, 10000000, CAST(936.02000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (287, 10, NULL, 1600001, 1700000, 10000000, CAST(1006.23000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (288, 10, NULL, 1700001, 1800000, 10000000, CAST(1081.69000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (289, 10, NULL, 1800001, 1900000, 10000000, CAST(1162.82000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (290, 10, NULL, 1900001, 2000000, 10000000, CAST(1250.03000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (291, 10, NULL, 2000001, 2100000, 10000000, CAST(1343.78000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (292, 10, NULL, 2100001, 2200000, 10000000, CAST(1444.57000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (293, 10, NULL, 2200001, 2300000, 10000000, CAST(1552.91000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (294, 10, NULL, 2300001, 2400000, 10000000, CAST(1669.38000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (295, 10, NULL, 2400001, 2500000, 10000000, CAST(1794.58000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (296, 10, NULL, 2500001, 2600000, 10000000, CAST(1929.18000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (297, 11, N'BaseRate', 1, 0, 10000000, CAST(475.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (298, 11, N'BaseRate', 2, 0, 10000000, CAST(625.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (299, 11, N'BaseRate', 3, 0, 10000000, CAST(825.00000 AS Decimal(18, 5)))
GO
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (300, 11, N'BaseRate', 4, 0, 10000000, CAST(1035.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (301, 5, NULL, 1, 250000, 5000000, CAST(300.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (302, 5, NULL, 250001, 500000, 5000000, CAST(450.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (303, 5, NULL, 500001, 999999999999999, 5000000, CAST(600.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (304, 5, NULL, 1, 250000, 10000000, CAST(375.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (305, 5, NULL, 250001, 500000, 10000000, CAST(525.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (306, 5, NULL, 500001, 999999999999999, 10000000, CAST(675.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (307, 5, NULL, 1, 250000, 20000000, CAST(450.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (308, 5, NULL, 250001, 500000, 20000000, CAST(525.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (309, 5, NULL, 500001, 999999999999999, 20000000, CAST(750.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (310, 1, N'BaseRate', NULL, NULL, 5000000, CAST(93.50000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (311, 1, N'BaseRate', NULL, NULL, 10000000, CAST(110.00000 AS Decimal(18, 5)))
INSERT [dbo].[PremiumCalculationReference] ([ID], [InsuranceTypeID], [ConditionType], [ConditionA], [ConditionB], [LiabilityLimit], [Rate]) VALUES (312, 1, N'BaseRate', NULL, NULL, 20000000, CAST(143.00000 AS Decimal(18, 5)))
SET IDENTITY_INSERT [dbo].[PremiumCalculationReference] OFF

--USER
SET IDENTITY_INSERT [adm].[User] ON 

INSERT [adm].[User] ([Id], [Username], [Password], [Salt], [IsActive], [CreatedBy], [CreatedDate]) VALUES (1, N'admin', N'AMV3EKKk856s05rIP7fBPotI5Ta3F92MjRCH7X8TOPb5jz/oUSavl5p8YYqslM6PgQ==', N'xXcQoqTznqzTmsg/t8E+iw==', 1, N'admin', CAST(N'2018-01-04T01:53:22.440' AS DateTime))
SET IDENTITY_INSERT [adm].[User] OFF

insert into [Lookup]
(Code, [Text], Value, [Description])
values
('BUDGETACCOMMODATIONDESC', NULL, 1, 'Business: {BusinessDescription}\n\n

Limits of Liability: \n\n

{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n

Inn Keepers Liability - $100,000 any one Occurrence and in the aggregate
during the Period of Insurance\n\n

Excess: {Excess} each and every claim\n

GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n

POLICY WORDING\n

Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n

QUOTING INSURER\n

CERTAIN UNDERWRITERS

')
insert into [Lookup]
(Code, [Text], Value, [Description])
values
('CLEANERSDESC', NULL, 2, 'Business: {BusinessDescription}\n\n
Limits of Liability:\n\n
{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n\n
Negligent Loss of Keys: $10,000 / $25,000 / $50,000
any one loss and in the aggregate\n\n
Excess: {Excess} each and every claim\n
$20,000 each and every claim in respect of injury &/or illness to
Labour Hire Personnel &/or Sub Contractors
Endorsements Applicable:\n\n
The following endorsements &/or exclusions attach to
and form part of the policy wording:
Excluding work performed on or in connection with:\n
- Cleaning of Schools during school hours\n
- Cleaning of Medical Facilities &/or Hospitals\n
- Cleaning of Shopping Centres during business hours\n
- Cleaning of Supermarkets & Retail Premises during business hours\n
- Pest Control\n
- Structural Construction\n
- Asbestos Removal\n\n
Negligent Loss of Keys Endorsement\n
This insurance is extended to include Property Damage and costs of
re-keying arising from the negligent loss of keys. However, the sub limit
for any loss sustained is limited to AUD $10,000 / $25,000 / $50,000 of
each and every loss and in the aggregate.\n\n
GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n\n
POLICY WORDING\n\n
Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n\n
QUOTING INSURER\n\n
CERTAIN UNDERWRITERS
')
insert into [Lookup]
(Code, [Text], Value, [Description])
values
('MOTELSANDBBDESC', NULL, 3, 'Business: {BusinessDescription}\n\n
Limits of Liability:\n\n
{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n\n
Inn Keepers Liability - $100,000 any one Occurrence and in the aggregate
during the Period of Insurance\n\n
Excess: {Excess} each and every claim\n\n
GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n\n
POLICY WORDING\n\n
Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n\n
QUOTING INSURER\n\n
CERTAIN UNDERWRITERS')
insert into [Lookup]
(Code, [Text], Value, [Description])
values
('PROJECTMANAGERSDESC', NULL, 6, 'Business: {BusinessDescription}\n\n
Limits of Liability:\n\n
{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n\n
Excess: {Excess} each and every claim\n\n
Excluded Activities:\n
- Excluding work performed in connection with marine work including work
on vessels/boats, underground mining, airfields and airports.
GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n\n
POLICY WORDING\n\n
Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n\n
QUOTING INSURER\n\n
CERTAIN UNDERWRITERS
')
insert into [Lookup]
(Code, [Text], Value, [Description])
values
('PROPERTYOWNERSDESC', NULL, 7, 'Business: {BusinessDescription}\n\n
Limits of Liability:\n\n
{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n\n
Excess: {Excess} each and every claim\n\n
GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n\n
POLICY WORDING\n\n
Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n\n
QUOTING INSURER\n\n
CERTAIN UNDERWRITERS')
insert into [Lookup]
(Code, [Text], Value, [Description])
values
('RESTAURANTSDESC', NULL, 4, 'Business: {BusinessDescription}\n\n
Limits of Liability:\n\n
{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n\n
Excess: {Excess} each and every claim\n\n
GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n\n
POLICY WORDING\n\n
Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n\n
QUOTING INSURER\n\n
CERTAIN UNDERWRITERS

')

insert into [Lookup]
(Code, [Text], Value, [Description])
values
('TRANSPORTANDLOGISTICSDESC', NULL, 5, 'Business: {BusinessDescription}\n\n
Limits of Liability:\n\n
{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n\n
Excess: {Excess} each and every claim\n\n
GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n\n
POLICY WORDING\n\n
Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n\n
QUOTING INSURER\n\n
CERTAIN UNDERWRITERS

')
insert into [Lookup]
(Code, [Text], Value, [Description])
values
('SCAFFOLDERSDESC', NULL, 8, 'Business: {BusinessDescription}\n\n
Limits of Liability:\n\n
{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n\n
Excess: $1,000 each and every claim for work up to 10 meters in height\n
$2,500 each and every claim for work over 10 meters in height\n
$10,000 each and every claim in respect of illness/injury to
labour hire personnel &/or contractors/sub contractors\n\n
The following endorsements &/or exclusions attach to\n
and form part of the policy wording:\n\n
- Excluding scaffolding used in connection with temporary seating, stages,
lighting towers, camera towers, concerts &/or sporting events, underground
mining, drag line excavators, refinery or gas producing or bulk fuel
storage facilities, high voltage powers supply, airports, railways, or
where the Insured''s work is outside of Australia.\n\n
GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n\n
POLICY WORDING\n\n
Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n\n
QUOTING INSURER\n\n
CERTAIN UNDERWRITERS

')
insert into [Lookup]
(Code, [Text], Value, [Description])
values
('VACANTLANDDESC', NULL, 10, 'Business: {BusinessDescription}\n\n
Limits of Liability:\n\n
{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n\n
Excess: {Excess} each and every claim\n\n
GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n\n
POLICY WORDING\n\n
Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n\n
QUOTING INSURER\n\n
CERTAIN UNDERWRITERS

')

insert into [Lookup]
(Code, [Text], Value, [Description])
values
('WELDERSDESC', NULL, 11, 'Business: {BusinessDescription}\n\n
Limits of Liability:\n\n
{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n\n
Excess: {Excess} each and every claim\n\n
The following endorsements &/or exclusions attach to
and form part of the policy wording:\n\n
- Excluding work performed on or in connection with
underground mining equipment, auto trades including
manufacturing of trailers and/or vehicle components,
marine work including work on or in connection with
vessels/boats and underwater welding.\n\n
GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n\n
POLICY WORDING\n\n
Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n\n
QUOTING INSURER\n\n
CERTAIN UNDERWRITERS

')
insert into [Lookup]
(Code, [Text], Value, [Description])
values
('SHOPPINGCENTREDESC', NULL, 12, 'Business: {BusinessDescription} \n\n
Limits of Liability:\n\n
{LiabilityLimit} any one Occurrence and\n
{LiabilityLimit} in the aggregate in respect to Products Liability\n\n
Excess: {Excess} each and every claim\n
$10,000 each and every claim in respect of injury &/or Illness to\n
labour hire personnel &/or Contractors/Sub Contractors\n\n
GEOGRAPHICAL LIMITS Worldwide Excluding North America & Canada\n\n
POLICY WORDING\n\n
Ryno General and Products Liability Policy Wording - RY.GPL.LLO.V260916\n\n
QUOTING INSURER\n\n
CERTAIN UNDERWRITERS

')



UPDATE
    [Lookup]
SET
    [Description] = replace([Description],'\n',char(13)+char(10))
where Code  LIKE '%DESC%'