﻿CREATE TABLE [dbo].[Logs] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [ReferenceId] VARCHAR (50)   NULL,
    [MachineName] VARCHAR (50)   NULL,
    [IpAddress]   VARCHAR (50)   NULL,
    [Level]       VARCHAR (24)   NULL,
    [CallSite]    VARCHAR (150)  NULL,
    [Message]     NVARCHAR (MAX) NULL,
    [Exception]   VARCHAR (64)   NULL,
    [StackTrace]  NVARCHAR (MAX) NULL,
    [Thread]      VARCHAR (10)   NULL,
    [LogDate]     DATETIME       NULL,
    CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED ([Id] ASC)
);



