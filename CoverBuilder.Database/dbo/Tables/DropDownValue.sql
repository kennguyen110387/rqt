﻿CREATE TABLE [dbo].[DropDownValue] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [InsuranceID] INT           NULL,
    [QuestionID]  INT           NULL,
    [Item]        VARCHAR (500) NULL,
    [Value]       VARCHAR (500) NULL,
    [OrderID]     INT           NULL,
    [GroupID]     INT           NULL,
    [IsActive]    BIT           NULL,
    CONSTRAINT [PK_DropDownValue] PRIMARY KEY CLUSTERED ([ID] ASC)
);

