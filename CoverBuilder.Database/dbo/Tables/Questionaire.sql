﻿CREATE TABLE [dbo].[Questionaire] (
    [QuestionId]  INT            IDENTITY (1, 1) NOT NULL,
    [InsuranceId] INT            NOT NULL,
    [Question]    VARCHAR (1000) NOT NULL,
    [TriggerDesc] VARCHAR (1000) NULL,
    [ParentId]    INT            NULL,
    [IsActive]    BIT            NOT NULL,
    [Sequence]    INT            NULL,
    CONSTRAINT [PK_Questionaire] PRIMARY KEY CLUSTERED ([QuestionId] ASC),
    CONSTRAINT [FK_Questionaire_LiabilityInsuranceType] FOREIGN KEY ([InsuranceId]) REFERENCES [dbo].[LiabilityInsuranceType] ([InsuranceId])
);



