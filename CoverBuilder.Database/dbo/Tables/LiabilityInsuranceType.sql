﻿CREATE TABLE [dbo].[LiabilityInsuranceType] (
    [InsuranceId]        INT           IDENTITY (1, 1) NOT NULL,
    [LiabilityInsurance] VARCHAR (500) NOT NULL,
    [Code]               VARCHAR (2)   NULL,
    [IsActive]           BIT           NOT NULL,
    CONSTRAINT [PK_LiabilityInsuranceType] PRIMARY KEY CLUSTERED ([InsuranceId] ASC)
);

