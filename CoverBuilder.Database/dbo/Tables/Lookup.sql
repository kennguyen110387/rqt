﻿CREATE TABLE [dbo].[Lookup] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (50)   NOT NULL,
    [Text]        VARCHAR (100)  NULL,
    [Value]       VARCHAR (100)  NOT NULL,
    [Description] NVARCHAR (150) NULL,
    [Sequence]    INT            NULL,
    [Group]       INT            NULL,
    CONSTRAINT [PK_Lookup] PRIMARY KEY CLUSTERED ([Id] ASC)
);

