﻿CREATE TABLE [dbo].[EmailTemplate] (
    [TemplateId]     INT           NOT NULL,
    [Description]    VARCHAR (MAX) NULL,
    [Subject]        VARCHAR (255) NULL,
    [Body]           VARCHAR (MAX) NULL,
    [From]           VARCHAR (255) NULL,
    [To]             VARCHAR (255) NULL,
    [Cc]             VARCHAR (MAX) NULL,
    [Bcc]            VARCHAR (255) NULL,
    [WithAttachment] BIT           NULL,
    [IsActive]       BIT           NULL,
    CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED ([TemplateId] ASC)
);

