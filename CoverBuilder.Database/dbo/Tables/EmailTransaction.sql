﻿CREATE TABLE [dbo].[EmailTransaction] (
    [Id]              INT          IDENTITY (1, 1) NOT NULL,
    [HangfireJobId]   INT          NULL,
    [ReferenceNumber] VARCHAR (50) NULL,
    [TemplateId]      INT          NULL,
    [Status]          VARCHAR (50) NULL,
    [IsActive]        BIT          NULL,
    CONSTRAINT [PK_EmailTransaction] PRIMARY KEY CLUSTERED ([Id] ASC)
);

