﻿CREATE TABLE [dbo].[LiabilityQuotationHeader] (
    [Id]                          UNIQUEIDENTIFIER NOT NULL,
    [ReferenceNumber]             VARCHAR (13)     NULL,
    [InsuranceId]                 INT              NULL,
    [ZohoId]                      VARCHAR (50)     NULL,
    [LeadId]                      VARCHAR (50)     NULL,
    [BrokerageName]               VARCHAR (500)    NULL,
    [FullName]                    VARCHAR (500)    NULL,
    [Email]                       VARCHAR (254)    NULL,
    [PhoneNo]                     VARCHAR (50)     NULL,
    [InsuredFullName]             VARCHAR (500)    NULL,
    [SitutationStreetName]        VARCHAR (500)    NULL,
    [SituationStreetNo]           VARCHAR (500)    NULL,
    [SituationSuburb]             VARCHAR (500)    NULL,
    [SituationState]              VARCHAR (500)    NULL,
    [SituationPostCode]           VARCHAR (500)    NULL,
    [InsurancePeriodFrom]         DATETIME         NULL,
    [InsurancePeriodTo]           DATETIME         NULL,
    [LiabilityLimit]              FLOAT (53)       NULL,
    [DiscInfoClaim]               BIT              NULL,
    [DiscInfoClaimDetail]         VARCHAR (500)    NULL,
    [DiscInfoDecline]             BIT              NULL,
    [DiscInfoDeclineDetail]       VARCHAR (500)    NULL,
    [DiscInfoRenewal]             BIT              NULL,
    [DiscInfoRenewalDetail]       VARCHAR (500)    NULL,
    [DiscInfoExcess]              BIT              NULL,
    [DiscInfoExcessDetail]        VARCHAR (500)    NULL,
    [DiscInfoClaimRejected]       BIT              NULL,
    [DiscInfoClaimRejectedDetail] VARCHAR (500)    NULL,
    [DiscInfoBankrupt]            BIT              NULL,
    [DiscInfoBankruptDetail]      VARCHAR (500)    NULL,
    [DiscInfoConvicted]           BIT              NULL,
    [DiscInfoConvictedDetail]     VARCHAR (500)    NULL,
    [TotalPremiumBase]            DECIMAL (18, 2)  NULL,
    [TotalGST]                    DECIMAL (18, 2)  NULL,
    [TotalStampTax]               DECIMAL (18, 2)  NULL,
    [TotalFinalPremium]           DECIMAL (18, 2)  NULL,
    [TotalUwFee]                  DECIMAL (18, 2)  NULL,
    [UWFee]                       DECIMAL (18, 2)  NULL,
    [UWFeeGSTRate]                DECIMAL (18, 2)  NULL,
    [BrokerComm]                  DECIMAL (18, 2)  NULL,
    [BrokerCommGST]               DECIMAL (18, 2)  NULL,
    [PremiumSubTotal]             DECIMAL (18, 2)  NULL,
    [Status]                      VARCHAR (50)     NULL,
    [RequoteLinkLockout]          INT              NULL,
    [CreatedBy]                   VARCHAR (50)     NULL,
    [CreateDate]                  DATETIME         CONSTRAINT [DF_LiabilityQuotationHeader_CreateDate] DEFAULT (getdate()) NULL,
    [ModifiedBy]                  VARCHAR (50)     NULL,
    [ModifiedDate]                DATETIME         NULL,
    [UnderwriterComments]         VARCHAR (MAX)    NULL,
    [Remarks]                     VARCHAR (MAX)    NULL,
    [PolicyEndorsement]           NVARCHAR (MAX)   NULL,
    [PolicyBusinessDescription]   NVARCHAR (MAX)   NULL,
    [PolicyExcess]                NVARCHAR (MAX)   NULL,
    [PolicyZoho]                  NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_LiabilityQuotationHeader] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_LiabilityQuotationHeader_LiabilityInsuranceType] FOREIGN KEY ([InsuranceId]) REFERENCES [dbo].[LiabilityInsuranceType] ([InsuranceId])
);










GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LiabilityQuotationHeader]
    ON [dbo].[LiabilityQuotationHeader]([ReferenceNumber] ASC);


GO
CREATE TRIGGER [dbo].[TriggerSetEndorsements]
    ON  [dbo].[LiabilityQuotationHeader]       
    AFTER INSERT     
AS       
    BEGIN         
        SET NOCOUNT ON;

        DECLARE @userId UNIQUEIDENTIFIER;

        SELECT TOP 1 @userId = i.Id
        FROM INSERTED i;

        EXEC [dbo].[usp_SetEndorsements] @userId;
    END;