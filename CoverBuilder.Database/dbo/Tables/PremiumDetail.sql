﻿CREATE TABLE [dbo].[PremiumDetail] (
    [Id]           INT              IDENTITY (1, 1) NOT NULL,
    [HeaderId]     UNIQUEIDENTIFIER NOT NULL,
    [BasePremium]  DECIMAL (18, 2)  NOT NULL,
    [GSTRate]      DECIMAL (18, 2)  NOT NULL,
    [GST]          DECIMAL (18, 2)  NOT NULL,
    [StateTaxRate] DECIMAL (18, 2)  NOT NULL,
    [StampTax]     DECIMAL (18, 2)  NOT NULL,
    [Excess]       DECIMAL (18, 2)  NOT NULL,
    [IsActive]     BIT              NOT NULL,
    [CreatedBy]    VARCHAR (500)    NULL,
    [CreateDate]   DATETIME         CONSTRAINT [DF_PremiumDetail_CreateDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_PremiumDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PremiumDetail_LiabilityQuotationHeader] FOREIGN KEY ([HeaderId]) REFERENCES [dbo].[LiabilityQuotationHeader] ([Id])
);

