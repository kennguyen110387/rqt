﻿CREATE TABLE [dbo].[LiabilityInsuranceResult] (
    [ResultId]     INT              IDENTITY (1, 1) NOT NULL,
    [UserInfoId]   UNIQUEIDENTIFIER NOT NULL,
    [QuestionId]   INT              NOT NULL,
    [Response]     VARCHAR (MAX)    NULL,
    [GroupId]      INT              NULL,
    [IsTriggered]  BIT              NULL,
    [IsActive]     BIT              NULL,
    [ModifiedDate] DATETIME         NULL,
    CONSTRAINT [PK_LiabilityInsuranceDetail] PRIMARY KEY CLUSTERED ([ResultId] ASC),
    CONSTRAINT [FK_LiabilityInsuranceResult_LiabilityQuotationHeader] FOREIGN KEY ([UserInfoId]) REFERENCES [dbo].[LiabilityQuotationHeader] ([Id]),
    CONSTRAINT [FK_LiabilityInsuranceResult_Questionaire] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[Questionaire] ([QuestionId])
);


GO
ALTER TABLE [dbo].[LiabilityInsuranceResult] NOCHECK CONSTRAINT [FK_LiabilityInsuranceResult_Questionaire];

