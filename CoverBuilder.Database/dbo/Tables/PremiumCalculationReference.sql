﻿CREATE TABLE [dbo].[PremiumCalculationReference] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [InsuranceTypeID] INT             NULL,
    [ConditionType]   VARCHAR (500)   NULL,
    [ConditionA]      FLOAT (53)      NULL,
    [ConditionB]      FLOAT (53)      NULL,
    [LiabilityLimit]  FLOAT (53)      NULL,
    [Rate]            DECIMAL (18, 5) NULL,
    CONSTRAINT [PK_PremiumCalculationReference] PRIMARY KEY CLUSTERED ([ID] ASC)
);

