﻿
CREATE PROC usp_UpdatePremiumDetail(
@UserInfoId VARCHAR(50)
)
AS
BEGIN

UPDATE PremiumDetail
SET IsActive = 0
WHERE HeaderId = @UserInfoId
AND IsActive = 1

END