﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdateRemarks]
	-- Add the parameters for the stored procedure here
	@id AS varchar(13),
	@remarks as varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


    -- Insert statements for procedure here
	UPDATE [CoverBuilder].[dbo].[LiabilityQuotationHeader]
   SET [Remarks] = @remarks
 WHERE ReferenceNumber = @id



END