﻿-- =============================================
-- Author:		James Patrick Y. Bautista
-- Create date: November 23, 2017
-- Description:	Updates Premium Info
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdatePremiumInfo]
	-- Add the parameters for the stored procedure here
	@referenceNo varchar(13), 
	@underwriterFee decimal(18,2),
	@stampTax decimal(18,2),
	@finalPremium decimal(18,2),
	@premiumBase decimal(18,2),
	@gst decimal(18,2),
	@underwriterFeeGst decimal(18,2),
	@underwriterComments varchar(max)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
	UPDATE [CoverBuilder].[dbo].[LiabilityQuotationHeader]
   SET [TotalPremiumBase] = @premiumBase
      ,[TotalGST] = @gst
      ,[TotalStampTax] = @stampTax
      ,[TotalFinalPremium] = @finalPremium
      ,[TotalUwFee] =  @underwriterFeeGst
      ,[UWFeeGSTRate] = @underwriterFee
      ,[UnderwriterComments] = @underwriterComments

 WHERE ReferenceNumber = @referenceNo

END