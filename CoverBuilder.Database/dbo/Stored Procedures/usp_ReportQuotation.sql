﻿CREATE PROCEDURE [dbo].[usp_ReportQuotation] --'7FC7BDD8-8261-4A4B-9E99-086CFEE75E29'  
 @userId UNIQUEIDENTIFIER  
  
AS  
BEGIN  
  
--SELECT @userId = '54680583-4190-4EB3-B843-B4367CA7234B'
  
DECLARE @headerId AS UNIQUEIDENTIFIER  
DECLARE @cols AS NVARCHAR(MAX)  
DECLARE @query AS NVARCHAR(MAX)  
 /*  
 USER HEADER  
 */  
 SELECT usrInfo.ReferenceNumber AS UserInfoId  
 ,usrInfo.InsuranceId as InsuranceId  
 ,liability.LiabilityInsurance as LIabilityInsurance  
 ,usrInfo.BrokerageName as BrokerageName  
 ,usrInfo.FullName as FullName  
 ,usrInfo.InsuredFullName as InsuredFullName  
 ,usrInfo.SituationStreetNo as SituationStreetNo  
 ,usrInfo.SitutationStreetName as SituationStreetName  
 ,usrInfo.SituationSuburb as SituationSuburb  
 ,usrInfo.SituationState as SituationState  
 ,usrInfo.SituationPostCode as SituationPostCode  
 ,usrInfo.InsurancePeriodFrom as InsurancePeriodFrom  
 ,usrInfo.InsurancePeriodTo as InsurancePeriodTo  
 ,usrInfo.CreateDate as DateCreated  
 ,usrInfo.TotalPremiumBase as TotalPremiumBase  
 ,usrInfo.TotalGST as TotalGST  
 ,usrInfo.TotalStampTax as TotalStampTax  
 ,usrInfo.TotalFinalPremium as TotalFinalPremium  
 ,usrInfo.UWFee as UWFee  
 ,usrInfo.UWFeeGSTRate as UWFeeGSTRate 
 ,usrInfo.BrokerComm
 ,usrInfo.BrokerCommGST
 ,usrInfo.PremiumSubTotal 
 ,usrInfo.TotalUwFee as TotalUwFee
 ,usrInfo.UnderwriterComments as UnderwriterComments
 ,usrInfo.LiabilityLimit as LiabilityLimit
 ,usrInfo.PolicyEndorsement
 ,usrInfo.PolicyBusinessDescription
 ,usrInfo.PolicyExcess
 ,usrInfo.PolicyZoho
  FROM LiabilityQuotationHeader usrInfo  
  LEFT JOIN LiabilityInsuranceType liability  
  ON usrInfo.InsuranceId = liability.InsuranceId  
  WHERE usrInfo.Id = @userId  
    
  
 /*  
 USER HEADER DETAILS  
 */  
 SELECT hdr.ReferenceNumber AS HeaderId  
 ,hdr.TotalPremiumBase AS BasePremium  
 ,.1 AS GSTRate  
 ,hdr.TotalGST AS GST  
 ,hdr.TotalStampTax AS StateTaxRate  
 ,dtl.Excess AS Excess  
 ,hdr.BrokerComm
 ,hdr.BrokerCommGST
 FROM LiabilityQuotationHeader hdr
 
 INNER JOIN PremiumDetail dtl
 ON hdr.id = dtl.HeaderId
 
 where hdr.Id = @userId  
 
 
   
  
    
  /*  
 USER TRIGGERS  
 */  
   
 select CASE WHEN DiscInfoClaim = 1 THEN 'Liability Claims in the last 5 years.' END AS TriggerDesc  
 from LiabilityQuotationHeader  
 where Id = @userId  
 and DiscInfoClaim in (1)  
 UNION ALL  
 select case when DATEADD(YEAR,1,InsurancePeriodFrom) != InsurancePeriodTo THEN 'Insurance Period is less than or greater than 1 year.' END AS TriggerDesc
 from LiabilityQuotationHeader  
 where Id = @userId  
 UNION ALL
 select CASE WHEN DiscInfoDecline = 1 THEN 'Insurance Declined in the last 5 years.' END AS TriggerDesc  
 from LiabilityQuotationHeader  
 where Id = @userId  
 and DiscInfoDecline in (1)  
 UNION ALL  
 select CASE WHEN DiscInfoClaimRejected = 1 THEN 'Claim rejected under a policy of insurance.' END AS TriggerDesc  
 from LiabilityQuotationHeader  
 where Id = @userId  
 and DiscInfoClaimRejected in (1)  
 UNION ALL  
 select CASE WHEN DiscInfoBankrupt = 1 THEN 'Been declared bankrupt or put into receivership or liquidation.' END AS TriggerDesc  
 from LiabilityQuotationHeader  
 where Id = @userId  
 and DiscInfoBankrupt in (1)  
 UNION ALL  
 select CASE WHEN DiscInfoConvicted = 1 THEN 'Been charged with or convicted of a criminal offence.' END AS TriggerDesc  
 from LiabilityQuotationHeader  
 where Id = @userId  
 and DiscInfoConvicted in (1)  
 UNION ALL  
 select CASE WHEN DiscInfoExcess = 1 THEN 'Special excess imposed on a policy insurance.' END AS TriggerDesc  
 from LiabilityQuotationHeader  
 where Id = @userId  
 and DiscInfoExcess in (1)  
 UNION ALL  
 select CASE WHEN DiscInfoRenewal = 1 THEN 'Insurer refused or not invited renewal.' END AS TriggerDesc  
 from LiabilityQuotationHeader  
 where Id = @userId  
 and DiscInfoRenewal in (1)  
 UNION ALL  
  SELECT quest.TriggerDesc  
  FROM LiabilityInsuranceResult res  
  INNER JOIN Questionaire quest  
  ON res.QuestionId = quest.QuestionId  
  WHERE UserInfoId = @userId  
  AND res.IsTriggered = 1  
  GROUP BY quest.TriggerDesc --Filter Checkbox Duplicates  
  
--select case when DATEADD(YEAR,1,'1/3/2017') = '1/2/2018' THEN '1 year differece' END AS dates
  
    
   /*  
 QUOTATION DETAILS
 */  
SELECT UserInfoId, GroupId,[Post Code] AS [PostCode],[State],[Street Name] AS [StreetName],[Street Number] AS StreetNumber,[Suburb],detail.BasePremium, detail.GST,detail.StateTaxRate from   
             (  
                select UserInfoId,GroupId, Question, Response from LiabilityInsuranceResult t1  
                        Inner join Questionaire t2  
                        on t1.QuestionId = t2.QuestionId  
                        where UserInfoId = @userId  
                        and t2.InsuranceId NOT IN (2)  
                        and GroupId IS NOT NULL  
            ) x  
            pivot   
            (  
                Max(x.Response)  
                for Question in ([Post Code],[State],[Street Name],[Street Number],[Suburb])  
            ) p   
            INNER JOIN (SELECT ROW_NUMBER() OVER (Order by Id) AS RowNumber, * FROM PremiumDetail WHERE HeaderId = @userId) detail   
            on p.GroupId = detail.RowNumber  




  /*  
 USER RESPONSES 
 */  
--declare @userId AS uniqueidentifier
--SELECT @userId = '54680583-4190-4EB3-B843-B4367CA7234B'
--Select * from LiabilityInsuranceResult where UserInfoId = @userIdTest

SELECT question.Question, response.Response
FROM LiabilityInsuranceResult response
INNER JOIN Questionaire question
ON response.QuestionId = question.QuestionId
WHERE UserInfoId = @userId AND response.IsActive = 1

  /*  
 GENERIC INFO RESPONSES 
 */  
SELECT [ReferenceNumber]
      ,[BrokerageName]
      ,[FullName]
      ,[Email]
      ,[PhoneNo]
      ,[InsuredFullName]
      ,[SitutationStreetName]
      ,[SituationStreetNo]
      ,[SituationSuburb]
      ,[SituationState]
      ,[SituationPostCode]
      ,[InsurancePeriodFrom]
      ,[InsurancePeriodTo]
      ,[LiabilityLimit]
      ,[DiscInfoClaim]
      ,[DiscInfoClaimDetail]
      ,[DiscInfoDecline]
      ,[DiscInfoDeclineDetail]
      ,[DiscInfoRenewal]
      ,[DiscInfoRenewalDetail]
      ,[DiscInfoExcess]
      ,[DiscInfoExcessDetail]
      ,[DiscInfoClaimRejected]
      ,[DiscInfoClaimRejectedDetail]
      ,[DiscInfoBankrupt]
      ,[DiscInfoBankruptDetail]
      ,[DiscInfoConvicted]
      ,[DiscInfoConvictedDetail]
  FROM [dbo].[LiabilityQuotationHeader] 
  WHERE id = @userId


/****************** POLICIES ******************/ 
--> RESET POLICY DETAILS (INCASE)
EXEC [usp_SetEndorsements] @userId;

--> GET POLICY DETAILS
DECLARE @tempEndorsementList TABLE(
	Code nvarchar(10)
	,Name nvarchar(50)
	,Description nvarchar(MAX)
	)
 
-- Insert the output of stored procedures into temp table
INSERT INTO @tempEndorsementList EXEC [usp_GetEndorsements] @userId;
 
-- Return Data From temp table 
SELECT * FROM @tempEndorsementList;


/****************** GENERIC INFO RESPONSES WITH QUESTIONS ******************/ 
--> GENERIC RESPONSES WITH QUESTIONS
DECLARE @tempGenericQuestionList TABLE(
	Question nvarchar(MAX)
	,Response nvarchar(MAX)
	)
	
DECLARE @referenceNumber nvarchar(MAX)
SET @referenceNumber = (SELECT ReferenceNumber  
					  FROM LiabilityQuotationHeader    
					  WHERE Id = @userId)
 
-- Insert the output of stored procedures into temp table
INSERT INTO @tempGenericQuestionList EXEC [usp_GetGenericQuestions] @referenceNumber;
 
-- Return Data From temp table 
SELECT * FROM @tempGenericQuestionList;


END