﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdateEmailStatus]
	-- Add the parameters for the stored procedure here
	@emailId as int,
	@emailStatus as varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


-- Insert statements for procedure here
UPDATE [CoverBuilder].[dbo].EmailTransaction
SET [Status] = @emailStatus
WHERE [Id] = @emailId



END