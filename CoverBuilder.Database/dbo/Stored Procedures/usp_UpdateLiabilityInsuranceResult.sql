﻿CREATE PROC usp_UpdateLiabilityInsuranceResult(
@UserInfoId VARCHAR(50)
)
AS
BEGIN

UPDATE LiabilityInsuranceResult
SET IsActive = 0, ModifiedDate = GETDATE()
WHERE UserInfoId = @UserInfoId
AND IsActive = 1

END