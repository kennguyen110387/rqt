﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpdateStatus]
	-- Add the parameters for the stored procedure here
	@id AS varchar(13),
	@status as varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


    -- Insert statements for procedure here
	UPDATE [CoverBuilder].[dbo].[LiabilityQuotationHeader]
   SET [Status] = @status
 WHERE ReferenceNumber = @id



END