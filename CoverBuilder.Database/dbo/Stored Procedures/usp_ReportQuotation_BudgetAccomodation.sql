﻿CREATE PROC [dbo].[usp_ReportQuotation_BudgetAccomodation]
AS
BEGIN
	SELECT UserInfoId
		  ,L.InsuranceId
		  ,L.LiabilityInsurance
		  ,BrokerageName
		  ,FullName
		  ,InsuredFullName
		  ,SituationStreetNo
		  ,SituationStreetName
		  ,SituationSuburb
		  ,SituationState
		  ,SituationPostCode
		  ,InsurancePeriodFrom 
		  ,InsurancePeriodTo
		  ,DateCreated 
	FROM UserInfo AS U
	INNER JOIN LiabilityInsuranceType AS L
	ON U.InsuranceId = L.InsuranceId
	AND L.IsActive = 1	

	SELECT PremiumBase
		  ,GSTRate
		  ,StateTaxRate
		  ,UWFee
		  ,UWFeeGSTRate
		  ,Excess
		  ,TotalGST
		  ,TotalStampTax
		  ,TotalUWFee
		  ,FinalPremium
	FROM Premium

END